#include "pch.h"
#include "extern.h"
#include <vector>

using namespace std;

CBISSCommunication::CBISSCommunication()
{
	//m_nSetWatt = 0;
	//m_nSetHour = 0;
	//m_nSetMinute = 0;
	//m_nSetSecond = 0;
	//m_nActualWatt = 0;
	//m_nActualWattR = 0;
	//m_nActualHour = 0;
	//m_nActualMinute = 0;
	//m_nActualSecond = 0;
	//m_dActualVDC = 0;
	//m_dActualTorr = 0;

	m_bOldPowerStatus = false;
	m_bisPowerON = false;

	m_hSetWattEvent = NULL;
	m_hSetHourEvent = NULL;
	m_hSetMinuteEvent = NULL;
	m_hGetSetParameterEvent = NULL;
	m_hGetActualParameterEvent = NULL;
	m_hPowerOnEvent = NULL;
	m_hPowerOffEvent = NULL;

	m_hSetWattEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hSetHourEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hSetMinuteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hGetSetParameterEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hGetActualParameterEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hPowerOnEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hPowerOffEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
}

CBISSCommunication::~CBISSCommunication()
{
	CloseSerialPort();
	CloseHandle(m_hSetWattEvent);
	m_hSetWattEvent = NULL;
	CloseHandle(m_hSetHourEvent);
	m_hSetHourEvent = NULL;
	CloseHandle(m_hSetMinuteEvent);
	m_hSetMinuteEvent = NULL;
	CloseHandle(m_hPowerOnEvent);
	m_hPowerOnEvent = NULL;
	CloseHandle(m_hPowerOffEvent);
	m_hPowerOffEvent = NULL;
	CloseHandle(m_hGetSetParameterEvent);
	m_hGetSetParameterEvent = NULL;
	CloseHandle(m_hGetActualParameterEvent);
	m_hGetActualParameterEvent = NULL;
}

int CBISSCommunication::OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
{
	int nRet = 0;

	nRet = OpenSerialPort(sPortName, dwBaud, wByte, wStop, wParity);

	if (nRet != 0)
	{
		CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("SERIAL PORT OPEN FAIL")));	//통신 상태 기록.
		CECommon::SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("SERIAL PORT OPEN FAIL")));	//통신 상태 기록.
		nRet = -1;
	}
	else
	{
		CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("SERIAL PORT OPEN")));	//통신 상태 기록.
		CECommon::SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("SERIAL PORT OPEN")));	//통신 상태 기록.
	}
	return nRet;
}

int CBISSCommunication::SendData(int nSize, char * lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	CString strSendData = lParam;

	nRet = Send(nSize, lParam, nTimeOut, nRetryCnt);
	if (nRet != 0)
	{
		CString strTemp;
		strTemp.Format(_T("PC -> Plasma : SEND FAIL (%d)"), nRet);
		CECommon::SaveLogFile("PlasmaCleaning_BISS", (LPSTR)(LPCTSTR)strTemp);	//통신 상태 기록.
	}
	else
		CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("PC -> Plasma : " + strSendData.Mid(0, strSendData.GetLength() - 2))));	//통신 상태 기록.

	return nRet;
}

int CBISSCommunication::ReceiveData(char * lParam, DWORD dwRead)
{
	CString strTemp, strText(_T(""));
	vector<CString> vecRecvData;

	int nIndex = 0;
	while (FALSE != AfxExtractSubString(strText, lParam, nIndex, _T('\n')))
	{
		vecRecvData.push_back(strText);
		nIndex++;
	}

	for (int i = 0; i < nIndex - 1; i++)
	{
		//		SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

		if (vecRecvData[i].Left(1) == "P") // P
		{
			CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			if (vecRecvData[i].Mid(1, 1) == "0") //P0 -> power off
			{
				m_bisPowerON = false;
				SetEvent(m_hPowerOffEvent);
			}
			else // P1 power on
			{
				m_bisPowerON = true;
				SetEvent(m_hPowerOnEvent);
			}
		}
		else if (vecRecvData[i].Left(1) == "W") // get Watt
		{
			CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			SetEvent(m_hSetWattEvent);
		}
		else if (vecRecvData[i].Left(1) == "H")  // get Hour
		{
			CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			SetEvent(m_hSetHourEvent);
		}
		else if (vecRecvData[i].Left(1) == "M")  // get minute
		{
			CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			SetEvent(m_hSetMinuteEvent);
		}
		else if (vecRecvData[i].Left(1) == "V") // get VDC
		{
			CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			strTemp.Format(_T("%s"), vecRecvData[i].Mid(1, 3));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPVDC, strTemp);
			//m_dActualVDC = atof(strTemp);
		}
		else if (vecRecvData[i].Left(1) == "T")	// get torr
		{
			CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			if (vecRecvData[i].Mid(2, 1) == "-")
			{
				strTemp.Format(_T("%se%s"), vecRecvData[i].Mid(1, 1), vecRecvData[i].Mid(2, 2));
				//m_dActualTorr = atof(strTemp);
			}
			else
			{
				strTemp.Format(_T("%03s"), vecRecvData[i].Mid(1, vecRecvData[i].GetLength() - 2));
				//m_dActualTorr = atof(strTemp);
			}
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPTORR, strTemp);
		}
		else if (vecRecvData[i].Left(1) == "S")  //get set value
		{
			CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			vector<CString> vecTempData;
			strText = _T("");


			int nIdx = 0;
			while (FALSE != AfxExtractSubString(strText, vecRecvData[i], nIdx, _T(',')))
			{
				vecTempData.push_back(strText);
				nIdx++;
			}
			//m_nSetWatt = atoi(vecTempData[0].Mid(3, 2));
			//m_nSetHour = atoi(vecTempData[2].Mid(1, 2));
			//m_nSetMinute = atoi(vecTempData[3].Mid(1, 2));
			//m_nSetSecond = atoi(vecTempData[4].Mid(1, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_SETWATT, vecTempData[0].Mid(3, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, vecTempData[2].Mid(1, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, vecTempData[3].Mid(1, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, vecTempData[4].Mid(1, 2));
			g_pPlasmaDlg->m_strDispSetWatt = vecTempData[0].Mid(3, 2);
			g_pPlasmaDlg->m_strDispSetHour = vecTempData[2].Mid(1, 2);
			g_pPlasmaDlg->m_strDispSetMinute = vecTempData[3].Mid(1, 2);
			g_pPlasmaDlg->m_strDispSetSecond = vecTempData[4].Mid(1, 2);
			vecTempData.clear();
			SetEvent(m_hGetSetParameterEvent);
		}
		else if (vecRecvData[i].Left(1) == "A")  // get actual value
		{
			CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			SetEvent(m_hGetActualParameterEvent);
			vector<CString> vecTempData;
			strText = _T("");

			int nIdx = 0;
			while (FALSE != AfxExtractSubString(strText, vecRecvData[i], nIdx, _T(',')))
			{
				vecTempData.push_back(strText);
				nIdx++;
			}
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPWATT, vecTempData[0].Mid(3, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPWATTRR, vecTempData[1].Mid(1, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPHOUR, vecTempData[2].Mid(1, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPMINUTE, vecTempData[3].Mid(1, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSECOND, vecTempData[4].Mid(1, 2));
			//g_pPlasmaDlg->UpdateData(TRUE);
			//m_nActualWatt = atoi(vecTempData[0].Mid(3, 2));
			//m_nActualWattR = atoi(vecTempData[1].Mid(1, 2));
			//m_nActualHour = atoi(vecTempData[2].Mid(1, 2));
			//m_nActualMinute = atoi(vecTempData[3].Mid(1, 2));
			//m_nActualSecond = atoi(vecTempData[4].Mid(1, 2));
			vecTempData.clear();
		}
		else
		{
			CECommon::SaveLogFile("PlasmaCleaning_BISS", _T((LPSTR)(LPCTSTR)("Parsing Error, Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1))));
		}
	}
	vecRecvData.clear();
	return 0;
}

int CBISSCommunication::sendPowerOn()
{
	int nRet = 0;

	CString strTemp = _T("P1\r\n");
	ResetEvent(m_hPowerOnEvent);
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp);
	if (nRet != 0)
	{
		SetEvent(m_hPowerOnEvent);
	}
	else
	{
		nRet = CECommon::WaitEvent(m_hPowerOnEvent, 1000);
	}

	return nRet;
}

int CBISSCommunication::sendPowerOff()
{
	int nRet = 0;

	CString strTemp = _T("P0\r\n");
	ResetEvent(m_hPowerOffEvent);
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp);
	if (nRet != 0)
	{
		SetEvent(m_hPowerOffEvent);
	}
	else
	{
		nRet = CECommon::WaitEvent(m_hPowerOffEvent, 1000);
	}

	return nRet;
}

int CBISSCommunication::sendSetParameterWatt(int watt)
{
	int nRet = 0;
	ResetEvent(m_hSetWattEvent);
	CString strTemp = "";
	strTemp.Format(_T("W%02d\r\n"), watt);
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp);
	if (nRet != 0)
	{
		SetEvent(m_hSetWattEvent);
	}
	else
	{
		nRet = CECommon::WaitEvent(m_hSetWattEvent, 1000);
	}

	return nRet;
}

int CBISSCommunication::sendSetParameterHour(int hour)
{
	int nRet = 0;
	ResetEvent(m_hSetHourEvent);
	CString strTemp = "";
	strTemp.Format(_T("H%02d\r\n"), hour);
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp);
	if (nRet != 0)
	{
		SetEvent(m_hSetHourEvent);
	}
	else
	{
		nRet = CECommon::WaitEvent(m_hSetHourEvent, 1000);
	}

	return nRet;
}

int CBISSCommunication::sendSetParameterMinute(int minute)
{
	int nRet = 0;
	ResetEvent(m_hSetMinuteEvent);
	CString strTemp = "";
	strTemp.Format(_T("M%02d\r\n"), minute);
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp);
	if (nRet != 0)
	{
		SetEvent(m_hSetMinuteEvent);
	}
	else
	{
		nRet = CECommon::WaitEvent(m_hSetMinuteEvent, 1000);
	}

	return nRet;
}

int CBISSCommunication::sendGetParameterSet()
{
	int nRet = 0;
	ResetEvent(m_hGetSetParameterEvent);
	CString strTemp = _T("GetS\r\n");
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp,0,1);
	if (nRet != 0)
	{
		SetEvent(m_hGetSetParameterEvent);
	}
	else
	{
		nRet = CECommon::WaitEvent(m_hGetSetParameterEvent, 1000);
	}

	return nRet;
}

int CBISSCommunication::sendGetParameterAct()
{
	int nRet = 0;
	ResetEvent(m_hGetActualParameterEvent);
	CString strTemp = _T("GetA\r\n");
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp);
	if (nRet != 0)
	{
		SetEvent(m_hGetActualParameterEvent);
	}
	else
	{
		nRet = CECommon::WaitEvent(m_hGetActualParameterEvent, 1000);
	}

	return nRet;
}

int CBISSCommunication::sendGetParameterVDC()
{
	int nRet = 0;

	CString strTemp = _T("V\r\n");
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp);

	return nRet;
}

int CBISSCommunication::sendGetParameterTorr()
{
	int nRet = 0;

	CString strTemp = _T("T\r\n");
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp);

	return nRet;
}

int CBISSCommunication::sendGetParameterPower()
{
	int nRet = 0;

	CString strTemp = _T("P\r\n");
	nRet = SendData(strTemp.GetLength(), (LPSTR)(LPCTSTR)strTemp);

	return nRet;
}

int CBISSCommunication::openBISSController()
{
	int nRet = 0;

	nRet = OpenPort("COM13", 9600, 8, 0, 0);

	return nRet;
}
