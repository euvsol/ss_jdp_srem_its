﻿// CPasswordDlg.cpp: 구현 파일
//

#include "pch.h"
#include "Plasma_cleaning.h"
#include "CPasswordDlg.h"
#include "afxdialogex.h"


// CPasswordDlg 대화 상자

IMPLEMENT_DYNAMIC(CPasswordDlg, CDialogEx)

CPasswordDlg::CPasswordDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PASSWORD_DIALOG, pParent)
	, m_strPassword(_T(""))
{

}

CPasswordDlg::~CPasswordDlg()
{
}

void CPasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//  DDX_Control(pDX, IDC_PASSWORD_EDIT, m_strPassword);
	DDX_Text(pDX, IDC_PASSWORD_EDIT, m_strPassword);
}


BEGIN_MESSAGE_MAP(CPasswordDlg, CDialogEx)
END_MESSAGE_MAP()


// CPasswordDlg 메시지 처리기
