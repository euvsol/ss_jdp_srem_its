#pragma once
class CSREMCommunication :public CEthernetCom
{
public:
	CSREMCommunication();
	~CSREMCommunication();

	virtual int	SendData(char *lParam, int nTimeOut = 60000, int nRetryCnt = 3);
	virtual int ReceiveData(char *lParam);
	virtual int On_Accept();
	virtual int WaitReceiveEventThread();


	BOOL m_isPreesureCheck;
	BOOL m_isInterlock;

	HANDLE m_hInterlcokCheckEvent;

	int openSocketServer();

	int sendGetInterlock();
	int sendPlasmaCleaningOn();
	int sendPlasmaCleaningOff();
	int sendBISSOn();
	int sendBISSOff();
	int sendTime(bool isRemainTime = TRUE);
	int sendThreadStart();
	int sendThreaddEnd();
	int sendActualParameter();
	int sendSetParameter();
	int sendTimerParameter();
};

