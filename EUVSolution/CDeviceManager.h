#pragma once
#include <vector>

using namespace std;

class IObserver
{
public:
	virtual ~IObserver() {}
	virtual void EmergencyStop() = 0;
};

class ISubject
{
public:
	virtual ~ISubject() {}
	virtual void RegisterObserver(IObserver *observer) = 0;
	virtual void RemoveObserver(IObserver *observer) = 0;
	virtual void OccurEmergency() = 0;
};

class CDeviceManager : public ISubject
{
public:
	~CDeviceManager()
	{
		vector<IObserver *>().swap(mObserver);
	}

	void RegisterObserver(IObserver *observer) override 
	{
		mObserver.push_back(observer);
	}

	void RemoveObserver(IObserver *observer) override
	{
		vector<IObserver *>::iterator iter;
		iter = find(mObserver.begin(), mObserver.end(), observer);
		if (iter != mObserver.end())
			mObserver.erase(iter);
	}

	void OccurEmergency() override
	{
		vector<IObserver *>::iterator iter = mObserver.begin();
		for (; iter != mObserver.end(); iter++)
		{
			IObserver *observer = *iter;
			observer->EmergencyStop();
		}
	}

private:
	vector<IObserver *> mObserver;
};

