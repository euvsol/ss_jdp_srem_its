﻿// CSubModuleDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CSubModuleDlg 대화 상자

IMPLEMENT_DYNAMIC(CSubModuleDlg, CDialogEx)

CSubModuleDlg::CSubModuleDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SUB_MODULE_DIALOG, pParent)
{

}

CSubModuleDlg::~CSubModuleDlg()
{
}

void CSubModuleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSubModuleDlg, CDialogEx)
	ON_BN_CLICKED(IDC_SUBMODULE_BUTTON_SHOWPLASMACLEANING, &CSubModuleDlg::OnBnClickedSubmoduleButtonShowplasmacleaning)	
END_MESSAGE_MAP()


// CSubModuleDlg 메시지 처리기

void CSubModuleDlg::OnBnClickedSubmoduleButtonShowplasmacleaning()
{
	g_pPlasmaCleaner->ShowWindow(SW_SHOW);
}
