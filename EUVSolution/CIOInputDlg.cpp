﻿// CIOInputDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

#define DRY_NO_ERROR		0
#define DRY_LLC_ALARM		1
#define DRY_MC_ALARM		2
#define DRY_LLC_WARNING		3
#define DRY_MC_WARNING		4

#define PULSE_CHECK_CNT		10

// CDigitalInput 대화 상자

IMPLEMENT_DYNAMIC(CDigitalInput, CDialogEx)

CDigitalInput::CDigitalInput(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_INPUT_DIALOG, pParent)
{
}

CDigitalInput::~CDigitalInput()
{
	m_brush.DeleteObject();
	m_font.DeleteObject();
}

void CDigitalInput::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDigitalInput, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CDigitalInput 메시지 처리기

BOOL CDigitalInput::OnInitDialog()
{
	CDialogEx::OnInitDialog(); 
	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);


	m_nLlc_DryPumpOn_Cnt = 0;
	m_nLlc_DryPumpAlarm_Cnt = 0;
	m_nLlc_DryPumpWarning_Cnt = 0;
	m_nMc_DryPumpOn_Cnt = 0;
	m_nMc_DryPumpAlarm_Cnt = 0;
	m_nMc_DryPumpWarning_Cnt = 0;
	m_nLlc_Tmp_Leak_Cnt = 0;
	m_nMc_Tmp_Leak_Cnt = 0;
	m_nACRack_SmokeDectect_Cnt = 0;
	m_nControlRack_SmokeDectect_Cnt = 0;
	m_nWaterReturnTemp_Alarm_Cnt = 0;
	m_nMainWater_Alarm_Cnt = 0;
	m_nMainAir_Alarm_Cnt = 0;
	m_nLLCMaskCheck_Cnt= 0;
	m_nLLCMaskTilt1_Cnt= 0;
	m_nLLCMaskTilt2_Cnt= 0;
	m_nMCMaskCheck_Cnt= 0;
	m_nMCMaskTilt1_Cnt= 0;
	m_nMCMaskTilt2_Cnt= 0;
	m_nLLCMaskCheck_VacRobot_Cnt = 0;
	m_nMCMaskCheck_VacRobot_Cnt = 0;
	m_Lid_Alarm_Cnt = 0;
	m_Isolator_Alarm_Cnt = 0;
	Dry_Pump_State = DRY_NO_ERROR;

	m_Message_ThreadExitFlag = FALSE;
	
	for (int a = 0; a < 14; a++)
		m_nFanCnt[a] = 0;

	for (int a = 0; a < 8; a++)
		m_nACFanCnt[a] = 0;

	for (int a = 0; a < 2; a++)
		m_nCTLCoolFanCnt[a] = 0;
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDigitalInput::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDigitalInput::OnDestroy()
{
	CDialogEx::OnDestroy();
	
	KillTimer(INPUT_DIALOG_TIMER);
}

void CDigitalInput::InitControls_DI(int nChannel)
{
	
	m_nChannel = nChannel;

	CString strTemp;

	for (int nIdx = nChannel * DIGITAL_IO_VIEW_NUMBER, nCnt = 0; nIdx < (nChannel * DIGITAL_IO_VIEW_NUMBER) + DIGITAL_IO_VIEW_NUMBER; nIdx++, nCnt++)
	{
		strTemp.Format(_T("X%04d"), nCnt + (nChannel * 100));
		SetDlgItemText(IDC_STATIC_DIGITALIN_NUM0 + nCnt, strTemp);
		GetDlgItem(IDC_STATIC_DIGITALIN_X000 + nCnt)->SetWindowText(g_pConfig->m_chDi[nIdx]);
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
	}

	SetTimer(INPUT_DIALOG_TIMER, 500, NULL);

}

void CDigitalInput::OnTimer(UINT_PTR nIDEvent)
{

	KillTimer(nIDEvent);

	if (nIDEvent == INPUT_DIALOG_TIMER)
	{
		//if (g_pIO->m_Crevis_Open_Port == TRUE)
		{
			OnUpdateDigitalInput();
			SetTimer(INPUT_DIALOG_TIMER, 500, NULL);
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}

///////////////////////////
// Digital Input Read /////
///////////////////////////

bool CDigitalInput::OnUpdateDigitalInput(void)
{
	CString value_io_str;
	CString cnt_io_str;
	char* error_str;
	

	for (int nIdx = m_nChannel * DIGITAL_IO_VIEW_NUMBER, nCnt = 0; nIdx < (m_nChannel* DIGITAL_IO_VIEW_NUMBER) + DIGITAL_IO_VIEW_NUMBER; nIdx++, nCnt++)
	{
		if (g_pIO->m_bCrevis_Open_Port == TRUE)
		{
			if (g_pIO->m_bDigitalIn[nIdx])
			{
				if ((nIdx >= g_pIO->DI::CONTROL_RACK_COOLING_FAN_1) && (nIdx <= g_pIO->DI::FRAME_COOLING_FAN_STATUS_14))
				{
					switch (nIdx)
					{
					case g_pIO->DI::CONTROL_RACK_COOLING_FAN_1:
						if (m_nCTLCoolFanCnt[0] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nCTLCoolFanCnt[0] = 0;
						}
						break;
					case g_pIO->DI::CONTROL_RACK_COOLING_FAN_2:
						if (m_nCTLCoolFanCnt[1] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nCTLCoolFanCnt[1] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_1:
						if (m_nACFanCnt[0] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nACFanCnt[0] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_2:
						if (m_nACFanCnt[1] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nACFanCnt[1] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_3:
						if (m_nACFanCnt[2] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nACFanCnt[2] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_4:
						if (m_nACFanCnt[3] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nACFanCnt[3] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_5:
						if (m_nACFanCnt[4] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nACFanCnt[4] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_6:
						if (m_nACFanCnt[5] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nACFanCnt[5] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_7:
						if (m_nACFanCnt[6] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nACFanCnt[6] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_8:
						if (m_nACFanCnt[7] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nACFanCnt[7] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_1:
						if (m_nFanCnt[0] <= PULSE_CHECK_CNT) {
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[0] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_2:
						if (m_nFanCnt[1] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[1] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_3:
						if (m_nFanCnt[2] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[2] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_4:
						if (m_nFanCnt[3] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[3] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_5:
						if (m_nFanCnt[4] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[4] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_6:
						if (m_nFanCnt[5] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[5] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_7:
						if (m_nFanCnt[6] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[6] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_8:
						if (m_nFanCnt[7] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[7] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_9:
						if (m_nFanCnt[8] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[8] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_10:
						if (m_nFanCnt[9] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[9] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_11:
						if (m_nFanCnt[10] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[10] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_12:
						if (m_nFanCnt[11] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[11] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_13:
						if (m_nFanCnt[12] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[12] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_14:
						if (m_nFanCnt[13] <= PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
							m_nFanCnt[13] = 0;
						}
						break;
					default:
						break;
					}

				}
				else if (nIdx == g_pIO->DI::WATER_LEAK_SENSOR1_LLC_TMP)
				{
					m_nLlc_Tmp_Leak_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// WATER LEAK SENSOR LLC TMP = 1 ->  정상 구동 상태, 0 이면 LEAK 발생.
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::WATER_LEAK_SENSOR2_MC_TMP))
				{
					m_nMc_Tmp_Leak_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// WATER LEAK SENSOR LLC TMP = 1 ->  정상 구동 상태, 0 이면 LEAK 발생.
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::LLC_DRY_PUMP_ALARM_STATUS))
				{
					m_nLlc_DryPumpAlarm_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// LLC DRY PUMP ALARM  = 1 ->  정상 상태 , 1 이면 ALARM 발생.
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::LLC_DRY_PUMP_WARNNING_STATUS))
				{
					m_nLlc_DryPumpWarning_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// LLC DRY PUMP WARNNING  = 1 ->  정상 상태 , 1 이면 WARNNING 발생.
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::MC_DRY_PUMP_ALARM_STATUS))
				{
					m_nMc_DryPumpAlarm_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// MC DRY PUMP ALARM  = 1 ->  정상 상태 , 1 이면 ALARM 발생.
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::MC_DRY_PUMP_WARNNING_STATUS))
				{
					m_nMc_DryPumpWarning_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// MC DRY PUMP WARNNING  = 1 ->  정상 상태 , 1 이면 WARNNING 발생.
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::LLC_MASK_CHECK))
				{
					m_nLLCMaskCheck_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// LLC MASK가 있으면  0 : 없으면 1
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::LLC_MASK_TILT1))
				{
					m_nLLCMaskTilt1_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// LLC MASK 기울림이 감지 되면  0 : 없으면 1
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::LLC_MASK_TILT2))
				{
					m_nLLCMaskTilt2_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// LLC MASK 기울림이 감지 되면  0 : 없으면 1
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::MC_MASK_CHECK))
				{
					m_nMCMaskCheck_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// LLC MASK가 있으면  0 : 없으면 1
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::MC_MASK_TILT1))
				{
					m_nMCMaskTilt1_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// LLC MASK 기울림이 감지 되면  0 : 없으면 1
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::MC_MASK_TILT2))
				{
					m_nMCMaskTilt2_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// LLC MASK 기울림이 감지 되면  0 : 없으면 1
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::VAC_ROBOT_MASK_CHECK_TO_LLC))
				{
					m_nLLCMaskCheck_VacRobot_Cnt = 0;
					// LLC 쪽으로 Vacuum Robot 의 마스크 감지 유무 -> 없으면 1 , 있으면 0
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::VAC_ROBOT_MASK_CHECK_TO_MC))
				{
					m_nMCMaskCheck_VacRobot_Cnt = 0;
					//g_pIO->m_nERROR_MODE = RUN_OFF;
					// MC 쪽으로 Vacuum Robot 의 마스크 감지 유무 -> 없으면 1 , 있으면 0
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if ((nIdx == g_pIO->DI::MAIN_AIR_SW))
				{
					m_nMainAir_Alarm_Cnt = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
				}
				else if ((nIdx == g_pIO->DI::ISOLATOR_AIR_SW))
				{
					m_Isolator_Alarm_Cnt = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
				}
				else if ((nIdx == g_pIO->DI::LID_AIR_SW))
				{
					m_Lid_Alarm_Cnt = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
				}
				else if ((nIdx == g_pIO->DI::WATER_FLOW_SW))
				{
					m_nMainWater_Alarm_Cnt = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
				}
				else if ((nIdx == g_pIO->DI::WATER_TEMP_ALARM))
				{
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]);

					m_nWater_Temp_Alarm_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::WATER_TEMP_ALARM]);
					cnt_io_str.Format("%d", m_nWater_Temp_Alarm_Cnt);
					if (m_nWater_Temp_Alarm_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Water Temp Alarm 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Water Temp Alarm 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Water Temp Alarm 감지 ")));

						g_pLog->Display(0, "Main Water Temp Alarm 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

						if (!g_pIO->Error_On(WATER_RETURN_TEMP_ALARM_ERROR, _T("Main Water Temp Alarm Error ")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Error Sequence Fail")));
					}

				}
				else if (((nIdx == g_pIO->DI::ISOLATOR_INTERLOCK_SENSOR1)))
				{
					//SaveLogFile("ISOLATOR INTERLOCK SENSOR", _T((LPSTR)(LPCTSTR)("SENSOR1 감지 - 정상")));
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
				}
				else if (((nIdx == g_pIO->DI::ISOLATOR_INTERLOCK_SENSOR2)))
				{
					//SaveLogFile("ISOLATOR INTERLOCK SENSOR", _T((LPSTR)(LPCTSTR)("SENSOR2 감지 - 정상")));
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
				}
				else
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
			}
			else
			{
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Fan 작동. 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if ((nIdx >= g_pIO->DI::CONTROL_RACK_COOLING_FAN_1) && (nIdx <= g_pIO->DI::FRAME_COOLING_FAN_STATUS_16))
				{
					switch (nIdx)
					{
					case g_pIO->DI::CONTROL_RACK_COOLING_FAN_1:
						m_nCTLCoolFanCnt[0]++;
						if (m_nCTLCoolFanCnt[0] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nCTLCoolFanCnt[0] = 0;
						}
						break;
					case g_pIO->DI::CONTROL_RACK_COOLING_FAN_2:
						m_nCTLCoolFanCnt[1]++;
						if (m_nCTLCoolFanCnt[1] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nCTLCoolFanCnt[1] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_1:
						m_nACFanCnt[0]++;
						if (m_nACFanCnt[0] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nACFanCnt[0] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_2:
						m_nACFanCnt[1]++;
						if (m_nACFanCnt[1] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nACFanCnt[1] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_3:
						m_nACFanCnt[2]++;
						if (m_nACFanCnt[2] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nACFanCnt[2] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_4:
						m_nACFanCnt[3]++;
						if (m_nACFanCnt[3] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nACFanCnt[3] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_5:
						m_nACFanCnt[4]++;
						if (m_nACFanCnt[4] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nACFanCnt[4] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_6:
						m_nACFanCnt[5]++;
						if (m_nACFanCnt[5] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nACFanCnt[5] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_7:
						m_nACFanCnt[6]++;
						if (m_nACFanCnt[6] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nACFanCnt[6] = 0;
						}
						break;
					case g_pIO->DI::AC_RACK_COOLING_FAN_STATUS_8:
						m_nACFanCnt[7]++;
						if (m_nACFanCnt[7] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nACFanCnt[7] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_1:
						m_nFanCnt[0]++;
						if (m_nFanCnt[0] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[0] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_2:
						m_nFanCnt[1]++;
						if (m_nFanCnt[1] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[1] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_3:
						m_nFanCnt[2]++;
						if (m_nFanCnt[2] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[2] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_4:
						m_nFanCnt[3]++;
						if (m_nFanCnt[3] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[3] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_5:
						m_nFanCnt[4]++;
						if (m_nFanCnt[4] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[4] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_6:
						m_nFanCnt[5]++;
						if (m_nFanCnt[5] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[5] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_7:
						m_nFanCnt[6]++;
						if (m_nFanCnt[6] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[6] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_8:
						m_nFanCnt[7]++;
						if (m_nFanCnt[7] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[7] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_9:
						m_nFanCnt[8]++;
						if (m_nFanCnt[8] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[8] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_10:
						m_nFanCnt[9]++;
						if (m_nFanCnt[9] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[9] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_11:
						m_nFanCnt[10]++;
						if (m_nFanCnt[10] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[10] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_12:
						m_nFanCnt[11]++;
						if (m_nFanCnt[11] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[11] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_13:
						m_nFanCnt[12]++;
						if (m_nFanCnt[12] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[12] = 0;
						}
						break;
					case g_pIO->DI::FRAME_COOLING_FAN_STATUS_14:
						m_nFanCnt[13]++;
						if (m_nFanCnt[13] > PULSE_CHECK_CNT)
						{
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
							m_nFanCnt[13] = 0;
						}
						break;
					default:
						break;
					}
				}
				
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Water Leak Sesnor 작동. (LLC TMP)
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if (nIdx == g_pIO->DI::WATER_LEAK_SENSOR1_LLC_TMP)
				{
					m_nLlc_Tmp_Leak_Cnt += 1;
					if (m_nLlc_Tmp_Leak_Cnt == 5)
					{
						value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::WATER_LEAK_SENSOR1_LLC_TMP]);
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]);  // Water leak sensor (LLC TMP)= 1-> 정상 구동 상태, 0 이면 leak 발생
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("WATER LEAK SENSOR (LLC TMP) 발생")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC TMP Water Leak Sensor IO Intput Value ::" + value_io_str)));	//통신 상태 기록.

						///////////////////////////////////////////////////
						// IO Line 연결 재확인 필요 
						///////////////////////////////////////////////////
						if(!g_pIO->Error_On(WATER_LLCTMP_LEAK_ALARM_ERROR, _T("LLC TMP Water Leak Sensor 작동 On")))
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Error Sequence Fail")));

						if (g_pIO->Close_WaterSupplyValve() == OPERATION_COMPLETED)
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(" MAIN_ERROR_ON() : Close Water Supply Valve Complete !")));
						else
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(" MAIN_ERROR_ON() : Close Water Supply Valve Fail!")));
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Water Leak Sesnor 작동. (MC TMP)
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if (nIdx == g_pIO->DI::WATER_LEAK_SENSOR2_MC_TMP)
				{
					m_nMc_Tmp_Leak_Cnt += 1;
					if (m_nMc_Tmp_Leak_Cnt == 5)
					{
						value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::WATER_LEAK_SENSOR2_MC_TMP]);
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]);  // Water leak sensor (MC TMP)= 1-> 정상 구동 상태, 0 이면 leak 발생
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("WATER LEAK SENSOR (MC TMP) 발생")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC TMP Water Leak Sensor IO Intput Value ::" + value_io_str)));	//통신 상태 기록.

						///////////////////////////////////////////////////
						// IO Line 연결 재확인 필요 
						///////////////////////////////////////////////////
						if(!g_pIO->Error_On(WATER_MCTMP_LEAK_ALARM_ERROR,_T("LLC TMP Water Leak Sensor 작동 On")))
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Error Sequence Fail")));

						
						if(g_pIO->Close_WaterSupplyValve() == OPERATION_COMPLETED)
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(" MAIN_ERROR_ON() : Close Water Supply Valve Complete !")));
						else
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(" MAIN_ERROR_ON() : Close Water Supply Valve Fail!")));
					}
				}

				/*
				물리적으로 ATM ROBOT Extend 신호는 없음
				Retrat 신호가 On 일 경우 Extend 신호는 Off
				Retart 신호가 Off 일 경우 Extend 신호는 On
				이라고 SW 에서 셋팅
				*/
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LLC DRY TMP ALARM 발생
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::LLC_DRY_PUMP_ALARM_STATUS))
				{
					m_nLlc_DryPumpAlarm_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::LLC_DRY_PUMP_ALARM_STATUS]);
					cnt_io_str.Format("%d", m_nLlc_DryPumpAlarm_Cnt);

					if (m_nLlc_DryPumpAlarm_Cnt == 5)
					{
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LL DRY PUMP ALARM = 1 -> 정상 구동 상태 , 0 이면 알람 발생

						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC DRY PUMP ALARM 발생")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Alarm IO Intput Value ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Alarm 발생에 따른 Error Sequence 작동")));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("LLC Dry Pump alarm 발생")));

						g_pLog->Display(0, "LLC Dry Pump Alarm 발생에 따른 Error Sequence 작동");
						error_str = _T("LLC Dry Pump Alarm 발생에 따른 Error Sequence 작동");
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(error_str)));

						/////////////////////////////////////////////////
						///// Error 발생 후 error sequence 등록 부분
						/////////////////////////////////////////////////
						//g_pIO->LLC_Dry_Pump_Error_Sequence();

						Dry_Pump_State = DRY_LLC_ALARM;
						m_Message_ThreadExitFlag = FALSE;
						if(!g_pIO->Error_On(LLC_DRY_PUMP_ALARM, _T("LLC DRY Pump Alarm 발생")))
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Error Sequence Fail")));

						///////////////////////////////////////////////////
						//Mesaage Box Thread 임시 주석 TEST 필요 
						///////////////////////////////////////////////////
						//m_Message_Thread = AfxBeginThread(Message_UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LLC DRY TMP WARNNING 발생
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::LLC_DRY_PUMP_WARNNING_STATUS))
				{
					m_nLlc_DryPumpWarning_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::LLC_DRY_PUMP_WARNNING_STATUS]);
					cnt_io_str.Format("%d", m_nLlc_DryPumpWarning_Cnt);

					if (m_nLlc_DryPumpWarning_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC DRY PUMP WARNING 발생")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Warning IO Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Warning 발생")));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Warning 발생")));
						g_pLog->Display(0, "LLC Dry Pump Warning 발생");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LL DRY PUMP WARNING = 1 -> 정상 구동 상태, 0 이면 WARNING 발생.

						/////////////////////////////////////////////////
						///// Error 발생 후 error sequence 등록 부분
						/////////////////////////////////////////////////
						Dry_Pump_State = DRY_LLC_WARNING;
						m_Message_ThreadExitFlag = FALSE;
						///////////////////////////////////////////////////
						//Mesaage Box Thread 임시 주석 TEST 필요 
						///////////////////////////////////////////////////
						//m_Message_Thread = AfxBeginThread(Message_UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
					}
				}

				 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				 // MC DRY TMP ALARM 발생
				 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::MC_DRY_PUMP_ALARM_STATUS))
				{
					m_nMc_DryPumpAlarm_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::MC_DRY_PUMP_ALARM_STATUS]);
					cnt_io_str.Format("%d", m_nMc_DryPumpAlarm_Cnt);

					if (m_nMc_DryPumpAlarm_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC DRY PUMP ALARM 발생")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump Alarm IO Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump Alarm 발생에 따른 Error Sequence 작동")));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("MC Dry Pump Ararm 발생")));
						g_pLog->Display(0, "MC Dry Pump Alarm 발생에 따른 Error Sequence 작동");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // MC DRY PUMP ALARM = 1 -> 정상 구동 상태 , 0 이면 알람 발생

						error_str = _T("MC Dry Pump Alarm 발생에 따른 Error Sequence 작동");
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(error_str)));

						/////////////////////////////////////////////////
						///// Error 발생 후 error sequence 등록 부분
						/////////////////////////////////////////////////
						//g_pIO->MC_Dry_Pump_Error_Sequence();
						Dry_Pump_State = DRY_MC_ALARM;
						m_Message_ThreadExitFlag = FALSE;
						g_pIO->Error_On(MC_DRY_PUMP_ALARM, _T("MC DRY PUMP ALARM 발생 "));
						///////////////////////////////////////////////////
						//Mesaage Box Thread 임시 주석 TEST 필요 
						///////////////////////////////////////////////////
						//m_Message_Thread = AfxBeginThread(Message_UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// MC DRY TMP WARNNING 발생
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::MC_DRY_PUMP_WARNNING_STATUS))
				{
					m_nMc_DryPumpWarning_Cnt += 1;

					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::MC_DRY_PUMP_WARNNING_STATUS]);
					cnt_io_str.Format("%d", m_nMc_DryPumpWarning_Cnt);
					if (m_nMc_DryPumpWarning_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC DRY PUMP WARNING 발생")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump Warning IO Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump Warning 발생")));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("MC Dry Pump Warning 발생")));

						g_pLog->Display(0, "MC Dry Pump Warning 발생");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // MC DRY PUMP WARNING = 1->정상 구동 상태, 0 이면 WARNING 발생.

						/////////////////////////////////////////////////
						///// Error 발생 후 error sequence 등록 부분
						/////////////////////////////////////////////////
						Dry_Pump_State = DRY_MC_WARNING;
						m_Message_ThreadExitFlag = FALSE;

						///////////////////////////////////////////////////
						//Mesaage Box Thread 임시 주석 TEST 필요 
						///////////////////////////////////////////////////
						//m_Message_Thread = AfxBeginThread(Message_UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
					}
				 }

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LLC Mask Check 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::LLC_MASK_CHECK))
				{
					m_nLLCMaskCheck_Cnt  += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::LLC_MASK_CHECK]);
					cnt_io_str.Format("%d", m_nLLCMaskCheck_Cnt);
					if (m_nLLCMaskCheck_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Check 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Check 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Check 감지 ")));

						g_pLog->Display(0, "LLC Mask 감지");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LLC Mask 기울어짐 감지 #1 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::LLC_MASK_TILT1))
				{
					m_nLLCMaskTilt1_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::LLC_MASK_TILT1]);
					cnt_io_str.Format("%d", m_nLLCMaskTilt1_Cnt);
					if (m_nLLCMaskTilt1_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Tilt#1 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Tilt#1 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Tilt#1 감지 ")));

						g_pLog->Display(0, "LLC Mask Tilt #1 감지");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LLC Mask 기울어짐 감지 #2
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::LLC_MASK_TILT2))
				{
					m_nLLCMaskTilt2_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::LLC_MASK_TILT2]);
					cnt_io_str.Format("%d", m_nLLCMaskTilt2_Cnt);
					if (m_nLLCMaskTilt2_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Tilt#2 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Tilt#2 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Tilt#2 감지 ")));

						g_pLog->Display(0, "LLC Mask Tilt #2 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// MC Mask Check 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::MC_MASK_CHECK))
				{
					m_nMCMaskCheck_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::MC_MASK_CHECK]);
					cnt_io_str.Format("%d", m_nMCMaskCheck_Cnt);
					if (m_nMCMaskCheck_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Check 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Check 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Check 감지 ")));

						g_pLog->Display(0, "MC MASK CHECK ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// MC Mask 기울어짐 감지 #1 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::MC_MASK_TILT1))
				{
					m_nMCMaskTilt1_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::MC_MASK_TILT1]);
					cnt_io_str.Format("%d", m_nMCMaskTilt1_Cnt);
					if (m_nMCMaskTilt1_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Tilt#1 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Tilt#1 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Tilt#1 감지 ")));

						g_pLog->Display(0, "MC Mask Tilt #1 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// MC Mask 기울어짐 감지 #2 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::MC_MASK_TILT2))
				{
					m_nMCMaskTilt2_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::MC_MASK_TILT2]);
					cnt_io_str.Format("%d", m_nMCMaskTilt2_Cnt);
					if (m_nMCMaskTilt2_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Tilt#2 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Tilt#2 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Tilt#2 감지 ")));

						g_pLog->Display(0, "MC Mask Tilt #2 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// VAC ROBOT MASK CHECK TO LLC 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::VAC_ROBOT_MASK_CHECK_TO_LLC))
				{
					m_nLLCMaskCheck_VacRobot_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::VAC_ROBOT_MASK_CHECK_TO_LLC]);
					cnt_io_str.Format("%d", m_nLLCMaskCheck_VacRobot_Cnt);
					if (m_nLLCMaskCheck_VacRobot_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Check(VAC ROBOT TO LLC) 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Check(VAC ROBOT TO LLC) 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Mask Check(VAC ROBOT TO LLC) 감지 ")));

						g_pLog->Display(0, "LLC Mask Check(VAC ROBOT TO LLC) 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]); // LLC MASK 감지 시 0 -> 감지 상태,.

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// VAC ROBOT MASK CHECK TO MC 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::VAC_ROBOT_MASK_CHECK_TO_MC))
				{
					m_nMCMaskCheck_VacRobot_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::VAC_ROBOT_MASK_CHECK_TO_MC]);
					cnt_io_str.Format("%d", m_nMCMaskCheck_VacRobot_Cnt);
					if (m_nMCMaskCheck_VacRobot_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Check (VAC ROBOT TO MC) 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Check (VAC ROBOT TO MC) 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Mask Check (VAC ROBOT TO MC) 감지 ")));

						g_pLog->Display(0, "MC Mask Check (VAC ROBOT TO MC) 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Main Air SW
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::MAIN_AIR_SW))
				{
					m_nMainAir_Alarm_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::MAIN_AIR_SW]);
					cnt_io_str.Format("%d", m_nMainAir_Alarm_Cnt);
					if (m_nMainAir_Alarm_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Air Alarm 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Air Alarm 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Air Alarm 감지 ")));

						g_pLog->Display(0, "MC Air Alarm 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..
						
						if (!g_pIO->Error_On(AIR_SUPPLY_ERROR, _T("Main Air 공급 Error ")));
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Error Sequence Fail")));
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// ISORLATOR Air SW
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::ISOLATOR_AIR_SW))
				{
					m_Isolator_Alarm_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::ISOLATOR_AIR_SW]);
					cnt_io_str.Format("%d", m_Isolator_Alarm_Cnt);
					if (m_Isolator_Alarm_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Isolator Air Alarm 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Isolator Air Alarm 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Isolator Air Alarm 감지 ")));

						g_pLog->Display(0, "Isolator Air Alarm 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

						if (!g_pIO->Error_On(AIR_SUPPLY_ERROR, _T("ISOLATOR Air 공급 Error ")));
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Error Sequence Fail")));

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LID Air SW
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::LID_AIR_SW))
				{
					m_Lid_Alarm_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::LID_AIR_SW]);
					cnt_io_str.Format("%d", m_Lid_Alarm_Cnt);
					if (m_Lid_Alarm_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Lid Air Alarm 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Lid Air Alarm 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Lid Air Alarm 감지 ")));

						g_pLog->Display(0, "Lid Air Alarm 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

						if (!g_pIO->Error_On(AIR_SUPPLY_ERROR, _T("Lid Air 공급 Error ")));
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Error Sequence Fail")));
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Main Water SW
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::WATER_FLOW_SW))
				{
					m_nMainWater_Alarm_Cnt += 1;
					value_io_str.Format("%d", g_pIO->m_bDigitalIn[g_pIO->DI::WATER_FLOW_SW]);
					cnt_io_str.Format("%d", m_nMainWater_Alarm_Cnt);
					if (m_nMainWater_Alarm_Cnt == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Water SW 감지 ")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Water SW 감지 Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Water SW 감지 ")));

						g_pLog->Display(0, "MC Water SW 감지 ");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LLC MASK 감지 시 0 -> 감지 상태, 0 이면 Mask 없음..

						if (!g_pIO->Error_On(WATER_SUPPLY_ERROR, _T("Main Water 공급 Error ")));
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Error Sequence Fail")));

						if (g_pIO->Close_WaterSupplyValve() == OPERATION_COMPLETED)
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(" MAIN_ERROR_ON() : Close Water Supply Valve Complete !")));
						else
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(" MAIN_ERROR_ON() : Close Water Supply Valve Fail!")));
					}

				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Main Water Temp SW
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if ((nIdx == g_pIO->DI::WATER_TEMP_ALARM))
				{
					m_nWater_Temp_Alarm_Cnt = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}

				else if (nIdx == g_pIO->DI::ATM_ROBOT_HAND_EX)
				{
					if(g_pIO->Is_ATM_Robot_Arm_Extend())
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
					else
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}

				else if (((nIdx == g_pIO->DI::ISOLATOR_INTERLOCK_SENSOR1)))
				{
					//SaveLogFile("ISOLATOR INTERLOCK SENSOR", _T((LPSTR)(LPCTSTR)("SENSOR1 감지 - 비정상")));
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else if (((nIdx == g_pIO->DI::ISOLATOR_INTERLOCK_SENSOR2)))
				{
					//SaveLogFile("ISOLATOR INTERLOCK SENSOR", _T((LPSTR)(LPCTSTR)("SENSOR2 감지 - 비정상")));
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
				else
				{
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}
			}
		}
	}
	   	
	
	return TRUE;
}


HBRUSH CDigitalInput::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_TEXT)
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
	}
	return hbr;
}

UINT CDigitalInput::Message_UpdataThread(LPVOID pParam)
{
	int ret = 0;

	CDigitalInput*  message_runthread = (CDigitalInput*)pParam;

	while (!message_runthread->m_Message_ThreadExitFlag)
	{
		message_runthread->Message_Thread();
		Sleep(100);
	}

	return 0;
}


void CDigitalInput::Message_Thread()
{

	CString message_str;

	switch (Dry_Pump_State)
	{
	case DRY_NO_ERROR :

		break;
	case DRY_LLC_ALARM :
		message_str = _T("LLC Dry Pump Alarm");
		break;
	case DRY_MC_ALARM :
		message_str = _T("MC Dry Pump Alarm");
		break;
	case DRY_LLC_WARNING :
		message_str = _T("LLC Dry Pump Warning");
		break;
	case DRY_MC_WARNING:
		message_str = _T("LLC Dry Pump Warning");
		break;
	default:
		break;
	}

	if (g_pIO->m_nERROR_MODE == RUN)
	{
		///////////////////////////////////
		// Buzzer On
		///////////////////////////////////
		g_pIO->WriteOutputDataBit(8, 3, 1);

		if (AfxMessageBox(message_str + "\n" + "발생으로 인한 ERROR [설비 비상 정지]", MB_OKCANCEL) == IDOK) // or IDCANCEL
		{
			m_Message_ThreadExitFlag = TRUE;

			///////////////////////////////////
			// Buzzer On
			///////////////////////////////////
			g_pIO->WriteOutputDataBit(8, 3, 0);

			if (m_Message_Thread != NULL)
			{
				HANDLE threadHandle = m_Message_Thread->m_hThread;
				DWORD dwResult;
				dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
				if (dwResult == WAIT_TIMEOUT)
				{
					DWORD dwExitCode = STILL_ACTIVE;
					::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
					if (dwExitCode == STILL_ACTIVE)	//259
					{
						TerminateThread(threadHandle, 0/*dwExitCode*/);
						CloseHandle(threadHandle);
					}
				}
				m_Message_Thread = NULL;
			}
		}
	}
}