﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CConfigurationEditorDlg 대화 상자

IMPLEMENT_DYNAMIC(CConfigurationEditorDlg, CDialogEx)

CConfigurationEditorDlg::CConfigurationEditorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CONFIG_DIALOG, pParent)
	, m_dGlobalOffsetX_mm(0)
	, m_dGlobalOffsetY_mm(0)
	, m_dLBOffsetX_mm(0)
	, m_dLBOffsetY_mm(0)
	, m_dLTOffsetX_mm(0)
	, m_dLTOffsetY_mm(0)
	, m_dRTOffsetX_mm(0)
	, m_dRTOffsetY_mm(0)
	, m_nSourceAutoOffTime_min(30)	//30*60sec = 30 min --> Thrufocus 시간때문에 늘림 kyd
	, m_dOM_PixelSizeX_um(0.86)
	, m_dOM_PixelSizeY_um(0.86)
	, m_nOMStageAreaLB_X_mm(172)
	, m_nOMStageAreaLB_Y_mm(10)
	, m_nOMStageAreaRT_X_mm(320)
	, m_nOMStageAreaRT_Y_mm(170)
	, m_nEUVStageAreaLB_X_mm(25)
	, m_nEUVStageAreaLB_Y_mm(10)
	, m_nEUVStageAreaRT_X_mm(172)
	, m_nEUVStageAreaRT_Y_mm(170)
{
	m_dPressure_ChangeToFast_MC_Rough_ini = 50.0;	// MC Slow rough -> Fast Rough
	m_dPressure_ChangeToFast_Rough_ini = 50.0;		// LLC Slow rough -> Fast Rough

	m_dPressure_ChangeToFast_MC_Rough = 50.0;		// MC Slow rough -> Fast Rough
	m_dPressure_ChangeToFast_Rough = 50.0;			// LLC Slow rough -> Fast Rough

	m_dPressure_ChangeToTMP_Rough = 0.02;			// [2 * 10^-2] 	Fast Rougn -> Tmp pumping
	m_dPressure_ChangeToTMP_Rough_ini = 0.02;		// [2 * 10^-2] 	Fast Rougn -> Tmp pumping

	m_dPressure_ChangeToMCTMP_Rough = 0.035;		// [35 * 10^-2]  MC Fast Rough -> TMP pumping 	
	m_dPressure_ChangeToMCTMP_Rough_ini = 0.035;	// [35 * 10^-2]  MC Fast Rough -> TMP pumping 	
	
	m_dPressure_Rough_End = 0.000009;				// Tmp pumping 조건 값	m_dPressure_ChangeToSlow2_Vent = 0.1;
	m_dPressure_Rough_End_ini = 0.000009;			// Tmp pumping 조건 값	m_dPressure_ChangeToSlow2_Vent = 0.1;

	m_dPressure_ChangeToVent = 0.003;				// MC 대기 Venting
	m_dPressure_ChangeToVent_ini = 0.003;			// MC 대기 Venting

	m_dPressure_ChangeToVent_LLC = 0.00002;	// 2 x 10^-5
	m_dPressure_ChangeToVent_LLC_ini = 0.00002;

	m_dPressure_ChangeToSlow2_Vent = 1;		//LLC slow vent -> Fast vent
	m_dPressure_ChangeToSlow2_Vent_ini = 1;			//LLC slow vent -> Fast vent

	m_dPressure_ChangeToSlow_Vent = 0.1;			// MC Vent 전 SLOW VENT 가능 조건 검사 압력 기준, MC slow vent -> mc fast vent 
	m_dPressure_ChangeToSlow_Vent_ini = 0.1;			// MC Vent 전 SLOW VENT 가능 조건 검사 압력 기준, MC slow vent -> mc fast vent 

	m_dPressure_ChangeToFast_Vent = 10.0; 
	
	m_dPressure_Vent_End = 760.0;
	m_dPressure_Vent_End_ini = 760.0;

	m_dPressure_Vent_Tolerance = 10.0;
	m_nTimeout_sec_LLKSlowRough = 90;				//LLC Slow Rough 시간
	m_nTimeout_sec_LLKFastRough = 300;
	m_nTimeout_sec_LLKRoughEnd = 1200;
	m_nTimeout_sec_LLKStandbyVent = 600;			// LLC 대기 시간
	m_nTimeout_sec_LLKSlow1Vent = 300;				// LLC Slow Vent 시간
	m_nTimeout_sec_LLKFastVent = 120;				// LLC Fast Vent 시간
	m_nTimeout_sec_LLKVentEnd = 1200;				// LLC Venting 시간.
	m_nTimeout_sec_ValveOperation = 10;
	m_nTimeout_sec_MCSlowRough = 600;
	m_nTimeout_sec_MCFastRough = 600;
	m_nTimeout_sec_MCTmpEnd = 3600;
	m_nTimeout_sec_MCLLCVent = 1800;				// MC Vent 시 TMP Gate Close 후 대기 venting 시간.(30분)
	m_nTimeout_sec_MCRoughEnd = 1000;
	m_nTimeout_sec_MCSlow1Vent = 1800;				// MC Vent 시 Slow Vent 시간 (30분)
	m_nTimeout_sec_MCFastVent = 1800;				// MC Vent 시 Fast Vent 시간 (30분)
	m_nTimeout_sec_MCVentEnd = 300;

	m_nTimeout_sec_LLKSlowRough_ini = m_nTimeout_sec_LLKSlowRough;
	m_nTimeout_sec_LLKFastRough_ini = m_nTimeout_sec_LLKFastRough;
	m_nTimeout_sec_LLKRoughEnd_ini = m_nTimeout_sec_LLKRoughEnd;
	m_nTimeout_sec_LLKSlow1Vent_ini = m_nTimeout_sec_LLKSlow1Vent;
	m_nTimeout_sec_LLKStandbyVent_ini = m_nTimeout_sec_LLKStandbyVent;
	m_nTimeout_sec_LLKFastVent_ini = m_nTimeout_sec_LLKFastVent;
	m_nTimeout_sec_LLKVentEnd_ini = m_nTimeout_sec_LLKVentEnd;
	m_nTimeout_sec_ValveOperation_ini = m_nTimeout_sec_ValveOperation;
	m_nTimeout_sec_MCSlowRough_ini = m_nTimeout_sec_MCSlowRough;
	m_nTimeout_sec_MCFastRough_ini = m_nTimeout_sec_MCFastRough;
	m_nTimeout_sec_MCRoughEnd_ini = m_nTimeout_sec_MCRoughEnd;
	m_nTimeout_sec_MCTmpEnd_ini = m_nTimeout_sec_MCTmpEnd;
	m_nTimeout_sec_MCLLCVent_ini = m_nTimeout_sec_MCLLCVent;
	m_nTimeout_sec_MCSlow1Vent_ini = m_nTimeout_sec_MCSlow1Vent; 
	m_nTimeout_sec_MCFastVent_ini = m_nTimeout_sec_MCFastVent;
	m_nTimeout_sec_MCVentEnd_ini = m_nTimeout_sec_MCVentEnd;

	m_dPressure_SlowMFC_Inlet_Valve_Open_Value = 0.1;	// Slow MFC Inlet Open 시점 값


	int i = 0;
	for (i = 0; i < MAX_STAGE_POSITION; i++)
	{
		m_stStagePos[i].x = m_stStagePos[i].y = 0.0;
		sprintf(m_stStagePos[i].chStagePositionString, "");
	}


	m_dOMAlignPointLB_X_mm = 0.0;
	m_dOMAlignPointLB_Y_mm = 0.0;
	m_dOMAlignPointLT_X_mm = 0.0;
	m_dOMAlignPointLT_Y_mm = 0.0;
	m_dOMAlignPointRT_X_mm = 0.0;
	m_dOMAlignPointRT_Y_mm = 0.0;
	m_dEUVAlignPointLB_X_mm = 0.0;
	m_dEUVAlignPointLB_Y_mm = 0.0;
	m_dEUVAlignPointLT_X_mm = 0.0;
	m_dEUVAlignPointLT_Y_mm = 0.0;
	m_dEUVAlignPointRT_X_mm = 0.0;
	m_dEUVAlignPointRT_Y_mm = 0.0;
	m_bOMAlignCompleteFlag = FALSE;
	m_bEUVAlignCompleteFlag = FALSE;
	
	m_bMtsRotateDone_Flag	= FALSE;
	m_bMtsFlipDone_Flag		= FALSE;
	m_nMaterialLocation		= -1;

	//BS Calibration
	m_nBsSelectedIndex = -1;
	m_nBsExposureTime = 0;
	m_dBsBlankPosX = 0.0;
	m_dBsBlankPosY = 0.0;
	m_dBsCenterPosX = 0.0;
	m_dBsCenterPosY = 0.0;

	m_dBSBGD1_Intensity = 0.0;
	m_dBSBGD1_Standardeviation = 0.0;
	m_dBSBGD2_Intensity = 0.0;
	m_dBSBGD2_Standardeviation = 0.0;
	m_dBSBGD3_Intensity = 0.0;
	m_dBSBGD3_Standardeviation = 0.0;

	m_dBSWOD1_Intensity = 0.0;
	m_dBSWOD1_Standardeviation = 0.0;
	m_dBSWOD2_Intensity = 0.0;
	m_dBSWOD2_Standardeviation = 0.0;
	m_dBSWOD3_Intensity = 0.0;
	m_dBSWOD3_Standardeviation = 0.0;

	m_dBSBSD1_Intensity = 0.0;
	m_dBSBSD1_Standardeviation = 0.0;
	m_dBSBSD2_Intensity = 0.0;
	m_dBSBSD2_Standardeviation = 0.0;
	m_dBSBSD3_Intensity = 0.0;
	m_dBSBSD3_Standardeviation = 0.0;

	//m_rcOMStageAreaRect.SetRect(175, 10, 330, 165);	//Stage의 OM 영역(RB_x, RB_y, LT_x, LT_y, )  -> SREM Chuck
	//m_rcEUVStageAreaRect.SetRect(30, 10, 175, 165);	//Stage의 EUV 영역(RB_x, RB_y, LT_x, LT_y, ) -> SREM Chuck
	//m_rcOMStageAreaRect.SetRect(190, 10, 340, 178);	//Stage의 OM 영역(RB_x, RB_y, LT_x, LT_y, )  -> PTR Chuck
	//m_rcEUVStageAreaRect.SetRect(30, 10, 190, 178);	//Stage의 EUV 영역(RB_x, RB_y, LT_x, LT_y, ) -> PTR Chuck
	m_rcOMStageAreaRect.SetRect(m_nOMStageAreaLB_X_mm, m_nOMStageAreaLB_Y_mm, m_nOMStageAreaRT_X_mm, m_nOMStageAreaRT_Y_mm);	//Stage의 OM 영역(LB_x, LB_y, RT_x, RT_y, )      -> Zerodur Chuck , config에 생성필요
	m_rcEUVStageAreaRect.SetRect(m_nEUVStageAreaLB_X_mm, m_nEUVStageAreaLB_Y_mm, m_nEUVStageAreaRT_X_mm, m_nEUVStageAreaRT_Y_mm);	//Stage의 EUV 영역(LB_x, LB_y, RT_x,RT_y, )     -> Zerodur Chuck , config에 생성필요


	m_bUseRotate		= TRUE;
	m_bUseFlip			= FALSE;
	m_nRotateAngle		= 90;

	m_dEUVVacuumRate = 0.0000007;

	m_dFilterSavedPosition = 0.;
}

CConfigurationEditorDlg::~CConfigurationEditorDlg()
{
}

void CConfigurationEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_GLOBAL_OFFSETX, m_dGlobalOffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_GLOBAL_OFFSETY, m_dGlobalOffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_LB_OFFSETX, m_dLBOffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_LB_OFFSETY, m_dLBOffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_LT_OFFSETX, m_dLTOffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_LT_OFFSETY, m_dLTOffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_RT_OFFSETX, m_dRTOffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_RT_OFFSETY, m_dRTOffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_SOURCE_AUTOOFF_TIME, m_nSourceAutoOffTime_min);
	DDX_Control(pDX, IDC_CHECK_CONFIG_USE_ROTATE, m_UseMtsRotateCtrl);
	DDX_Control(pDX, IDC_COMBO_CONFIG_ROTATE_ANGLE, m_MtsRotateAngleCtrl);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_PIXEL_POSX, m_dOM_PixelSizeX_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_PIXEL_POSY, m_dOM_PixelSizeY_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_STAGE_LB_POSX, m_nOMStageAreaLB_X_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_STAGE_LB_POSY, m_nOMStageAreaLB_Y_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_STAGE_RT_POSX, m_nOMStageAreaRT_X_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_STAGE_RT_POSY, m_nOMStageAreaRT_Y_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_EUV_STAGE_LB_POSX, m_nEUVStageAreaLB_X_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_EUV_STAGE_LB_POSY, m_nEUVStageAreaLB_Y_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_RT_STAGE_RT_POSX, m_nEUVStageAreaRT_X_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_RT_STAGE_RT_POSY, m_nEUVStageAreaRT_Y_mm);
}


BEGIN_MESSAGE_MAP(CConfigurationEditorDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_CAL_LOAD, &CConfigurationEditorDlg::OnBnClickedButtonConfigCalLoad)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_CAL_SAVE, &CConfigurationEditorDlg::OnBnClickedButtonConfigCalSave)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_GLOBAL_OFFSETX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalGlobalOffsetx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_GLOBAL_OFFSETY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalGlobalOffsety)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_LB_OFFSETX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalLbOffsetx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_LB_OFFSETY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalLbOffsety)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_LT_OFFSETX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalLtOffsetx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_LT_OFFSETY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalLtOffsety)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_RT_OFFSETX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalRtOffsetx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_RT_OFFSETY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalRtOffsety)
	ON_BN_CLICKED(IDC_BUTTON_SET_AUTO_OFFSET, &CConfigurationEditorDlg::OnBnClickedButtonSetAutoOffset)
	ON_BN_CLICKED(IDC_BUTTON_SET_LB_OFFSET, &CConfigurationEditorDlg::OnBnClickedButtonSetLbOffset)
	ON_BN_CLICKED(IDC_BUTTON_SET_LT_OFFSET, &CConfigurationEditorDlg::OnBnClickedButtonSetLtOffset)
	ON_BN_CLICKED(IDC_BUTTON_SET_RT_OFFSET, &CConfigurationEditorDlg::OnBnClickedButtonSetRtOffset)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_ETC_LOAD, &CConfigurationEditorDlg::OnBnClickedButtonConfigEtcLoad)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_ETC_SAVE, &CConfigurationEditorDlg::OnBnClickedButtonConfigEtcSave)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET0, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet0)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET1, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet1)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET2, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet2)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET3, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet3)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET4, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet4)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET5, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet5)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET6, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet6)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET7, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet7)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET8, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet8)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET9, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet9)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET10, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet10)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CONNECT_ETHERNET11, &CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet11)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET0, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet0)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET1, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet1)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET2, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet2)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET3, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet3)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET4, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet4)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET5, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet5)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET6, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet6)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET7, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet7)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET8, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet8)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET9, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet9)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET10, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet10)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET11, &CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet11)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_OPEN_SERIAL0, &CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial0)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_OPEN_SERIAL1, &CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial1)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_OPEN_SERIAL2, &CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial2)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_OPEN_SERIAL3, &CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial3)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_OPEN_SERIAL4, &CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial4)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_OPEN_SERIAL5, &CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial5)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_OPEN_SERIAL6, &CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial6)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_OPEN_SERIAL7, &CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial7)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_OPEN_SERIAL8, &CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial8)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CLOSE_SERIAL0, &CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial0)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CLOSE_SERIAL1, &CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial1)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CLOSE_SERIAL2, &CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial2)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CLOSE_SERIAL3, &CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial3)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CLOSE_SERIAL4, &CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial4)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CLOSE_SERIAL5, &CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial5)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CLOSE_SERIAL6, &CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial6)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CLOSE_SERIAL7, &CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial7)
	ON_BN_CLICKED(IDC_CONFIG_BUTTON_CLOSE_SERIAL8, &CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial8)
END_MESSAGE_MAP()


// CConfigurationEditorDlg 메시지 처리기


BOOL CConfigurationEditorDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CConfigurationEditorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);

	InitialICon();
	ReadFile();

	m_font.CreateFont(20, 8, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));
	int i = 0;


	m_MtsRotateAngleCtrl.AddString(_T("90"));
	m_MtsRotateAngleCtrl.AddString(_T("180"));
	m_MtsRotateAngleCtrl.AddString(_T("270"));
	m_MtsRotateAngleCtrl.SetCurSel(0);
	m_UseMtsRotateCtrl.SetCheck(m_bUseRotate);

	ReadETCInfo();

	ReadRecoveryData();
	ReadVacuumSeqData();


	UpdateData(FALSE);

	SetTimer(PORT_CHECK_TIMER, 1000, NULL);


	GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET3)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET3)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET4)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET4)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET10)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET10)->EnableWindow(false);

	GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL0)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL0)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL5)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL5)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL6)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL6)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL8)->EnableWindow(false);
	GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL8)->EnableWindow(false);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CConfigurationEditorDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(PORT_CHECK_TIMER);

}


void CConfigurationEditorDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case PORT_CHECK_TIMER:
		PortCheck();
		SetTimer(nIDEvent, 1000, NULL);
		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}

int CConfigurationEditorDlg::SaveFile()
{
	int nRet = 0, i = 0;
	char chTmp[125];
	CString strSector, strKey;

	return nRet;
}

int CConfigurationEditorDlg::ReadFile()
{
	int nRet = 0, i = 0;
	char chTmp[125];
	CString strSector,strKey;
	HWND hwnd_com_port, hwnd_rate, hwnd_bit, hwnd_stop, hwnd_parity, hwnd_ip, hwnd_ip_port;
	

	strSector.Format(_T("GENERAL_CONFIG"));
	strKey.Format(_T("EQUIPMENT_MODE"));
	nRet = GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
	if (nRet == 0)
	{
		nRet = -1;
		return nRet;
	}
	m_nEquipmentMode = atoi(chTmp);

	strSector.Format(_T("COMMUNICATION_CONFIG"));
	for (i = 0; i < ETHERNET_COM_NUMBER; i++)
	{
		strKey.Format(_T("ETHERNET_IP_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chIP[i], chTmp);
		strKey.Format(_T("ETHERNET_PORT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nPORT[i] = atoi(chTmp);
		sprintf(m_chIPPORT[i], chTmp);
	}
	

	for (i = 0; i < SERIAL_COM_NUMBER; i++)
	{
		strKey.Format(_T("SERIAL_PORT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chPORT[i], chTmp);
		strKey.Format(_T("SERIAL_BAUD_RATE_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nBAUD_RATE[i] = atoi(chTmp);
		sprintf(m_ch_nBAUD_RATE[i], chTmp);
		strKey.Format(_T("SERIAL_USE_BIT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nUSE_BIT[i] = atoi(chTmp);
		sprintf(m_ch_nUSE_BIT[i], chTmp);
		strKey.Format(_T("SERIAL_STOP_BIT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nSTOP_BIT[i] = atoi(chTmp);
		sprintf(m_ch_nSTOP_BIT[i], chTmp);
		strKey.Format(_T("SERIAL_PARITY_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nPARITY[i] = atoi(chTmp);
		sprintf(m_ch_nPARITY[i], chTmp);
	}

	strSector.Format(_T("DIGITAL_INPUT"));
	for (i = 0; i < DIGITAL_INPUT_NUMBER; i++)
	{
		strKey.Format(_T("%03d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, IOMAP_FILE_FULLPATH);
		if (nRet == 0) sprintf_s(m_chDi[i], "EMPTY");
		sprintf_s(m_chDi[i], chTmp);
	}

	strSector.Format(_T("DIGITAL_OUTPUT"));
	for (i = 0; i < DIGITAL_OUTPUT_NUMBER; i++)
	{
		strKey.Format(_T("%03d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, IOMAP_FILE_FULLPATH);
		if (nRet == 0) sprintf_s(m_chDo[i], "EMPTY");
		sprintf(m_chDo[i], chTmp);
	}

	strSector.Format(_T("ANALOG_INPUT"));
	for (int nIdx = 0; nIdx < ANALOG_INPUT_NUMBER; nIdx++)
	{
		strKey.Format(_T("%03d"), nIdx);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, IOMAP_FILE_FULLPATH);
		if (nRet == 0) sprintf_s(m_chAi[i], "EMPTY");
		sprintf(m_chAi[nIdx], chTmp);
	}

	strSector.Format(_T("ANALOG_OUTPUT"));
	for (int nIdx = 0; nIdx < ANALOG_OUTPUT_NUMBER; nIdx++)
	{
		strKey.Format(_T("%03d"), nIdx);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, IOMAP_FILE_FULLPATH);
		if (nRet == 0) sprintf_s(m_chAo[i], "EMPTY");
		sprintf(m_chAo[nIdx], chTmp);
		CString buff;
			buff.Format("%d", nRet);
		g_pLog->Display(0, buff);
	}

	return nRet;
}


void CConfigurationEditorDlg::SaveStagePositionToDB()
{
	CString strAppName, strString;

	for (int i = 0; i < MAX_STAGE_POSITION; i++)
	{
		strAppName.Format("STAGE_POSITION_%02d", i);
		strString.Format("%s", m_stStagePos[i].chStagePositionString);
		WritePrivateProfileString(strAppName, "NAME", strString, STAGEPOSITION_FILE_FULLPATH);
		strString.Format("%4.6lf", m_stStagePos[i].x);
		WritePrivateProfileString(strAppName, "X(mm)", strString, STAGEPOSITION_FILE_FULLPATH);
		strString.Format("%4.6lf", m_stStagePos[i].y);
		WritePrivateProfileString(strAppName, "Y(mm)", strString, STAGEPOSITION_FILE_FULLPATH);
	}
}

void CConfigurationEditorDlg::BackupStagePositionToDB(CString sPath)
{
	CString strAppName, strString;

	for (int i = 0; i < MAX_STAGE_POSITION; i++)
	{
		strAppName.Format("STAGE_POSITION_%02d", i);
		strString.Format("%s", m_stStagePos[i].chStagePositionString);
		WritePrivateProfileString(strAppName, "NAME", strString, sPath);
		strString.Format("%4.6lf", m_stStagePos[i].x);
		WritePrivateProfileString(strAppName, "X(mm)", strString, sPath);
		strString.Format("%4.6lf", m_stStagePos[i].y);
		WritePrivateProfileString(strAppName, "Y(mm)", strString, sPath);
	}
}

void CConfigurationEditorDlg::ReadVacuumSeqData()
{
		char chTmp[125];
		CString strSector, strKey;

		strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
		
		strKey.Format(_T("Mc_StandBy_Vent_Value"));
		strKey.Format(_T("Mc_Slow_Vent_Value"));
		strKey.Format(_T("Mc_Fast_Vent_Value"));
		strKey.Format(_T("Mc_Slow_Rough_Value"));
		strKey.Format(_T("Mc_Fast_Rough_Value"));
		strKey.Format(_T("Mc_Tmp_Rough_Value"));
		strKey.Format(_T("Mc_StandBy_Vent_Time"));
		strKey.Format(_T("Mc_Slow_Vent_Time"));
		strKey.Format(_T("Mc_Fast_Vent_Time"));
		strKey.Format(_T("Mc_Slow_Rough_Time"));
		strKey.Format(_T("Mc_Fast_Rough_Time"));
		strKey.Format(_T("Mc_Tmp_Rough_Time"));
		strKey.Format(_T("Llc_StandBy_Vent_Value"));
		strKey.Format(_T("Llc_Slow_Vent_Value"));
		strKey.Format(_T("Llc_Slow_Rough_Value"));
		strKey.Format(_T("Llc_Fast_Rough_Value"));
		strKey.Format(_T("Llc_Tmp_Rough_Value"));
		strKey.Format(_T("Llc_Fast_Vent_Value"));

		
		
		//Mc Data

		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToVent);

		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToSlow_Vent);
		
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_Vent_End);

		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToFast_MC_Rough);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToMCTMP_Rough);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_Rough_End);

		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCLLCVent);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCSlow1Vent);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCFastVent);

		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCSlowRough);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCFastRough);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCTmpEnd);


		//Llc Data
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToVent_LLC);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToSlow2_Vent);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_Vent_End);

		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToFast_Rough);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToTMP_Rough);
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_Rough_End);

		strKey.Format(_T("Llc_StandBy_Vent_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKStandbyVent);
		strKey.Format(_T("Llc_Slow_Vent_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKSlow1Vent);
		strKey.Format(_T("Llc_Fast_Vent_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKFastVent);

		strKey.Format(_T("Llc_Slow_Rough_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKSlowRough);
		strKey.Format(_T("Llc_Fast_Rough_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKFastRough);
		strKey.Format(_T("Llc_Tmp_Rough_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKRoughEnd);
}

void CConfigurationEditorDlg::SaveVacuumSeqData()
{
		CString strSector, strKey, strTemp;

		strSector.Format(_T("VACUUM_SEQUENCE_INFO"));

		strKey.Format(_T("Mc_StandBy_Vent_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Slow_Vent_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToSlow_Vent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Fast_Vent_Value"));
		strTemp.Format("%f", m_dPressure_Vent_End);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Mc_Slow_Rough_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToFast_MC_Rough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Fast_Rough_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToMCTMP_Rough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Tmp_Rough_Value"));
		strTemp.Format("%f", m_dPressure_Rough_End);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Mc_StandBy_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCLLCVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Slow_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCSlow1Vent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Fast_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCFastVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Mc_Slow_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCSlowRough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Fast_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCFastRough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Tmp_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCTmpEnd);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);


		strKey.Format(_T("Llc_StandBy_Vent_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToVent_LLC);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Slow_Vent_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToSlow2_Vent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Fast_Vent_Value"));
		strTemp.Format("%f", m_dPressure_Vent_End);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Llc_Slow_Rough_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToFast_Rough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Fast_Rough_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToTMP_Rough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Tmp_Rough_Value"));
		strTemp.Format("%f", m_dPressure_Rough_End);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Llc_StandBy_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKStandbyVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Slow_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKSlow1Vent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Fast_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKFastVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Llc_Slow_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKSlowRough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Fast_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKFastRough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Tmp_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKRoughEnd);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
}

void CConfigurationEditorDlg::ReadStagePositionFromDB()
{
	CString strAppName;
	char chGetString[128];
	int i = 0;

	for (i = 0; i < MAX_STAGE_POSITION; i++)
	{
		m_stStagePos[i].x = m_stStagePos[i].y = 0.0;
		sprintf(m_stStagePos[i].chStagePositionString, "");

		strAppName.Format("STAGE_POSITION_%02d", i);
		GetPrivateProfileString(strAppName, "NAME", "", chGetString, DATA_STRING_SIZE, STAGEPOSITION_FILE_FULLPATH);
		strcpy(m_stStagePos[i].chStagePositionString, chGetString);
		GetPrivateProfileString(strAppName, "X(mm)", "0.0", chGetString, DATA_STRING_SIZE, STAGEPOSITION_FILE_FULLPATH);
		m_stStagePos[i].x = atof(chGetString);
		GetPrivateProfileString(strAppName, "Y(mm)", "0.0", chGetString, DATA_STRING_SIZE, STAGEPOSITION_FILE_FULLPATH);
		m_stStagePos[i].y = atof(chGetString);
	}
}

void CConfigurationEditorDlg::ReadCalibrationInfo()
{
		CString strAppName, strKeyName;
		char chGetString[DATA_STRING_SIZE];

		strAppName.Format(_T("CALIBRATION_INFO"));

		strKeyName.Format(_T("Global_Offset_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dGlobalOffsetX_mm);
		strKeyName.Format(_T("Global_Offset_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dGlobalOffsetY_mm);
		strKeyName.Format(_T("LB_Align_Offset_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLBOffsetX_mm);
		strKeyName.Format(_T("LB_Align_Offset_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLBOffsetY_mm);
		strKeyName.Format(_T("LT_Align_Offset_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLTOffsetX_mm);
		strKeyName.Format(_T("LT_Align_Offset_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLTOffsetY_mm);
		strKeyName.Format(_T("RT_Align_Offset_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dRTOffsetX_mm);
		strKeyName.Format(_T("RT_Align_Offset_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dRTOffsetY_mm);

	UpdateData(FALSE);
}

void CConfigurationEditorDlg::SaveCalibrationInfo()
{
		CString strAppName, strKeyName, strValue;

		strAppName.Format(_T("CALIBRATION_INFO"));

		strKeyName.Format(_T("Global_Offset_X"));
		strValue.Format("%.4f", m_dGlobalOffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Global_Offset_Y"));
		strValue.Format("%.4f", m_dGlobalOffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LB_Align_Offset_X"));
		strValue.Format("%.4f", m_dLBOffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LB_Align_Offset_Y"));
		strValue.Format("%.4f", m_dLBOffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LT_Align_Offset_X"));
		strValue.Format("%.4f", m_dLTOffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LT_Align_Offset_Y"));
		strValue.Format("%.4f", m_dLTOffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("RT_Align_Offset_X"));
		strValue.Format("%.4f", m_dRTOffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("RT_Align_Offset_Y"));
		strValue.Format("%.4f", m_dRTOffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		
}

void CConfigurationEditorDlg::ReadRecoveryData()
{
		CString strAppName, strKeyName;
		char chGetString[DATA_STRING_SIZE];

		strAppName.Format(_T("RECOVERY_INFO"));
		strKeyName.Format(_T("OMAlignPointLB_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointLB_X_mm);
		strKeyName.Format(_T("OMAlignPointLB_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointLB_Y_mm);
		strKeyName.Format(_T("OMAlignPointLT_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointLT_X_mm);
		strKeyName.Format(_T("OMAlignPointLT_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointLT_Y_mm);
		strKeyName.Format(_T("OMAlignPointRT_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointRT_X_mm);
		strKeyName.Format(_T("OMAlignPointRT_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointRT_Y_mm);

		strKeyName.Format(_T("EUVAlignPointLB_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointLB_X_mm);
		strKeyName.Format(_T("EUVAlignPointLB_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointLB_Y_mm);
		strKeyName.Format(_T("EUVAlignPointLT_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointLT_X_mm);
		strKeyName.Format(_T("EUVAlignPointLT_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointLT_Y_mm);
		strKeyName.Format(_T("EUVAlignPointRT_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointRT_X_mm);
		strKeyName.Format(_T("EUVAlignPointRT_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointRT_Y_mm);

		strKeyName.Format(_T("OMAlignComplete_Flag"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_bOMAlignCompleteFlag);
		strKeyName.Format(_T("EUVAlignComplete_Flag"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_bEUVAlignCompleteFlag);

		strAppName.Format(_T("MTS_DATA"));
		strKeyName.Format(_T("RotateDone"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_bMtsRotateDone_Flag);
		strKeyName.Format(_T("FlipDone"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_bMtsFlipDone_Flag);

		strAppName.Format(_T("TRANSFER_INFO"));
		strKeyName.Format(_T("CurrentProcess"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nPreviousProcess);
		strKeyName.Format(_T("MaterialLocation"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMaterialLocation);
}

void CConfigurationEditorDlg::SaveRecoveryData()
{
		CString strAppName, strKeyName, strValue;

		strAppName.Format(_T("RECOVERY_INFO"));
		strKeyName.Format(_T("OMAlignPointLB_X"));
		strValue.Format("%.6f", m_dOMAlignPointLB_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointLB_Y"));
		strValue.Format("%.6f", m_dOMAlignPointLB_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointLT_X"));
		strValue.Format("%.6f", m_dOMAlignPointLT_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointLT_Y"));
		strValue.Format("%.6f", m_dOMAlignPointLT_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointRT_X"));
		strValue.Format("%.6f", m_dOMAlignPointRT_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointRT_Y"));
		strValue.Format("%.6f", m_dOMAlignPointRT_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strKeyName.Format(_T("EUVAlignPointLB_X"));
		strValue.Format("%.6f", m_dEUVAlignPointLB_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointLB_Y"));
		strValue.Format("%.6f", m_dEUVAlignPointLB_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointLT_X"));
		strValue.Format("%.6f", m_dEUVAlignPointLT_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointLT_Y"));
		strValue.Format("%.6f", m_dEUVAlignPointLT_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointRT_X"));
		strValue.Format("%.6f", m_dEUVAlignPointRT_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointRT_Y"));
		strValue.Format("%.6f", m_dEUVAlignPointRT_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strKeyName.Format(_T("OMAlignComplete_Flag"));
		strValue.Format("%d", m_bOMAlignCompleteFlag);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignComplete_Flag"));
		strValue.Format("%d", m_bEUVAlignCompleteFlag);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);


		strAppName.Format(_T("MTS_DATA"));
		strKeyName.Format(_T("RotateDone"));
		strValue.Format("%d", m_bMtsRotateDone_Flag);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("FlipDone"));
		strValue.Format("%d", m_bMtsFlipDone_Flag);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strAppName.Format(_T("TRANSFER_INFO"));
		strKeyName.Format(_T("CurrentProcess"));
		strValue.Format("%d", m_nPreviousProcess);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("MaterialLocation"));
		strValue.Format("%d", m_nMaterialLocation);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
}

void CConfigurationEditorDlg::SaveCurrentProcess(int nProcess)
{
	m_nPreviousProcess = nProcess;

		CString strSector, strKey, strTemp;
		strSector.Format(_T("TRANSFER_INFO"));
		strKey.Format(_T("CurrentProcess"));
		strTemp.Format("%d", m_nPreviousProcess);
		WritePrivateProfileString(strSector, strKey, strTemp, RECOVERY_FILE_FULLPATH);
}

void CConfigurationEditorDlg::SaveMaterialLocation(int nLoc)
{
	m_nMaterialLocation = nLoc;

		CString strSector, strKey, strTemp;
		strSector.Format(_T("TRANSFER_INFO"));
		strKey.Format(_T("MaterialLocation"));
		strTemp.Format("%d", m_nMaterialLocation);
		WritePrivateProfileString(strSector, strKey, strTemp, RECOVERY_FILE_FULLPATH);
}

void CConfigurationEditorDlg::SaveMtsRecoveryData()
{
		CString strSector, strKey, strTemp;
		strSector.Format(_T("MTS_DATA"));
		strKey.Format(_T("RotateDone"));
		strTemp.Format("%d", m_bMtsRotateDone_Flag);
		WritePrivateProfileString(strSector, strKey, strTemp, RECOVERY_FILE_FULLPATH);
}

int CConfigurationEditorDlg::EthernetRead()
{
	CString strKey, strSector;
	char chTmp[125];
	int nRet = 0;

	strSector.Format(_T("COMMUNICATION_CONFIG"));
	for (int i = 0; i < ETHERNET_COM_NUMBER; i++)
	{
		strKey.Format(_T("ETHERNET_NAME_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chNAME[i], chTmp);
		strKey.Format(_T("ETHERNET_IP_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chIP[i], chTmp);
		strKey.Format(_T("ETHERNET_PORT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		//m_nPORT[i] = atoi(chTmp);
		sprintf(m_chIPPORT[i], chTmp);
	}

	for (int nIdx = 0; nIdx < ETHERNET_COM_NUMBER; nIdx++)
	{
		GetDlgItem(IDC_STATIC_IP_NAME_0 + nIdx)->SetWindowText(m_chNAME[nIdx]);
		GetDlgItem(IDC_EDIT_CREVIS + nIdx)->SetWindowText(m_chIP[nIdx]);
		GetDlgItem(IDC_EDIT_CREVIS_PORT + nIdx)->SetWindowText(m_chIPPORT[nIdx]);
		GetDlgItem(IDC_STATIC_IP_NAME_0 + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_CREVIS + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_CREVIS_PORT + nIdx)->SetFont(&m_font);
	}

	return nRet;
}

int CConfigurationEditorDlg::EthernetSave()
{
	int nRet = 0, i = 0;
	char chTmp[125];
	CString strSector, strKey, str;

	strSector.Format(_T("COMMUNICATION_CONFIG"));

	for (i = 0; i < ETHERNET_COM_NUMBER; i++)
	{
		strKey.Format(_T("ETHERNET_NAME_%d"), i);
		GetDlgItem(IDC_STATIC_IP_NAME_0 + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		strKey.Format(_T("ETHERNET_IP_%d"), i);
		GetDlgItem(IDC_EDIT_CREVIS + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		strKey.Format(_T("ETHERNET_PORT_%d"), i);
		GetDlgItem(IDC_EDIT_CREVIS_PORT + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
	}
	
	return nRet;
}

int CConfigurationEditorDlg::SerialRead()
{
	CString strKey, strSector;
	char chTmp[125];
	int nRet = 0;

	strSector.Format(_T("COMMUNICATION_CONFIG"));
	for (int i = 0; i < SERIAL_COM_NUMBER; i++)
	{
		strKey.Format(_T("SERIAL_PORT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chPORT[i], chTmp);
		strKey.Format(_T("SERIAL_BAUD_RATE_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nBAUD_RATE[i] = atoi(chTmp);
		sprintf(m_ch_nBAUD_RATE[i], chTmp);
		strKey.Format(_T("SERIAL_USE_BIT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nUSE_BIT[i] = atoi(chTmp);
		sprintf(m_ch_nUSE_BIT[i], chTmp);
		strKey.Format(_T("SERIAL_STOP_BIT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nSTOP_BIT[i] = atoi(chTmp);
		sprintf(m_ch_nSTOP_BIT[i], chTmp);
		strKey.Format(_T("SERIAL_PARITY_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nPARITY[i] = atoi(chTmp);
		sprintf(m_ch_nPARITY[i], chTmp);
	}

	for (int nIdx = 0; nIdx < SERIAL_COM_NUMBER; nIdx++)
	{
		GetDlgItem(IDC_EDIT_ISOLATOR_PORT + nIdx)->SetWindowText(m_chPORT[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_BAUD_RATE + nIdx)->SetWindowText(m_ch_nBAUD_RATE[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_USE_BIT + nIdx)->SetWindowText(m_ch_nUSE_BIT[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_STOP_BIT + nIdx)->SetWindowText(m_ch_nSTOP_BIT[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_PARITY + nIdx)->SetWindowText(m_ch_nPARITY[nIdx]);

		GetDlgItem(IDC_EDIT_ISOLATOR_PORT + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_BAUD_RATE + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_USE_BIT + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_STOP_BIT + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_PARITY + nIdx)->SetFont(&m_font);
	}

	return nRet;
}

int CConfigurationEditorDlg::SerialSave()
{
	int nRet = 0, i = 0;
	char chTmp[125];
	CString strSector, strKey, str;

	strSector.Format(_T("COMMUNICATION_CONFIG"));
	for (int i = 0; i < SERIAL_COM_NUMBER; i++)
	{
		strKey.Format(_T("SERIAL_PORT_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_PORT + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }

		strKey.Format(_T("SERIAL_BAUD_RATE_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_BAUD_RATE + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }

		strKey.Format(_T("SERIAL_USE_BIT_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_USE_BIT + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }

		strKey.Format(_T("SERIAL_STOP_BIT_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_STOP_BIT + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }

		strKey.Format(_T("SERIAL_PARITY_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_PARITY + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
	}

	return nRet;

}


void CConfigurationEditorDlg::OnBnClickedButtonConfigCalLoad()
{
	g_pLog->Display(0, _T("CConfigurationEditorDlg::OnBnClickedButtonConfigCalLoad() 버튼 클릭!"));
	
	ReadCalibrationInfo();
}


void CConfigurationEditorDlg::OnBnClickedButtonConfigCalSave()
{
	g_pLog->Display(0, _T("CConfigurationEditorDlg::OnBnClickedButtonConfigCalSave() 버튼 클릭!"));
	
	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	if (pwdlg.m_strTxt != "1234")
	{
		::AfxMessageBox("Password가 일치 하지 않습니다.");
		return;
	}

	UpdateData(TRUE);

	SaveCalibrationInfo();
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalTx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalTy()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalGlobalOffsetx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalGlobalOffsety()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalLbOffsetx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalLbOffsety()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalLtOffsetx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalLtOffsety()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalRtOffsetx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalRtOffsety()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnBnClickedButtonSetAutoOffset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CConfigurationEditorDlg::OnBnClickedButtonSetLbOffset()
{
	g_pLog->Display(0, _T("CConfigurationEditorDlg::OnBnClickedButtonSetLbOffset() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	//if (IDYES != AfxMessageBox("Mask Offset 설정 작업을 시작 하시겠습니까?", MB_YESNO)) return;

	//if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	//{
	//	MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
	//	return ;
	//}

	//AfxMessageBox(" OM화면에서 Align Point를 Center에 두고 확인버튼 눌러주세요.");

	//double posX1 = 0.0, posY1 = 0.0;
	//posX1 = GetPosmm(STAGE_X_AXIS);
	//posY1 = GetPosmm(STAGE_Y_AXIS);

	// EUV로 이동

	//AfxMessageBox(" EUV 화면에서 Align Point를 Center에 두고 확인버튼 눌러주세요.");
	//double posX2 = 0.0, posY2 = 0.0;
	//posX2 = GetPosmm(STAGE_X_AXIS);
	//posY2 = GetPosmm(STAGE_Y_AXIS);

	//m_dLBOffsetX_mm = posX2 - posX1;
	//m_dLBOffsetY_mm = posY2 - posY1;
	UpdateData(FALSE);
}

void CConfigurationEditorDlg::OnBnClickedButtonSetLtOffset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CConfigurationEditorDlg::OnBnClickedButtonSetRtOffset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap2()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap1()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap3()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap4()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::ReadETCInfo()
{
	//Get data from config file
	CString strAppName, strKeyName;
	char chGetString[DATA_STRING_SIZE];

	strAppName.Format(_T("ETC_INFO"));
	strKeyName.Format(_T("UseMtsRotate"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_bUseRotate);
	strKeyName.Format(_T("MtsRotateAngle"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nRotateAngle);
	strKeyName.Format(_T("Auto_Source_Off_Time_sec"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nSourceAutoOffTime_min);
	strKeyName.Format(_T("EUVVacuumRate"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dEUVVacuumRate);

	strAppName.Format(_T("OM_INFO"));
	strKeyName.Format(_T("PixelResolutionX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dOM_PixelSizeX_um);
	strKeyName.Format(_T("PixelResolutionY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dOM_PixelSizeY_um);

	strAppName.Format(_T("OM_STAGE_AREA"));
	strKeyName.Format(_T("LeftBottomPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nOMStageAreaLB_X_mm);
	strKeyName.Format(_T("LeftBottomPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nOMStageAreaLB_Y_mm);
	strKeyName.Format(_T("RightTopPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nOMStageAreaRT_X_mm);
	strKeyName.Format(_T("RightTopPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nOMStageAreaRT_Y_mm);

	strAppName.Format(_T("EUV_STAGE_AREA"));
	strKeyName.Format(_T("LeftBottomPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nEUVStageAreaLB_X_mm);
	strKeyName.Format(_T("LeftBottomPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nEUVStageAreaLB_Y_mm);
	strKeyName.Format(_T("RightTopPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nEUVStageAreaRT_X_mm);
	strKeyName.Format(_T("RightTopPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nEUVStageAreaRT_Y_mm);

	m_rcOMStageAreaRect.SetRect(m_nOMStageAreaLB_X_mm, m_nOMStageAreaLB_Y_mm, m_nOMStageAreaRT_X_mm, m_nOMStageAreaRT_Y_mm);
	m_rcEUVStageAreaRect.SetRect(m_nEUVStageAreaLB_X_mm, m_nEUVStageAreaLB_Y_mm, m_nEUVStageAreaRT_X_mm, m_nEUVStageAreaRT_Y_mm);

	//Set value to controls
	CString sAngle;
	sAngle.Format("%d", m_nRotateAngle);
	int nIdx = m_MtsRotateAngleCtrl.FindStringExact(0, sAngle);
	m_MtsRotateAngleCtrl.SetCurSel(nIdx);
	m_UseMtsRotateCtrl.SetCheck(m_bUseRotate);

	UpdateData(FALSE);
}

void CConfigurationEditorDlg::SaveETCInfo()
{
	UpdateData(TRUE);

	//Get value from controls
	CString sAngle;
	int nIdx = m_MtsRotateAngleCtrl.GetCurSel();
	m_MtsRotateAngleCtrl.GetLBText(nIdx, sAngle);
	m_nRotateAngle = atoi(sAngle);
	m_bUseRotate = m_UseMtsRotateCtrl.GetCheck();

	//Save config data
	CString strAppName, strKeyName, strValue;
	strAppName.Format(_T("ETC_INFO"));
	strKeyName.Format(_T("UseMtsRotate"));
	strValue.Format("%d", m_bUseRotate);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("MtsRotateAngle"));
	strValue.Format("%d", m_nRotateAngle);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("Auto_Source_Off_Time_sec"));
	strValue.Format("%d", m_nSourceAutoOffTime_min);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strAppName.Format(_T("OM_INFO"));
	strKeyName.Format(_T("PixelResolutionX"));
	strValue.Format("%lf", m_dOM_PixelSizeX_um);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("PixelResolutionY"));
	strValue.Format("%lf", m_dOM_PixelSizeY_um);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strAppName.Format(_T("OM_STAGE_AREA"));
	strKeyName.Format(_T("LeftBottomPosX"));
	strValue.Format("%d", m_nOMStageAreaLB_X_mm);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("LeftBottomPosY"));
	strValue.Format("%d", m_nOMStageAreaLB_Y_mm);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("RightTopPosX"));
	strValue.Format("%d", m_nOMStageAreaRT_X_mm);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("RightTopPosY"));
	strValue.Format("%d", m_nOMStageAreaRT_Y_mm);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strAppName.Format(_T("EUV_STAGE_AREA"));
	strKeyName.Format(_T("LeftBottomPosX"));
	strValue.Format("%d", m_nEUVStageAreaLB_X_mm);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("LeftBottomPosY"));
	strValue.Format("%d", m_nEUVStageAreaLB_Y_mm);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("RightTopPosX"));
	strValue.Format("%d", m_nEUVStageAreaRT_X_mm);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("RightTopPosY"));
	strValue.Format("%d", m_nEUVStageAreaRT_Y_mm);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	m_rcOMStageAreaRect.SetRect(m_nOMStageAreaLB_X_mm, m_nOMStageAreaLB_Y_mm, m_nOMStageAreaRT_X_mm, m_nOMStageAreaRT_Y_mm);
	m_rcEUVStageAreaRect.SetRect(m_nEUVStageAreaLB_X_mm, m_nEUVStageAreaLB_Y_mm, m_nEUVStageAreaRT_X_mm, m_nEUVStageAreaRT_Y_mm);
}

void CConfigurationEditorDlg::OnBnClickedButtonConfigEtcLoad()
{
	ReadETCInfo();
}


void CConfigurationEditorDlg::OnBnClickedButtonConfigEtcSave()
{
	SaveETCInfo();
}

void CConfigurationEditorDlg::ReadBsCalibrationInfo()
{
	int nRet;
	char chTmp[125];
	CString strAppName, strKeyName;
	char chGetString[DATA_STRING_SIZE];
	strAppName.Format(_T("BS_CALIBRATION"));
	strKeyName.Format(_T("SelectedIndex"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nBsSelectedIndex);

	strKeyName.Format(_T("ExposureTime"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nBsExposureTime);

	strKeyName.Format(_T("BlankPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBsBlankPosX);
	strKeyName.Format(_T("BlankPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBsBlankPosY);

	strKeyName.Format(_T("CenterPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBsCenterPosX);
	strKeyName.Format(_T("CenterPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBsCenterPosY);

	strKeyName.Format(_T("BGD1_Intensity"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBGD1_Intensity);
	strKeyName.Format(_T("BGD1_StandardDeviation"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBGD1_Standardeviation);
	strKeyName.Format(_T("BGD2_Intensity"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBGD2_Intensity);
	strKeyName.Format(_T("BGD2_StandardDeviation"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBGD2_Standardeviation);	
	strKeyName.Format(_T("BGD3_Intensity"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBGD3_Intensity);
	strKeyName.Format(_T("BGD3_StandardDeviation"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBGD3_Standardeviation);

	strKeyName.Format(_T("WOD1_Intensity"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSWOD1_Intensity);
	strKeyName.Format(_T("WOD1_StandardDeviation"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSWOD1_Standardeviation);
	strKeyName.Format(_T("WOD2_Intensity"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSWOD2_Intensity);
	strKeyName.Format(_T("WOD2_StandardDeviation"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSWOD2_Standardeviation);
	strKeyName.Format(_T("WOD3_Intensity"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSWOD3_Intensity);
	strKeyName.Format(_T("WOD3_StandardDeviation"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSWOD3_Standardeviation);

	strKeyName.Format(_T("BSD1_Intensity"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBSD1_Intensity);
	strKeyName.Format(_T("BSD1_StandardDeviation"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBSD1_Standardeviation);
	strKeyName.Format(_T("BSD2_Intensity"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBSD2_Intensity);
	strKeyName.Format(_T("BSD2_StandardDeviation"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBSD2_Standardeviation);
	strKeyName.Format(_T("BSD3_Intensity"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBSD3_Intensity);
	strKeyName.Format(_T("BSD3_StandardDeviation"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dBSBSD3_Standardeviation);

	strKeyName.Format(_T("DataFile"));
	GetPrivateProfileString(strAppName, strKeyName, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
	sprintf(m_chBsData, chTmp);


}

void CConfigurationEditorDlg::SaveBsCalibrationInfo()
{
	//Save config data
	CString strAppName, strKeyName, strValue;
	strAppName.Format(_T("BS_CALIBRATION"));
	strKeyName.Format(_T("SelectedIndex"));
	strValue.Format("%d", m_nBsSelectedIndex);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("ExposureTime"));
	strValue.Format("%d", m_nBsExposureTime);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strKeyName.Format(_T("BGD1_Intensity"));
	strValue.Format("%0.6f", m_dBSBGD1_Intensity);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BGD1_StandardDeviation"));
	strValue.Format("%0.6f", m_dBSBGD1_Standardeviation);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BGD2_Intensity"));
	strValue.Format("%0.6f", m_dBSBGD2_Intensity);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BGD2_StandardDeviation"));
	strValue.Format("%0.6f", m_dBSBGD2_Standardeviation);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BGD3_Intensity"));
	strValue.Format("%0.6f", m_dBSBGD3_Intensity);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BGD3_StandardDeviation"));
	strValue.Format("%0.6f", m_dBSBGD3_Standardeviation);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strKeyName.Format(_T("WOD1_Intensity"));
	strValue.Format("%0.6f", m_dBSWOD1_Intensity);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("WOD1_StandardDeviation"));
	strValue.Format("%0.6f", m_dBSWOD1_Standardeviation);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("WOD2_Intensity"));
	strValue.Format("%0.6f", m_dBSWOD2_Intensity);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("WOD2_StandardDeviation"));
	strValue.Format("%0.6f", m_dBSWOD2_Standardeviation);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("WOD3_Intensity"));
	strValue.Format("%0.6f", m_dBSWOD3_Intensity);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("WOD3_StandardDeviation"));
	strValue.Format("%0.6f", m_dBSWOD3_Standardeviation);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strKeyName.Format(_T("BSD1_Intensity"));
	strValue.Format("%0.6f", m_dBSBSD1_Intensity);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BSD1_StandardDeviation"));
	strValue.Format("%0.6f", m_dBSBSD1_Standardeviation);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BSD2_Intensity"));
	strValue.Format("%0.6f", m_dBSBSD2_Intensity);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BSD2_StandardDeviation"));
	strValue.Format("%0.6f", m_dBSBSD2_Standardeviation);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BSD3_Intensity"));
	strValue.Format("%0.6f", m_dBSBSD3_Intensity);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("BSD3_StandardDeviation"));
	strValue.Format("%0.6f", m_dBSBSD3_Standardeviation);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
}


void CConfigurationEditorDlg::InitialICon()
{
	//((CStatic*)GetDlgItem(IDC_ICON_IO_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_ADAM_CON_STATE))->SetIcon(m_LedIcon[0]);
	////((CStatic*)GetDlgItem(IDC_ICON_AF_CON_STATE))->SetIcon(m_LedIcon[0]);
	////((CStatic*)GetDlgItem(IDC_ICON_ZP_CON_STATE))->SetIcon(m_LedIcon[0]);
	////((CStatic*)GetDlgItem(IDC_ICON_MIRROR_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_SOURCE_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_MTS_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_VMTR_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_NAVI_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_BEAM_CON_STATE))->SetIcon(m_LedIcon[0]);
	////((CStatic*)GetDlgItem(IDC_ICON_SCAN_CON_STATE))->SetIcon(m_LedIcon[0]);
	////((CStatic*)GetDlgItem(IDC_ICON_ISO_CON_STATE))->SetIcon(m_LedIcon[0]);
	for (size_t i = 0; i < ETHERNET_COM_NUMBER; i++)
	{
		((CStatic*)GetDlgItem(IDC_CONFIG_ICON_ETHERNET0 + i))->SetIcon(m_LedIcon[0]);
	}
	for (size_t i = 0; i <= SERIAL_COM_NUMBER; i++)
	{
		((CStatic*)GetDlgItem(IDC_CONFIG_ICON_SERIAL0 + i))->SetIcon(m_LedIcon[0]);
	}
	//((CStatic*)GetDlgItem(IDC_ICON_GAUGE_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_LIGHT_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_LLCTMP_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_FILTER_CON_STATE))->SetIcon(m_LedIcon[0]);
	////((CStatic*)GetDlgItem(IDC_ICON_AF2_CON_STATE))->SetIcon(m_LedIcon[0]);
	////((CStatic*)GetDlgItem(IDC_ICON_REVOL_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_MCTMP_CON_STATE))->SetIcon(m_LedIcon[0]);
	//((CStatic*)GetDlgItem(IDC_ICON_SPARE_CON_STATE))->SetIcon(m_LedIcon[0]);


}

void CConfigurationEditorDlg::PortCheck()
{
	if (g_pIO != NULL)
	{
		if (g_pIO->Is_CREVIS_Connected() == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_ETHERNET0))->SetIcon(m_LedIcon[1]);
			GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET0)->EnableWindow(FALSE);
			GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET0)->EnableWindow(TRUE);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_ETHERNET0))->SetIcon(m_LedIcon[2]);
			GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET0)->EnableWindow(TRUE);
			GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET0)->EnableWindow(FALSE);
		}
	}

	if (g_pAnimationGUI != NULL)
	{
		if (g_pAnimationGUI->m_bConnected == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_ETHERNET2))->SetIcon(m_LedIcon[1]);
			GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET2)->EnableWindow(FALSE);
			GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET2)->EnableWindow(TRUE);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_ETHERNET2))->SetIcon(m_LedIcon[2]);
			GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET2)->EnableWindow(TRUE);
			GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET2)->EnableWindow(FALSE);
		}
	}

	if (g_pEfem != NULL)
	{
		if (g_pEfem->Is_MTS_Connected() == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_ETHERNET6))->SetIcon(m_LedIcon[1]);
			GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET6)->EnableWindow(FALSE);
			GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET6)->EnableWindow(TRUE);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_ETHERNET6))->SetIcon(m_LedIcon[2]);
			GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET6)->EnableWindow(TRUE);
			GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET6)->EnableWindow(FALSE);
		}
	}

	if (g_pVacuumRobot != NULL)
	{
		if (g_pVacuumRobot->Is_VMTR_Connected() == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_ETHERNET7))->SetIcon(m_LedIcon[1]);
			GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET7)->EnableWindow(FALSE);
			GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET7)->EnableWindow(TRUE);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_ETHERNET7))->SetIcon(m_LedIcon[2]);
			GetDlgItem(IDC_CONFIG_BUTTON_CONNECT_ETHERNET7)->EnableWindow(TRUE);
			GetDlgItem(IDC_CONFIG_BUTTON_DISCONNECT_ETHERNET7)->EnableWindow(FALSE);
		}
	}

	if (g_pGauge_IO != NULL)
	{
		if (g_pGauge_IO->Is_GAUGE_Connected() == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_SERIAL1))->SetIcon(m_LedIcon[1]);
			GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL1)->EnableWindow(FALSE);
			GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL1)->EnableWindow(TRUE);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_SERIAL1))->SetIcon(m_LedIcon[2]);
			GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL1)->EnableWindow(TRUE);
			GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL1)->EnableWindow(FALSE);
		}
	}

	if (g_pLLCTmp_IO != NULL)
	{
		if (g_pLLCTmp_IO->Is_LLC_Tmp_Connected() == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_SERIAL3))->SetIcon(m_LedIcon[1]);
			GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL3)->EnableWindow(FALSE);
			GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL3)->EnableWindow(TRUE);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_SERIAL3))->SetIcon(m_LedIcon[2]);
			GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL3)->EnableWindow(TRUE);
			GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL3)->EnableWindow(FALSE);
		}
	}

	if (g_pMCTmp_IO != NULL)
	{
		if (g_pMCTmp_IO->Is_MC_Tmp_Connected() == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_SERIAL7))->SetIcon(m_LedIcon[1]);
			GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL7)->EnableWindow(FALSE);
			GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL7)->EnableWindow(TRUE);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_CONFIG_ICON_SERIAL7))->SetIcon(m_LedIcon[2]);
			GetDlgItem(IDC_CONFIG_BUTTON_OPEN_SERIAL7)->EnableWindow(TRUE);
			GetDlgItem(IDC_CONFIG_BUTTON_CLOSE_SERIAL7)->EnableWindow(FALSE);
		}
	}
}

void CConfigurationEditorDlg::ConnectPortviaDevice(int nModuleNumber)
{
	int nRet = -1;
	CString str;
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
		return;
	}
	switch (nModuleNumber)
	{
	case MODULE_ETHERNET_CREVIS:
		if (g_pIO != NULL)
		{
			if (!g_pIO->m_bCrevis_Open_Port)
			{
				nRet = g_pIO->OpenDevice(g_pConfig->m_chIP[ETHERNET_CREVIS]);
				if (nRet != 0)
				{
					nRet = -87001;
					g_pAlarm->SetAlarm((nRet));
				}
			}
			else
				AfxMessageBox(_T("이미 연결 되어 있습니다"));
		}
		else
		{
			AfxMessageBox(_T("g_pIO == NULL Error"));
		}
		break;

	case MODULE_ETHERNET_MONITORING:
		if (g_pAnimationGUI != NULL)
		{
			if (g_pAnimationGUI->m_bConnected == FALSE)
			{
				nRet = g_pAnimationGUI->OpenDevice();
				if (nRet != 0)
				{
					str.Format(_T("GUI Connection Fail Error : %d"), nRet);
					AfxMessageBox(str);
				}
			}
			else
			{
				AfxMessageBox(_T("이미 연결 되어 있습니다"));
			}
		}
		break;

	case MODULE_ETHERNET_MTS:
		if (g_pEfem != NULL)
		{
			if (!g_pEfem->Is_MTS_Connected())
			{
				nRet = g_pEfem->OpenDevice();
				if (nRet != 0)
				{
					str.Format(_T("MTS Connection Fail Error : %d"), nRet);
					AfxMessageBox(str);
				}
			}
			else
				AfxMessageBox(_T("이미 연결 되어 있습니다"));
		}
		break;
	case MODULE_ETHERNET_VMTR:
		if (g_pVacuumRobot != NULL)
		{
			if (g_pVacuumRobot->Is_VMTR_Connected() == FALSE)
			{
				nRet = g_pVacuumRobot->OpenDevice();
				if (nRet != 0)
				{
					str.Format(_T("VMTR Connection Fail Error : %d"), nRet);
					AfxMessageBox(str);
				}
			}
			else
			{
				AfxMessageBox(_T("이미 연결 되어 있습니다"));
			}
		}
		break;

	case MODULE_SERIAL_VACUUMGAUGE:
		if (g_pGauge_IO != NULL)
		{
			if (g_pGauge_IO->Is_GAUGE_Connected() == FALSE)
			{
				nRet = g_pGauge_IO->OpenDevice();
				if (nRet != 0)
				{
					str.Format(_T("Gauge IO Connection Fail Error : %d"), nRet);
					AfxMessageBox(str);
				}
			}
			else
			{
				AfxMessageBox(_T("이미 연결 되어 있습니다"));
			}
		}
		break;

	case MODULE_SERIAL_MC_TMP:
		if (g_pMCTmp_IO != NULL)
		{
			if (g_pMCTmp_IO->Is_MC_Tmp_Connected() == FALSE)
			{
				nRet = g_pMCTmp_IO->OpenDevice();
				if (nRet != 0)
				{
					str.Format(_T("Main Chamber TMP Connection Fail Error : %d"), nRet);
					AfxMessageBox(str);
				}
			}
			else
			{
				AfxMessageBox(_T("이미 연결 되어 있습니다"));
			}
		}
		break;
	case MODULE_SERIAL_LLC_TMP:
		if (g_pLLCTmp_IO != NULL)
		{
			if (g_pLLCTmp_IO->Is_LLC_Tmp_Connected() == FALSE)
			{
				nRet = g_pLLCTmp_IO->OpenDevice();
				if (nRet != 0)
				{
					str.Format(_T("Loadlock Chamber TMP Connection Fail Error : %d"), nRet);
					AfxMessageBox(str);
				}
			}
			else
			{
				AfxMessageBox(_T("이미 연결 되어 있습니다"));
			}
		}
		break;
	}
}

void CConfigurationEditorDlg::DisconnectPortviaDevice(int nModuleNumber)
{
	int nRet = -1;
	CString str;
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
		return;
	}
	switch (nModuleNumber)
	{
	case MODULE_ETHERNET_CREVIS:
		if (g_pIO != NULL)
		{
			if (g_pIO->Is_CREVIS_Connected() == TRUE)
			{
				g_pIO->Closedevice_IO();
			}
			else
			{
				AfxMessageBox(_T("연결이 해제 되어 있습니다"));
			}
		}
		break;
	case MODULE_ETHERNET_MONITORING:
		if (g_pAnimationGUI != NULL)
		{
			if (g_pAnimationGUI->m_bConnected == TRUE)
			{
				g_pAnimationGUI->CloseDevice();
			}
			else
			{
				AfxMessageBox(_T("연결이 해제 되어 있습니다"));
			}
		}
		break;

	case MODULE_ETHERNET_MTS:
		if (g_pEfem != NULL)
		{
			if (g_pEfem->Is_MTS_Connected() == TRUE)
			{
				g_pEfem->CloseDevice();
			}
			else
			{
				AfxMessageBox(_T("연결이 해제 되어 있습니다"));
			}
		}
		break;
	case MODULE_ETHERNET_VMTR:
		if (g_pVacuumRobot != NULL)
		{
			if (g_pVacuumRobot->Is_VMTR_Connected() == TRUE)
			{
				g_pVacuumRobot->CloseDevice();
			}
			else
			{
				AfxMessageBox(_T("연결이 해제 되어 있습니다"));
			}
		}
		break;


	case MODULE_SERIAL_VACUUMGAUGE:
		if (g_pGauge_IO != NULL)
		{
			if (g_pGauge_IO->Is_GAUGE_Connected() == TRUE)
			{
				g_pGauge_IO->CloseDevice();
			}
			else
			{
				AfxMessageBox(_T("연결이 해제 되어 있습니다"));
			}
		}
		break;

	case MODULE_SERIAL_MC_TMP:
		if (g_pMCTmp_IO != NULL)
		{
			if (g_pMCTmp_IO->Is_MC_Tmp_Connected() == TRUE)
			{
				g_pMCTmp_IO->CloseDevice();
			}
			else
			{
				AfxMessageBox(_T("연결이 해제 되어 있습니다"));
			}
		}
		break;
	case MODULE_SERIAL_LLC_TMP:
		if (g_pLLCTmp_IO != NULL)
		{
			if (g_pLLCTmp_IO->Is_LLC_Tmp_Connected() == TRUE)
			{
				g_pLLCTmp_IO->CloseDevice();
			}
			else
			{
				AfxMessageBox(_T("연결이 해제 되어 있습니다"));
			}
		}
		break;
	}
}

void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet0()
{
	ConnectPortviaDevice(MODULE_ETHERNET_CREVIS);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet1()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet2()
{
	ConnectPortviaDevice(MODULE_ETHERNET_MONITORING);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet3()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet4()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet5()
{
	
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet6()
{
	ConnectPortviaDevice(MODULE_ETHERNET_MTS);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet7()
{
	ConnectPortviaDevice(MODULE_ETHERNET_VMTR);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet8()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet9()
{
	
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet10()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonConnectEthernet11()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet0()
{
	DisconnectPortviaDevice(MODULE_ETHERNET_CREVIS);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet1()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet2()
{
	DisconnectPortviaDevice(MODULE_ETHERNET_MONITORING);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet3()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet4()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet5()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet6()
{
	DisconnectPortviaDevice(MODULE_ETHERNET_MTS);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet7()
{
	DisconnectPortviaDevice(MODULE_ETHERNET_VMTR);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet8()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet9()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet10()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonDisconnectEthernet11()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial0()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial1()
{
	ConnectPortviaDevice(MODULE_SERIAL_VACUUMGAUGE);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial2()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial3()
{
	ConnectPortviaDevice(MODULE_SERIAL_LLC_TMP);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial4()
{
	
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial5()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial6()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial7()
{
	ConnectPortviaDevice(MODULE_SERIAL_MC_TMP);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonOpenSerial8()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial0()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial1()
{
	DisconnectPortviaDevice(MODULE_SERIAL_VACUUMGAUGE);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial2()
{

}


void CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial3()
{
	DisconnectPortviaDevice(MODULE_SERIAL_LLC_TMP);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial4()
{
	
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial5()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial6()
{
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial7()
{
	DisconnectPortviaDevice(MODULE_SERIAL_MC_TMP);
}


void CConfigurationEditorDlg::OnBnClickedConfigButtonCloseSerial8()
{
}
