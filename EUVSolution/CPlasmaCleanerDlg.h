﻿#pragma once

#include "ChartViewer.h"
// CPlasmaCleanerDlg 대화 상자
#define	PLASMASAMPLESIZE 20000

class CPlasmaCleanerDlg : public CDialogEx, public CEthernetCom, public IObserver
{
	DECLARE_DYNAMIC(CPlasmaCleanerDlg)

public:
	CPlasmaCleanerDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CPlasmaCleanerDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PLASMA_CLEANER_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	HICON m_LedIcon[3];
	DECLARE_MESSAGE_MAP()

public:

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnViewPortChanged();

	afx_msg void OnBnClickedPlasmaButtonShow();
	afx_msg void OnBnClickedPlasmaButtonRun();
	afx_msg void OnBnClickedPlasmaButtonTimerrun();
	afx_msg void OnBnClickedPlasmaButtonOpen();
	afx_msg void OnBnClickedPlasmaButtonClose();
	afx_msg void OnBnClickedPlasmaButtonCancel();
	afx_msg void OnClickedPlasmaCheck();

	afx_msg void OnDeltaposPlasmaSpinBookhour(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPlasmaSpinBookminute(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPlasmaSpinSethour(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPlasmaSpinSetminute(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPlasmaSpinSetsecond(NMHDR *pNMHDR, LRESULT *pResult);

	BOOL m_bBISSStatus;
	BOOL m_bisInterlockFail;
	BOOL m_bExitFlagConnectBISSThread;
	BOOL m_bExitFlagInterlockCheckDuringCleanning;

	CString m_strActualWatt;
	CString m_strActualWattR;
	CString m_strActualHour;
	CString m_strActualMinute;
	CString m_strActualSecond;
	CString m_strActualTorr;
	CString m_strActualVDC;
	CString m_strBookHour;
	CString m_strBookMinute;

	int m_nActualWatt;
	int m_nActualWattR;
	int m_nActualHour;
	int m_nActualMinute;
	int m_nActualSecond;
	double m_dActualTorr;
	double m_dActualVDC;
	int m_nBookHour;
	int m_nBookMinute;

	HANDLE m_hSetWattEvent;
	HANDLE m_hSetHourEvent;
	HANDLE m_hSetMinuteEvent;
	HANDLE m_hGetSetParameterEvent;
	HANDLE m_hGetActualParameterEvent;
	HANDLE m_hPowerOnEvent;
	HANDLE m_hPowerOffEvent;

	void runPlasmacleaning();
	//void runPlasmacleaning(int nWatt = 50, int nSetHour = 3, int nSetMinute = 0, BOOL isTimer = FALSE, int nBookHour = 0, int nBookMinute = 0);

	int m_nPlasmaCleaningStatus;
	int m_nPlasmaCleaningOldStatus;

	BOOL Display(CString strLogMsg = _T(""), BOOL bDisplay = TRUE, BOOL bSaveFile = TRUE);

	void setTimerWindowState(BOOL state);
	void updateui();
	void setControlUI(bool status);
	void getSetParameter();
	int checkSetParameter();

	BOOL CheckInterlock();

	BOOL m_bisPressurebeforeStartError;
	BOOL m_bisLoadingPositionError;
	BOOL m_bisMaterialPositionError;
	BOOL m_bisEUVStatusError;
	BOOL m_bisGagueStatusError;
	BOOL m_bisMainChamberError;
	BOOL m_bisLoadlockChamberError;

	BOOL CheckPressureBeforeCleaningStart(BOOL isDisplay = FALSE);
	BOOL CheckLoadingPosition(BOOL isDisplay = FALSE);
	BOOL CheckMaterialPosition(BOOL isDisplay = FALSE);
	BOOL CheckEUVStatus(BOOL isDisplay = FALSE);
	BOOL CheckGagueStatus(BOOL isDisplay = FALSE);
	BOOL CheckMainChamberStatus(BOOL isDisplay = FALSE);
	BOOL CheckLoadlockChamberStatus(BOOL isDisplay = FALSE);

	void drawChart(CChartViewer *viewer);
	CChartViewer m_ChartViewer;
	XYChart *Multiline_chart;

	int m_extBgColor;
	int getDefaultBgColor();
	bool Clear_state;

	double m_nextDataTime;
	int sampleSize;
	int DataInterval;
	int initialFullRange;

	double m_timeStamps[PLASMASAMPLESIZE];	// The timestamps for the data series
	double m_dataSeriesA[PLASMASAMPLESIZE];	// The values for the data series A
	double m_dataSeriesB[PLASMASAMPLESIZE];	// The values for the data series B

	void DrawPressureChart();
	void StopDrawChart();
	void ResetChartData();

	void getChartData();
	int m_currentIndex;
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	double moveScrollBar(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	void updateControls(CChartViewer *viewer);
	void OnChartUpdateTimer();

	CScrollBar m_chartScroll;

	//TCP
	virtual int ReceiveData(char* lParam);
	virtual int SendData(char *lParam, int nTimeOut = 60000, int nRetryCnt = 3);
	virtual int WaitReceiveEventThread();

	virtual int connectBISSviaEtherNet();
	CWinThread* m_pThreadConnectBISS;
	static	UINT __cdecl	ConnectBISSThreadFunc(LPVOID pClass) { ((CPlasmaCleanerDlg *)pClass)->connectBISSviaEtherNet(); return 0; }

	virtual int checkStatusduringCleanning();
	CWinThread* m_pThreadCheckStatusduringCleanning;
	static UINT __cdecl CheckStatusThreadFunc(LPVOID pClass) { ((CPlasmaCleanerDlg *)pClass)->checkStatusduringCleanning(); return 0; }

	int sendMessageGetActualParameter();
	int sendMessageGetSetParameter();
	int sendMessageGetTimerParameter();
	int sendMessageSetSetParameter();
	int sendMessageSetTimerParameter();
	int sendMessageRun();
	int sendMessageStop();
	int sendMessageCheck();
	int sendMessageUIStatus(BOOL status);

	BOOL checkTimer();

	void EmergencyStop();
};
