#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

#ifdef _DEBUG
#define _DEBUG_WAS_DEFINED 1
#undef _DEBUG
#endif

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy\arrayobject.h>
#include <numpy\npy_3kcompat.h>

#ifdef _DEBUG_WAS_DEFINED
#define _DEBUG
#endif


#ifdef _DEBUG
#define new DEBUG_NEW


#endif


// CEUVPTRDlg 대화 상자










CEUVSOLUTIONDlg::CEUVSOLUTIONDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_EUVSOLUTION_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CEUVSOLUTIONDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EUVSOL_BEVEL1, m_EuvsolBevel1Ctrl);
	DDX_Control(pDX, IDC_EUVSOL_BEVEL2, m_EuvsolBevel2Ctrl);
	DDX_Control(pDX, IDC_CLOCKFRAME, m_Clock);
	DDX_Control(pDX, IDC_MAIN_EUV_WARMUP_TIME, m_EuvWarmupCtrl);
}

BEGIN_MESSAGE_MAP(CEUVSOLUTIONDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_OPERATOR_BUTTON, &CEUVSOLUTIONDlg::OnBnClickedOperatorButton)
	ON_BN_CLICKED(IDC_ENGINEER_BUTTON, &CEUVSOLUTIONDlg::OnBnClickedEngineerButton)
	ON_BN_CLICKED(IDC_EXIT_BUTTON, &CEUVSOLUTIONDlg::OnBnClickedExitButton)
	ON_BN_CLICKED(IDC_CONFIGURATION_BUTTON, &CEUVSOLUTIONDlg::OnBnClickedConfigurationButton)
	ON_BN_CLICKED(IDC_EVALUATION_BUTTON, &CEUVSOLUTIONDlg::OnBnClickedEvaluationButton)
	ON_WM_TIMER()

	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()


	ON_BN_CLICKED(IDC_BUTTON_SHOW_UI, &CEUVSOLUTIONDlg::OnBnClickedButtonShowUi)
	ON_BN_CLICKED(IDC_BUTTON_HIDE_UI, &CEUVSOLUTIONDlg::OnBnClickedButtonHideUi)
	ON_BN_CLICKED(IDC_BUTTON_ALARM, &CEUVSOLUTIONDlg::OnBnClickedButtonAlarm)
END_MESSAGE_MAP()


// CEUVPTRDlg 메시지 처리기


BOOL CEUVSOLUTIONDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는





	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	int nRet = 0;
	int nX = 0, nY = 0;

	InitializeControls();

	//ShowWindow(SW_MAXIMIZE);
	//MIL ALLOC 초기화





	MappAlloc(M_DEFAULT, &g_milApplication);
	MsysAlloc(M_SYSTEM_HOST, M_DEF_SYSTEM_NUM, M_COMPLETE, &g_milSystemHost);
	MsysAlloc(M_SYSTEM_DEFAULT, M_DEF_SYSTEM_NUM, M_COMPLETE, &g_milSystemGigEVision);

	// Python 초기화




	int iSInitialized = Py_IsInitialized();
	if (iSInitialized == 0)
	{
		Py_Initialize();
		//import_array();
	}
	// Create Modules
	nRet = CreateModules();

	if (g_pEfem->Is_POD_OnLPM() == TRUE)
	{
		if (g_pEfem->Is_POD_Open() == TRUE)
		{
			SetDlgItemText(IDC_POD_STATUS, _T("ON LPM(OPEN)"));
			((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			SetDlgItemText(IDC_POD_STATUS, _T("ON LPM(CLOSE)"));
			((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[0]);
		}
	}
	else
		SetDlgItemText(IDC_POD_STATUS, _T("NOT EXISTS"));


	SetForegroundWindow();

	InitClock();

	SetTimer(MAIN_DIALOG_INFO_TIMER, 100, NULL);

	if (g_pLog != NULL)
		g_pLog->Display(0, _T("EUV Solution Start!"));


	/* 바탕화면 작업표시줄 표기를 위한 UI 수정 2020.11.30 kjh */

	///////////////////////////////////////////////////////////////////////
	//ShowWindow(SW_SHOWMAXIMIZED);

	LONG style = ::GetWindowLong(m_hWnd, GWL_STYLE);

	style &= ~WS_CAPTION;
	style &= ~WS_SYSMENU;

	::SetWindowLong(m_hWnd, GWL_STYLE, style);
	int screenx = GetSystemMetrics(SM_CXSCREEN);
	int screeny = GetSystemMetrics(SM_CYSCREEN);

	SetWindowPos(NULL, -10, -10, screenx + 30, screeny - 30, SWP_NOZORDER);
	/////////////////////////////////////////////////////////////////////


	// 1.H/W Module 초기화 수행










	// 2.System Monotoring 수행 시작(Timer?, Thread?)
	//chlee
	SetTimer(MARS_LOG_FILE_CREATE_TIMER, 1000, NULL);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CEUVSOLUTIONDlg::InitializeControls()
{
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_ON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_OFF))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_SHUTTER_OPEN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_SHUTTER_CLOSE))->SetIcon(m_LedIcon[2]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_LOADINGPOS))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_EQ_STATUS))->SetIcon(m_LedIcon[1]);
}

void CEUVSOLUTIONDlg::GetEquipmentInfo()
{
	CString sTemp;

	// MKS 390 Gauge 
	if (g_pGauge_IO->Is_GAUGE_Connected() == TRUE)
	{
		sTemp.Format(_T("%.2e Torr"), g_pGauge_IO->GetLlcVacuumRate());
		SetDlgItemText(IDC_LLC_VACCUM_VALUE, sTemp);
		sTemp.Format(_T("%.2e Torr"), g_pGauge_IO->GetMcVacuumRate());
		SetDlgItemText(IDC_MC_VACCUM_VALUE, sTemp);
	}
	else
	{
		SetDlgItemText(IDC_LLC_VACCUM_VALUE, "연결 확인 필요");
		SetDlgItemText(IDC_MC_VACCUM_VALUE, "연결 확인 필요");
	}

	if (g_pEfem->Is_POD_OnLPM() == TRUE)
	{
		if (g_pEfem->Is_POD_Open() == TRUE)
			sTemp = _T("ON LPM(OPEN)");
		else
			sTemp = _T("ON LPM(CLOSE)");
	}
	else
		sTemp = _T("NOT EXISTS");

	SetDlgItemText(IDC_POD_STATUS, sTemp);

	if (g_pEfem->Is_POD_Open() == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[0]);

	int nLoc = g_pConfig->m_nMaterialLocation;

	switch (nLoc)
	{
	case MTS_POD:			sTemp = _T("MTS POD");		break;
	case MTS_ROBOT_HAND:	sTemp = _T("MTS ROBOT");	break;
	case MTS_ROTATOR:		sTemp = _T("MTS ROTATOR");	break;
	case LLC:				sTemp = _T("LLC");			break;
	case VACUUM_ROBOT_HAND: sTemp = _T("VAC ROBOT");	break;
	case CHUCK:				sTemp = _T("CHUCK");		break;
	default:				sTemp = _T("-");			break;
	}

	SetDlgItemText(IDC_MAIN_MASK_LOCATION, sTemp);

}

void CEUVSOLUTIONDlg::SetEquipmentStatus(CString sTemp)
{
	CString sMsg = sTemp + _T(" NG");
	SetDlgItemText(IDC_MAIN_EQ_STATUS, sMsg);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_EQ_STATUS))->SetIcon(m_LedIcon[2]);
}

void CEUVSOLUTIONDlg::InitClock()
{
	//	m_Clock.Start(IDB_CLOCKST_PANE, IDB_CLOCKST_BIG, IDB_CLOCKST_SMALL,true);
	m_Clock.SetOn(FALSE);
	m_Clock.SetTextOffColor(RGB(0, 255, 0));
	m_Clock.SetBackgroundOffColor(RGB(0, 0, 50));
	m_Clock.SetBold();
	m_Clock.SetPointFont(15, "Arial");
	m_Clock.SetModalFrame();
	m_Clock.Start();
}
// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면


//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는


//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CEUVSOLUTIONDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		DisplayVersion();
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서


//  이 함수를 호출합니다.
HCURSOR CEUVSOLUTIONDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CEUVSOLUTIONDlg::OnBnClickedExitButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedExitButton() 버튼 클릭!"));


	CDialogEx::OnOK();
}

void CEUVSOLUTIONDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(OUTPUT_LINE_PART_DIALOG_TIMER);
	KillTimer(INPUT_LINE_PART_DIALOG_TIMER);
	KillTimer(OUTPUT_PART_DIALOG_TIMER);
	KillTimer(INPUT_PART_DIALOG_TIMER);
	KillTimer(MAIN_DIALOG_INFO_TIMER);
	KillTimer(MAP_UPDATE_TIMER);
	KillTimer(OMIMAGE_DISPLAY_TIMER);
	KillTimer(ADAMDATA_DISPLAY_TIMER);
	KillTimer(VMTRROBOT_UPDATE_TIMER);
	KillTimer(MTS_UPDATE_TIMER);
	KillTimer(AFM_UPDATE_TIMER);
	KillTimer(LLC_TMP_UPDATE_TIMER);
	KillTimer(MC_TMP_UPDATE_TIMER);
	KillTimer(GAUGE_UPDATE_TIMER);
	KillTimer(IO_UPDATE_TIMER);
	KillTimer(REVOLVER_UPDATE_TIMER);
	KillTimer(EUV_SOURCE_UPDATE_TIMER);
	KillTimer(NAVISTAGE_UPDATE_TIMER);
	KillTimer(SCANSTAGE_UPDATE_TIMER);
	KillTimer(MASK_FLATNESS_MEASUREMENT_TIMER);
	KillTimer(STAGE_MEASUREMENT_TIMER);
	KillTimer(SOURCE_MEASUREMENT_TIMER);
	KillTimer(FILTERSTAGE_UPDATE_TIMER);
	KillTimer(PI_STAGE_MEASUREMENT_TIMER);
	KillTimer(VACCUM_GAUGE_WRITE_TIMER);
	KillTimer(INPUT_DIALOG_TIMER);
	KillTimer(OUTPUT_DIALOG_TIMER);
	KillTimer(XRAY_CAMERA_UPDATE_TIMER);
	KillTimer(MC_PUMP_SEQUENCE_CHECK_TIMER);
	KillTimer(LLC_PUMP_SEQUENCE_CHECK_TIMER);
	KillTimer(MC_VENT_SEQUENCE_CHECK_TIMER);
	KillTimer(LLC_VENT_SEQUENCE_CHECK_TIMER);
	//KillTimer(REFERENCE_POS_CHECK_TIMER);
	KillTimer(XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING);
	KillTimer(ADAMDATA_CONNECTION_TIMER);
	KillTimer(PORT_CHECK_TIMER);
	KillTimer(NAVISTAGE_LOG_WRITE_TIMER);
	KillTimer(MARS_LOG_FILE_CREATE_TIMER);

	WaitSec(1);


	if (g_pAnimationGUI != NULL)	DELETECLASS(g_pAnimationGUI);
	//	if (g_pXrayCamera != NULL)	DELETEDIALOG(g_pXrayCamera);
		//if (g_pPhase != NULL)	DELETEDIALOG(g_pPhase);
		//if (g_pXrayCameraConfig != NULL)	DELETEDIALOG(g_pXrayCameraConfig);
	if (g_pMaindialog != NULL)	DELETEDIALOG(g_pMaindialog);
	if (g_pIO != NULL)	DELETEDIALOG(g_pIO);
	if (g_pEfem != NULL)	DELETEDIALOG(g_pEfem);
	if (g_pMCTmp_IO != NULL)	DELETEDIALOG(g_pMCTmp_IO);
	if (g_pLLCTmp_IO != NULL)	DELETEDIALOG(g_pLLCTmp_IO);
	if (g_pGauge_IO != NULL)	DELETEDIALOG(g_pGauge_IO);
	if (g_pVacuumRobot != NULL)	DELETEDIALOG(g_pVacuumRobot);
	if (g_pConfig != NULL)	DELETEDIALOG(g_pConfig);
	if (g_pCommStat != NULL)	DELETEDIALOG(g_pCommStat);
	if (g_pWarning != NULL)	DELETEDIALOG(g_pWarning);
	if (g_pSubMenu != NULL)	DELETEDIALOG(g_pSubMenu);
	if (g_pAlarm != NULL)	DELETEDIALOG(g_pAlarm);
	if (g_pAP != NULL)	DELETECLASS(g_pAP);
	if (g_pVP != NULL)	DELETECLASS(g_pVP);
	if (g_pSubTestMenu != NULL)	DELETEDIALOG(g_pSubTestMenu);
	if (g_pPlasmaCleaner != NULL) DELETEDIALOG(g_pPlasmaCleaner);
	if (g_pSubModule != NULL) DELETEDIALOG(g_pSubModule);
	if (g_pDevMgr != NULL) DELETECLASS(g_pDevMgr);

	if (g_pLog != NULL)
		g_pLog->Display(0, _T("EUV Solution END!"));
	DELETEDIALOG(g_pLog);


	//MIL Free
	if (g_milSystemGigEVision != M_NULL)
	{
		MsysFree(g_milSystemGigEVision);
		g_milSystemGigEVision = M_NULL;
	}

	if (g_milSystemHost != M_NULL)
	{
		MsysFree(g_milSystemHost);
		g_milSystemHost = M_NULL;
	}

	if (g_milApplication != M_NULL)
	{
		MappFree(g_milApplication);
		g_milApplication = M_NULL;
	}

	int iSInitialized = Py_IsInitialized();
	if (iSInitialized != 0)
	{
		Py_Finalize();
	}
}

int CEUVSOLUTIONDlg::CreateModules()
{
	int nRet = 0;
	CRect rROI;
	rROI.SetRectEmpty();

	GetDlgItem(IDC_REFERENCE)->GetWindowRect(&rROI);
	ScreenToClient(&rROI);

	CREATEDIALOG(g_pLog, CLogDisplayDlg, this, CPoint(rROI.left + 2140, rROI.top + 1600), SW_HIDE);
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("LogDisplay Creation Success!"));
	else
		g_pLog->Display(0, _T("LogDisplay Creation Fail!"));

	CREATEDIALOG(g_pConfig, CConfigurationEditorDlg, this, CPoint(rROI.left + 400, rROI.top - 20), SW_HIDE);
	if (g_pConfig != NULL)
	{
		//g_pConfig->ReadFile();
		g_pLog->Display(0, _T("ConfigurationEditor Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("ConfigurationEditor Creation Fail!"));
		return nRet;
	}

	CRect rect;
	GetDlgItem(IDC_STATIC_ESOL_LOGO)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	CString sTitle;

	sTitle = _T("PTR Loading ....");
	m_hLogo = (HBITMAP)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_ESOL_EUVPTR_LOGO)/*"ESOL_Logo_EUVPTR.bmp"*/, IMAGE_BITMAP, rect.Width(), rect.Height(), LR_DEFAULTCOLOR /*LR_LOADFROMFILE | LR_CREATEDIBSECTION*/);

	((CStatic*)GetDlgItem(IDC_STATIC_ESOL_LOGO))->SetBitmap(m_hLogo);

	g_pLoadingScreen.Create(IDD_LOADING_SCREEN_DIALOG, this);
	((CStatic*)g_pLoadingScreen.GetDlgItem(IDC_SPLASH))->SetBitmap(m_hLogo);
	g_pLoadingScreen.ShowWindow(SW_SHOW);
	g_pLoadingScreen.SetTitleMessage(sTitle);
	g_pLoadingScreen.SetTextMessage(_T("Create Modules !!"));
	WaitSec(2);

	g_pWarning = new CWarningDlg(this);
	CRect rt;
	g_pWarning->GetWindowRect(&rt);
	rt.OffsetRect(CPoint(rROI.left + 2300, rROI.top + 50));
	g_pWarning->MoveWindow(rt);
	if (g_pWarning != NULL)
	{
		g_pLog->Display(0, _T("Warning Dialog Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("Warning Dialog Creation Fail!"));
		return nRet;
	}


	g_pAlarm = new CAlarmDlg(this);
	if (g_pAlarm != NULL)
	{
		g_pLog->Display(0, _T("Alarm Dialog Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("Alarm Dialog Creation Fail!"));
		return nRet;
	}



	CREATEDIALOG(g_pSubMenu, CSubMenuDlg, this, CPoint(rROI.left + 430, rROI.top + 75), SW_HIDE);
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("SubMenu Creation Success!"));
	else
		g_pLog->Display(0, _T("SubMenu Creation Fail!"));

	CREATEDIALOG(g_pSubTestMenu, CSubTestMenuDlg, this, CPoint(rROI.left + 430, rROI.top + 75), SW_HIDE);
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("SubMenu Creation Success!"));
	else
		g_pLog->Display(0, _T("SubMenu Creation Fail!"));



	CREATEDIALOG(g_pCommStat, CHWCommStatusDlg, this, CPoint(rROI.left + 500, rROI.top + 200), SW_HIDE);
	if (g_pCommStat != NULL)
	{
		g_pLog->Display(0, _T("HW Communication Status Display Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("HW Communication Status Display Creation Fail!"));
		return nRet;
	}


	//CREATECLASS(g_pBeamCon, CBeamConnect);
	//if (g_pBeamCon != NULL)
	//	g_pLog->Display(0, _T("Beam Optimization Connect Creation Success!"));
	//else
	//	g_pLog->Display(0, _T("Beam Optimization Connect Creation Fail!"));

	CREATECLASS(g_pDevMgr, CDeviceManager);

	//////////////////////////////////////////////////////// HW 통신 모듈 Create! ///////////////////////////////////////////////
	CREATECLASS(g_pAnimationGUI, CAnimationGUI);
	if (g_pAnimationGUI != NULL)
	{
		g_pLog->Display(0, _T("Animation GUI Creation Success!"));
		nRet = g_pAnimationGUI->OpenDevice();
		if (nRet == 0)
		{
		}
	}
	else
		g_pLog->Display(0, _T("Animation GUI Creation Fail!"));

	//CREATEDIALOG(g_pAdam, CADAMDlg, this, CPoint(rROI.left + 2140, rROI.top + 10), SW_HIDE);	 // ADAM DLG 위치가 이상해서 2250에서 1250으로 수정함_KYD_20191024
	//if (g_pAdam != NULL)
	//{
	//	g_pLog->Display(0, _T("ADAM Creation Success!"));

	//	if (g_pConfig->m_nEquipmentMode != OFFLINE)
	//	{

	//		nRet = g_pAdam->OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_ADAM], g_pConfig->m_nPORT[ETHERNET_ADAM], FALSE, ADAM_RECEIVE_BUFFER_SIZE);
	//		
	//		if (nRet == 0)
	//		{
	//			WaitSec(0.5);
	//			g_pAdam->Command_SetAverageCount(g_pAdam->m_AverageCount);
	//		}

	//		if (nRet != 0)
	//		{
	//			g_pCommStat->ADAMCommStatus(FALSE);
	//			SetEquipmentStatus(_T("ADAM"));
	//			WaitSec(3);
	//		}
	//		else
	//		{
	//			g_pCommStat->ADAMCommStatus(TRUE);
	//		}
	//	}
	//}
	//else
	//{
	//	nRet = -1;
	//	g_pLog->Display(0, _T("ADAM Creation Fail!"));
	//	return nRet;
	//}

	CREATEDIALOG(g_pGauge_IO, CVacuumGaugeDlg, this, CPoint(rROI.left + 1550, rROI.top + 200), SW_HIDE);
	if (g_pGauge_IO != NULL)
	{
		g_pLog->Display(0, _T("Vacuum Gauge Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pGauge_IO->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->LLCGaugeCommStatus(FALSE);
				SetEquipmentStatus(_T("LLC GAUGE"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->LLCGaugeCommStatus(TRUE);
			}
		}
	}
	else
	{
		g_pLog->Display(0, _T("LLC Vacuum Gauge Creation Fail!"));
	}

	CREATEDIALOG(g_pIO, CIODlg, this, CPoint(rROI.left + 500, rROI.top + 200), SW_HIDE);
	if (g_pIO != NULL)
	{
		g_pLog->Display(0, _T("IO Module Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pIO->OpenDevice(g_pConfig->m_chIP[ETHERNET_CREVIS]);
			if (nRet != 0)
			{
				g_pCommStat->CrevisCommStatus(FALSE);
				SetEquipmentStatus(_T("CREVIS"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->CrevisCommStatus(TRUE);
			}
		}
	}
	else
	{
		g_pLog->Display(0, _T("IO Module Creation Fail!"));
	}

	CREATEDIALOG(g_pEfem, CMaskEFEMDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pEfem != NULL)
	{
		g_pLog->Display(0, _T("Mask Transfer System Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pEfem->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->EfemCommStatus(FALSE);
				SetEquipmentStatus(_T("MTS"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->EfemCommStatus(TRUE);
			}
		}
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("Mask Transfer System Creation Fail!"));
		return nRet;
	}

	CREATEDIALOG(g_pVacuumRobot, CVacuumRobotDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pVacuumRobot != NULL)
	{
		g_pLog->Display(0, _T("Vacuum Robot Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pVacuumRobot->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->VacuumRobotCommStatus(FALSE);
				SetEquipmentStatus(_T("VMTR"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->VacuumRobotCommStatus(TRUE);
			}
		}
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("Vacuum Robot Creation Fail!"));
		return nRet;
	}

	CREATECLASS(g_pVP, CVacuumProcess);
	if (g_pVP != NULL)
		g_pLog->Display(0, _T("Vacuum Process Creation Success!"));
	else
		g_pLog->Display(0, _T("Vacuum Process Creation Fail!"));

	CREATECLASS(g_pAP, CAutoProcess);
	if (g_pAP != NULL)
	{
		g_pAP->Initialize();
		g_pLog->Display(0, _T("Auto Process Creation Success!"));
	}
	else
		g_pLog->Display(0, _T("Auto Process Creation Fail!"));

	CREATEDIALOG(g_pMCTmp_IO, CTurboPumpMCDlg, this, CPoint(rROI.left + 450, rROI.top + 200), SW_HIDE);
	if (g_pMCTmp_IO != NULL)
	{
		g_pLog->Display(0, _T("MC Turbo Pump Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pMCTmp_IO->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->MCTmpCommStatus(FALSE);
				SetEquipmentStatus(_T("MC TMP"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->MCTmpCommStatus(TRUE);
			}

		}
	}
	else
		g_pLog->Display(0, _T("MC Turbo Pump Creation Fail!"));

	CREATEDIALOG(g_pLLCTmp_IO, CTurboPumpDlg, this, CPoint(rROI.left + 450, rROI.top + 1000), SW_HIDE);
	if (g_pLLCTmp_IO != NULL)
	{
		g_pLog->Display(0, _T("LLC Turbo Pump Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pLLCTmp_IO->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->LLCGaugeCommStatus(FALSE);
				SetEquipmentStatus(_T("LLC TMP"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->LLCGaugeCommStatus(TRUE);
			}
		}
	}
	else
		g_pLog->Display(0, _T("LLC Turbo Pump Creation Fail!"));


	CREATEDIALOG(g_pMaindialog, CMaindialog, this, CPoint(rROI.left + 2135, rROI.top + 50), SW_HIDE);
	if (g_pMaindialog != NULL) g_pLog->Display(0, _T("SREM Main Dialog Creation Success!"));
	else g_pLog->Display(0, _T("SREM Main Dialog Creation Fail!"));

	CREATEDIALOG(g_pPlasmaCleaner, CPlasmaCleanerDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pPlasmaCleaner != NULL) g_pLog->Display(0, _T("Cleaner Dialog Creation Success!"));
	else g_pLog->Display(0, _T("Cleaner Dialog Creation Fail!"));

	CREATEDIALOG(g_pSubModule, CSubModuleDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pSubModule != NULL) g_pLog->Display(0, _T("Sub Module Dialog Creation Success!"));
	else g_pLog->Display(0, _T("Sub Module Dialog Creation Fail!"));

	WaitSec(3);
	g_pLoadingScreen.SetLoadingComplete();

	SetDisplay(OPERATOR);

	return nRet;
}

BOOL CEUVSOLUTIONDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CEUVSOLUTIONDlg::OnBnClickedOperatorButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedOperatorButton() 버튼 클릭!"));

	SetDisplay(OPERATOR);
}



void CEUVSOLUTIONDlg::OnBnClickedEngineerButton()
{

	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedEngineerButton() 버튼 클릭!"));
	g_pLog->Display(0, _T("Log 파일 생성 시작  "));
	
	SaveLogFile("Operation", "Engineer Button Click");
	
	g_pLog->Display(0, m_strLogPath);

	CString tmp_buff;

	g_pLog->Display(0, tmp_buff);
	
	SetDisplay(ENGINEER);
}


int CEUVSOLUTIONDlg::SetDisplay(int Mode)
{
	g_pEfem->ShowWindow(SW_HIDE);
	g_pMCTmp_IO->ShowWindow(SW_HIDE);
	g_pGauge_IO->ShowWindow(SW_HIDE);
	g_pLLCTmp_IO->ShowWindow(SW_HIDE);
	g_pVacuumRobot->ShowWindow(SW_HIDE);
	g_pConfig->ShowWindow(SW_HIDE);
	g_pIO->ShowWindow(SW_HIDE);
	g_pCommStat->ShowWindow(SW_HIDE);
	g_pLog->ShowWindow(SW_HIDE);
	g_pSubMenu->ShowWindow(SW_HIDE);
	if (g_pMaindialog != NULL) g_pMaindialog->ShowWindow(SW_HIDE);
	g_pSubTestMenu->ShowWindow(SW_HIDE);
	//g_pPTR->ShowWindow(SW_HIDE);
	//g_pPlasmaCleaner->ShowWindow(SW_HIDE);
	g_pSubModule->ShowWindow(SW_HIDE);
	//Stage 위치를 확인해서 OM or EUV 영상 Dlg를 띄우자

	switch (Mode)
	{
	case OPERATOR:
		break;
	case ENGINEER:
		g_pSubMenu->ShowWindow(SW_SHOW);
		//g_pCamera->ShowWindow(SW_SHOW);		
		g_pEfem->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		break;
	case CONFIGURATION:
		g_pConfig->ShowWindow(SW_SHOW);
		break;
	case EVALUATION:
		//g_pCamera->ShowWindow(SW_SHOW);
		//g_pRecipe->ShowWindow(SW_SHOW);
		g_pSubTestMenu->ShowWindow(SW_SHOW);
		//g_pChart->ShowWindow(SW_SHOW);
		//g_pChartline->ShowWindow(SW_SHOW);
		break;
	default:
		break;
	}

	return 0;
}

int CEUVSOLUTIONDlg::DisplayVersion()
{
	CString strText;
	CFont font, *pOldFont;
	CBrush br(BLACK);
	CRect rc;
	strText = GetVersion();
	CStatic* pFrame = (CStatic*)GetDlgItem(IDC_STATIC_ESOL_LOGO);
	pFrame->GetClientRect(rc);
	CClientDC dc(pFrame);
	CBrush* pOld = dc.SelectObject(&br);
	dc.SetBkMode(TRANSPARENT);
	dc.SetTextColor(PURPLE);
	dc.SetTextCharacterExtra(3);
	font.CreateFont(25, 13, 0, 0, FW_NORMAL, TRUE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("HY헤드라인M"));
	pOldFont = (CFont*)dc.SelectObject(&font);

	dc.TextOut(13, 240, strText);
	font.DeleteObject();
	dc.SelectObject(pOld);
	return 0;
}

CString CEUVSOLUTIONDlg::GetVersion(void)
{
	CString strSWVersion;
	CWinApp *pWinApp = AfxGetApp();
	CString sAppName;
	sAppName.Format("%s.exe", pWinApp->m_pszExeName);
	char temp_path[125];
	GetModuleFileName(AfxGetInstanceHandle(), temp_path, sizeof(temp_path));

	// 버전정보를 담을 버퍼















	char* buffer = NULL;

	DWORD infoSize = 0;

	// 파일로부터 버전정보데이터의 크기가 얼마인지를 구합니다.
	infoSize = GetFileVersionInfoSize(temp_path, 0);
	if (infoSize == 0) return "";

	// 버퍼할당















	buffer = new char[infoSize];
	if (buffer)
	{
		// 버전정보데이터를 가져옵니다.
		if (GetFileVersionInfo(temp_path, 0, infoSize, buffer) != 0)
		{
			VS_FIXEDFILEINFO* pFineInfo = NULL;
			UINT bufLen = 0;
			// buffer로 부터 VS_FIXEDFILEINFO 정보를 가져옵니다.
			if (VerQueryValue(buffer, "\\", (LPVOID*)&pFineInfo, &bufLen) != 0)
			{
				WORD majorVer, minorVer, buildNum, revisionNum;
				majorVer = HIWORD(pFineInfo->dwFileVersionMS);
				minorVer = LOWORD(pFineInfo->dwFileVersionMS);
				buildNum = HIWORD(pFineInfo->dwFileVersionLS);
				revisionNum = LOWORD(pFineInfo->dwFileVersionLS);

				strSWVersion.Format("S/W VERSION %d.%d.%d.%d", majorVer, minorVer, buildNum, revisionNum);
			}
		}
		delete[] buffer;
	}

	return strSWVersion;
}

void CEUVSOLUTIONDlg::MarsLogNewFile()
{	
	CTime T_Now = CTime::GetCurrentTime();
	CString strFilePath, strOldPath, strZipPath;

		if (T_Now.GetDay() == T_Old.GetDay())
		{
			if (T_Now.GetHour() != T_Old.GetHour())
			{
				SaveMarsLogFile(_T("EnvetLog"), "");
				T_Old = CTime::GetCurrentTime();
				return;
			}
		}
		else
		{
			//CGFMarsLog 생성 : Config 항목(ECID) Define 필요
			SaveMarsLogFile(_T("EnvetLog"), "CFG Data Value\t\n");
			T_Old = CTime::GetCurrentTime();
			return;
		}
}


void CEUVSOLUTIONDlg::OnBnClickedConfigurationButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedConfigurationButton() 버튼 클릭!"));

	SetDisplay(CONFIGURATION);
}


void CEUVSOLUTIONDlg::OnBnClickedEvaluationButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedEvaluationButton() 버튼 클릭!"));

	SetDisplay(EVALUATION);
}

void CEUVSOLUTIONDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == MAIN_DIALOG_INFO_TIMER)
	{
		GetEquipmentInfo();
		SetTimer(MAIN_DIALOG_INFO_TIMER, 100, NULL);
	}
	else if(nIDEvent == MARS_LOG_FILE_CREATE_TIMER)
	{
		MarsLogNewFile();
		SetTimer(MARS_LOG_FILE_CREATE_TIMER, 1000, NULL);
	}
	__super::OnTimer(nIDEvent);
}

void CEUVSOLUTIONDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	//상단부 마우스 좌클릭으로 윈도우 이동















	if (point.y < 15)
	{
		SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0);
	}

	__super::OnLButtonDown(nFlags, point);
}


void CEUVSOLUTIONDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	//상단부 마우스 더블클릭시 윈도우 최대화















	if (point.y < 15)
	{
		SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0);
		//ShowWindow(SW_SHOWMAXIMIZED);

		LONG style = ::GetWindowLong(m_hWnd, GWL_STYLE);

		style &= ~WS_CAPTION;
		style &= ~WS_SYSMENU;

		::SetWindowLong(m_hWnd, GWL_STYLE, style);
		int screenx = GetSystemMetrics(SM_CXSCREEN);
		int screeny = GetSystemMetrics(SM_CYSCREEN);

		SetWindowPos(NULL, -10, -10, screenx + 30, screeny - 30, SWP_NOZORDER);
	}

	__super::OnLButtonDblClk(nFlags, point);
}

void CEUVSOLUTIONDlg::OnBnClickedButtonShowUi()
{
	g_pAnimationGUI->SendMessageData(_T("APP_SHOW"));
}


void CEUVSOLUTIONDlg::OnBnClickedButtonHideUi()
{
	g_pAnimationGUI->SendMessageData(_T("APP_HIDE"));
}

//chlee
void CEUVSOLUTIONDlg::OnBnClickedButtonAlarm()
{
	g_pAlarm->ShowWindow(SW_SHOW);
}