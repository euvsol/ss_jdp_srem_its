
#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


BEGIN_MESSAGE_MAP(CLegendWnd, CWnd)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_TIMER()
END_MESSAGE_MAP()


CLegendWnd::CLegendWnd()
{
}

CLegendWnd::~CLegendWnd()
{
}

void CLegendWnd::OnDestroy()
{
	CWnd::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CLegendWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.
}


void CLegendWnd::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnTimer(nIDEvent);
}


void CLegendWnd::DoDataExchange(CDataExchange* pDX)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CWnd::DoDataExchange(pDX);
}


BOOL CLegendWnd::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CWnd::DestroyWindow();
}


BOOL CLegendWnd::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CWnd::PreTranslateMessage(pMsg);
}


BOOL CLegendWnd::Create(DWORD dwStyle, RECT& rect, CWnd* pParentWnd)
{
	BOOL ret;
	static CString className = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	ret = CWnd::Create(className, NULL, dwStyle, rect, pParentWnd, 0);

	rcClient.left = 0;
	rcClient.top = 0;
	rcClient.right = rect.right - rect.left;
	rcClient.bottom = rect.bottom - rect.top;

	int nPMRectSize = 130;
	//m_RectTracker.m_rect.left = rcClient.Width() / 2 - nPMRectSize;
	//m_RectTracker.m_rect.top = rcClient.Height() / 2 - nPMRectSize;
	//m_RectTracker.m_rect.right = rcClient.Width() / 2 + nPMRectSize;
	//m_RectTracker.m_rect.bottom = rcClient.Height() / 2 + nPMRectSize;
	//m_RectTracker.m_nStyle = CRectTracker::dottedLine;

	return ret;
}

