#pragma once

class CAnimationGUI : public CAnimationGUICom
{
public:
	CAnimationGUI();
	~CAnimationGUI();

private:

	struct MonitoringData
	{
		int message_id;
		int message_size;

		//CONNECTION INFO
		BOOL bConnAdam;
		BOOL bConnCrevis;
		BOOL bConnMts;
		BOOL bConnVmtr;
		BOOL bConnCam1;
		BOOL bConnScanStage;
		BOOL bConnNaviStage;
		BOOL bConnFilterStage;
		BOOL bConnSourcePc;
		BOOL bConnVacuumGauge;
		BOOL bConnLlcTmp;
		BOOL bConnMcTmp;
		BOOL bConnAfm;
		BOOL bConnRevolver;
		BOOL bConnXrayCamera;

		//SEQUENCE WORKING
		BOOL bSequenceWorking;

		//LPM MODULE
		BOOL bLpmPodExist;
		BOOL bLpmMaskExist;
		BOOL bLpmInitComp;
		BOOL bLpmOpened;
		BOOL bLpmWorking;
		BOOL bLpmError;

		//MTS ATR 
		BOOL bAtrMaskExist;
		BOOL bAtrInitComp;
		BOOL bAtrWorking;
		BOOL bAtrError;
		//int	 nAtrRobotStation;

		//MTS FLIPPER
		BOOL bFlipperMaskExist;
		BOOL bFlipperInitComp;
		BOOL bFlipperWorking;
		BOOL bFlipperError;

		//MTS ROTATOR
		BOOL bRotatorMaskExist;
		BOOL bRotatorInitComp;
		BOOL bRotatorWorking;
		BOOL bRotatorError;

		//LLC TMP
		BOOL bLlcMaskExist;
		BOOL bLlcGateValveOpened;
		BOOL bLlcTmpGateValveOpened;
		int	 bLlcTmpWorking;
		BOOL bLlcTmpError;
		BOOL bLlcForelineValveOpened;
		BOOL bLlcSlowRoughingValveOpened;
		BOOL bLlcFastRoughingValveOpened;
		BOOL bLlcDryPumpWorking;
		BOOL bLlcDryPumpAlarm;
		BOOL bLlcDryPumpWarning;
		char sLlcVacuumRate[20];

		//MC TMP
		BOOL bMcChuckMaskExist;
		BOOL bMcTrGateValveOpened;
		BOOL bMcTmpGateValveOpened;
		int  bMcTmpWorking;
		BOOL bMcTmpError;
		BOOL bMcForelineValveOpened;
		BOOL bMcSlowRoughingValveOpened;
		BOOL bMcFastRoughingValveOpened;
		BOOL bMcDryPumpWorking;
		BOOL bMcDryPumpAlarm;
		BOOL bMcDryPumpWarning;
		char sMcVacuumRate[20];

		//VMTR
		BOOL bVmtrMaskExist;
		BOOL bVmtrServoOn;
		BOOL bVmtrInitComp;
		BOOL bVmtrWorking;
		BOOL bVmtrExtended;
		BOOL bVmtrRetracted;
		BOOL bVmtrError;
		BOOL bVmtrEMO;
		double dVmtrFeedbackPosT;
		double dVmtrFeedbackPosL;
		double dVmtrFeedbackPosZ;
		//int nVmtrRobotStation;

		//NAVI POS
		BOOL bNaviLoadingPos;
		char sNaviFeedbackPosX[20];
		char sNaviFeedbackPosY[20];
		
		//MFC
		BOOL bSlowVentValveOpened;
		BOOL bSlowInletValveOpened;
		BOOL bSlowOutletValveOpened;
		BOOL bFastVentValveOpened;
		BOOL bFastInletValveOpened;
		BOOL bFastOutletValveOpened;
		double dMFC1N2;
		double dMFC2N2;
		
		//SOURCE
		char sSrcMcVacuumRate[20];
		char sSrcScVacuumRate[20];
		char sSrcBcVacuumRate[20];
		BOOL bEuvLaserShutterOpened;
		BOOL bEuvOn;
		BOOL bSrcMechShutterOpened;
		char sSrcMirrorStagePos[20];

		BOOL bWaterSupplyValveOpened;
		BOOL bWaterReturnValveOpened;
		BOOL bMainAir;
		BOOL bMainWater;
		BOOL bWaterLeakLlcTmp;
		BOOL bWaterLeakMcTmp;
		BOOL bSmokeDetectCb;
		BOOL bSmokeDetectVacSt;
		BOOL bAlarmWaterReturnTemp;
		BOOL bAvailableSrcGateOpened;

		BOOL bMcMaskTiltError1;
		BOOL bMcMaskTiltError2;
		BOOL bLlcMaskTiltError1;
		BOOL bLlcMaskTiltError2;

		int nMaskLocation;
	};


	BOOL				m_bThreadExitFlag;
	BOOL				m_bConnectThreadExitFlag;
	CWinThread*			m_pStatusThread;
	CWinThread*			m_pConnectThread;
	MonitoringData		data;

	static UINT	SendDataThread(LPVOID pParam);
	static UINT	TryConnectThread(LPVOID pParam);

public:
	
	int		OpenDevice();
	void	CloseDevice();
	void	InitializeData();
	int		SendMessageData(CString sMsg);

	//int		SetEtcConfigData();
	//int		SetCalibrationData();
	//int		SetRecoveryData();
	//int		SetVacuumSeqData();
	//int		SetPtrData();
	//int		SetPhaseData();
	//int		SetXRayCamData();
	//int		GetConfigData(CString sMsg);

	//stEtcConfigData*	GetEtcConfigData()	 { return m_stEtcData; }
	//stCalibrationData*	GetCalibrationData() { return m_stCalibrationData; }
	//stRecoveryData*		GetRecoveryData()	 { return m_stRecoveryData; }
	//stVacuumSeqData*	GetVacuumSeqData()	 { return m_stVacuumSeqData; }
	//stPtrData*			GetPtrData()		 { return m_stPtrData; }
	//stPhaseData*		GetPhaseData()		 { return m_stPhaseData; }
	//stXRayCamData*		GetXRayCamData()	 { return m_stXRayCamData; }
};