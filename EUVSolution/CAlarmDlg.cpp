﻿// CAlarmDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CAlarmDlg 대화 상자

IMPLEMENT_DYNAMIC(CAlarmDlg, CDialogEx)

CAlarmDlg::CAlarmDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ALARM_DIALOG, pParent)
	, m_nAlarmCode(0)
	, m_strAlarmText(_T(""))
	, m_strAlarmDiscription(_T(""))
{
	m_bAlarmSet = FALSE;
	m_nAlarmCategory = 0;
	Create(CAlarmDlg::IDD, pParent);
}

CAlarmDlg::~CAlarmDlg()
{
}

void CAlarmDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_ALARMCODE, m_EditAlarmCodeCtrl);
	DDX_Text(pDX, IDC_EDIT_ALARMCODE, m_nAlarmCode);
	DDX_Control(pDX, IDC_EDIT_ALARMTEXT, m_EditAlarmTextCtrl);
	DDX_Text(pDX, IDC_EDIT_ALARMTEXT, m_strAlarmText);
	DDX_Text(pDX, IDC_EDIT_ALARMDISCRIPTION, m_strAlarmDiscription);
	DDX_Control(pDX, IDC_ALARM_LIST, m_AlarmList);
}


BEGIN_MESSAGE_MAP(CAlarmDlg, CDialogEx)
	ON_NOTIFY(NM_CLICK, IDC_ALARM_LIST, &CAlarmDlg::OnNMClickAlarmList)
	ON_BN_CLICKED(IDOK, &CAlarmDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDBUZZEROFF, &CAlarmDlg::OnBnClickedBuzzeroff)
END_MESSAGE_MAP()


// CAlarmDlg 메시지 처리기

BOOL CAlarmDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	
	//InitWindowPos();
	CenterWindow();
	InitEditFont();
	InitAlarmDiscriptTxt();

	CRect rect;
	m_AlarmList.GetClientRect(&rect);
	m_AlarmList.InsertColumn(1, _T("ALARM CODE"), LVCFMT_CENTER, 300);
	m_AlarmList.InsertColumn(2, _T("TEXT"), LVCFMT_CENTER, rect.Width() - 300);
	m_AlarmList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	CFont font;
	font.CreatePointFont(120, "Arial");
	m_AlarmList.SetFont(&font);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAlarmDlg::InitWindowPos()
{
	CRect rc;
	GetWindowRect(rc);
	int width = rc.Width();
	int height = rc.Height();
	rc.top = 600;
	rc.left = 800;
	rc.bottom = rc.top + height;
	rc.right = rc.left + width;
	MoveWindow(rc, true);
}

void CAlarmDlg::InitEditFont()
{
	LOGFONT lf;

	m_EditAlarmTextCtrl.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	lf.lfHeight = 28;
	strcpy(lf.lfFaceName, "Arial");
	Font.CreateFontIndirect(&lf);

	m_EditAlarmTextCtrl.SetFont(&Font);
	if (PROCESS_ALARM == m_uiCategory)	m_EditAlarmTextCtrl.SetBkColor(RGB(0, 0, 200));
	else 	m_EditAlarmTextCtrl.SetBkColor(PURPLE);
	m_EditAlarmTextCtrl.SetTextColor(RGB(255, 255, 255));

	m_EditAlarmCodeCtrl.SetFont(&Font);
	if (PROCESS_ALARM == m_uiCategory) m_EditAlarmCodeCtrl.SetBkColor(RGB(0, 0, 200));
	else 	m_EditAlarmCodeCtrl.SetBkColor(PURPLE);
	m_EditAlarmCodeCtrl.SetTextColor(RGB(255, 255, 255));
}

void CAlarmDlg::InitAlarmDiscriptTxt()
{
	CRect rect;
	GetDlgItem(IDC_EDIT_ALARMDISCRIPTION)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	m_reCtrl.Create(ES_READONLY | ES_LEFT | ES_MULTILINE | WS_VSCROLL | ES_AUTOVSCROLL | WS_BORDER | LVS_EX_FLATSB, rect, this, 1);
	CHARFORMAT charformat;
	m_reCtrl.GetDefaultCharFormat(charformat);
	charformat.yHeight = 10 * 24;
	charformat.dwMask = CFM_SIZE;
	if (charformat.dwEffects & CFE_AUTOCOLOR) charformat.dwEffects -= CFE_AUTOCOLOR;
	charformat.crTextColor = WHITE;
	charformat.dwMask = CFM_COLOR;
	m_reCtrl.SetDefaultCharFormat(charformat);
	if (PROCESS_ALARM == m_uiCategory)	m_reCtrl.SetBackgroundColor(false, RGB(0, 0, 200));
	else	m_reCtrl.SetBackgroundColor(false, PURPLE);
	m_reCtrl.ShowWindow(SW_SHOW);

}

BOOL CAlarmDlg::GetAlarmInfoFromFile(int alarmcode)
{
	if (g_pLog->IsFileExist(ALARMCODEFILE))
	{
		if (!OpenFile(ALARMCODEFILE, CFile::modeRead))
		{
			return FALSE;//file open error
		}
	}
	else
	{
		return	FALSE;// error file is not exist
	}

	CString strAlarmCode;
	char buf0[10];
	char buf1[64];
	char buf2[512];
	ZeroMemory(buf0, 10);
	ZeroMemory(buf1, 64);
	ZeroMemory(buf2, 512);
	strAlarmCode.Format("[%d]", alarmcode);
	int ret = FindItem((char *)(LPCTSTR)strAlarmCode);
	if (ret > 0)
	{
		GetItem(buf0);
		GetItem(buf1);
		GetLine(buf2);

		m_nAlarmCode = alarmcode;
		m_strAlarmText = buf1;
		m_strAlarmDiscription = buf2;
	}

	Close();

	if (ret > 0)
		return TRUE;
	else
		return FALSE;
}

void CAlarmDlg::SetAlarm(int alarmcode, char* pchAlarmData)
{
	char pchAlarmcode[10] = "";
	BOOL bRet = FALSE;
	int uiCategory = 0;
	CString strErrorLog;



	//AlarmCategory를 찾는다.
	uiCategory = GetAlarmCategory(alarmcode);



	bRet = GetAlarmInfoFromFile(alarmcode);	//해당 Alarm Code에 대한 정보를 가져온다.
	if (bRet == TRUE)
	{
		itoa(alarmcode, pchAlarmcode, 10);

		//Alarm창읠 띄웠을때만 AlarmSet변수 set.
		m_bAlarmSet = TRUE;



		BOOL bExist = FALSE;

		int nItemNum = m_AlarmList.GetItemCount();

		for (int nIndex = 0; nIndex < nItemNum; nIndex++)
		{
			CString sCode = m_AlarmList.GetItemText(nIndex, 0);
			if (atoi(sCode) == alarmcode)
				bExist = TRUE;
		}

		if (bExist == FALSE)
		{
			m_AlarmList.InsertItem(nItemNum, pchAlarmcode);
			m_AlarmList.SetItemText(nItemNum, 1, m_strAlarmText);

			// Log 남기기.
			CString sTemp = m_strAlarmDiscription;
			sTemp.Remove('\n');
			sTemp.Remove('\r');
			sTemp.Remove('\t');
			strErrorLog.Format(_T("[%s] [%s] [%s]"), pchAlarmcode, m_strAlarmText, sTemp);
			SaveLogFile("Error", (LPSTR)(LPCTSTR)strErrorLog);
		}

		m_AlarmList.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
		m_AlarmList.EnsureVisible(0, FALSE);
		m_AlarmList.SetFocus();

		GetSelectedAlarmInfo(0);
		ShowWindow(SW_SHOW);

		//Signal Tower Red On
		g_pIO->On_SiganlTowerErrorOn();
		//Buzzer On
		g_pIO->On_SignalTowerBuzzer_2();
	}
	else
	{
		itoa(alarmcode, pchAlarmcode, 10);
		// Log 남기기.
		strErrorLog.Format(_T("[%s] [%s] [%s]"), pchAlarmcode, "Error code Data 파일에 정의되지 않음", "");
		SaveLogFile("Error", (LPSTR)(LPCTSTR)strErrorLog);
	}
}

void CAlarmDlg::GetSelectedAlarmInfo(int nIndex)
{
	BOOL bRet = FALSE;

	if (nIndex >= 0 && nIndex < m_AlarmList.GetItemCount())
	{
		CString sAlarmCode = m_AlarmList.GetItemText(nIndex, 0);
		CString sAlarmText = m_AlarmList.GetItemText(nIndex, 1);

		bRet = GetAlarmInfoFromFile(atoi(sAlarmCode));	//해당 Alarm Code에 대한 정보를 가져온다.
		if (bRet == TRUE)
		{
			GetDlgItem(IDC_EDIT_ALARMCODE)->SetWindowText(sAlarmCode);
			GetDlgItem(IDC_EDIT_ALARMTEXT)->SetWindowText(sAlarmText);
			m_reCtrl.SetWindowText(m_strAlarmDiscription);
		}
	}
}

int CAlarmDlg::GetAlarmCategory(int alarmcode)
{
	unsigned int uiCategory = 0;

	if (g_pLog->IsFileExist(ALARMCODEFILE))
	{
		if (!OpenFile(ALARMCODEFILE, CFile::modeRead))
		{
			return -1;//file open error
		}
	}
	else
	{
		return	-1;// error file is not exist
	}

	CString strAlarmCode;
	char buf1[32];
	ZeroMemory(buf1, 32);
	strAlarmCode.Format("[%d]", alarmcode);
	int ret = FindItem((char *)(LPCTSTR)strAlarmCode);
	if (ret > 0)
	{
		uiCategory = GetInt();
	}

	Close();

	return uiCategory;
}


BOOL CAlarmDlg::OpenFile(LPCTSTR lpszFileName, UINT nOpenFlags)
{
	if (nOpenFlags == CFile::modeRead)
	{
		if ((fp = fopen(lpszFileName, "rb")) == NULL)
			return FALSE;
	}
	else if (nOpenFlags == CFile::modeWrite)
	{
		if ((fp = fopen(lpszFileName, "wb")) == NULL)
			return FALSE;
	}
	else
	{
		if ((fp = fopen(lpszFileName, "rb+")) == NULL)
			return FALSE;
	}
	return TRUE;

}

void CAlarmDlg::Close()
{
	if (fp != 0)
		fclose(fp);
}

long CAlarmDlg::FindItem(char *Item, int Index)
{
	char line[255];
	long Cnt = 0;
	CString str;
	int dwPos, len;

	CString finditem = Item;
	finditem.MakeUpper();

	fseek(fp, 0L, SEEK_SET);
	do {
		str = fgets(line, sizeof(line), fp);
		if (str.IsEmpty())
			break;
		str = line;
		str.MakeUpper();
		int n = 0;
		if ((n = str.Find(finditem)) >= 0)
		{
			dwPos = ftell(fp);
			int lenth = 0;
			lenth = str.GetLength();
			len = str.GetLength() - (n + (strlen(Item)));
			fseek(fp, dwPos - len, SEEK_SET);
			Cnt++;
		}
	} while (Cnt < Index);
	return(Cnt);
}

BOOL CAlarmDlg::GetLine(char *line)
{
	if (fgets((char *)(LPCSTR)line, 512, fp) == NULL)
		return FALSE;
	else
		return TRUE;
}

int CAlarmDlg::GetInt()
{
	char buf[60];
	GetItem(buf);
	return atoi(buf);
}

int CAlarmDlg::GetItem(char* Buffer)
{
	int Cnt = 0, TCnt = 0, pre_read = 0;
	char Byte = 0;
	char pBuffer[255];

	DWORD dwPos = ftell(fp);
	// 만약 처음에 divider가 읽혀질 경우 count한다.
	do {
		if ((Byte = fgetc(fp)) < 1)
			return -1;
		if ((pre_read++) >= 255)
			return -1;
	} while ((Byte == ';') || (Byte == ' ' || Byte == '\t'));//(Byte == 0x0d) || (Byte<=' ') ||

	if (Byte != 0x0d)
	{
		// 여기부터 실제 데이터를 읽는다.
		do {
			if ((Byte = fgetc(fp)) < 1)
				return -1;

			if ((Cnt++) >= 255)
				return (-1);

		} while ((Byte != 0x0d) && (Byte != ';') && (Byte != ' ') && (Byte != '\t'));// 0x0d는 CR임
		fseek(fp, dwPos, SEEK_SET);
		fread(pBuffer, pre_read + Cnt, 1, fp);
		fseek(fp, dwPos + pre_read + Cnt, SEEK_SET);
	}
	else
	{
		fseek(fp, dwPos, SEEK_SET);
		fread(pBuffer, pre_read + Cnt, 1, fp);
		fseek(fp, dwPos + pre_read + Cnt, SEEK_SET);
	}

	if (Byte == 0x0d) {
		pBuffer[(pre_read + Cnt)] = 0;
		pBuffer[(pre_read + Cnt) - 1] = 0;
		//this->Read(&Byte,1);
	}
	else {
		Cnt--;
		pBuffer[pre_read + Cnt] = 0;
	}
	strcpy(Buffer, pBuffer + (--pre_read));

	Buffer[strlen(Buffer)] = 0;

	return Cnt;
}

void CAlarmDlg::OnNMClickAlarmList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	int nIndex = pNMItemActivate->iItem;

	GetSelectedAlarmInfo(nIndex);

	*pResult = 0;
}

void CAlarmDlg::OnBnClickedOk()
{
	m_AlarmList.DeleteAllItems();
	GetDlgItem(IDC_EDIT_ALARMCODE)->SetWindowText("");
	GetDlgItem(IDC_EDIT_ALARMTEXT)->SetWindowText("");
	m_reCtrl.SetWindowText("");

	//Signal Tower Red Off , Green On
	g_pIO->Off_SiganlTowerErrorOn();

	//Buzzer Off
	g_pIO->Off_SignalTowerBuzzer_2();
}


void CAlarmDlg::OnBnClickedBuzzeroff()
{
	g_pIO->Off_SignalTowerBuzzer_2();
}
