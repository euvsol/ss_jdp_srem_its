﻿// CSubMenuDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CSubMenuDlg 대화 상자

IMPLEMENT_DYNAMIC(CSubMenuDlg, CDialogEx)

CSubMenuDlg::CSubMenuDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SUB_MENU_DIALOG, pParent)
{

}

CSubMenuDlg::~CSubMenuDlg()
{
}

void CSubMenuDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSubMenuDlg, CDialogEx)
	ON_BN_CLICKED(IDC_MTS_BUTTON, &CSubMenuDlg::OnBnClickedMtsButton)
	ON_BN_CLICKED(IDC_VMTR_BUTTON, &CSubMenuDlg::OnBnClickedVmtrButton)
	ON_BN_CLICKED(IDC_PUMP_BUTTON, &CSubMenuDlg::OnBnClickedPumpButton)
	ON_BN_CLICKED(IDC_DIO_BUTTON, &CSubMenuDlg::OnBnClickedDioButton)
	ON_BN_CLICKED(IDC_HWSTATE_BUTTON, &CSubMenuDlg::OnBnClickedHwstateButton)
	ON_BN_CLICKED(IDC_GEMAUTO_BUTTON, &CSubMenuDlg::OnBnClickedGemautoButton)
END_MESSAGE_MAP()


// CSubMenuDlg 메시지 처리기


void CSubMenuDlg::OnBnClickedMtsButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedMtsButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_MTS);
}


void CSubMenuDlg::OnBnClickedVmtrButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedVmtrButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_VMTR);
}


void CSubMenuDlg::OnBnClickedPumpButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedPumpButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_TURBOPUMP);
}


void CSubMenuDlg::OnBnClickedDioButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedDioButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_DIO);
}


void CSubMenuDlg::OnBnClickedHwstateButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedSubModuleButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_HWSTATUS);
}


void CSubMenuDlg::OnBnClickedGemautoButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedGemautoButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_GEMAUTO);
}


int CSubMenuDlg::SetDisplay(int Mode)
{
	g_pSubTestMenu->ShowWindow(SW_HIDE);

	g_pIO->ShowWindow(SW_HIDE);
	g_pEfem->ShowWindow(SW_HIDE);
	
	g_pMCTmp_IO->ShowWindow(SW_HIDE);
	g_pLLCTmp_IO->ShowWindow(SW_HIDE);
	g_pGauge_IO->ShowWindow(SW_HIDE);
	g_pConfig->ShowWindow(SW_HIDE);
	g_pCommStat->ShowWindow(SW_HIDE);
	g_pVacuumRobot->ShowWindow(SW_HIDE);
	g_pMaindialog->ShowWindow(SW_HIDE);
	g_pSubMenu->ShowWindow(SW_SHOW);
	g_pLog->ShowWindow(SW_HIDE);

	g_pSubModule->ShowWindow(SW_HIDE);
	g_pConfig->ShowWindow(SW_HIDE);
	switch (Mode)
	{
	case DISPLAY_MTS:
		g_pEfem->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_VMTR:
		g_pVacuumRobot->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_ISOLATOR:
		g_pLog->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_ETC:
		break;
	case DISPLAY_TURBOPUMP:
		g_pMCTmp_IO->ShowWindow(SW_SHOW);
		g_pLLCTmp_IO->ShowWindow(SW_SHOW);
		g_pGauge_IO->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_DIO:
		g_pIO->ShowWindow(SW_SHOW);
		g_pMaindialog->ShowWindow(SW_SHOW);		
		g_pLog->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_HWSTATUS:
		g_pSubModule->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		break;
	default:
		break;
	}

	return 0;
}



BOOL CSubMenuDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}



