#pragma once

extern CLogDisplayDlg*							g_pLog;
extern CEUVSOLUTIONDlg*							g_pEUVSolution;
extern CIODlg*									g_pIO;
extern CLogDisplayDlg*							g_pLog;
extern CMaskEFEMDlg*							g_pEfem;
extern CTurboPumpMCDlg*							g_pMCTmp_IO;
extern CTurboPumpDlg*							g_pLLCTmp_IO;
extern CVacuumGaugeDlg*							g_pGauge_IO;
extern CVacuumRobotDlg*							g_pVacuumRobot;
extern CConfigurationEditorDlg*					g_pConfig;
extern CHWCommStatusDlg*						g_pCommStat;
extern CAutoProcess*							g_pAP;
extern CVacuumProcess*							g_pVP;
extern CWarningDlg*								g_pWarning;
extern CAlarmDlg*								g_pAlarm;
extern CSubMenuDlg*								g_pSubMenu;
extern CSubTestMenuDlg*							g_pSubTestMenu;
extern CAnimationGUI*							g_pAnimationGUI;
extern CMaindialog*								g_pMaindialog;
extern CLoadingScreenDlg						g_pLoadingScreen;

extern CPlasmaCleanerDlg*						g_pPlasmaCleaner;
extern CSubModuleDlg*							g_pSubModule;
extern CDeviceManager*							g_pDevMgr;

extern MIL_ID							        g_milApplication;
extern MIL_ID							        g_milSystemHost;
extern MIL_ID							        g_milSystemGigEVision;
