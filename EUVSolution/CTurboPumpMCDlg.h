﻿/**
 * MC Turbo Pump Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CTurboPumpMCDlg 대화 상자

class CTurboPumpMCDlg : public CDialogEx, public CPfeifferHiPace2300TMPCtrl, public IObserver
{
	DECLARE_DYNAMIC(CTurboPumpMCDlg)

public:
	CTurboPumpMCDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CTurboPumpMCDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TURBO_PUMP_MC_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	clock_t		m_start_time, m_finish_time;
	HICON		m_LedIcon[3];
	int			m_lock_state;
	bool		m_mc_tmp_ThreadExitFlag;
	CWinThread*	m_mc_tmp_Thread;

	void Mc_Tmp_ReceiveData();		// Tmp 상태 확인
	void TmpSendUpdate();			// Tmp 상태 요청

	static UINT Mc_Tmp_UpdataThread(LPVOID pParam);

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedMcTmpOn();
	afx_msg void OnBnClickedMcTmpOff();
	afx_msg void OnBnClickedMcTmpErrorReset();
	afx_msg void OnBnClickedMcTmpLock();
	DECLARE_MESSAGE_MAP()

public:
	int  OpenDevice();
	void CloseDevice();

	int	 Mc_Tmp_Off();
	int	 Mc_Tmp_On();
	void EmergencyStop();				// Error 발생 시 실행 함수.

	int	GetMcTmpHz()			{ return m_nMcTmp_ReceiveDataHz; }
	int	GetMcTmpSetHz()			{ return m_nMcTmp_ReceiveDataSetHz; }
	int	GetMcTmpErrorCode()		{ return m_nMcTmp_ReceiveDataErrorCode; }
	int	GetMcTmpRpm()			{ return m_nMcTmp_ReceiveDataRpm; }
	int	GetMcTmpSetRpm()		{ return m_nMcTmp_ReceiveDataSetRpm; }
	int	GetMcTmpTemperature_1() { return m_nMcTmp_ReceiveDataTemperature_1; }
	int	GetMcTmpTemperature_2() { return m_nMcTmp_ReceiveDataTemperature_2; }
	int	GetMcTmpTemperature_3() { return m_nMcTmp_ReceiveDataTemperature_3; }
	int	GetMcTmpTemperature_4() { return m_nMcTmp_ReceiveDataTemperature_4; }
	int	GetMcTmpOnOffState()	{ return m_nMcTmp_ReceiveDataOnOffState; }
	int	GetMcTmpState()			{ return m_nMcTmp_ReceiveDataState;  }
	BOOL Is_MC_Tmp_Connected()	{ return m_bSerialConnected; }
	
	enum 
	{
		Stop = 0,
		TMP_TURN_OFF,
		TMP_TURN_ON,
		Deceleration,
		Acceleration,
		Running,
		Fail,
		Error,
	};
};
