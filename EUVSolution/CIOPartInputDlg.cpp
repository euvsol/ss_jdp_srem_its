﻿// CIOPartInputDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CDigitalInputPart 대화 상자

IMPLEMENT_DYNAMIC(CDigitalInputPart, CDialogEx)

CDigitalInputPart::CDigitalInputPart(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_PART_INPUT_DIALOG, pParent)
{

}

CDigitalInputPart::~CDigitalInputPart()
{
	m_brush.DeleteObject();
	m_brush2.DeleteObject();
	m_font.DeleteObject();
}

void CDigitalInputPart::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDigitalInputPart, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CDigitalInputPart 메시지 처리기


HBRUSH CDigitalInputPart::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if ((pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_PART_TEXT) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_PART_TEXT2))
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
		else if (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITAIN_TEXT)
		{
			pDC->SetBkColor(GRAY);
			return m_brush2;
		}
	}

	return hbr;
}


void CDigitalInputPart::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == INPUT_PART_DIALOG_TIMER)
	{
		OnUpdateDigitalInput_Part();
		SetTimer(INPUT_PART_DIALOG_TIMER, 100, NULL);
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CDigitalInputPart::OnDestroy()
{
	CDialogEx::OnDestroy();
	KillTimer(INPUT_PART_DIALOG_TIMER);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


BOOL CDigitalInputPart::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_brush2.CreateSolidBrush(GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CDigitalInputPart::InitControls_DI_PART()
{
	CString strTemp;
	int Digital_In_Static_num = 18;

	for (int nIdx = 0; nIdx < Digital_In_Static_num; nIdx++)
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y000 + nIdx))->SetIcon(m_LedIcon[0]);

	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM0, "Y0108");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM1, "X0410");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM2, "X0411");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM3, "X0406");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM4, "X0407");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM5, "X0414");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM6, "X0415");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM7, "X0402");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM8, "X0403");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM9, "X0400");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM10, "X0401");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM11, "Y0103");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM12, "X0408");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM13, "X0409");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM14, "X0412");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM15, "X0413");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM16, "X0404");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM17, "X0405");


	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y000, "MC SLOW ROUGHING V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y001, "MC FAST ROUGHING V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y002, "MC ROUGHING V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y003, "MC#1 TMP GATE V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y004, "MC#1 TMP GATE V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y005, "MC FORLINE#1 V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y006, "MC FORLINE#1 V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y007, "TR GATE OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y008, "TR GATE CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y009, "LLC GATE OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y010, "LLC GATE CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y011, "LLC SLOW ROUGHING V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y012, "LLC FAST ROUGHING V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y013, "LLC ROUGHING V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y014, "LLC FORLINE V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y015, "LLC FORLINE V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y016, "LLC TMP GATE V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y017, "LLC TMP GATE V/V CLOSE");

	SetTimer(INPUT_PART_DIALOG_TIMER, 100, NULL);
}

void CDigitalInputPart::OnUpdateDigitalInput_Part()
{

	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if (g_pIO->Is_MC_SlowRoughValve_Open() == VALVE_OPEN)
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y000))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y000))->SetIcon(m_LedIcon[0]);

		if (g_pIO->Is_MC_FastRoughValve_Open() == VALVE_OPEN)
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y001))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y001))->SetIcon(m_LedIcon[0]);

		if (g_pIO->m_bDigitalIn[g_pIO->DI::MC_ROUGHING_VALVE_CLOSE] == true)
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y002))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y002))->SetIcon(m_LedIcon[0]);

		if (g_pIO->Is_MC_TMP_GateValve_Open() == VALVE_OPEN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y003))->SetIcon(m_LedIcon[1]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y004))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y003))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y004))->SetIcon(m_LedIcon[1]);
		}


		if (g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_OPEN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y005))->SetIcon(m_LedIcon[1]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y006))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y005))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y006))->SetIcon(m_LedIcon[1]);
		}

		if (g_pIO->Is_TRGateValve_Open() == VALVE_OPEN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y007))->SetIcon(m_LedIcon[1]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y008))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y007))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y008))->SetIcon(m_LedIcon[1]);
		}

		if (g_pIO->Is_LLCGateValve_Open() == VALVE_OPEN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y009))->SetIcon(m_LedIcon[1]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y010))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y009))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y010))->SetIcon(m_LedIcon[1]);
		}


		if (g_pIO->Is_LLC_SlowRoughValve_Open() == VALVE_OPEN)
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y011))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y011))->SetIcon(m_LedIcon[0]);

		if (g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_OPEN)
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y012))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y012))->SetIcon(m_LedIcon[0]);

		if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_ROUGHING_VALVE_CLOSE] == true)
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y013))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y013))->SetIcon(m_LedIcon[0]);


		if (g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_OPEN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y014))->SetIcon(m_LedIcon[1]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y015))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y014))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y015))->SetIcon(m_LedIcon[1]);
		}

		if (g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_OPEN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y016))->SetIcon(m_LedIcon[1]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y017))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y016))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y017))->SetIcon(m_LedIcon[1]);

		}
	}
}