#pragma once
class CLegendWnd :	public CWnd
{
public:
	CLegendWnd();
	virtual ~CLegendWnd();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL DestroyWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL Create(DWORD dwStyle, RECT& rect, CWnd* pParentWnd);

	CRect rcClient;

	MIL_ID m_MilApplication; /* Application identifier.   */
	MIL_ID m_MilSystem;      /* System identifier.        */
	MIL_ID m_MilDisplay;

	MIL_ID m_MilLut;                   // Display color LUT.
	MIL_ID m_MilLutLegend;             // Display color LUT legend.

	MIL_INT m_nImageSizeX;
	MIL_INT m_nImageSizeY;
	MIL_INT m_nImageBand;
};

