﻿// CIOInputAnalogDlg.cpp: 구현 파일
//


#include "stdafx.h"
#include "Include.h"
#include "Extern.h"



// CAnalogInput 대화 상자

IMPLEMENT_DYNAMIC(CAnalogInput, CDialogEx)

CAnalogInput::CAnalogInput(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_INPUT_AN_DIALOG, pParent)
{
}

CAnalogInput::~CAnalogInput()
{
	m_brush.DeleteObject();
	m_font.DeleteObject();
}

void CAnalogInput::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAnalogInput, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CAnalogInput 메시지 처리기

BOOL CAnalogInput::OnInitDialog()
{
	CDialogEx::OnInitDialog(); 

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CAnalogInput::InitControls_AI()
{
	m_nChannel = 0;
	CString strTemp;
	CString str;
	for (int nIdx = 0; nIdx < DIGITAL_IO_VIEW_NUMBER; nIdx++)
	{

		((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X000 + nIdx))->SetIcon(m_LedIcon[0]);
		strTemp.Format(_T("X%04d"), nIdx);
		SetDlgItemText(IDC_STATIC_ANALOGIN_NUM0 + nIdx, strTemp);
		SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE0 + nIdx, "0.0 V");
		GetDlgItem(IDC_STATIC_ANALOGIN_X000 + nIdx)->SetWindowText(g_pConfig->m_chAi[nIdx]);
		GetDlgItem(IDC_STATIC_ANALOGIN_X000 + nIdx)->GetWindowTextA(str);
		if (str == "EMPTY") SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE0 + nIdx, " - ");
		else if (str == " ") SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE0 + nIdx, " - ");
		else SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE0 + nIdx, "0");
	}

	//GetDlgItem(IDC_STATIC_TEXT_ANALOG_INPUT)->SetFont(&m_font);
	SetTimer(ANALOG_INPUT_CHECK_TIMER, 500, NULL);

}

bool CAnalogInput::OnUpdateDigitalInput_AI()
{
	int nCnt = 0;

	/*
	***************************************
	CREVIS Analog INPUT VOLTAGE READ Value.

	 INPUT VOLT   HEX        DEC
		0 V =    0x00,		0
		1 V =	 0x668,		1,640
		2 V =	 0xCCE,		3,278
		3 V =	 0x1335,	4,917
		4 V =	 0x199A,	6,554
		5 V =	 0X2002,	8,194
		6 V =	 0x2666,	9,830

	****************************************
	*/
	//Crevis Analog Input ST-3444  
	//double nCh_1 = (double)(g_pIO->m_nDecimal_Analog_in_ch_1) / 1638.0; // LLC FORELINE GAUGE 722b monitor
	//double nCh_2 = (double)(g_pIO->m_nDecimal_Analog_in_ch_2) / 1638.0; // MC FORELINE GAUGE 722b monitor
	//double nCh_3 = (double)(g_pIO->m_nDecimal_Analog_in_ch_3) / 1638.0; // empty
	//double nCh_4 = (double)(g_pIO->m_nDecimal_Analog_in_ch_4) / 1638.0; // empty
	//double nCh_5 = (double)(g_pIO->m_nDecimal_Analog_in_ch_5) / 1638.0; // #mfc#2 N2 10 sccm
	//double nCh_6 = (double)(g_pIO->m_nDecimal_Analog_in_ch_6) / 1638.0; // #mfc#1 N2 20000 sccm
	//double nCh_7 = (double)(g_pIO->m_nDecimal_Analog_in_ch_7) / 1638.0; // #mfc#3 O2 5 sccm
	//double nCh_8 = (double)(g_pIO->m_nDecimal_Analog_in_ch_8) / 1638.0; // empty
	

	//Crevis Analog Input ST-3424
	double nCh_1 = (double)(g_pIO->m_nDecimal_Analog_in_ch_1) / ANALOG_INPUT_UNIT_CONVERSION; // LLC FORELINE GAUGE 722b monitor
	double nCh_2 = (double)(g_pIO->m_nDecimal_Analog_in_ch_2) / ANALOG_INPUT_UNIT_CONVERSION; // MC FORELINE GAUGE 722b monitor
	double nCh_3 = (double)(g_pIO->m_nDecimal_Analog_in_ch_3) / ANALOG_INPUT_UNIT_CONVERSION; // empty
	double nCh_4 = (double)(g_pIO->m_nDecimal_Analog_in_ch_4) / ANALOG_INPUT_UNIT_CONVERSION; // empty
	double nCh_5 = (double)(g_pIO->m_nDecimal_Analog_in_ch_5) / ANALOG_INPUT_UNIT_CONVERSION; // #mfc#2 N2 10 sccm
	double nCh_6 = (double)(g_pIO->m_nDecimal_Analog_in_ch_6) / ANALOG_INPUT_UNIT_CONVERSION; // #mfc#1 N2 20000 sccm
	double nCh_7 = (double)(g_pIO->m_nDecimal_Analog_in_ch_7) / ANALOG_INPUT_UNIT_CONVERSION; // #mfc#3 O2 5 sccm
	double nCh_8 = (double)(g_pIO->m_nDecimal_Analog_in_ch_8) / ANALOG_INPUT_UNIT_CONVERSION; // empty
	double nCh_9 = g_pIO->Get_AC_Rack_Temp();// AC RACK temp
	double nCh_10 = g_pIO->Get_ControlBox_Temp(); // Contorl Temp

	// MFC SCCM 환산.
	double nCh_5_MFC2 = nCh_5 * 2;
	double nCh_6_MFC1 = nCh_6 * 4000;
	double nCh_7_MFC1 = nCh_7 ;



	CString nCh_1_str;
	CString nCh_2_str;
	CString nCh_3_str;
	CString nCh_4_str;
	CString nCh_5_str;
	CString nCh_6_str;
	CString nCh_7_str;
	CString nCh_8_str;
	CString nCh_9_str;
	CString nCh_10_str;
	
	nCh_1_str.Format("%0.2f", nCh_1);
	nCh_2_str.Format("%0.2f", nCh_2);
	
	nCh_5_str.Format("%0.2f", nCh_5_MFC2);
	nCh_6_str.Format("%0.f", nCh_6_MFC1);
	nCh_7_str.Format("%0.2f", nCh_7);
	//nCh_7_str.Format("%0.f", nCh_7_MFC1);

	//nCh_5_str.Format("%0.2f", nCh_5);
	//nCh_6_str.Format("%0.2f", nCh_6);
	
	nCh_9_str.Format("%0.2f", nCh_9);
	nCh_10_str.Format("%0.2f", nCh_10);


	//2v (DEC 3278) Magin DEC 3270 ~ 3280
	unsigned long long pBuffer_2v_1 = 0xcc6;
	unsigned long long pBuffer_2v_2 = 0xcd0;

	//3v (DEC 4917) Magin DEC 4910 ~ 4925
	unsigned long long pBuffer_3v_1 = 0x132e;
	unsigned long long pBuffer_3v_2 = 0x133d;

	//4v (DEC 6,554) Magin DEC 6549 ~ 6560 
	unsigned long long pBuffer_4v_1 = 0x1995;
	unsigned long long pBuffer_4v_2 = 0x19a0;

	//5v (DEC 8191) Magin // DEC 8185 ~ 8195 
	unsigned long long pBuffer_5v_1 = 0x1ff9;
	unsigned long long pBuffer_5v_2 = 0x2003;

	//6v (DEC 9830) Magin // DEC 9825 ~ 9835
	unsigned long long pBuffer_6v_1 = 0x2661;
	unsigned long long pBuffer_6v_2 = 0x266b;




	if (g_pIO->m_bCrevis_Open_Port != FALSE)
	{
		// LLC FORELINE GAUGE , MC FORELINE GAUGE (MKS 722B)

		SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE0, nCh_1_str + " V");
		SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE1, nCh_2_str + " V");

		// Temp
		SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE8, nCh_9_str + " ℃");
		SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE9, nCh_10_str + " ℃");
		
		if (nCh_9 > 55.0)
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X008))->SetIcon(m_LedIcon[1]);
		else if (nCh_9 < 5.0)
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X008))->SetIcon(m_LedIcon[0]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X008))->SetIcon(m_LedIcon[2]);

		if (nCh_10 > 55.0)
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X009))->SetIcon(m_LedIcon[1]);
		else if (nCh_10 < 5.0)
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X009))->SetIcon(m_LedIcon[0]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X009))->SetIcon(m_LedIcon[2]);

		if (nCh_1 > 9.0)
		{
			SetDlgItemText(IDC_STATIC_AN_STATE_1, "Line 대기상태");
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X000))->SetIcon(m_LedIcon[0]);
		}
		else if (nCh_1 < 9.0 && nCh_1 > 2.0) SetDlgItemText(IDC_STATIC_AN_STATE_1, "Line Pumping 중");
		else if (nCh_1 < 1.0)
		{
			SetDlgItemText(IDC_STATIC_AN_STATE_1, "Line 진공상태");
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X000))->SetIcon(m_LedIcon[2]);

		}

		if (nCh_2 > 9.0)
		{
			SetDlgItemText(IDC_STATIC_AN_STATE_2, "Line 대기상태");
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X001))->SetIcon(m_LedIcon[0]);
		}
		else if (nCh_2 < 9.0 && nCh_2 > 2.0) SetDlgItemText(IDC_STATIC_AN_STATE_2, "Line Pumping 중");
		else if (nCh_2 < 1.0)
		{
			SetDlgItemText(IDC_STATIC_AN_STATE_2, "Line 진공상태");
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X001))->SetIcon(m_LedIcon[2]);

		}

		// SLOW MFC , FAST MFC , O2 MFC

		if (g_pIO->Is_SlowVentValve_Open() == VALVE_OPENED)
		{
			SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE4 + nCnt, nCh_5_str + " SCCM");
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X004))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE4 + nCnt, " 0 SCCM");
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X004))->SetIcon(m_LedIcon[0]);
		}

		if (g_pIO->Is_FastVentValve_Open() == VALVE_OPENED)
		{
			SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE5 + nCnt, nCh_6_str + " SCCM");
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X005))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE5 + nCnt, " 0 SCCM");
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGIN_X005))->SetIcon(m_LedIcon[0]);
		}

	}
	else
	{
		SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE0 ,  " - V");
		SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE1 ,  " - V");
		SetDlgItemText(IDC_STATIC_AN_STATE_1, " 연결실패 ");
		SetDlgItemText(IDC_STATIC_AN_STATE_2, " 연결실패 ");
		SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE4 , " 연결실패 ");
		SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE5 , " 연결실패 ");
	}

	return TRUE;
}

void CAnalogInput::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == ANALOG_INPUT_CHECK_TIMER)
	{
		OnUpdateDigitalInput_AI();
		SetTimer(ANALOG_INPUT_CHECK_TIMER, 500, NULL);
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CAnalogInput::OnDestroy()
{
	CDialogEx::OnDestroy();
	KillTimer(ANALOG_INPUT_CHECK_TIMER);
}


HBRUSH CAnalogInput::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (pWnd->GetDlgCtrlID() == IDC_STATIC_ANALOGIN_TEXT)
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
	}
	return hbr;
}


