﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CWarningDlg 대화 상자

IMPLEMENT_DYNAMIC(CWarningDlg, CDialogEx)

CWarningDlg::CWarningDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_WARNING_DIALOG, pParent)
	, m_strWarningMessageVal(_T(""))
{
	Create(CWarningDlg::IDD, pParent);
}

CWarningDlg::~CWarningDlg()
{
}

void CWarningDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_WARNNING_MESSAGE, m_strWarningMessageVal);
	DDX_Control(pDX, IDC_WARNNING_MESSAGE, m_StaticMessageCtrl);
}


BEGIN_MESSAGE_MAP(CWarningDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDOK, &CWarningDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CWarningDlg 메시지 처리기


BOOL CWarningDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CWarningDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	LOGFONT lf;

	n_bMessageMoxFlag = true;

	m_StaticMessageCtrl.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	lf.lfHeight = 25;
	strcpy(lf.lfFaceName, "Arial");
	Font.CreateFontIndirect(&lf);

	m_StaticMessageCtrl.SetFont(&Font);
	m_StaticMessageCtrl.SetBkColor(WHITE);
	m_StaticMessageCtrl.SetTextColor(BLACK);

	m_StaticMessageCtrl.SetBlinkTextColors(PURPLE, BLACK);
	m_StaticMessageCtrl.StartTextBlink(TRUE, CColorStaticST::ST_FLS_FAST);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CWarningDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CWarningDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect rc;
	GetWindowRect(&rc);
	::ClipCursor(&rc);

	CDialogEx::OnMouseMove(nFlags, point);
}

void CWarningDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CRect rc;
	GetWindowRect(&rc);
	if(bShow)
		::ClipCursor(&rc);
	else
		::ClipCursor(NULL);

	//ScreenToClient(&rc);
	//rc.left = rc.Width()/2;
	////rc.right = rc.left + rc.Width();
	////int rcwidth = rc.right - rc.left;
	//MoveWindow(rc);

	CDialogEx::OnShowWindow(bShow, nStatus);
}

void CWarningDlg::OnBnClickedOk()
{
	::ClipCursor(NULL);

	n_bMessageMoxFlag = false;

	CDialogEx::OnOK();
}
