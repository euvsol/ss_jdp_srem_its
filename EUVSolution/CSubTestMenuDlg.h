﻿#pragma once


// CSubTestMenuDlg 대화 상자

class CSubTestMenuDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSubTestMenuDlg)

public:
	CSubTestMenuDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSubTestMenuDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SUB_TEST_MENU_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedSubTestButton1();

	int SetDisplay(int Mode);

	afx_msg void OnBnClickedSubTestButton2();
	afx_msg void OnBnClickedSubTestButton3();
	afx_msg void OnBnClickedSubTestButton0();
};
