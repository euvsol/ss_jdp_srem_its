#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <array>

CAnimationGUI::CAnimationGUI()
{
	m_bThreadExitFlag			= FALSE;
	m_bConnectThreadExitFlag	= FALSE;
	m_pStatusThread				= NULL;
	m_pConnectThread			= NULL;
}

CAnimationGUI::~CAnimationGUI()
{
	m_bConnectThreadExitFlag = TRUE;

	if (m_pConnectThread != NULL)
	{
		HANDLE threadHandle = m_pConnectThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				m_pConnectThread->SuspendThread();
				m_pConnectThread->ExitInstance();
			}
		}

		delete m_pConnectThread;
		m_pConnectThread = NULL;
	}

	m_bThreadExitFlag = TRUE;

	if (m_pStatusThread != NULL)
	{
		HANDLE threadHandle = m_pStatusThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				m_pStatusThread->SuspendThread();
				m_pStatusThread->ExitInstance();
			}
		}

		delete m_pStatusThread;
		m_pStatusThread = NULL;
	}
}

int CAnimationGUI::OpenDevice()
{
	int nRet = 0;
	nRet = OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_MONITORING], g_pConfig->m_nPORT[ETHERNET_MONITORING], FALSE);
	if (nRet == 0)
	{
		InitializeData();
		m_pStatusThread = AfxBeginThread(SendDataThread, (LPVOID)this, 0, 0, CREATE_SUSPENDED, 0);
		m_pStatusThread->m_bAutoDelete = FALSE;
		m_pStatusThread->ResumeThread();
	}
	
	m_pConnectThread = AfxBeginThread(TryConnectThread, (LPVOID)this, 0, 0, CREATE_SUSPENDED, 0);
	m_pConnectThread->m_bAutoDelete = FALSE;
	m_pConnectThread->ResumeThread();

	return nRet;
}

void CAnimationGUI::CloseDevice()
{
}

void CAnimationGUI::InitializeData()
{
	data.bConnAdam = FALSE;
	data.bConnCrevis = FALSE;
	data.bConnMts = FALSE;
	data.bConnVmtr = FALSE;
	data.bConnCam1 = FALSE;
	data.bConnScanStage = FALSE;
	data.bConnNaviStage = FALSE;
	data.bConnFilterStage = FALSE;
	data.bConnSourcePc = FALSE;
	data.bConnVacuumGauge = FALSE;
	data.bConnLlcTmp = FALSE;
	data.bConnMcTmp = FALSE;
	data.bConnAfm = FALSE;
	data.bConnRevolver = FALSE;
	data.bConnXrayCamera = FALSE;

	data.bSequenceWorking = FALSE;

	data.bLpmPodExist = FALSE;
	data.bLpmMaskExist = FALSE;
	data.bLpmInitComp = FALSE;
	data.bLpmOpened = FALSE;
	data.bLpmWorking = FALSE;
	data.bLpmError = FALSE;

	data.bAtrMaskExist = FALSE;
	data.bAtrInitComp = FALSE;
	data.bAtrWorking = FALSE;
	data.bAtrError = FALSE;

	data.bFlipperMaskExist = FALSE;
	data.bFlipperInitComp = FALSE;
	data.bFlipperWorking = FALSE;
	data.bFlipperError = FALSE;

	data.bRotatorMaskExist = FALSE;
	data.bRotatorInitComp = FALSE;
	data.bRotatorWorking = FALSE;
	data.bRotatorError = FALSE;

	data.bLlcMaskExist = FALSE;
	data.bLlcGateValveOpened = FALSE;
	data.bLlcTmpGateValveOpened = FALSE;
	data.bLlcTmpWorking = 0;
	data.bLlcTmpError = FALSE;
	data.bLlcForelineValveOpened = FALSE;
	data.bLlcSlowRoughingValveOpened = FALSE;
	data.bLlcFastRoughingValveOpened = FALSE;
	data.bLlcDryPumpWorking = FALSE;
	data.bLlcDryPumpAlarm = FALSE;
	data.bLlcDryPumpWarning = FALSE;
	strcpy(data.sLlcVacuumRate, _T(""));

	data.bMcChuckMaskExist = FALSE;
	data.bMcTrGateValveOpened = FALSE;
	data.bMcTmpGateValveOpened = FALSE;
	data.bMcTmpWorking = 0;
	data.bMcTmpError = FALSE;
	data.bMcForelineValveOpened = FALSE;
	data.bMcSlowRoughingValveOpened = FALSE;
	data.bMcFastRoughingValveOpened = FALSE;
	data.bMcDryPumpWorking = FALSE;
	data.bMcDryPumpAlarm = FALSE;
	data.bMcDryPumpWarning = FALSE;
	strcpy(data.sMcVacuumRate, _T(""));

	data.bVmtrMaskExist = FALSE;
	data.bVmtrServoOn = FALSE;
	data.bVmtrInitComp = FALSE;
	data.bVmtrWorking = FALSE;
	data.bVmtrExtended = FALSE;
	data.bVmtrRetracted = FALSE;
	data.bVmtrError = FALSE;
	data.bVmtrEMO = FALSE;
	data.dVmtrFeedbackPosT = 0.0;
	data.dVmtrFeedbackPosL = 0.0;
	data.dVmtrFeedbackPosZ = 0.0;

	data.bNaviLoadingPos = FALSE;
	strcpy(data.sNaviFeedbackPosX, _T("0.0"));
	strcpy(data.sNaviFeedbackPosY, _T("0.0"));

	data.bSlowVentValveOpened = FALSE;
	data.bSlowInletValveOpened = FALSE;
	data.bSlowOutletValveOpened = FALSE;
	data.bFastVentValveOpened = FALSE;
	data.bFastInletValveOpened = FALSE;
	data.bFastOutletValveOpened = FALSE;
	data.dMFC1N2 = 0.0;
	data.dMFC2N2 = 0.0;

	strcpy(data.sSrcMcVacuumRate, _T(""));
	strcpy(data.sSrcScVacuumRate, _T(""));
	strcpy(data.sSrcBcVacuumRate, _T(""));
	data.bEuvLaserShutterOpened = FALSE;
	data.bEuvOn = FALSE;
	data.bSrcMechShutterOpened = FALSE;
	strcpy(data.sSrcMirrorStagePos, _T(""));

	data.bWaterSupplyValveOpened = FALSE;
	data.bWaterReturnValveOpened = FALSE;
	data.bMainAir = FALSE;
	data.bMainWater = FALSE;
	data.bWaterLeakLlcTmp = FALSE;
	data.bWaterLeakMcTmp = FALSE;
	data.bSmokeDetectCb = FALSE;
	data.bSmokeDetectVacSt = FALSE;
	data.bAlarmWaterReturnTemp = FALSE;
	data.bAvailableSrcGateOpened = FALSE;

	data.bMcMaskTiltError1 = FALSE;
	data.bMcMaskTiltError2 = FALSE;
	data.bLlcMaskTiltError1 = FALSE;
	data.bLlcMaskTiltError2 = FALSE;

	data.nMaskLocation = -1;
}

int CAnimationGUI::SendMessageData(CString sMsg)
{
	int nRet;

	MessageData _data;
	_data.message_id = MSG_ID_MESSAGE;
	_data.message_size = sizeof(MessageData);
	strcpy(_data.Message, sMsg);

	nRet = SendStringMessage(_data);

	return nRet;
}

UINT CAnimationGUI::TryConnectThread(LPVOID pParam)
{
	CAnimationGUI* pCtrl = (CAnimationGUI*)pParam;
	
	while (!pCtrl->m_bConnectThreadExitFlag)
	{
		if (pCtrl->m_bConnected == FALSE)
		{
			int nRet = 0;

			//20210121 jkseo, CloseTcpIpSocket() 함수 호출 시 다른 모듈 문제 발생하여 임시 주석처리
			//pCtrl->CloseTcpIpSocket();
			nRet = pCtrl->OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_MONITORING], g_pConfig->m_nPORT[ETHERNET_MONITORING], FALSE);
			if (nRet == 0)
			{
				if (pCtrl->m_pStatusThread == NULL)
				{
					pCtrl->InitializeData();
					pCtrl->m_pStatusThread = AfxBeginThread(SendDataThread, (LPVOID)pCtrl, THREAD_PRIORITY_NORMAL);
				}
			}
		}

		Sleep(3000);
	}

	return 0;
}

UINT CAnimationGUI::SendDataThread(LPVOID pParam)
{
	CAnimationGUI* pCtrl = (CAnimationGUI*)pParam;

	while (!pCtrl->m_bThreadExitFlag)
	{
		if (pCtrl->m_bConnected == TRUE)
		{
			pCtrl->data.message_id = MSG_ID_MONITORING;
			pCtrl->data.message_size = sizeof(struct MonitoringData);

			if (g_pIO != NULL)
			{
				pCtrl->data.bConnCrevis = g_pIO->Is_CREVIS_Connected();

				pCtrl->data.bLlcMaskExist = g_pIO->Is_Mask_Check_Only_OnLLC();
				pCtrl->data.bLlcGateValveOpened = g_pIO->Is_LLCGateValve_Open();
				pCtrl->data.bLlcTmpGateValveOpened = g_pIO->Is_LLC_TMP_GateValve_Open();

				pCtrl->data.bLlcForelineValveOpened = g_pIO->Is_LLC_TMP_ForelineValve_Open();
				pCtrl->data.bLlcSlowRoughingValveOpened = g_pIO->Is_LLC_SlowRoughValve_Open();
				pCtrl->data.bLlcFastRoughingValveOpened = g_pIO->Is_LLC_FastRoughValve_Open();
				pCtrl->data.bLlcDryPumpWorking = g_pIO->Is_LLC_DryPump_Status() == 1 ? TRUE : FALSE;
				pCtrl->data.bLlcDryPumpAlarm = g_pIO->Is_LLC_DryPump_Status() == -2 ? TRUE : FALSE;
				pCtrl->data.bLlcDryPumpWarning = g_pIO->Is_LLC_DryPump_Status() == -1 ? TRUE : FALSE;

				pCtrl->data.bMcChuckMaskExist = g_pIO->Is_Mask_Check_Only_OnChuck();
				pCtrl->data.bMcTrGateValveOpened = g_pIO->Is_TRGateValve_Open();
				pCtrl->data.bMcTmpGateValveOpened = g_pIO->Is_MC_TMP_GateValve_Open();

				pCtrl->data.bMcForelineValveOpened = g_pIO->Is_MC_TMP_ForelineValve_Open();
				pCtrl->data.bMcSlowRoughingValveOpened = g_pIO->Is_MC_SlowRoughValve_Open();
				pCtrl->data.bMcFastRoughingValveOpened = g_pIO->Is_MC_FastRoughValve_Open();
				pCtrl->data.bMcDryPumpWorking = g_pIO->Is_MC_DryPump_Status() == 1 ? TRUE : FALSE;
				pCtrl->data.bMcDryPumpAlarm = g_pIO->Is_MC_DryPump_Status() == -2 ? TRUE : FALSE;
				pCtrl->data.bMcDryPumpWarning = g_pIO->Is_MC_DryPump_Status() == -1 ? TRUE : FALSE;

				if (g_pIO->Is_Mask_OnVMTR() == TRUE || g_pIO->Is_Mask_OnVMTR_LLC() == TRUE)
					pCtrl->data.bVmtrMaskExist = TRUE;
				else
					pCtrl->data.bVmtrMaskExist = FALSE;

				pCtrl->data.bVmtrExtended = g_pIO->Is_VAC_Robot_Arm_Extend();
				pCtrl->data.bVmtrRetracted = g_pIO->Is_VAC_Robot_Arm_Retract();

				pCtrl->data.bSlowVentValveOpened = g_pIO->Is_SlowVentValve_Open();
				pCtrl->data.bSlowInletValveOpened = g_pIO->Is_SlowVent_Inlet_Valve_Open();
				pCtrl->data.bSlowOutletValveOpened = g_pIO->Is_SlowVent_Outlet_Valve_Open();
				pCtrl->data.bFastVentValveOpened = g_pIO->Is_FastVentValve_Open();
				pCtrl->data.bFastInletValveOpened = g_pIO->Is_FastVent_Inlet_Valve_Open();
				pCtrl->data.bFastOutletValveOpened = g_pIO->Is_FastVent_Outlet_Valve_Open();
				pCtrl->data.dMFC1N2 = g_pIO->Get_MFC1_N2_10sccm_In();
				pCtrl->data.dMFC2N2 = g_pIO->Get_MFC2_N2_20000sccm_In();

				pCtrl->data.bWaterSupplyValveOpened = g_pIO->Is_Water_Supply_Status();
				//pCtrl->data.bWaterReturnValveOpened = g_pIO->Water_Return_Valve_Open();
				pCtrl->data.bMainAir = g_pIO->Is_Air_Supply_Status();
				pCtrl->data.bMainWater = g_pIO->Is_Water_Supply_Status();
				pCtrl->data.bWaterLeakLlcTmp = g_pIO->Is_Tmp_LLC_Leak_Status() == 0 ? TRUE : FALSE;
				pCtrl->data.bWaterLeakMcTmp = g_pIO->ls_Tmp_MC_Leak_Status() == 0 ? TRUE : FALSE;
				pCtrl->data.bSmokeDetectCb = FALSE;
				pCtrl->data.bSmokeDetectVacSt = FALSE;
				pCtrl->data.bAlarmWaterReturnTemp = FALSE;
				pCtrl->data.bAvailableSrcGateOpened = g_pIO->Is_SourceGate_OpenOn_Check();

				pCtrl->data.bMcMaskTiltError1 = g_pIO->Is_MC_Mask_Slant1_Check();
				pCtrl->data.bMcMaskTiltError2 = g_pIO->Is_MC_Mask_Slant2_Check();
				pCtrl->data.bLlcMaskTiltError1 = g_pIO->Is_LLC_Mask_Slant1_Check();
				pCtrl->data.bLlcMaskTiltError2 = g_pIO->Is_LLC_Mask_Slant2_Check();
			}

			if (g_pEfem != NULL)
			{
				pCtrl->data.bConnMts = g_pEfem->Is_MTS_Connected();

				pCtrl->data.bLpmPodExist = g_pEfem->Is_POD_OnLPM();
				pCtrl->data.bLpmMaskExist = g_pEfem->Is_MASK_InPOD();
				pCtrl->data.bLpmInitComp = g_pEfem->Is_LPM_Initialized();
				pCtrl->data.bLpmOpened = g_pEfem->Is_POD_Open();
				pCtrl->data.bLpmWorking = g_pEfem->Is_LPM_Working();
				pCtrl->data.bLpmError = g_pEfem->Is_LPM_Error();

				pCtrl->data.bAtrMaskExist = g_pEfem->Is_MASK_OnMTSRobot();
				pCtrl->data.bAtrInitComp = g_pEfem->Is_ATR_Initialized();
				pCtrl->data.bAtrWorking = g_pEfem->Is_ATR_Working();
				pCtrl->data.bAtrError = g_pEfem->Is_ATR_Error();

				pCtrl->data.bRotatorMaskExist = g_pEfem->Is_MASK_InRotator();
				pCtrl->data.bRotatorInitComp = g_pEfem->Is_Rotator_Initialized();
				pCtrl->data.bRotatorWorking = g_pEfem->Is_Rotator_Working();
				pCtrl->data.bRotatorError = g_pEfem->Is_Rotator_Error();
			}

			if (g_pVacuumRobot != NULL)
			{
				pCtrl->data.bConnVmtr = g_pVacuumRobot->Is_VMTR_Connected();

				pCtrl->data.bVmtrServoOn = g_pVacuumRobot->Is_VMTR_ServoOn();
				pCtrl->data.bVmtrInitComp = g_pVacuumRobot->Is_VMTR_Home();
				pCtrl->data.bVmtrWorking = g_pVacuumRobot->Is_VMTR_Working();

				pCtrl->data.bVmtrError = g_pVacuumRobot->Is_VMTR_Error();
				pCtrl->data.bVmtrEMO = g_pVacuumRobot->Is_VMTR_EMO();

				pCtrl->data.dVmtrFeedbackPosT = g_pVacuumRobot->Get_Feedback_PosT();
				pCtrl->data.dVmtrFeedbackPosL = g_pVacuumRobot->Get_Feedback_PosL();
				pCtrl->data.dVmtrFeedbackPosZ = g_pVacuumRobot->Get_Feedback_PosZ();
			}

			if (g_pGauge_IO != NULL)
			{
				pCtrl->data.bConnVacuumGauge = g_pGauge_IO->Is_GAUGE_Connected();

				sprintf(pCtrl->data.sLlcVacuumRate, _T("%.2e"), g_pGauge_IO->GetLlcVacuumRate());
				sprintf(pCtrl->data.sMcVacuumRate, _T("%.2e"), g_pGauge_IO->GetMcVacuumRate());
			}

			if (g_pLLCTmp_IO != NULL)
			{
				pCtrl->data.bConnLlcTmp = g_pLLCTmp_IO->Is_LLC_Tmp_Connected();
				pCtrl->data.bLlcTmpWorking = g_pLLCTmp_IO->GetLlcTmpState() == g_pLLCTmp_IO->Running ? true : false;
				pCtrl->data.bLlcTmpError = g_pLLCTmp_IO->GetLlcTmpErrorCode() != g_pLLCTmp_IO->NoError ? true : false;
			}

			if (g_pMCTmp_IO != NULL)
			{
				pCtrl->data.bConnMcTmp = g_pMCTmp_IO->Is_MC_Tmp_Connected();
				pCtrl->data.bMcTmpWorking = g_pMCTmp_IO->GetMcTmpState() == g_pMCTmp_IO->Running ? true : false;
				pCtrl->data.bMcTmpError = g_pMCTmp_IO->GetMcTmpErrorCode() == 0 ? true : false;
			}

			pCtrl->data.bConnAfm = FALSE;
			pCtrl->data.bConnRevolver = FALSE;

			Sleep(500);
		}
	}
	return 0;
}


