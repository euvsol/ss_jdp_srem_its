#pragma once

class CLoadingScreenDlg :
	public CDialogEx
{
	DECLARE_DYNAMIC(CLoadingScreenDlg)

public:
	CLoadingScreenDlg(CWnd* pParent = nullptr);
	virtual ~CLoadingScreenDlg();

	enum { IDD = IDD_LOADING_SCREEN_DIALOG };


	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CStatic			m_pic;
	CStatic			m_TextCtrl;
	CStatic			m_TitleCtrl;

	CString			m_sMessage;
	BOOL			m_bLoadingComp;

	HICON			m_LedIcon[3];

public:
	void SetTextMessage(CString sMsg);
	void SetTitleMessage(CString sMsg);
	void SetLoadingComplete();

};

