﻿#pragma once


// CAlarmDlg 대화 상자
//<ALARM_CATEGORY>
#define SYSTEM_ALARM		1
#define PROCESS_ALARM	2

//System Alarm Code List
#define	UNKNOWNSYSTEMERROR	-1900
#define	HOSTRESPONSETIMEOUT	-9501
#define	LLKMASKSENSINGFAIL	-9114

class CAlarmDlg : public CDialogEx, public CFile,  public CECommon
{
	DECLARE_DYNAMIC(CAlarmDlg)

public:
	CAlarmDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CAlarmDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ALARM_DIALOG };
//#endif

protected:

	CColorStaticST m_EditAlarmCodeCtrl;
	CColorStaticST m_EditAlarmTextCtrl;
	CString m_strAlarmText;
	CString m_strAlarmDiscription;
	CRichEditCtrl m_reCtrl;
	CFont Font;
	unsigned int m_uiCategory;
	int m_nAlarmCategory;
	int m_nAlarmCode;
	BOOL m_bAlarmSet;

	void InitWindowPos();
	void InitEditFont();
	void InitAlarmDiscriptTxt();
	int GetAlarmCategory(int alarmcode);
	BOOL GetAlarmInfoFromFile(int alarmcode);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	FILE* fp;
	BOOL OpenFile(LPCTSTR lpszFileName, UINT nOpenFlags = CFile::modeReadWrite);
	void Close();
	/** fp를 file의 처음위치로 이동시킨후 해당 Item을 index 갯수 만큼 찾아 있으면 갯수를 return
		fp는 Item의 다음에 위치. **/
	long FindItem(char* Item, int Index = 1);
	/** 현재 위치한 fp줄의 다음줄 처음 위치로 fp를 위치시킨다. **/
	int GetInt();
	/** 현재 위치한 fp 다음에 나오는 data를 double형으로 return한다. **/
	/** 현재 줄에서 fp가 위치한 다음부터 한 줄을 line에 입력하고 fp는 다음 줄에 위치. **/
	BOOL GetLine(char *line);
	/** 현재 위치한 fp 다음에 나오는 data를 buffer에 읽어들인다.
		fp는 data 다음에 위치.**/
	int GetItem(char* Buffer);

	/**
	 * Discription :Alarm 발생 message box display
	 * input
	 *       : alarmcode = (int)Alarm Code
	 *       : pchAlarmData = (char*) Alarm Data(로그에 기록될 내용)
	 */
	void SetAlarm(int alarmcode, char* pchAlarmData = "");

	void GetSelectedAlarmInfo(int nIndex);

	 CListCtrl m_AlarmList;
	 afx_msg void OnNMClickAlarmList(NMHDR *pNMHDR, LRESULT *pResult);
	 afx_msg void OnBnClickedOk();
	 afx_msg void OnBnClickedBuzzeroff();
};
