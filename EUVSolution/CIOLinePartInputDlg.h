﻿#pragma once


// CDigitalInputLinePart 대화 상자

class CDigitalInputLinePart : public CDialogEx
{
	DECLARE_DYNAMIC(CDigitalInputLinePart)

public:
	CDigitalInputLinePart(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDigitalInputLinePart();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_LINE_PART_INPUT_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	HICON	m_LedIcon[3];

	CBrush  m_brush;
	CBrush  m_brush2;
	CFont	m_font;

	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();

	void OnUpdateDigitalInput_Line_Part();
	void InitControls_DI_LINE_PART();
};
