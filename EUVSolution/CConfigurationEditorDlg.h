﻿/**
 * Equipment Configuration Editor Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

#define DATA_STRING_SIZE		128

 // CConfigurationEditorDlg 대화 상자
class CConfigurationEditorDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CConfigurationEditorDlg)

public:
	CConfigurationEditorDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CConfigurationEditorDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CONFIG_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	
	HICON			m_LedIcon[3];

	int				m_nEquipmentMode;					///< 0: OFF_LINE, 1: ONLINE

	void			InitialICon();
	void			PortCheck();

	void			ConnectPortviaDevice(int nModuleNumber);
	void			DisconnectPortviaDevice(int nModuleNumber);


	CFont	m_font;

	int				ReadFile();
	int				SaveFile();

	

//[Communication Config]
	//ETHERNET
	char			m_chNAME[ETHERNET_COM_NUMBER][125]{};
	char			m_chIP[ETHERNET_COM_NUMBER][125];
	char			m_chIPPORT[ETHERNET_COM_NUMBER][125];
	int				m_nPORT[ETHERNET_COM_NUMBER];
	//COM
	char			m_chPORT[SERIAL_COM_NUMBER][125];
	char			m_ch_nBAUD_RATE[SERIAL_COM_NUMBER][125];
	char			m_ch_nUSE_BIT[SERIAL_COM_NUMBER][125];
	char			m_ch_nSTOP_BIT[SERIAL_COM_NUMBER][125];
	char			m_ch_nPARITY[SERIAL_COM_NUMBER][125];

	int				m_nBAUD_RATE[SERIAL_COM_NUMBER];
	int				m_nUSE_BIT[SERIAL_COM_NUMBER];
	int				m_nSTOP_BIT[SERIAL_COM_NUMBER];
	int				m_nPARITY[SERIAL_COM_NUMBER];

	//IO
	char			m_chDi[DIGITAL_INPUT_NUMBER][125]{};
	char			m_chDo[DIGITAL_OUTPUT_NUMBER][125]{};
	char			m_chAi[ANALOG_INPUT_NUMBER][125]{};
	char			m_chAo[ANALOG_INPUT_NUMBER][125]{};

	// Ethernet 정보 읽어 오는 함수
	int			EthernetRead();

	// Ethernet 정보 파일에 쓰는 함수
	int			EthernetSave();

	// Serial 정보 읽어 오는 함수
	int			SerialRead();

	// Serial 정보 파일에 쓰는 함수
	int			SerialSave();

	int			ip_config_mode;
	int			serial_config_mode;


//[Vacuum Config]
	double	m_dPressure_ChangeToFast_MC_Rough_ini;
	double	m_dPressure_ChangeToFast_Rough_ini;
	double	m_dPressure_ChangeToMCTMP_Rough_ini;
	double	m_dPressure_Rough_End_ini;
	double	m_dPressure_ChangeToSlow_Vent_ini;
	double	m_dPressure_Vent_End_ini;
	double	m_dPressure_ChangeToVent_ini;
	double	m_dPressure_ChangeToSlow2_Vent_ini;
	double	m_dPressure_ChangeToVent_LLC_ini;
	double	m_dPressure_ChangeToTMP_Rough_ini;

	double  m_dPressure_ChangeToVent;			//McStandbyVentValue
	double	m_dPressure_ChangeToSlow_Vent;		//McSlowVentValue
	double	m_dPressure_Vent_End;				//LlcFastVentValue, McFastVentValue
	double	m_dPressure_ChangeToFast_MC_Rough;	//McSlowRoughValue
	double  m_dPressure_ChangeToMCTMP_Rough;	//McFastRoughValue
	double	m_dPressure_Rough_End;				//LlcTmpRoughValue, McTmpRoughValue

	double	m_dPressure_ChangeToVent_LLC;		//LlcStandbyVentValue
	double	m_dPressure_ChangeToSlow2_Vent;		//LlcSlowVentValue
	double	m_dPressure_ChangeToFast_Rough;		//LlcSlowRoughValue
	double	m_dPressure_ChangeToTMP_Rough;		//LlcFastRoughValue
	
	double	m_dPressure_ChangeToFast_Vent;
	double	m_dPressure_Vent_Tolerance;

	int		m_nTimeout_sec_MCLLCVent;		//McStandbyVentTime
	int		m_nTimeout_sec_MCSlow1Vent;		//McSlowVentTime
	int		m_nTimeout_sec_MCFastVent;		//McFastVentTime
	int		m_nTimeout_sec_MCSlowRough;		//McSlowRoughTime
	int		m_nTimeout_sec_MCFastRough;		//McFastRoughTime
	int		m_nTimeout_sec_MCTmpEnd;		//McTmpRoughTime

	int     m_nTimeout_sec_LLKStandbyVent;  //LlcStandbyVentTime
	int		m_nTimeout_sec_LLKSlow1Vent;    //LlcSlowVentTime
	int		m_nTimeout_sec_LLKFastVent;		//LlcFastVentTime
	int		m_nTimeout_sec_LLKSlowRough;	//LlcSlowRoughTime
	int		m_nTimeout_sec_LLKFastRough;	//LlcFastRoughTime
	int		m_nTimeout_sec_LLKRoughEnd;		//LlcTmpRoughTime
	
	int		m_nTimeout_sec_LLKVentEnd;
	int		m_nTimeout_sec_MCVentEnd;
	int		m_nTimeout_sec_MCRoughEnd;
	int		m_nTimeout_sec_ValveOperation;

	//time initial
	int		m_nTimeout_sec_LLKSlowRough_ini;
	int		m_nTimeout_sec_LLKFastRough_ini;
	int		m_nTimeout_sec_LLKRoughEnd_ini;
	int		m_nTimeout_sec_LLKSlow1Vent_ini;
	int     m_nTimeout_sec_LLKStandbyVent_ini;
	int		m_nTimeout_sec_LLKFastVent_ini;
	int		m_nTimeout_sec_LLKVentEnd_ini;
	int		m_nTimeout_sec_ValveOperation_ini;
	int		m_nTimeout_sec_MCSlowRough_ini;
	int		m_nTimeout_sec_MCFastRough_ini;
	int		m_nTimeout_sec_MCRoughEnd_ini;
	int		m_nTimeout_sec_MCTmpEnd_ini;
	int		m_nTimeout_sec_MCLLCVent_ini;
	int		m_nTimeout_sec_MCSlow1Vent_ini;
	int		m_nTimeout_sec_MCFastVent_ini;
	int		m_nTimeout_sec_MCVentEnd_ini;

	// Slow MFC Inlet Valve Open Range Setting ( 50 ~ 0.02 )
	// [ m_dPressure_ChangeToVent_LLC ~ m_dPressure_ChangeToFast_Rough ] 
	double	m_dPressure_SlowMFC_Inlet_Valve_Open_Value;

	void ReadVacuumSeqData();
	void SaveVacuumSeqData();


//[Navigation Stage Config]
	struct _Pos {
		double x;
		double y;
		char chStagePositionString[128];
	};
	_Pos m_stStagePos[MAX_STAGE_POSITION];


	/** Stage Position 데이터를  File에서 읽어 오는 함수 */
	void		ReadStagePositionFromDB();

	/** Stage Position 데이터를  File에 쓰는 함수 */
	void		SaveStagePositionToDB();

	void		BackupStagePositionToDB(CString sPath);

//[Recovery Config]

	/** Align 완료된 경우 Mask Unloading 되지 않았다면 프로그램 껏다켜도 Align 하지 않아도되게 추가 */
	double		m_dOMAlignPointLB_X_mm, m_dOMAlignPointLB_Y_mm;
	double		m_dOMAlignPointLT_X_mm, m_dOMAlignPointLT_Y_mm;
	double		m_dOMAlignPointRT_X_mm, m_dOMAlignPointRT_Y_mm;
	double		m_dEUVAlignPointLB_X_mm, m_dEUVAlignPointLB_Y_mm;
	double		m_dEUVAlignPointLT_X_mm, m_dEUVAlignPointLT_Y_mm;
	double		m_dEUVAlignPointRT_X_mm, m_dEUVAlignPointRT_Y_mm;

	BOOL		m_bOMAlignCompleteFlag;
	BOOL		m_bEUVAlignCompleteFlag;

	/** Recovery 시 MTS Rotate 및 Flip 했는지 확인 용 */
	BOOL		m_bMtsRotateDone_Flag;
	BOOL		m_bMtsFlipDone_Flag;

	int			m_nPreviousProcess;

	/** 현재 Material Location */
	int			m_nMaterialLocation;

	/** Recovery 데이터를  File에서 읽어 오는 함수 */
	void		ReadRecoveryData();

	/** Recovery 데이터를  File에 쓰는 함수 */
	void		SaveRecoveryData();
	
	void		SaveCurrentProcess(int nProcess);

	void		SaveMaterialLocation(int nLoc);

	void		SaveMtsRecoveryData();


//[Calibration Config]

	/** Stage Area */
	CRect		m_rcOMStageAreaRect;
	CRect		m_rcEUVStageAreaRect;

	/** OM Pixel Size */
	double		m_dOM_PixelSizeX_um;
	double		m_dOM_PixelSizeY_um;

	/** OM-EUV Offset */
	double		m_dGlobalOffsetX_mm;  // 140.7108;   // 140.727(LidOpen,SourceAlign); // 140.736(LidOpen); // 140.724; // 140.732;
	double		m_dGlobalOffsetY_mm;	 //   2.4086;   // 2.347(LidOpen,SourceAlign);   // 2.403(LidOpen);   // 2.445;   // 2.462;
	double		m_dLBOffsetX_mm;		 // 140.7098;   // 140.725(LidOpen,SourceAlign); // 140.735(LidOpen); // 140.721; // 140.732;
	double		m_dLBOffsetY_mm;		 //   2.4082;   // 2.347(LidOpen,SourceAlign);   // 2.403(LidOpen);   // 2.444;   // 2.461;
	double		m_dLTOffsetX_mm;		 // 140.7100;   // 140.725(LidOpen,SourceAlign); // 140.735(LidOpen); // 140.721; // 140.733;
	double		m_dLTOffsetY_mm;		 //   2.4079;   // 2.347(LidOpen,SourceAlign);   // 2.403(LidOpen);   // 2.444;   // 2.463;
	double		m_dRTOffsetX_mm;		 // 140.7126;   // 140.730(LidOpen,SourceAlign); // 140.739(LidOpen); // 140.727; // 140.732;
	double		m_dRTOffsetY_mm;		 //   2.4096;   // 2.348(LidOpen,SourceAlign);   // 2.405(LidOpen);   // 2.447;   // 2.464;

	/** 각종 Calibration 데이터를  File에서 읽어 오는 함수 */
	void		ReadCalibrationInfo();

	/** 각종 Calibration 데이터를  File에 쓰는 함수 */
	void		SaveCalibrationInfo();
	

	afx_msg void OnBnClickedButtonConfigCalLoad();
	afx_msg void OnBnClickedButtonConfigCalSave();
	afx_msg void OnEnChangeEditConfigCalTx();
	afx_msg void OnEnChangeEditConfigCalTy();
	afx_msg void OnEnChangeEditConfigCalGlobalOffsetx();
	afx_msg void OnEnChangeEditConfigCalGlobalOffsety();
	afx_msg void OnEnChangeEditConfigCalLbOffsetx();
	afx_msg void OnEnChangeEditConfigCalLbOffsety();
	afx_msg void OnEnChangeEditConfigCalLtOffsetx();
	afx_msg void OnEnChangeEditConfigCalLtOffsety();
	afx_msg void OnEnChangeEditConfigCalRtOffsetx();
	afx_msg void OnEnChangeEditConfigCalRtOffsety();
	afx_msg void OnBnClickedButtonSetAutoOffset();
	afx_msg void OnBnClickedButtonSetLbOffset();
	afx_msg void OnBnClickedButtonSetLtOffset();
	afx_msg void OnBnClickedButtonSetRtOffset();
	afx_msg void OnEnChangeEditConfigCalFocusCap1();
	afx_msg void OnEnChangeEditConfigCalFocusCap2();
	afx_msg void OnEnChangeEditConfigCalFocusCap3();
	afx_msg void OnEnChangeEditConfigCalFocusCap4();


	int m_nOMStageAreaLB_X_mm;
	int m_nOMStageAreaLB_Y_mm;
	int m_nOMStageAreaRT_X_mm;
	int m_nOMStageAreaRT_Y_mm;
	int m_nEUVStageAreaLB_X_mm;
	int m_nEUVStageAreaLB_Y_mm;
	int m_nEUVStageAreaRT_X_mm;
	int m_nEUVStageAreaRT_Y_mm;
	

//[ETC Configuration]

	//MTS Info
	BOOL		m_bUseFlip;
	BOOL		m_bUseRotate;
	int			m_nRotateAngle;

	int			m_nSourceAutoOffTime_min;
	double		m_dEUVVacuumRate;

	CButton		m_UseMtsRotateCtrl;
	CComboBox	m_MtsRotateAngleCtrl;

//[BS Calibration]
	int m_nBsSelectedIndex;
	int m_nBsExposureTime;

	double m_dBsBlankPosX;
	double m_dBsBlankPosY;
	double m_dBsCenterPosX;
	double m_dBsCenterPosY;
	char   m_chBsData[125];

	double m_dBSBGD1_Intensity;
	double m_dBSBGD1_Standardeviation;
	double m_dBSBGD2_Intensity;
	double m_dBSBGD2_Standardeviation;
	double m_dBSBGD3_Intensity;
	double m_dBSBGD3_Standardeviation;

	double m_dBSWOD1_Intensity;
	double m_dBSWOD1_Standardeviation;
	double m_dBSWOD2_Intensity;
	double m_dBSWOD2_Standardeviation;
	double m_dBSWOD3_Intensity;
	double m_dBSWOD3_Standardeviation;

	double m_dBSBSD1_Intensity;
	double m_dBSBSD1_Standardeviation;
	double m_dBSBSD2_Intensity;
	double m_dBSBSD2_Standardeviation;
	double m_dBSBSD3_Intensity;
	double m_dBSBSD3_Standardeviation;

	/** 각종 ETC 데이터(분류되지 않은 데이터)를  File에서 읽어 오는 함수 */
	void		ReadETCInfo();

	/** 각종 ETC 데이터(분류되지 않은 데이터)를  File에 쓰는 함수 */
	void		SaveETCInfo();

	void ReadBsCalibrationInfo();
	void SaveBsCalibrationInfo();
	void caculateBSCalibrationInfo();

	void saveFilterStageInfo();
	void readFilterStageInfo();

	double m_dFilterSavedPosition;

	afx_msg void OnBnClickedButtonConfigEtcLoad();
	afx_msg void OnBnClickedButtonConfigEtcSave();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnBnClickedConfigButtonConnectEthernet0();
	afx_msg void OnBnClickedConfigButtonConnectEthernet1();
	afx_msg void OnBnClickedConfigButtonConnectEthernet2();
	afx_msg void OnBnClickedConfigButtonConnectEthernet3();
	afx_msg void OnBnClickedConfigButtonConnectEthernet4();
	afx_msg void OnBnClickedConfigButtonConnectEthernet5();
	afx_msg void OnBnClickedConfigButtonConnectEthernet6();
	afx_msg void OnBnClickedConfigButtonConnectEthernet7();
	afx_msg void OnBnClickedConfigButtonConnectEthernet8();
	afx_msg void OnBnClickedConfigButtonConnectEthernet9();
	afx_msg void OnBnClickedConfigButtonConnectEthernet10();
	afx_msg void OnBnClickedConfigButtonConnectEthernet11();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet0();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet1();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet2();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet3();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet4();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet5();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet6();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet7();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet8();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet9();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet10();
	afx_msg void OnBnClickedConfigButtonDisconnectEthernet11();

	afx_msg void OnBnClickedConfigButtonOpenSerial0();
	afx_msg void OnBnClickedConfigButtonOpenSerial1();
	afx_msg void OnBnClickedConfigButtonOpenSerial2();
	afx_msg void OnBnClickedConfigButtonOpenSerial3();
	afx_msg void OnBnClickedConfigButtonOpenSerial4();
	afx_msg void OnBnClickedConfigButtonOpenSerial5();
	afx_msg void OnBnClickedConfigButtonOpenSerial6();
	afx_msg void OnBnClickedConfigButtonOpenSerial7();
	afx_msg void OnBnClickedConfigButtonOpenSerial8();
	afx_msg void OnBnClickedConfigButtonCloseSerial0();
	afx_msg void OnBnClickedConfigButtonCloseSerial1();
	afx_msg void OnBnClickedConfigButtonCloseSerial2();
	afx_msg void OnBnClickedConfigButtonCloseSerial3();
	afx_msg void OnBnClickedConfigButtonCloseSerial4();
	afx_msg void OnBnClickedConfigButtonCloseSerial5();
	afx_msg void OnBnClickedConfigButtonCloseSerial6();
	afx_msg void OnBnClickedConfigButtonCloseSerial7();
	afx_msg void OnBnClickedConfigButtonCloseSerial8();
};
