﻿// CIOPartOutputDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CDigitalOutputPart 대화 상자

IMPLEMENT_DYNAMIC(CDigitalOutputPart, CDialogEx)

CDigitalOutputPart::CDigitalOutputPart(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_PART_OUTPUT_DIALOG, pParent)
{

}

CDigitalOutputPart::~CDigitalOutputPart()
{
	m_brush.DeleteObject();
	m_brush2.DeleteObject();
	m_font.DeleteObject();
}

void CDigitalOutputPart::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDigitalOutputPart, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y000, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY000)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y001, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY001)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y002, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY002)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y003, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY003)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y004, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY004)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y005, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY005)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y006, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY006)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y007, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY007)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y008, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY008)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y009, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY009)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y010, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY010)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y011, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY011)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y012, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY012)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y013, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY013)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y014, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY014)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_PART_Y015, &CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY015)
END_MESSAGE_MAP()


// CDigitalOutputPart 메시지 처리기

BOOL CDigitalOutputPart::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_brush2.CreateSolidBrush(GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


HBRUSH CDigitalOutputPart::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if ((pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_PART_TEXT) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_PART_TEXT2) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_PART_TEXT3))
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
		else if (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_TEXT)
		{
			pDC->SetBkColor(GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush2;
		}
	}
	return hbr;
}
void CDigitalOutputPart::InitControls_DO_PART()
{

	CString strTemp;
	CString Get_str;
	CButton* btn;

	for (int nIdx = 0; nIdx < DIGITAL_IO_VIEW_NUMBER; nIdx++)
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y000 + nIdx))->SetIcon(m_LedIcon[0]);

	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM0 , "Y0011");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM1 , "Y0012");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM2 , "Y0107");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM3 , "Y0014");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM4 , "Y0109");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM5 , "Y0110");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM6 , "Y0005");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM7 , "Y0006");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM8 , "Y0007");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM9 , "Y0008");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM10 , "Y0013");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM11 , "Y0106");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM12 , "Y0010");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM13 , "Y0104");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM14 , "Y0009");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_NUM15 , "Y0103");


	SetTimer(OUTPUT_PART_DIALOG_TIMER, 100, NULL);
}

void CDigitalOutputPart::OnUpdateDigitalOutput_Part()
{

	CButton* btn;
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y000, "MC SLOW ROUGH V/V OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y000);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::MC_SLOW_ROUGHING] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y000))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y000))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y001, "MC FAST ROUGH V/V OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y001);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::MC_FAST_ROUGHING] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y001))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y001))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y002, "MC#1 TMP GATE OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y002);
	if (g_pIO->m_bDigitalOut[g_pIO->MC_TMP_GATE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y002))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y002))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y003, "MC FORELINE#1 V/V OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y003);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::MC_FORELINE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y003))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y003))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y004, "TR GATE OPEN");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y004);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y004))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y004))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y005, "TR GATE CLOSE");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y005);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y005))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y005))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y006, "LLC GATE OPEN");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y006);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y006))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y006))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y007, "LLC GATE CLOSE");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y007);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y007))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y007))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y008, "LLC SLOW ROUGH V/V OP/CL	");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y008);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::LLC_SLOW_ROUGHING] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y008))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y008))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y009, "LLC FAST ROUGH V/V OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y009);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::LLC_FAST_ROUGHING] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y009))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y009))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y010, "LLC FORELINE V/V OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y010);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::LLC_FORELINE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y010))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y010))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y011, "LLC TMP GATE OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y011);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::LLC_TMP_GATE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y011))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y011))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y012, "SLOW VENT INLET V/V OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y012);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::SLOW_VENT_INLET] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y012))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y012))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y013, "SLOW VENT OUTLET OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y013);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::SLOW_VENT_OUTLET] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y013))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y013))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y014, "FAST VENT INLET V/V OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y014);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::FAST_VENT_INLET] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y014))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y014))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}
	SetDlgItemText(IDC_STATIC_DIGITALOUT_PART_Y015, "FAST VENT OUTLET OP/CL");
	btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y015);
	if (g_pIO->m_bDigitalOut[g_pIO->DO::FAST_VENT_OUTLET] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y015))->SetIcon(m_LedIcon[1]);
		btn->SetCheck(true);
		btn->SetWindowTextA(_T("On"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_PART_Y015))->SetIcon(m_LedIcon[0]);
		btn->SetCheck(false);
		btn->SetWindowTextA(_T("Off"));
	}

}

void CDigitalOutputPart::SetOutputPartBitOnOff(int nIndex)
{
	int nRet;
	int ADDR, bitdex;
	CString state_str, mode_str, event_str;


	CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_PART_Y000 + nIndex);

	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		CString str;
		CString str_nCheck;

		int nCheck = btn->GetCheck();
		int SEL;

		if (nCheck)
		{
			state_str = _T("On/Open");
		}
		else
		{
			state_str = _T("Off/Close");
		}

		if (g_pIO->m_nIOMode == MAINT_MODE_ON)
		{
			mode_str = _T("MAINT_MODE");

			switch (nIndex)
			{
			case 0:
				str = "MC SLOW ROUGH V/V OP/CL";
				event_str = _T("MC Slow Roughing Valve");
				if (nCheck)
				{
					// FORELINE V/V 가 열려 있는데 ROUGH V/V를 열시 기계적 치명적인 손상을 유발 할 수 있다. 
					// 그러므로 MAINT 모드이지만, FORELINE V/V 상태 확인 후 진행
					//if ((g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FORELINE_VALVE_CLOSE] == false) || (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FORELINE_VALVE_OPEN] == true))
					if(g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSE)
					{
						str.Format(_T("MC ROUGH OPEN 불가 !! MC FORELINE VALVE OPEN 되어 있습니다"));
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
					else if (g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_CLOSE) 
						nRet = g_pIO->DigitalWriteIO(g_pIO->DO::MC_SLOW_ROUGHING, VALVE_OPEN);
					else
					{
						// OPEN 과 CLOSE 가 아닌 1:1, 0:0 -----> IO LINE INPUT 오류 임.
						str = "MC SLOW ROUGH OPEN 불가 !! MC FORELINE VALVE 확인 요망";
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
				}
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::MC_SLOW_ROUGHING, VALVE_CLOSE);
				break;
			case 1:
				str = "MC FAST ROUGH V/V OP/CL";
				event_str = _T("MC Fast Roughing Valve");
				if (nCheck)
				{
					// FORELINE V/V 가 열려 있는데 ROUGH V/V를 열시 기계적 치명적인 손상을 유발 할 수 있다. 
					// 그러므로 MAINT 모드이지만, FORELINE V/V 상태 확인 후 진행
					// m_bDigitalIn[108] MC FORELINE V/V OPEN STATE ,  m_bDigitalIn[109] MC FORELINE V/V CLOSE STATE ( 1 : 0 -> FORELINE V/V OPEN 상태 )  
					//if ((g_pIO->m_bDigitalIn[108] == true) && (g_pIO->m_bDigitalIn[109] == false))
					if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSE)
					{
						str.Format(_T("MC ROUGH OPEN 불가 !! MC FORELINE VALVE OPEN 되어 있습니다"));
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
					else if (g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_CLOSE)
						nRet = g_pIO->DigitalWriteIO(g_pIO->DO::MC_FAST_ROUGHING, VALVE_OPEN);
					else
					{
						str = "MC FAST ROUGH OPEN 불가 !! MC FORELINE VALVE 확인 요망";
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
				}
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::MC_FAST_ROUGHING, VALVE_CLOSE);
				break;
			case 2:
				str = "MC#1 TMP GATE OP / CL";
				event_str = _T("MC Tmp Valve");
				if (nCheck)
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::MC_TMP_GATE, VALVE_OPEN);
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::MC_TMP_GATE, VALVE_CLOSE);
				break;
			case 3:
				str = "MC FORELINE#1 V/V OP / CL";
				event_str = _T("MC Foreline Valve");
				if (nCheck)
				{
					// ROUGH V/V 가 열려 있는데 FORELINE V/V를 열시 기계적 치명적인 손상을 유발 할 수 있다. 
					// 그러므로 MAINT 모드이지만, ROUGH V/V 상태 확인 후 진행
					// m_bDigitalIn[105] MC ROUGHING V/V CLOSE STATE ,  m_bDigitalIn[103] MC SLOW ROUGHING V/V OPEN STATE ,  m_bDigitalIn[104] MC FAST ROUGHING V/V OPEN STATE ( 1 : 0 : 0 -> ROUGHING V/V CLOSE 상태 )  
					if ((g_pIO->m_bDigitalIn[g_pIO->DI::MC_ROUGHING_VALVE_CLOSE] == true) && (g_pIO->m_bDigitalIn[g_pIO->DI::MC_FAST_ROUGHING_VALVE_OPEN] == false) && (g_pIO->m_bDigitalOut[g_pIO->DO::MC_SLOW_ROUGHING] == false))
						nRet = g_pIO->DigitalWriteIO(g_pIO->DO::MC_FORELINE, VALVE_OPEN);
					else
					{
						str = "MC FORELINE OPEN 불가 !! MC Rough Valve Open 되어 있습니다";
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
				}
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::MC_FORELINE, VALVE_CLOSE);
				break;
			case 4:
				str = "TR GATE OPEN";
				event_str = _T("TR Gate Valve");
				if (nCheck)
					nRet = ((g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS, VALVE_OPEN)) || (g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS, VALVE_CLOSE)));
				else
					nRet = ((g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS, VALVE_CLOSE)) || (g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS, VALVE_OPEN)));
				break;
			case 5:
				str = "TR GATE CLOSE";
				event_str = _T("TR Gate Valve");
				if(nCheck)
					nRet = ((g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS, VALVE_CLOSE)) || (g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS, VALVE_OPEN)));
				else
					nRet = ((g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS, VALVE_OPEN)) || (g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS, VALVE_CLOSE)));
				break;
			case 6:
				str = "LLC GATE OPEN";
				event_str = _T("LLC Gate Valve");
				if (nCheck)
					nRet = ((g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS, VALVE_OPEN)) || (g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS, VALVE_CLOSE)));
				else
					nRet = ((g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS, VALVE_CLOSE)) || (g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS, VALVE_OPEN)));
				break;
			case 7:
				str = "LLC GATE CLOSE";
				event_str = _T("LLC Gate Valve");
				if(nCheck)
					nRet = ((g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS, VALVE_CLOSE)) || (g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS, VALVE_OPEN)));
				else
					nRet = ((g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS, VALVE_OPEN)) || (g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS, VALVE_CLOSE)));
				break;
			case 8:
				str = "LLC SLOW ROUGH V/V";
				event_str = _T("LLC Slow Roughing Valve");
				if (nCheck)
				{
					// FORELINE V/V 가 열려 있는데 ROUGH V/V를 열시 기계적 치명적인 손상을 유발 할 수 있다. 
					// 그러므로 MAINT 모드이지만, FORELINE V/V 상태 확인 후 진행
					if(g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
					{
						str = "LLC ROUGH VALVE OPEN 불가 !! LLC FORELINE VALVE OPEN 되어 있습니다";
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
					else if (g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_CLOSE)
						nRet = g_pIO->DigitalWriteIO(g_pIO->DO::LLC_SLOW_ROUGHING, VALVE_OPEN);
					else
					{
						// OPEN 과 CLOSE 가 아닌 1:1, 0:0 -----> IO LINE INPUT 오류 임.
						str = "LLC SLOW ROUGH OPEN 불가 !! LLC FORELINE VALVE 확인 요망";
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
				}
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::LLC_SLOW_ROUGHING, VALVE_CLOSE);
				
				break;
			case 9:
				str = "LLC FAST ROUGN V/V";
				event_str = _T("LLC Fast Roughing Valve");
				if (nCheck)
				{
					// FORELINE V/V 가 열려 있는데 ROUGH V/V를 열시 기계적 치명적인 손상을 유발 할 수 있다. 
					// 그러므로 MAINT 모드이지만, FORELINE V/V 상태 확인 후 진행
					if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
					{
						str = "LLC ROUGH VALVE OPEN 불가 !! LLC FORELINE VALVE OPEN 되어 있습니다";
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
					else if (g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_CLOSE)
						nRet = g_pIO->DigitalWriteIO(g_pIO->DO::LLC_FAST_ROUGHING, VALVE_OPEN);
					else
					{
						// OPEN 과 CLOSE 가 아닌 1:1, 0:0 -----> IO LINE INPUT 오류 임.
						str = "LLC SLOW ROUGH OPEN 불가 !! LLC FORELINE VALVE 확인 요망";
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
				}
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::LLC_FAST_ROUGHING, VALVE_CLOSE);
				break;
			case 10:
				str = "LLC FORELINE V/V";
				event_str = _T("LLC Foreline Valve");
				if (nCheck)
				{
				
					////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					//
					// ROUGH V/V 가 열려 있는데 FORELINE V/V를 열시 기계적 치명적인 손상을 유발 할 수 있다. 
					// 그러므로 MAINT 모드이지만, ROUGH V/V 상태 확인 후 진행
					// m_bDigitalIn[102] LLC ROUGHING V/V CLOSE STATE ,  m_bDigitalIn[100] LLC SLOW ROUGHING V/V OPEN STATE ,  m_bDigitalIn[101] MC FAST ROUGHING V/V OPEN STATE ( 1 : 0 : 0 -> ROUGHING V/V CLOSE 상태 )  
					//
					////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

					if ((g_pIO->m_bDigitalIn[g_pIO->DI::LLC_ROUGHING_VALVE_CLOSE] == true) && (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FAST_ROUGHING_VALVE_OPEN] == false) && (g_pIO->m_bDigitalOut[g_pIO->DO::LLC_SLOW_ROUGHING] == false))
						nRet = g_pIO->DigitalWriteIO(g_pIO->DO::LLC_FORELINE, VALVE_OPEN);
					else
					{
						str = "LLC FORELINE OPEN 불가 !! LLC Rough Valve Open 되어 있습니다";
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " :: " + str)));	//IO 버튼 Event 결과 상태 기록.
						::AfxMessageBox(str, MB_ICONSTOP);
					}
				}
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::LLC_FORELINE, VALVE_CLOSE);
				break;
			case 11:
				str = "LLC TMP GATE V/V";
				event_str = _T("LLC Tmp Gate Valve");
				if (nCheck)
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::LLC_TMP_GATE, VALVE_OPEN);
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::LLC_TMP_GATE, VALVE_CLOSE);
				break;
			case 12:
				str = "SLOW VENT INLET V/V";
				event_str = _T("Slow Vent Inlet Valve");
				if (nCheck)
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::SLOW_VENT_INLET, VALVE_OPEN);
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::SLOW_VENT_INLET, VALVE_CLOSE);
				break;
			case 13:
				str = "SLOW VENT OUTLET V/V";
				event_str = _T("Slow Vent Outlet Valve");
				if (nCheck)
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::SLOW_VENT_OUTLET, VALVE_OPEN);
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::SLOW_VENT_OUTLET, VALVE_CLOSE);
				break;
			case 14:
				str = "FAST VENT INLET V/V";
				event_str = _T("Fast Vent Inlet Valve");
				if (nCheck)
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::FAST_VENT_INLET, VALVE_OPEN);
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::FAST_VENT_INLET, VALVE_CLOSE);
				break;
			case 15:
				str = "FAST VENT OUTLET V/V";
				event_str = _T("Fast Vent Outlet Valve");
				if (nCheck)
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::FAST_VENT_OUTLET, VALVE_OPEN);
				else
					nRet = g_pIO->DigitalWriteIO(g_pIO->DO::FAST_VENT_OUTLET, VALVE_CLOSE);
				break;
			default:
				break;
			}
			

			g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " Click Evnet")));	//IO 버튼 Event 상태 기록.
			if (nRet != 0)
			{
				g_pAlarm->SetAlarm((IO_WRITE_ERROR));
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " 실행 실패 ")));	//IO 버튼 Event 결과 상태 기록.
			}
			else
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " 실행 완료 ")));	//IO 버튼 Event 결과 상태 기록.


		}
		else if (g_pIO->m_nIOMode == OPER_MODE_ON)
		{
		mode_str = _T("Oper Mode");
			if (nCheck)
			{
				str_nCheck = _T(" On 또는 Open");
				btn->SetWindowTextA(_T("On"));
			}
			else
			{
				str_nCheck = _T(" Off 또는 Close");
				btn->SetWindowTextA(_T("Off"));
			}

			switch (nIndex)
			{
			case 0:
				str = "MC SLOW ROUGH V/V";
				event_str = _T("Mc Slow Roughing Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_MC_SlowRoughValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str = "MC SLOW ROUGH V/V OPEN 실패.";
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.

					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_MC_SlowRoughValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str = "MC SLOW ROUGH V/V CLOSE 실패";
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.'
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 1:
				str = "MC FAST ROUGH V/V";
				event_str = _T("Mc Fast Roughing Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_MC_FastRoughValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str = "MC FAST ROUGH V/V OPEN 실패.";
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_MC_FastRoughValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str = "MC FAST ROUGH V/V CLOSE 실패";
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 2:
				str = "MC TMP GATE V/V";
				event_str.Format( _T("Mc Tmp Gate Valve"));
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_MC_TMP_GateValve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("MC TMP OPEN 실패."));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_MC_TMP_GateValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("MC TMP GATE V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 3:
				str = "MC TMP FORELINE V/V";
				event_str = _T("Mc Tmp Foreline Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_MC_TMP_ForelineValve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("MC TMP FORELINE V/V OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_MC_TMP_ForelineValve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("MC TMP FORELINE V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 4:
				str = "TR GATE OPEN";
				event_str = _T("TR Gate Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_TRGateValve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("TR GATE OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						// OPEN 을 OFF 시키는 Signal. OPEN을 OFF 한다고 하여 실질적으로 CLOSE 가 되지 않는다. CLOSE 되는 IO 가 별도 존재.
						nRet = g_pIO->Close_TRGateValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("TR GATE V/V OPEN SIGNAL OUT 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 5:
				str = "TR GATE CLOSE";
				event_str = _T("TR Gate Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Close_TRGateValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("TR GATE CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						// CLOSE 을 OFF 시키는 Signal. CLOSE를 OFF 한다고 하여 실질적으로 OPEN 되지 않는다. OPEN 되는 IO 가 별도 존재.
						nRet = g_pIO->Open_TRGateValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("TR GATE V/V CLOSE SIGNAL OUT 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 6:
				str = "LLC GATE OPEN";
				event_str = _T("LLC Gate Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_LLCGateValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC GATE OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_LLCGateValve();
						if (nRet != 0)
						{
							str.Format(_T("LLC GATE CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 7:
				str = "LLC GATE CLOSE";
				event_str = _T("LLC Gate Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Close_LLCGateValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC GATE CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Open_LLCGateValve();
						if (nRet != 0)
						{
							str.Format(_T("LLC GATE OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 8:
				str = "LLC SLOW ROUGN V/V";
				event_str = _T("LLC Slow Roughing Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_LLC_SlowRoughValve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC SLOW ROUGN V/V OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_LLC_SlowRoughValve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC SLOW ROUGH V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 9:
				str = "LLC FAST ROUGN V/V";
				event_str = _T("LLC Fast Roughing Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_LLC_FastRoughValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC FAST ROUGN V/V OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_LLC_FastRoughValve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC FAST ROUGH V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 10:
				str = "LLC FORELINE V/V";
				event_str = _T("LLC Foreline Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_LLC_TMP_ForelineValve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC FORELINE V/V OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_LLC_TMP_ForelineValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC TMP FORLIEN V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 11:
				str = "LLC TMP GATE V/V";
				event_str = _T("LLC Tmp Gate Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_LLC_TMP_GateValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC TMP GATE V/V OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_LLC_TMP_GateValve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("LLC TMP GATE V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 12:
				str = "SLOW VENT INLET V/V";
				event_str = _T("Slow Vent Inlet Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_SlowVent_Inlet_Valve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("SLOW VENT INLET V/V OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_SlowVent_Inlet_Valve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("SLOW VENT INLET GATE V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 13:
				str = "SLOW VENT OUTLET V/V";
				event_str = _T("Slow Vent Outlet Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_SlowVent_Outlet_Valve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("SLOW VENT OUTLET V/V OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_SlowVent_Outlet_Valve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("SLOW VENT OUTLET GATE V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 14:
				str = "FAST VENT INLET V/V";
				event_str = _T("Fast Vent Inlet Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_FastVent_Inlet_Valve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("FAST VENT INLET V/V OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_FastVent_Inlet_Valve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("FAST VENT INLET GATE V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.

					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			case 15:
				str = "FAST VENT OUTLET V/V";
				event_str = _T("Fast Vent Outlet Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					if (nCheck)
					{
						nRet = g_pIO->Open_FastVent_Outlet_Valve();
						if (nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("FAST VENT OUTLET V/V OPEN 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
					else if (!nCheck)
					{
						nRet = g_pIO->Close_FastVent_Outlet_Valve();
						if ( nRet != OPERATION_COMPLETED)
						{
							str.Format(_T("FAST VENT OUTLET GATE V/V CLOSE 실패"));
							g_pLog->Display(0, str);
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + str)));	//IO 버튼 Event 상태 기록.
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패 :: " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
							g_pAlarm->SetAlarm((nRet));
						}
						else
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Io_Main_Part [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				break;
			default:
				break;
			}
		}
	}
	else
	{
		btn->SetCheck(false);
		AfxMessageBox("IO 연결이 되어 있지 않습니다. 연결 확인 후 재 시도 해주세요");
	}

}

void CDigitalOutputPart::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == OUTPUT_PART_DIALOG_TIMER)
	{
		//if (g_pIO->m_Crevis_Open_Port == TRUE)
		{
			OnUpdateDigitalOutput_Part();
			SetTimer(OUTPUT_PART_DIALOG_TIMER, 100, NULL);
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CDigitalOutputPart::OnDestroy()
{
	CDialogEx::OnDestroy();
	KillTimer(OUTPUT_PART_DIALOG_TIMER);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}



void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY000()
{
	SetOutputPartBitOnOff(0);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY001()
{
	SetOutputPartBitOnOff(1);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY002()
{
	SetOutputPartBitOnOff(2);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY003()
{
	SetOutputPartBitOnOff(3);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY004()
{
	SetOutputPartBitOnOff(4);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY005()
{
	SetOutputPartBitOnOff(5);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY006()
{
	SetOutputPartBitOnOff(6);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY007()
{
	SetOutputPartBitOnOff(7);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY008()
{
	SetOutputPartBitOnOff(8);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY009()
{
	SetOutputPartBitOnOff(9);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY010()
{
	SetOutputPartBitOnOff(10);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY011()
{
	SetOutputPartBitOnOff(11);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY012()
{
	SetOutputPartBitOnOff(12);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY013()
{
	SetOutputPartBitOnOff(13);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY014()
{
	SetOutputPartBitOnOff(14);
}


void CDigitalOutputPart::OnBnClickedCheckDigitaloutPartY015()
{
	SetOutputPartBitOnOff(15);
}
