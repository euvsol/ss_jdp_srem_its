﻿/**
 * Vacuum Gauge Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CVacuumGaugeDlg 대화 상자

class CVacuumGaugeDlg : public CDialogEx , public CMKS390Gauge
{
	DECLARE_DYNAMIC(CVacuumGaugeDlg)

public:
	CVacuumGaugeDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CVacuumGaugeDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VACUUM_GAUGE_DIALOG };
//#endif

private:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();


	HICON m_LedIcon[3];
	int	  m_nGaugeState;
	BOOL  m_bThreadExitFlag;
	CWinThread*	m_Thread;
	static UINT UpdataThread(LPVOID pParam);
	
	void RequestData();
	void UpdateVacuumData();

	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()

public:
	int OpenDevice();
	void CloseDevice();

	int	   GetGaugeState()	  { return m_nGaugeState; }
	double GetMcVacuumRate()  { return m_dPressureMC; }
	double GetLlcVacuumRate() { return m_dPressureLLC; }
	BOOL Is_GAUGE_Connected() { return m_bSerialConnected; }
	
	enum
	{
		Error = 0,
		NotConnected,
		IDLE,
		Running,
	};
 };
