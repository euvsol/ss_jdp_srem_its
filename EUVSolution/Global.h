#pragma once

CEUVSOLUTIONDlg*				g_pEUVSolution = NULL;
CIODlg*							g_pIO = NULL;
CLogDisplayDlg*					g_pLog = NULL;
CMaskEFEMDlg*					g_pEfem = NULL;
CTurboPumpMCDlg*				g_pMCTmp_IO = NULL;
CTurboPumpDlg*					g_pLLCTmp_IO = NULL;
CVacuumGaugeDlg*				g_pGauge_IO = NULL;
CVacuumRobotDlg*				g_pVacuumRobot = NULL;
CConfigurationEditorDlg*		g_pConfig = NULL;
CHWCommStatusDlg*				g_pCommStat = NULL;
CAutoProcess*					g_pAP = NULL;
CVacuumProcess*					g_pVP = NULL;
CWarningDlg*					g_pWarning = NULL;
CAlarmDlg*						g_pAlarm = NULL;
CSubMenuDlg*					g_pSubMenu = NULL;
CSubTestMenuDlg*				g_pSubTestMenu = NULL;
CAnimationGUI*					g_pAnimationGUI		= NULL;
CMaindialog*					g_pMaindialog = NULL;
CLoadingScreenDlg				g_pLoadingScreen;
//CBeamConnect*					g_pBeamCon = NULL;


CPlasmaCleanerDlg*				g_pPlasmaCleaner = NULL;
CSubModuleDlg*					g_pSubModule = NULL;
CDeviceManager*					g_pDevMgr = NULL;

MIL_ID	                        g_milApplication = M_NULL;
MIL_ID	                        g_milSystemHost = M_NULL;
MIL_ID	                        g_milSystemGigEVision = M_NULL;