#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CLogDisplayDlg 대화 상자

IMPLEMENT_DYNAMIC(CLogDisplayDlg, CDialogEx)

CLogDisplayDlg::CLogDisplayDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LOG_DIALOG, pParent)
{

}

CLogDisplayDlg::~CLogDisplayDlg()
{
}

void CLogDisplayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLogDisplayDlg, CDialogEx)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CLogDisplayDlg 메시지 처리기


BOOL CLogDisplayDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

BOOL CLogDisplayDlg::Display(int nModule, CString strLogMsg, BOOL bDisplay, BOOL bSaveFile)
{
	CString strOuptLog;
	CListBox* pBox = (CListBox*)GetDlgItem(IDC_LOG_DISPLAY);
	if (pBox == NULL)
	{
		return FALSE;
	}
	int numItem = pBox->GetCount();
	if (numItem >= 1000)
	{
		pBox->ResetContent();
	}

	if (bDisplay)
	{
		CString strDate, strTime;
		GetSystemDateTime(&strDate, &strTime);

		CString str;
		str.Format("%s %s\r\n", strTime, strLogMsg);

		pBox->SetCurSel(pBox->AddString(str));
		pBox->GetHorizontalExtent();
		pBox->SetHorizontalExtent(256);
	}

	if (bSaveFile)
	{
		SaveLogFile("EUVSolution", (LPSTR)(LPCTSTR)strLogMsg);
	}

	return TRUE;
}

BOOL CLogDisplayDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CLogDisplayDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}
