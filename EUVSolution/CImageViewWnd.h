#pragma once
class CImageViewWnd :	public CWnd
{
public:
	CImageViewWnd();
	virtual ~CImageViewWnd();
	DECLARE_MESSAGE_MAP()

	typedef struct _MOUSEPOSITION
	{
		void Set(MIL_INT DisplayPositionX, MIL_INT DisplayPositionY, MIL_DOUBLE BufferPositionX, MIL_DOUBLE BufferPositionY)
		{
			m_DisplayPositionX = DisplayPositionX;
			m_DisplayPositionY = DisplayPositionY;
			m_BufferPositionX = BufferPositionX;
			m_BufferPositionY = BufferPositionY;
		}
		_MOUSEPOSITION()
		{
			Set(M_INVALID, M_INVALID, M_INVALID, M_INVALID);
		}
		_MOUSEPOSITION& operator=(const _MOUSEPOSITION& MousePosition)
		{
			Set(MousePosition.m_DisplayPositionX,
				MousePosition.m_DisplayPositionY,
				MousePosition.m_BufferPositionX,
				MousePosition.m_BufferPositionY);

			return *this;
		}
		MIL_INT     m_DisplayPositionX;
		MIL_INT     m_DisplayPositionY;
		MIL_DOUBLE  m_BufferPositionX;
		MIL_DOUBLE  m_BufferPositionY;
	} MOUSEPOSITION;
public:
	virtual BOOL Create(DWORD dwStyle, RECT& rect, CWnd* pParentWnd);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL DestroyWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void DoDataExchange(CDataExchange* pDX);

	afx_msg void OnPaint();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

	CRect rcClient;

	MIL_ID m_MilApplication; /* Application identifier.   */
	MIL_ID m_MilSystem;      /* System identifier.        */
	MIL_ID m_MilDisplay;

	//MIL_ID Lut;

	MIL_INT m_nDigSizeX;
	MIL_INT m_nDigSizeY;
	MIL_INT m_nDigBand;

	MIL_INT m_nCurrScaleMin;
	MIL_INT m_nCurrScaleMax;

	MIL_INT m_nSetScaleMin;
	MIL_INT m_nSetScaleMax;

	/** 현재 보이는 OM 화면 상에서 마우스 Point 위치 */
	BOOL m_bScaleImage;

	CRectTracker m_RectTracker;

	MIL_ID m_MilGraphList;
	MIL_ID m_MilGraphContext;

	MIL_ID m_MilFullScaleColorBuff;
	MIL_ID* m_MilColorCouponScaleBuff;
	MIL_ID** m_MilColorRegionScaleBuff;
	MIL_ID m_MilFullScaleMonoBuff;
	MIL_ID* m_MilMonoCouponScaleBuff;
	MIL_ID** m_MilMonoRegionScaleBuff;

	int m_nAllocCouponNumber;
	int* m_nAllocRegionNumber;

	int* m_CouponCornerXArray = NULL;
	int* m_CouponCornerYArray = NULL;
	int* m_CouponWidthArray = NULL;
	int* m_CouponHeightArray = NULL;

	int** m_RegionCornerXArray = NULL;
	int** m_RegionCornerYArray = NULL;
	int** m_RegionWidthArray = NULL;
	int** m_RegionHeightArray = NULL;

	double* m_CouponMinArray = NULL;
	double* m_CouponMaxArray = NULL;
	//
	//int* m_CurrentDataNum = NULL;

	double*** m_ImageDataRawArray = NULL;
	MIL_UINT8*** m_ImageDataUint8Array = NULL; // MONO
	MIL_UINT32*** m_ImageDataUint32Array = NULL; // RGB

	MIL_DOUBLE m_MilZoomLimitX = 1, m_MilZoomLimitY = 1;
	void zoomIn();
	void zoomOut();
	void zoomOn();

	void zoomInCenter();
	void zoomOutCenter();

	static MIL_INT MFTYPE MouseMoveFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr);
	static MIL_INT MFTYPE MouseLefeButtonUpFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr);
	static MIL_INT MFTYPE MouseLefeButtonDownFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr);
	afx_msg void OnParentNotify(UINT message, LPARAM lParam);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);

	BOOL m_bisMouseFlag;
	BOOL m_bMouseTracking = FALSE;

	MIL_DOUBLE m_dDisplayPosX;
	MIL_DOUBLE m_dDisplayPosY;
	MIL_DOUBLE m_dBufferPosX;
	MIL_DOUBLE m_dBufferPosY;

	CWinThread *m_pMoveImageThread;
	static UINT moveImage(LPVOID lParam);

	void allocDisplay(CProcessData *Data);
	void deleteDisplay();

	void allocMilImageInfo(CProcessData *Data);
	void deleteMilImageInfo();

	void allocMilImageBuffer();
	void deleteMilImageBuffer();

	void allocImageDataArray();
	void deleteImageDataArray();

	MIL_UINT32 packToBGR32(MIL_UINT8 blue, MIL_UINT8 green, MIL_UINT8 red);
	MIL_UINT8 getColorFromIndex(MIL_INT Band, MIL_INT Index, MIL_INT MaxIndex);
	MIL_INT getIndexFromRawData(double Data);
	MIL_INT MinVal(MIL_INT a, MIL_INT b);
	MIL_DOUBLE Remap(MIL_DOUBLE pos, MIL_DOUBLE size, MIL_DOUBLE min, MIL_DOUBLE max);
	MIL_UINT Mandelbrot(MIL_INT PosX, MIL_INT PosY, MIL_DOUBLE RefX, MIL_DOUBLE RefY, MIL_DOUBLE Dim);

	void mapData(int nCouponNumber = -1);
	void mapResultData(int nCouponIndex, double nLutLow, double nLutHigh);

	void drawMeasureData(int nCouponNumber);
	void loadMeasureData(int nCouponNumber, double* dMeasureData);

};