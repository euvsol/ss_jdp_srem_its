﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CTurboPumpMCDlg 대화 상자

IMPLEMENT_DYNAMIC(CTurboPumpMCDlg, CDialogEx)

CTurboPumpMCDlg::CTurboPumpMCDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TURBO_PUMP_MC_DIALOG, pParent)
{
	m_mc_tmp_Thread = NULL;
	m_mc_tmp_ThreadExitFlag = FALSE;	//ThreadExit 
	m_lock_state = 0;

	g_pDevMgr->RegisterObserver(this);
}

CTurboPumpMCDlg::~CTurboPumpMCDlg()
{
}

void CTurboPumpMCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTurboPumpMCDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_MC_TMP_ON, &CTurboPumpMCDlg::OnBnClickedMcTmpOn)
	ON_BN_CLICKED(IDC_MC_TMP_OFF, &CTurboPumpMCDlg::OnBnClickedMcTmpOff)
	ON_BN_CLICKED(IDC_MC_TMP_ERROR_RESET, &CTurboPumpMCDlg::OnBnClickedMcTmpErrorReset)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_MC_TMP_LOCK, &CTurboPumpMCDlg::OnBnClickedMcTmpLock)
END_MESSAGE_MAP()


// CTurboPumpMCDlg 메시지 처리기


BOOL CTurboPumpMCDlg::OnInitDialog()
{
	__super::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_CON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_3))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_SPEED_ON))->SetIcon(m_LedIcon[0]);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CTurboPumpMCDlg::OnDestroy()
{
	__super::OnDestroy();

	m_mc_tmp_ThreadExitFlag = TRUE;

	if (m_mc_tmp_Thread != NULL)
	{
		HANDLE threadHandle = m_mc_tmp_Thread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/3000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				m_mc_tmp_Thread->SuspendThread();
				m_mc_tmp_Thread->ExitInstance();
			}
		}

		delete m_mc_tmp_Thread;
		m_mc_tmp_Thread = NULL;
	}
}


BOOL CTurboPumpMCDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return __super::PreTranslateMessage(pMsg);
}


int CTurboPumpMCDlg::OpenDevice()
{
	CString sTemp;
	int nRet = OpenSerialPort(g_pConfig->m_chPORT[SERIAL_MC_TMP],
								g_pConfig->m_nBAUD_RATE[SERIAL_MC_TMP],
								g_pConfig->m_nUSE_BIT[SERIAL_MC_TMP],
								g_pConfig->m_nSTOP_BIT[SERIAL_MC_TMP],
								g_pConfig->m_nPARITY[SERIAL_MC_TMP]);

	if (nRet != 0) 
	{
		sTemp.Format(_T("Connection Fail"));
		m_mc_tmp_ThreadExitFlag = TRUE;

		SetDlgItemText(IDC_MC_TMP_HZ, sTemp);
		SetDlgItemText(IDC_MC_TMP_STATE, sTemp);
		SetDlgItemText(IDC_MC_TMP_ERROR_CODE, sTemp);
		SetDlgItemText(IDC_MC_TMP_ERROR_STATE, sTemp);
		SetDlgItemText(IDC_MC_TMP_TEMP2, sTemp);
		SetDlgItemText(IDC_MC_TMP_TEMP3, sTemp);
		((CStatic*)GetDlgItem(IDC_MC_ICON_CON))->SetIcon(m_LedIcon[2]);
		sTemp = _T("터보 펌프(MC) 연결에 실패했습니다.");
	}
	else 
	{
		m_mc_tmp_ThreadExitFlag = FALSE;
		m_mc_tmp_Thread = AfxBeginThread(Mc_Tmp_UpdataThread, (LPVOID)this, 0, 0, CREATE_SUSPENDED, 0);
		m_mc_tmp_Thread->m_bAutoDelete = FALSE;
		m_mc_tmp_Thread->ResumeThread();
		
		SetTimer(MC_TMP_UPDATE_TIMER, 100, NULL);
		((CStatic*)GetDlgItem(IDC_MC_ICON_CON))->SetIcon(m_LedIcon[1]);
		sTemp = _T("터보 펌프(MC) 연결에 성공했습니다.");
	}

	SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP : " + sTemp))); //통신 상태 기록.

	return nRet;
}

void CTurboPumpMCDlg::CloseDevice()
{

	m_mc_tmp_ThreadExitFlag = TRUE;

	if (m_mc_tmp_Thread != NULL)
	{
		HANDLE threadHandle = m_mc_tmp_Thread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/3000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				m_mc_tmp_Thread->SuspendThread();
				m_mc_tmp_Thread->ExitInstance();
			}
		}

		delete m_mc_tmp_Thread;
		m_mc_tmp_Thread = NULL;

	}

	CString str;
	CloseSerialPort();
	KillTimer(MC_TMP_UPDATE_TIMER);
	str.Format(_T("연결 상태 확인"));
	SetDlgItemText(IDC_MC_TMP_HZ, str);					// 속도 (hz)
	SetDlgItemText(IDC_MC_TMP_RPM, str);					// 속도 (rpm)
	SetDlgItemText(IDC_MC_TMP_HZ_SET, str);				// 설정 속도 (hz)
	SetDlgItemText(IDC_MC_TMP_RPM_SET, str);			// 설정 속도 (rpm)
	SetDlgItemText(IDC_MC_TMP_TEMP1, str);		// 전자장치온도
	SetDlgItemText(IDC_MC_TMP_TEMP2, str);		//펌프하단부온도
	SetDlgItemText(IDC_MC_TMP_TEMP3, str);		//모터온도
	SetDlgItemText(IDC_MC_TMP_TEMP4, str);		//모터온도
	SetDlgItemText(IDC_MC_TMP_ERROR_CODE, str);		// Error code
	((CStatic*)GetDlgItem(IDC_MC_ICON_CON))->SetIcon(m_LedIcon[2]);
}

void CTurboPumpMCDlg::Mc_Tmp_ReceiveData()
{
	CString str;

	str.Format(_T("%d hz"), m_nMcTmp_ReceiveDataHz);
	SetDlgItemText(IDC_MC_TMP_HZ, str);					// 속도 (hz)

	str.Format(_T("%d rpm"), m_nMcTmp_ReceiveDataRpm);
	SetDlgItemText(IDC_MC_TMP_RPM, str);					// 속도 (rpm)
	
	str.Format(_T("%d hz"), m_nMcTmp_ReceiveDataSetHz);
	SetDlgItemText(IDC_MC_TMP_HZ_SET, str);				// 설정 속도 (hz)

	str.Format(_T("%d rpm"), m_nMcTmp_ReceiveDataSetRpm);
	SetDlgItemText(IDC_MC_TMP_RPM_SET, str);			// 설정 속도 (rpm)

	str.Format(_T("%d ℃"), m_nMcTmp_ReceiveDataTemperature_1);
	SetDlgItemText(IDC_MC_TMP_TEMP1, str);		// 전자장치온도

	str.Format(_T("%d ℃"), m_nMcTmp_ReceiveDataTemperature_2);
	SetDlgItemText(IDC_MC_TMP_TEMP2, str);		//펌프하단부온도

	str.Format(_T("%d ℃"), m_nMcTmp_ReceiveDataTemperature_3);
	SetDlgItemText(IDC_MC_TMP_TEMP3, str);		//모터온도

	str.Format(_T("%d ℃"), m_nMcTmp_ReceiveDataTemperature_4);
	SetDlgItemText(IDC_MC_TMP_TEMP4, str);		//모터온도

	
	str.Format(_T("%05d"), m_nMcTmp_ReceiveDataErrorCode);
	SetDlgItemText(IDC_MC_TMP_ERROR_CODE, str);		// Error code

	switch (m_nMcTmp_ReceiveDataOnOffState)
	{
	case 0:
		str.Format(_T("TMP Turn Off"));
		break;
	case 1:
		str.Format(_T("TMP Turn On"));
		break;
	default:
		str.Format(_T("???"));
		break;
	}
	SetDlgItemText(IDC_MC_TMP_ON_OFF, str);		// ON/OFF 상태


	SetDlgItemText(IDC_MC_TMP_ERROR_STATE, str_mc_tmp_ReceiveDataErrorState);	// Error 설명

	switch (m_nMcTmp_ReceiveDataState)
	{
	case Stop:
		str.Format(_T("Stop"));
		break;
	case TMP_TURN_OFF:
		str.Format(_T("Tmp Turn Off"));
		break;
	case TMP_TURN_ON:
		str.Format(_T("Tmp Turn On"));
		break;
	case Deceleration:
		str.Format(_T("Deceleration"));
		break;
	case Acceleration:
		str.Format(_T("Acceleration"));
		break;
	case Running:
		str.Format(_T("Running"));
		break;
	case Fail:
		str.Format(_T("Fail"));
		break;
	default:
		str.Format(_T("???"));
		break;
	}
	SetDlgItemText(IDC_MC_TMP_STATE, str);				// tmp 상태

	if (m_Mc_Tmp_Speed_SetOn == true)
		((CStatic*)GetDlgItem(IDC_MC_ICON_SPEED_ON))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_MC_ICON_SPEED_ON))->SetIcon(m_LedIcon[0]);

	if (m_nMcTmp_ReceiveDataState == Stop)
	{
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[2]);

		//if (g_pIO->m_nERROR_MODE == RUN)	//jkseo 확인
		//{
		//	str.Format(_T("Error Mode 발생"));
		//	SaveLogFile("MC_TMP_LOG", str);
		//	EmergencyStop(); // TEST 및 수정 필요. ( Sequence 에 따른 Error mode 확인 필요 )
		//}
	}
	else if (m_nMcTmp_ReceiveDataState == Running)
	{
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	}
	else if (m_nMcTmp_ReceiveDataState == Deceleration)
	{
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[0]);
	}
	else if (m_nMcTmp_ReceiveDataState == Acceleration)
	{
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[0]);
	}
	else if (m_nMcTmp_ReceiveDataState == Error)
	{
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_2))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	}
}

UINT CTurboPumpMCDlg::Mc_Tmp_UpdataThread(LPVOID pParam)
{
	CTurboPumpMCDlg*  mc_tmp_runthread = (CTurboPumpMCDlg*)pParam;

	while (!mc_tmp_runthread->m_mc_tmp_ThreadExitFlag)
	{
		mc_tmp_runthread->TmpSendUpdate();
		Sleep(1000);
	}

	return 0;
}

void CTurboPumpMCDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == MC_TMP_UPDATE_TIMER)
	{
		if (!m_mc_tmp_ThreadExitFlag)
		{
			Mc_Tmp_ReceiveData();
			SetTimer(MC_TMP_UPDATE_TIMER, 100, NULL);
		}
	}

	__super::OnTimer(nIDEvent);
}

void CTurboPumpMCDlg::OnBnClickedMcTmpOn()
{
	if (m_bSerialConnected != TRUE) return;

	int nRet = Mc_Tmp_On();
	if (nRet != 0)
		AfxMessageBox(_T("MC 터보 펌프 On 동작이 실패했습니다."));
}

void CTurboPumpMCDlg::OnBnClickedMcTmpOff()
{
	if (m_bSerialConnected != TRUE) return;

	int nRet = Mc_Tmp_Off();
	if (nRet != 0)
		AfxMessageBox(_T("MC 터보 펌프 Off 동작이 실패했습니다."));
}

void CTurboPumpMCDlg::OnBnClickedMcTmpErrorReset()
{
	if (m_bSerialConnected != TRUE) return;

	int nRet = g_pMCTmp_IO->ResetPump();
	if (nRet != 0)
		AfxMessageBox(_T("MC 터보 펌프 Reset이 실패했습니다."));
}

int CTurboPumpMCDlg::Mc_Tmp_Off()
{
	int nRet = g_pMCTmp_IO->SetPUmpOff();
	if (nRet != 0)
		return RUN;
	else
		return RUN_OFF;
}

int CTurboPumpMCDlg::Mc_Tmp_On()
{
	int nRet = g_pMCTmp_IO->SetPumpOn();
	if (nRet != 0)
		return RUN_OFF;
	else
		return RUN;
}

void CTurboPumpMCDlg::TmpSendUpdate()
{
	int ret = -1;

	// TMP HZ READ
	ret = GetTmpHz();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpHz() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpHz()")));	//통신 상태 기록.

	//ERROR VIEW	
	ret = GetErrorView();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetErrorView() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetErrorView()")));	//통신 상태 기록.

	//셋팅 Hz
	ret = GetTmpSetHz();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetHz() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetHz()")));	//통신 상태 기록.

	//셋팅 rpm
	ret = GetTmpSetRpm();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetRpm() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetRpm()")));	//통신 상태 기록.

	//실제 rpm
	ret = GetTmpRpm();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpRpm() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpRpm()")));	//통신 상태 기록.

	//목표 속도 도달 
	ret = GetTmpSetSpeedOn();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetSpeedOn() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetSpeedOn()")));	//통신 상태 기록.

	//전자 장치 온도
	ret = GetTmpElectronicDeviceTemp();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpElectronicDeviceTemp() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpElectronicDeviceTemp()")));	//통신 상태 기록.


	//펌프 하단부 온도
	ret = GetTmpLowerPartTemp();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpLowerPartTemp() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpLowerPartTemp()")));	//통신 상태 기록.

	//모터온도	
	ret = GetTmpMotorTemp();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpMotorTemp() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpMotorTemp()")));	//통신 상태 기록.

	//베어링 온도	
	ret = GetTmpBearingTemp();
	Sleep(300);
	if (ret != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpBearingTemp() Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpBearingTemp()")));	//통신 상태 기록.
}

void CTurboPumpMCDlg::OnBnClickedMcTmpLock()
{
	if (m_lock_state)
	{
		GetDlgItem(IDC_MC_TMP_ON)->EnableActiveAccessibility();	//jkseo ??
		GetDlgItem(IDC_MC_TMP_OFF)->EnableActiveAccessibility();
		GetDlgItem(IDC_MC_TMP_ON)->EnableWindow(false);
		GetDlgItem(IDC_MC_TMP_OFF)->EnableWindow(false);
		m_lock_state = 0;
	}
	else
	{
		GetDlgItem(IDC_MC_TMP_ON)->EnableWindow(true);
		GetDlgItem(IDC_MC_TMP_OFF)->EnableWindow(true);
		m_lock_state = 1;
	}
}

void CTurboPumpMCDlg::EmergencyStop()
{
	TRACE(_T("MCTMP START\n"));

	if (g_pIO->Off_McDryPump_Valve())
	{
		CString log_str = " MAIN_ERROR_ON() : Error 발생 후 MC TMP STOP , MC Dry Pump Off 실행 !";
		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		//////////////////////////////////////////////////////////////////
		// TMP 완전 멈춤, 그후 DRY PUMP OFF 후 MC FORELINE GATE CLOSE
		//////////////////////////////////////////////////////////////////

		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤, 그 후 MC Foreline Gate  Close !";
		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		if (g_pIO->Close_MC_TMP_ForelineValve() != OPERATION_COMPLETED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 명령 에러 발생 !";
			SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, log_str);
			g_pLog->Display(0, g_pIO->Log_str);
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 명령 에러 발생으로 인해 재 시도 !";
			SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, log_str);
			g_pLog->Display(0, g_pIO->Log_str);
		
			if (g_pIO->Close_MC_TMP_ForelineValve() != OPERATION_COMPLETED)
			{
				log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 명령 에러 발생 !";
				SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
				SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
				g_pLog->Display(0, log_str);
				g_pLog->Display(0, g_pIO->Log_str);
				return;
			}
		}

		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_CLOSE) break;
		}

		if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 확인 에러 발생 !";
			SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			g_pLog->Display(0, log_str);

			return ;
		}

		log_str = "MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 확인 완료!";
		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

	}
	else
	{
		CString log_str = " MAIN_ERROR_ON() : Error 발생 후 MC TMP STOP , MC Dry Pump Off 실행 Error (확인필요) !";
		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}

	TRACE(_T("MCTMP END\n"));
}