#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

CBeamConnect::CBeamConnect()
{
	m_strReceiveEndOfStreamSymbol.Empty();
	InitializeData();
}

CBeamConnect::~CBeamConnect()
{
	CloseDevice();
	m_strReceiveEndOfStreamSymbol.Empty();
}

int	CBeamConnect::OpenDevice()
{
	int nRet = 0;
	nRet = OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_BEAM], g_pConfig->m_nPORT[ETHERNET_BEAM], TRUE, 100);
	if (nRet == 0)
	{
		m_BeamConnect_IpAddr = g_pConfig->m_chIP[ETHERNET_BEAM];
		m_BeamConnect_nPort = g_pConfig->m_nPORT[ETHERNET_BEAM];
		return nRet;
	}
	else
	{
		// 연결 실패
		return nRet;
	}

}

void CBeamConnect::CloseDevice()
{
	CloseTcpIpSocket();
}
void CBeamConnect::InitializeData()
{
	m_beam_bConnected = FALSE;
	DataReceiveOnFlagForBeam = FALSE;
	m_Adam_Data_On = FALSE;
	//m_strReceiveEndOfStreamSymbol = _T("CRLF");
	m_strReceiveEndOfStreamSymbol = _T("\r\n");
}


int	CBeamConnect::WaitReceiveEventThread()
{
	int		nRet = 0;
	int		i = 0;
	
	CString Temp;

	char*	pInBuf = new char[m_nReceiveMaxBufferSize];
	char*	pOutBuf = new char[m_nReceiveMaxBufferSize];
	int		total = 0;
	int		nCondition = 0;
	unsigned long	tmp = 0;
	struct timeval timeout;
	fd_set fds;
	SOCKET	tmpSocket = INVALID_SOCKET;
	if (m_bIsServerSocket)
	{
		tmpSocket = m_AcceptSocket;
	}
	else
	{
		tmpSocket = m_Socket;
	}

	TRACE("TCPIP Beam Optimization Program Receive Thread Start..\n");

	CString strLog;
	strLog = _T("TCPIP Beam Optimization Program Receive Thread Start");
	//SaveLogFile("BeamOptimization_TCPIP_Log", (LPSTR)(LPCTSTR)strLog);

	memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
	while (!m_bKillReceiveThread)
	{
		m_bConnected = TRUE;
		timeout.tv_sec = 0;
		timeout.tv_usec = 100;
		FD_ZERO(&fds);
		FD_SET(m_Socket, &fds);
		m_nSocketStatus = select(sizeof(fds) * 8, &fds, NULL, NULL, &timeout);

		if (m_nSocketStatus == -1)
		{
			m_bKillReceiveThread = TRUE;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);

			
			Temp.Format(_T("::::::: m_nSocketStatus [ -1 ] IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
			//SaveLogFile("BeamOptimization_TCPIP_Log", (LPSTR)(LPCTSTR)Temp);
		}

		if (tmpSocket != INVALID_SOCKET)
		{
			memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
			int bytesReceived = recv(tmpSocket, pInBuf, m_nReceiveMaxBufferSize, NULL);

			if (m_bKillReceiveThread)
			{
				break;
			}

			SetEvent(m_hSocketEvent);
			if (bytesReceived > 0)
			{
				memcpy(pOutBuf, pInBuf, sizeof(byte) * bytesReceived);
				ReceiveData(pOutBuf, bytesReceived);
			}
			else
			{
				memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
				memset(pInBuf, '\0', m_nReceiveMaxBufferSize);

				CString strLog;
				strLog.Format("[Error]BeamOptimizationProgram bytesReceived: bytesReceived = %d", bytesReceived);
				//SaveLogFile("BeamOptimization_TCPIP_Log", (LPSTR)(LPCTSTR)strLog);

				m_bKillReceiveThread = TRUE;
				////////////////////////////////////////
				//Client Socket 제거
				////////////////////////////////////////
				closesocket(m_AcceptSocket);

				m_AcceptSocket = INVALID_SOCKET;
				////////////////////////////////////
				m_bConnected = FALSE;
				nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
				m_nRet = nRet;
				m_beam_bConnected = FALSE;
				//SetEvent(m_hSocketEvent);			
			}
		}
		else
		{
			if (m_bIsServerSocket)
			{
				if (m_beam_bConnected)
				{
					tmpSocket = m_AcceptSocket;
				}
			}
			else
			{
				m_Socket = INVALID_SOCKET;
				m_beam_bConnected = FALSE;
			}

			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}

		WaitSec(0.5);
	}

	delete[] pInBuf;
	pInBuf = NULL;
	delete[] pOutBuf;
	pOutBuf = NULL;

	TRACE("TCPIP Beam Optimization Program Receive Thread End..\n");


	return nRet;

}

int	CBeamConnect::ReceiveData(char *lParam, int nLength)
{
	int nRet = 0;
	char* Senddata;
	char* command;
	CString str;


	//memset(m_BeamReceivedbuf, '\0', nLength);
	//memcpy(m_BeamReceivedbuf, lParam, nLength);

	Senddata = lParam;
	DataReceiveOnFlagForBeam = true;

	//command = strtok(Senddata, "CRLF");
	command = strtok(Senddata, m_strReceiveEndOfStreamSymbol);
	
	g_pAdam->UpdateBeamReceiveData(command);
	
	str.Format("%s", command);
	if (str == _T("Get"))
	{
		GetReadAdamData();
		//AfxMessageBox(_T("Send 완료"));
	}

	//GetAdamData();
	SetSendData();

	return nRet;
}

int CBeamConnect::On_Accept()
{
	int nRet = 0;
	CString strLog;
	strLog = _T("Accept Beam Optimization Program Server On_Accept Thread Start.");
	//TRACE("Accept Beam Optimization Program Server On_Accept Thread Start..\n");
	TRACE(strLog + "\n");
	
	SaveLogFile("BeamOptimization_TCPIP_Log", (LPSTR)(LPCTSTR)strLog);

	while (!m_bKillAcceptThread)
	{
		if (m_AcceptSocket == INVALID_SOCKET)
		{
			m_AcceptSocket = accept(m_Socket, NULL, NULL);
			m_beam_bConnected = TRUE;
			//
			// Client 에 의해 강제 종료된 Receive reOpen
			//
			m_bKillReceiveThread = FALSE;
			m_pReceiveThread = ::AfxBeginThread(ReceiveThreadFunc, this, THREAD_PRIORITY_NORMAL, 0, 0);
		}
		WaitSec(0.01);
	}

	strLog = _T("Accept Beam Optimization Program Server On_Accept Thread End..");
	TRACE(strLog+"\n");
	SaveLogFile("BeamOptimization_TCPIP_Log", (LPSTR)(LPCTSTR)strLog);
	
	return nRet;

}

int CBeamConnect::GetReadAdamData()
{
	////////////////////////////////////////////////////////
	// Adam Cap Read Command
	////////////////////////////////////////////////////////
	int nRet = 0;

	if (g_pAdam->Command_AverageRunAfterTimeout()) //return 0면 정상.. 아니면 error사항.. 
	{
		::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
		m_Adam_Data_On = FALSE;
	}
	else
	{	
		///////////////////////////////////////////////////////////
		// Adam 연결 시
		///////////////////////////////////////////////////////////
		//AdamData.m_Detector1 = g_pAdam->AdamData.m_nDetector1;
		//AdamData.m_Detector2 = g_pAdam->AdamData.m_nDetector2;
		//AdamData.m_Detector3 = g_pAdam->AdamData.m_nDetector3;
		//AdamData.m_Detector4 = g_pAdam->AdamData.m_nDetector4;
		//AdamData.m_Capsensor1 = g_pAdam->AdamData.m_dCapsensor1;
		//AdamData.m_Capsensor2 = g_pAdam->AdamData.m_dCapsensor2;
		//AdamData.m_Capsensor3 = g_pAdam->AdamData.m_dCapsensor3;
		//AdamData.m_Capsensor4 = g_pAdam->AdamData.m_dCapsensor4;

		///////////////////////////////////////////////////////////
		// Adam Simulator
		///////////////////////////////////////////////////////////

		GetAdamData.m_Capsensor1 = g_pAdam->AdamData.m_dCapsensor1;
		GetAdamData.m_Capsensor2 = g_pAdam->AdamData.m_dCapsensor2;
		GetAdamData.m_Capsensor3 = g_pAdam->AdamData.m_dCapsensor3;
		GetAdamData.m_Capsensor4 = g_pAdam->AdamData.m_dCapsensor4;
		GetAdamData.m_Detector1 = g_pAdam->AdamData.m_dDetector1;
		GetAdamData.m_Detector2 = g_pAdam->AdamData.m_dDetector2;
		GetAdamData.m_Detector3 = g_pAdam->AdamData.m_dDetector3;
		GetAdamData.m_Detector4 = g_pAdam->AdamData.m_dDetector4;


		m_Adam_Data_On = TRUE;
	}
	
	return nRet;
}

int CBeamConnect::SetSendData()
{
	int nRet = 0;
	char double_data[1000];
	double double_data_0 = 0.00000;
	double double_data_1 = 1.11111;
	double double_data_2 = 2.22222;
	double double_data_3 = 3.33333;
	double double_data_4 = 4.44444;
	double double_data_5 = 5.55555;
	double double_data_6 = 6.66666;
	double double_data_7 = 7.77777;
	double double_data_8 = 8.88888;
	BOOL bool_data_0 = TRUE;
	BOOL bool_data_1 = FALSE;
	BOOL bool_data_2 = TRUE;

	//char* comma = _T(",");
	//char* m_cr = _T("\r");
	//char* m_lr = _T("\n");

	//sprintf(double_data, "%f%s%f%s%f%s%f%s%f%s%f%s%f%s%f%s%f%s%d%s%d%s%d%s%s%s", double_data_0, comma, double_data_1, comma, double_data_2, comma, double_data_3, comma, double_data_4, comma, double_data_5, comma,
		//double_data_6, comma, double_data_7, comma, double_data_8, comma, bool_data_0, comma, bool_data_1, comma, bool_data_2, comma, m_cr, m_lr);

	//test = double_data;
	//SendDataForBeam = new char[strlen(double_data)];
	//memcpy(SendDataForBeam, double_data, strlen((double_data)-1));
	
	if (m_Adam_Data_On)
	{
		int size;
		memset(double_data, -1, 1000);
		//size = sprintf(double_data, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d\r\n", double_data_0, double_data_1, double_data_2, double_data_3, double_data_4, double_data_5, double_data_6, double_data_7, double_data_8, bool_data_0, bool_data_1, bool_data_2);

		size = sprintf(double_data, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d\r\n", GetAdamData.m_Detector1, GetAdamData.m_Detector2, GetAdamData.m_Detector3, GetAdamData.m_Detector4, GetAdamData.m_Capsensor1, GetAdamData.m_Capsensor2, GetAdamData.m_Capsensor3, GetAdamData.m_Capsensor4,double_data_8, bool_data_0, bool_data_1, bool_data_2);
		//Send(double_data, 6000, 3);
		SendData(size, double_data);

		//int size2;
		//size2 = strlen(double_data);
	}
	else
	{
		
		int size;
		//char* str;
		//str = _T("SendFail");
		//SendData(sizeof(str), str);

		memset(double_data, -1, 1000);
		size = sprintf(double_data, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d\r\n", double_data_0, double_data_1, double_data_2, double_data_3, double_data_4, double_data_5, double_data_6, double_data_7, double_data_8, bool_data_0, bool_data_1, bool_data_2);
		SendData(size, double_data);
	}
	return nRet;
}


int CBeamConnect::SendData(int nSize, char * lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	CString Temp;
	Temp.Format(_T("SREM -> BeamOptimization : Send Data(Size %d)"), nSize);
	SaveLogFile("BeamOptimization_TCPIP_Log", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
	nRet = Send(nSize, lParam, nTimeOut);
	if (nRet != 0)
	{
		Temp.Format(_T("SREM -> BeamOptimization : SEND FAIL (%d)"), nRet);
		SaveLogFile("BeamOptimization_TCPIP_Log", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
	}
	return nRet;
}