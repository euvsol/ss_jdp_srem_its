#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include "tinyxml2.h"

using namespace tinyxml2;

#define MAX_ALIGN_NUM  4

CProcessData::CProcessData()
{
	//pMeasureList = NULL;	
	TotalCouponNum = 0;
	TotalMeasureNum = 0;
	MeasurePointNum = 0;
	MeasureScanNum = 0;
	TotalAlignNum = 0;
	RecipeGenDateTime.Empty();
	EquipmentModelName.Empty();
	Substrate.Empty();
	SubstrateID.Empty();

	ResetData();
}

CProcessData::~CProcessData()
{
	ResetData();
}

void CProcessData::ResetData()
{
	RecipeGenDateTime.Empty();
	EquipmentModelName.Empty();
	Substrate.Empty();
	SubstrateID.Empty();
	TotalCouponNum = 0;
	TotalMeasureNum = 0;
	MeasurePointNum = 0;
	MeasureScanNum = 0;
	TotalAlignNum = 0;
	m_stOriginCoordinate_um.X = m_stOriginCoordinate_um.Y = 0.0;
	m_stBeamDiagnosisPos_um.X = m_stBeamDiagnosisPos_um.Y = 0.0;
	for (int i = 0; i < MAX_ALIGN_NUM; i++)
	{
		m_stAlignmentPoint_um[i].X = 0.0;
		m_stAlignmentPoint_um[i].Y = 0.0;
	}
	
	ClearCouponData();
}

int CProcessData::LoadHeaderInfo(char *fpath)
{	
	int ret = 0;

	tinyxml2::XMLDocument doc;
	ret = doc.LoadFile(fpath);
	if (ret != XML_SUCCESS)
		return RECIPE_LOAD_XML_FAIL;

	XMLElement* root = doc.RootElement();
	XMLElement* MainElement = root->FirstChildElement("EquipmentModelName");
	if (MainElement == NULL) return RECIPE_LOAD_HEADER_MAINELEMENT_FAIL;
	EquipmentModelName = MainElement->GetText();

	MainElement = root->FirstChildElement("Substrate");
	if (MainElement == NULL) return RECIPE_LOAD_HEADER_MAINELEMENT_FAIL;
	Substrate = MainElement->GetText();

	MainElement = root->FirstChildElement("SubstrateID");
	if (MainElement == NULL) return RECIPE_LOAD_HEADER_MAINELEMENT_FAIL;
	SubstrateID = MainElement->GetText();

	MainElement = root->FirstChildElement("SubstrateSize");
	if (MainElement == NULL) return RECIPE_LOAD_HEADER_MAINELEMENT_FAIL;
	else
	{
		XMLElement* SubElement = MainElement->FirstChildElement("Width");
		SubstrateWidth = atof(SubElement->GetText());
		SubElement = SubElement->NextSiblingElement();
		SubstrateHeight = atof(SubElement->GetText());
	}

	//MainElement = root->FirstChildElement("Lot");
	//if (MainElement == NULL) return -2;
	//Lot = MainElement->GetText();
	//
	//MainElement = root->FirstChildElement("Step");
	//if (MainElement == NULL) return -2;
	//Step = MainElement->GetText();
	//
	//MainElement = root->FirstChildElement("Device");
	//if (MainElement == NULL) return -2;
	//Device = MainElement->GetText();
	//
	//MainElement = root->FirstChildElement("Slot");
	//if (MainElement == NULL) return -2;
	//Slot = atoi(MainElement->GetText());

	//MainElement = root->FirstChildElement("SubstrateDirection");
	//if (MainElement == NULL) return -2;
	//SubstrateDirection = MainElement->GetText();

	MainElement = root->FirstChildElement("SubstrateCenterPos");
	if (MainElement == NULL) 
		return RECIPE_LOAD_HEADER_MAINELEMENT_FAIL;
	else
	{
		XMLElement* SubElement = MainElement->FirstChildElement("PosX");
		m_stSubstrateCenterPos_um.X = atof(SubElement->GetText());
		SubElement = SubElement->NextSiblingElement();
		m_stSubstrateCenterPos_um.Y = atof(SubElement->GetText());
	}

	MainElement = root->FirstChildElement("AlignmentData");
	if (MainElement == NULL)
		return RECIPE_LOAD_HEADER_MAINELEMENT_FAIL;

	XMLElement* SubElement = MainElement->FirstChildElement("Align");
	if (SubElement == NULL)	return RECIPE_LOAD_HEADER_SUBELEMENT_FAIL;
	else
	{
		for (XMLElement* ele = SubElement; ele != NULL; ele = ele->NextSiblingElement())
		{
			XMLElement* data = ele->FirstChildElement("PosX");
			m_stAlignmentPoint_um[TotalAlignNum].X = atoi(data->GetText());
			data = ele->FirstChildElement("PosY");
			m_stAlignmentPoint_um[TotalAlignNum].Y = atoi(data->GetText());
			TotalAlignNum++;
		}
	}

	MainElement = root->FirstChildElement("OriginCoordinate");
	if (MainElement == NULL) return RECIPE_LOAD_HEADER_MAINELEMENT_FAIL;
	else
	{
		XMLElement* SubElement = MainElement->FirstChildElement("PosX");
		m_stOriginCoordinate_um.X = atof(SubElement->GetText());
		SubElement = SubElement->NextSiblingElement();
		m_stOriginCoordinate_um.Y = atof(SubElement->GetText());
	}

	MainElement = root->FirstChildElement("BeamDiagnosisPos");
	if (MainElement == NULL)
		return RECIPE_LOAD_HEADER_MAINELEMENT_FAIL;
	else
	{
		XMLElement* SubElement = MainElement->FirstChildElement("PosX");
		m_stBeamDiagnosisPos_um.X = atof(SubElement->GetText());
		SubElement = SubElement->NextSiblingElement();
		m_stBeamDiagnosisPos_um.Y = atof(SubElement->GetText());
	}

	//MainElement = root->FirstChildElement("BeamDiagnosisExposureTime");
	//if (MainElement == NULL) return -2;
	//m_nBeamDiagPosExposureTime_ms = atoi(MainElement->GetText());


	MainElement = root->FirstChildElement("CouponData");
	if (MainElement == NULL) return RECIPE_LOAD_HEADER_MAINELEMENT_FAIL;

	SubElement = MainElement->FirstChildElement("Coupon");
	if (SubElement == NULL)	return RECIPE_LOAD_HEADER_SUBELEMENT_FAIL;
	else
	{
		for (XMLElement* ele = SubElement; ele != NULL; ele = ele->NextSiblingElement())
		{
			_CouponList coupon;
			XMLElement* data = ele->FirstChildElement("No");
			coupon.No = atoi(data->GetText());

			data = ele->FirstChildElement("Width");
			coupon.Width = atof(data->GetText());

			data = ele->FirstChildElement("Height");
			coupon.Height = atof(data->GetText());

			data = ele->FirstChildElement("PosX");
			coupon.CenterPosX = atof(data->GetText());

			data = ele->FirstChildElement("PosY");
			coupon.CenterPosY = atof(data->GetText());

			AddCouponData(coupon);
		}
	}

	MainElement = root->FirstChildElement("MeasureData");
	if (MainElement != NULL)
	{
		SubElement = MainElement->FirstChildElement("PointData");
		if (SubElement != NULL)
		{
			XMLElement* Element = SubElement->FirstChildElement("Point");

			for (XMLElement* ele = Element; ele != NULL; ele = ele->NextSiblingElement())
			{
				int nGridX, nGridY;
				XMLElement* data;
				data = ele->FirstChildElement("GridX");
				nGridX = atoi(data->GetText());
				data = ele->FirstChildElement("GridY");
				nGridY = atoi(data->GetText());
				if (nGridX == 1 && nGridY == 1)
					MeasurePointNum++;
				else
					MeasurePointNum += (nGridX * nGridY);
			}
		}

		SubElement = MainElement->FirstChildElement("ScanData");
		if (SubElement != NULL)
		{
			XMLElement* Element = SubElement->FirstChildElement("Scan");

			for (XMLElement* ele = Element; ele != NULL; ele = ele->NextSiblingElement())
			{
				int nGridX, nGridY;
				XMLElement* data;
				data = ele->FirstChildElement("GridX");
				nGridX = atoi(data->GetText());
				data = ele->FirstChildElement("GridY");
				nGridY = atoi(data->GetText());
				if (nGridX == 1 && nGridY == 1)
					MeasureScanNum++;
				else
					MeasureScanNum += (nGridX * nGridY);
			}
		}

		TotalMeasureNum = MeasurePointNum + MeasureScanNum;
	}
	return 0;
}

int CProcessData::LoadPTRMeasureData(char *fpath)
{
	int ret = 0;

	tinyxml2::XMLDocument doc;
	ret = doc.LoadFile(fpath);
	if (ret != XML_SUCCESS)
		return RECIPE_LOAD_XML_FAIL;

	XMLElement* root = doc.RootElement();

	XMLElement* MainElement = root->FirstChildElement("MeasureData");
	if (MainElement == NULL)
		return RECIPE_LOAD_MASUREDATA_MAINELEMENT_FAIL;

	XMLElement* SubElement = MainElement->FirstChildElement("PointData");
	if (SubElement != NULL)
	{
		XMLElement* Element = SubElement->FirstChildElement("Point");

		for (XMLElement* ele = Element; ele != NULL; ele = ele->NextSiblingElement())
		{
			_MeasurePointList point;
			XMLElement* data = ele->FirstChildElement("No");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_NO_FAIL;
			point.No = atoi(data->GetText());

			data = ele->FirstChildElement("CouponNo");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_COUPONNO_FAIL;
			point.CouponNo = atoi(data->GetText());

			data = ele->FirstChildElement("GridX");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_GRIDX_FAIL;
			point.GridX = atoi(data->GetText());

			data = ele->FirstChildElement("GridY");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_GRIDY_FAIL;
			point.GridY = atoi(data->GetText());

			data = ele->FirstChildElement("PosX");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_POSX_FAIL;
			point.CenterPosX = atof(data->GetText());

			data = ele->FirstChildElement("PosY");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_POSY_FAIL;
			point.CenterPosY = atof(data->GetText());

			data = ele->FirstChildElement("PitchX");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_PITCHX_FAIL;
			point.PitchX = atof(data->GetText());

			data = ele->FirstChildElement("PitchY");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_PITCHY_FAIL;
			point.PitchY = atof(data->GetText());

			data = ele->FirstChildElement("ExposureTime");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_EXPOSURE_FAIL;
			point.ExposureTime = atof(data->GetText());

			data = ele->FirstChildElement("RepeatNum");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_REPEATNUM_FAIL;
			point.RepeatNum = atoi(data->GetText());

			if (point.GridX == 1 && point.GridY == 1)
			{
				for (auto iter = GetCoupons().begin(); iter != GetCoupons().end(); iter++)
				{
					if (iter->No == point.CouponNo)
						iter->AddPointData(point);
				}
			}
			else
			{
				double dTempPosX = point.CenterPosX - (point.PitchX * (0.5 * (point.GridX - 1)));
				double dTempPosY = point.CenterPosY + (point.PitchY * (0.5 * (point.GridY - 1)));

				for (int row = 0; row < point.GridY; row++)
				{
					for (int col = 0; col < point.GridX; col++)
					{
						point.CenterPosX = dTempPosX + (point.PitchX * col);
						point.CenterPosY = dTempPosY - (point.PitchY * row);

						for (auto iter = GetCoupons().begin(); iter != GetCoupons().end(); iter++)
						{
							if (iter->No == point.CouponNo)
								iter->AddPointData(point);
						}
					}
				}
			}
		}
	}

	SubElement = MainElement->FirstChildElement("ScanData");
	if (SubElement != NULL)
	{
		XMLElement* Element = SubElement->FirstChildElement("Scan");

		for (XMLElement* ele = Element; ele != NULL; ele = ele->NextSiblingElement())
		{
			_MeasureScanList scan;
			XMLElement* data = ele->FirstChildElement("No");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_NO_FAIL;
			scan.No = atoi(data->GetText());

			data = ele->FirstChildElement("CouponNo");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_COUPONNO_FAIL;
			scan.CouponNo = atoi(data->GetText());

			data = ele->FirstChildElement("GridX");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_GRIDX_FAIL;
			scan.GridX = atoi(data->GetText());

			data = ele->FirstChildElement("GridY");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_GRIDY_FAIL;
			scan.GridY = atoi(data->GetText());

			data = ele->FirstChildElement("Width");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_WIDTH_FAIL;
			scan.Width = atof(data->GetText());

			data = ele->FirstChildElement("Height");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_HEIGHT_FAIL;
			scan.Height = atof(data->GetText());

			data = ele->FirstChildElement("PosX");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_POSX_FAIL;
			scan.CenterPosX = atof(data->GetText());

			data = ele->FirstChildElement("PosY");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_POSY_FAIL;
			scan.CenterPosY = atof(data->GetText());

			data = ele->FirstChildElement("PitchX");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_PITCHX_FAIL;
			scan.PitchX = atof(data->GetText());

			data = ele->FirstChildElement("PitchY");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_PITCHY_FAIL;
			scan.PitchY = atof(data->GetText());

			data = ele->FirstChildElement("Velocity");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_VELOCITY_FAIL;
			scan.Velocity = atof(data->GetText());

			data = ele->FirstChildElement("Distance");
			if (data == NULL)
				return RECIPE_LOAD_MASUREDATA_ELEMENT_DISTANCE_FAIL;
			scan.Distance = atof(data->GetText());

			if (scan.GridX == 1 && scan.GridY == 1)
			{
				for (auto iter = GetCoupons().begin(); iter != GetCoupons().end(); iter++)
				{
					if (iter->No == scan.CouponNo)
						iter->AddScanData(scan);
				}
			}
			else
			{
				double dTempPosX = scan.CenterPosX - (scan.PitchX * (0.5 * (scan.GridX - 1)));
				double dTempPosY = scan.CenterPosY + (scan.PitchY * (0.5 * (scan.GridY - 1)));

				for (int row = 0; row < scan.GridY; row++)
				{
					for (int col = 0; col < scan.GridX; col++)
					{
						scan.CenterPosX = dTempPosX + (scan.PitchX * col);
						scan.CenterPosY = dTempPosY - (scan.PitchY * row);

						for (auto iter = GetCoupons().begin(); iter != GetCoupons().end(); iter++)
						{
							if (iter->No == scan.CouponNo)
								iter->AddScanData(scan);
						}
					}
				}
			}
		}
	}

	return 0;
}
