#include "stdafx.h"
#include "CPfeifferHiPace2300TMPCtrl.h"


CPfeifferHiPace2300TMPCtrl::CPfeifferHiPace2300TMPCtrl()
{
	m_hPumpOnEvent	  = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hPumpOffEvent	  = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hResetPumpEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_nMcTmp_ReceiveDataHz = 0;
	m_nMcTmp_ReceiveDataSetHz = 0;
	m_nMcTmp_ReceiveDataState = 0;
	m_nMcTmp_ReceiveDataErrorCode = 0;
	m_nMcTmp_ReceiveDataErrorState = 0;
	m_nMcTmp_ReceiveDataOnOffState = 0;
	m_nMcTmp_ReceiveDataRpm = 0;
	m_nMcTmp_ReceiveDataSetRpm = 0;
	m_nMcTmp_ReceiveDataTemperature_1 = 0;
	m_nMcTmp_ReceiveDataTemperature_2 = 0;
	m_nMcTmp_ReceiveDataTemperature_3 = 0;
	m_nMcTmp_ReceiveDataTemperature_4 = 0;
	m_nMcTmp_Error_War_State = 0;
	m_nMcTmp_ReveiveDataSetHz = 0;
	m_nMcTmp_ReveiveDataSetRpm = 0;
	str_mc_tmp_ReceiveDataErrorState.Empty();
	str_CR = "\r";
	m_nPumpPreData = 0;
}

CPfeifferHiPace2300TMPCtrl::~CPfeifferHiPace2300TMPCtrl()
{
	CloseHandle(m_hPumpOnEvent);
	CloseHandle(m_hPumpOffEvent);
	CloseHandle(m_hResetPumpEvent);
}

int CPfeifferHiPace2300TMPCtrl::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	m_strSendMsg = lParam;
	nRet = Send(lParam, nTimeOut, nRetryCnt);
	
	if (nRet != 0)
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : " + m_strSendMsg + "  Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : " + m_strSendMsg)));	//통신 상태 기록.

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::ReceiveData(char *lParam, DWORD dwRead)
{
	CString	str_GetMCTMPRecevieData;
	str_GetMCTMPRecevieData.Empty();

	str_GetMCTMPRecevieData = (LPSTR)lParam;
	ParsingData(str_GetMCTMPRecevieData);

	return 0;
}

void CPfeifferHiPace2300TMPCtrl::ParsingData(CString strRecvMsg)
{
	int	cnt = 0;
	int pre_data = 0;

	CString	StrTempLog;
	CString strRecvTmpMsg = strRecvMsg;
	CString StrRecvTmpMsgCommand;				// 명령값 추출
	CString StrRecvTmpMsgSubDataCommand;		// 리시브 값 추출
	CString StrRevvTmpMsgErrWarCommand;			// Error 코드 중 Error , Warning 구분
	CString StrRevvTmpMsgErrCode;				// Error 코드 
	CString str_mc_tmp_ReceiveDataState;


	StrRecvTmpMsgCommand = strRecvTmpMsg.Mid(5, 3);			 //명령값
	StrRecvTmpMsgSubDataCommand = strRecvTmpMsg.Mid(10, 6);  //데이터값

	SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("MC_TMP -> PC [RECEIVE]:  Command : " + StrRecvTmpMsgCommand + "    ReceiveData : " + StrRecvTmpMsgSubDataCommand))); //통신 상태 기록.

	// 펌핑스테이션
	if (StrRecvTmpMsgCommand == _T("010"))
	{
		if (StrRecvTmpMsgSubDataCommand == _T("000000"))
		{
			m_nMcTmp_ReceiveDataOnOffState = 0;
			SetEvent(m_hPumpOffEvent);
		}
		else if (StrRecvTmpMsgSubDataCommand == "111111")
		{
			m_nMcTmp_ReceiveDataOnOffState = 1;
			SetEvent(m_hPumpOnEvent);
		}
		else
		{
			strRecvTmpMsg = _T("ERROR");
			SetEvent(m_hPumpOnEvent);
			SetEvent(m_hPumpOffEvent);
		}
	}
	// 실제 회전속도 (hz)
	else if (StrRecvTmpMsgCommand == _T("309"))
	{
		m_nMcTmp_ReceiveDataHz = atoi(StrRecvTmpMsgSubDataCommand);

		if (m_nMcTmp_ReceiveDataHz == 0)
		{
			m_nMcTmp_ReceiveDataState = 0;
			str_mc_tmp_ReceiveDataState = _T("Stop");
		}
		if (m_nMcTmp_ReceiveDataHz == 525)
		{
			m_nMcTmp_ReceiveDataState = 5;
			str_mc_tmp_ReceiveDataState = _T("Running");
		}
		else if (m_nMcTmp_ReceiveDataHz < m_nPumpPreData)
		{
			m_nMcTmp_ReceiveDataState = 3;
			str_mc_tmp_ReceiveDataState = _T("Deceleration");
			SetEvent(m_hPumpOffEvent);
		}
		else if (m_nMcTmp_ReceiveDataHz > m_nPumpPreData)
		{
			m_nMcTmp_ReceiveDataState = 4;
			str_mc_tmp_ReceiveDataState = _T("Acceleration");
			SetEvent(m_hPumpOnEvent);
		}
		else if (m_nMcTmp_ReceiveDataHz == m_nPumpPreData)
		{
			cnt++;
			if (cnt == 2000) 
			{
				m_nMcTmp_ReceiveDataState = 5;
				str_mc_tmp_ReceiveDataState = _T("Running");
				cnt = 0;
			}
		}
		m_nPumpPreData = m_nMcTmp_ReceiveDataHz;

		StrTempLog.Format(_T("%d hz"), m_nMcTmp_ReceiveDataHz);
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("MC_TMP -> PC [RECEIVE]: " + str_mc_tmp_ReceiveDataState + " ::  " + StrTempLog))); //통신 상태 기록.
	}
	////목표 속도 도달 
	else if (StrRecvTmpMsgCommand == _T("306"))
	{
		if (StrRecvTmpMsgSubDataCommand == "000000") m_Mc_Tmp_Speed_SetOn = false;
		else if (StrRecvTmpMsgSubDataCommand == "111111") m_Mc_Tmp_Speed_SetOn = true;
		else m_Mc_Tmp_Speed_SetOn = false;
	}
	//설정 Hz
	else if (StrRecvTmpMsgCommand == _T("308"))
	{
		m_nMcTmp_ReceiveDataSetHz = atoi(StrRecvTmpMsgSubDataCommand);
		StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: 설정 Hz  :: %d hz"), m_nMcTmp_ReceiveDataSetHz);
		SaveLogFile("MC_TMP_Com", StrTempLog); //통신 상태 기록.
	}
	//설정 rpm
	else if (StrRecvTmpMsgCommand == _T("397"))
	{
		m_nMcTmp_ReceiveDataSetRpm = atoi(StrRecvTmpMsgSubDataCommand);
		StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: 설정 Rpm  :: %d Rpm"), m_nMcTmp_ReceiveDataSetRpm);
		SaveLogFile("MC_TMP_Com", StrTempLog); //통신 상태 기록.
	}
	//현재 rpm
	else if (StrRecvTmpMsgCommand == _T("398"))
	{
		m_nMcTmp_ReceiveDataRpm = atoi(StrRecvTmpMsgSubDataCommand);
		StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: 현재 Rpm :: %d Rpm"), m_nMcTmp_ReceiveDataRpm);
		SaveLogFile("MC_TMP_Com", StrTempLog); //통신 상태 기록.
	}
	//전자 장치 온도
	else if (StrRecvTmpMsgCommand == _T("326"))
	{
		m_nMcTmp_ReceiveDataTemperature_1 = atoi(StrRecvTmpMsgSubDataCommand);
		StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: 전자 장치 온도 :: %d ℃"), m_nMcTmp_ReceiveDataTemperature_1);
		SaveLogFile("MC_TMP_Com", StrTempLog); //통신 상태 기록.
	}
	//펌프 하단부 온도
	else if (StrRecvTmpMsgCommand == _T("330"))
	{
		m_nMcTmp_ReceiveDataTemperature_2 = atoi(StrRecvTmpMsgSubDataCommand);
		StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: 펌프 하단부 온도 ::  %d ℃"), m_nMcTmp_ReceiveDataTemperature_2);
		SaveLogFile("MC_TMP_Com", StrTempLog); //통신 상태 기록.
	}
	//모터온도	
	else if (StrRecvTmpMsgCommand == _T("346"))
	{
		m_nMcTmp_ReceiveDataTemperature_3 = atoi(StrRecvTmpMsgSubDataCommand);
		StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: 모터 온도 ::  %d ℃"), m_nMcTmp_ReceiveDataTemperature_3);
		SaveLogFile("MC_TMP_Com", StrTempLog); //통신 상태 기록.
	}
	//베어링 온도	
	else if (StrRecvTmpMsgCommand == _T("342"))
	{
		m_nMcTmp_ReceiveDataTemperature_4 = atoi(StrRecvTmpMsgSubDataCommand);
		StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: 베어링 온도 ::  %d ℃"), m_nMcTmp_ReceiveDataTemperature_4);
		SaveLogFile("MC_TMP_Com", StrTempLog); //통신 상태 기록.
	}
	//오류코드
	else if (StrRecvTmpMsgCommand == _T("303"))
	{
		StrRevvTmpMsgErrWarCommand = StrRecvTmpMsgSubDataCommand.Mid(1, 3);
		m_nMcTmp_ReceiveDataErrorCode = atoi(StrRecvTmpMsgSubDataCommand.Mid(4, 3));
		if (StrRevvTmpMsgErrWarCommand == _T("Err"))
		{
			m_nMcTmp_ReceiveDataState = 7;
			str_mc_tmp_ReceiveDataState = "Error";
			StrRevvTmpMsgErrCode = StrRecvTmpMsgSubDataCommand.Mid(4, 3);
			m_nMcTmp_ReceiveDataErrorCode = atoi(StrRevvTmpMsgErrCode);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("MC_TMP -> PC [RECEIVE]: Error  :: %d" + m_nMcTmp_ReceiveDataErrorCode))); //통신 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE : %d " + m_nMcTmp_ReceiveDataErrorCode))); //ERROR 상태 기록
		}

		if (m_nMcTmp_ReceiveDataErrorCode == 1)
		{
			str_mc_tmp_ReceiveDataErrorState = "과잉 회전 속도";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 2)
		{
			str_mc_tmp_ReceiveDataErrorState = "과잉 전압";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 6)
		{
			str_mc_tmp_ReceiveDataErrorState = "런업 오류(확인필요)";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 7)
		{
			str_mc_tmp_ReceiveDataErrorState = "작동유 낮음";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 8)
		{
			str_mc_tmp_ReceiveDataErrorState = "전자드라이브 유닛 연결 결함";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 10)
		{
			str_mc_tmp_ReceiveDataErrorState = "내부 장치 오류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 21)
		{
			str_mc_tmp_ReceiveDataErrorState = "전자드라이브 유닛 터보 펌프 감지 못함 발생";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 41)
		{
			str_mc_tmp_ReceiveDataErrorState = "드라이브 결함";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 43)
		{
			str_mc_tmp_ReceiveDataErrorState = "내부 구성 오류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 44)
		{
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도, 전자 장치";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 45)
		{
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도, 모터";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 46)
		{
			str_mc_tmp_ReceiveDataErrorState = "내부 초기화 오류 (장치 결함)";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 73)
		{
			str_mc_tmp_ReceiveDataErrorState = "축 방향의 자기 베어링 과부하 (압력 상승률이 너무 높음)";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 74)
		{
			str_mc_tmp_ReceiveDataErrorState = "방시상의 자기 베어링 과부하 (압력 상승률이 너무 높음)";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 89)
		{
			str_mc_tmp_ReceiveDataErrorState = "로터 불안정";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 91)
		{
			str_mc_tmp_ReceiveDataErrorState = "내부 장치 오류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 92)
		{
			str_mc_tmp_ReceiveDataErrorState = "알수 없는 연결 패널";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 93)
		{
			str_mc_tmp_ReceiveDataErrorState = "모터 온도 평가 결함";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 94)
		{
			str_mc_tmp_ReceiveDataErrorState = "전자 장치 온도 평가 결함";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 98)
		{
			str_mc_tmp_ReceiveDataErrorState = "내부 통신 오류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 107)
		{
			str_mc_tmp_ReceiveDataErrorState = "최종 단계 그룹 오류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 108)
		{
			str_mc_tmp_ReceiveDataErrorState = "회전 속도 측정 결함";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 109)
		{
			str_mc_tmp_ReceiveDataErrorState = "소프트웨어 릴리스 되지 않음";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 110)
		{
			str_mc_tmp_ReceiveDataErrorState = "작동유 센서 결함";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 111)
		{
			str_mc_tmp_ReceiveDataErrorState = "작동유 펌프 통신 오류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 112)
		{
			str_mc_tmp_ReceiveDataErrorState = "작동유 펌프 그룹 오류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 113)
		{
			str_mc_tmp_ReceiveDataErrorState = "로터온도부정확";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 114)
		{
			str_mc_tmp_ReceiveDataErrorState = "최종 단계 온도 평가 결함";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 117)
		{
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도 펌프 하단부";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 118)
		{
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도 최종 단계";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 119)
		{
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도 베어링";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 143)
		{
			str_mc_tmp_ReceiveDataErrorState = "작동유 펌프 과잉 온도";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 777)
		{
			str_mc_tmp_ReceiveDataErrorState = "공칭 회전 속도가 확인 되지 않음";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 800)
		{
			str_mc_tmp_ReceiveDataErrorState = "자기 베어링 과류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 802)
		{
			str_mc_tmp_ReceiveDataErrorState = "자기 베어링 센서 기술 결함";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 810)
		{
			str_mc_tmp_ReceiveDataErrorState = "내부 구성 오류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 815)
		{
			str_mc_tmp_ReceiveDataErrorState = "자기 베어링 과류";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 890)
		{
			m_nMcTmp_ReceiveDataErrorCode = 890;
			str_mc_tmp_ReceiveDataErrorState = "안전 베어링 마모";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 891)
		{
			m_nMcTmp_ReceiveDataErrorCode = 891;
			str_mc_tmp_ReceiveDataErrorState = "로터 불균형이 너무 높음";
			StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: Error::TMP ERROR CODE :: %d   , ERROR :: %s "), m_nMcTmp_ReceiveDataErrorCode, str_mc_tmp_ReceiveDataErrorState);
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)(StrTempLog))); //ERROR 상태 기록.
		}
		else if (m_nMcTmp_ReceiveDataErrorCode == 0)
		{
			m_nMcTmp_ReceiveDataErrorCode = 0;
			str_mc_tmp_ReceiveDataErrorState = "No Error";
		}
	}
	else // UnKnown receive data
	{
		StrTempLog.Format(_T("MC_TMP -> PC [RECEIVE]: UnKnown Command :: %s , UnKnown SubData :: %s"), StrRecvTmpMsgCommand, StrRecvTmpMsgSubDataCommand);
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)(StrTempLog))); //통신 상태 기록.
	}

	StrTempLog.Format(_T("MC_TMP_HZ : %d , TMP STATE : %s , TMP Error Code :  %d"), m_nMcTmp_ReceiveDataHz, str_mc_tmp_ReceiveDataState, m_nMcTmp_ReceiveDataErrorCode);
	SaveLogFile("MC_TMP_LOG", (LPSTR)(LPCTSTR)StrTempLog); //통신 상태 기록.
}

CString CPfeifferHiPace2300TMPCtrl::GetCheckSumValue(CString command)
{
	CString strCommand = command;
	byte	btCheckSum = 0x00;
	CString strCS;

	for (int nIdx = 0; nIdx < strCommand.GetLength(); nIdx++)
	{
		btCheckSum += (byte)strCommand[nIdx];
	}

	sprintf_s(LPSTR(LPCTSTR(strCS)), sizeof(strCS), "%03d", btCheckSum);
	strCS.Format("%s", strCS);
	strCommand.Append(strCS);

	return strCommand;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpHz()
{
	int nRet = 0;
	
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020030902=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetErrorView()
{
	int nRet = 0;

	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020030302=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpSetHz()
{
	int nRet = 0;
	
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020030802=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpSetRpm()
{
	int nRet = 0;

	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020039702=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpRpm()
{
	int nRet = 0;
	
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020039802=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpSetSpeedOn()
{
	int nRet = 0;
	
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020030602=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpElectronicDeviceTemp()
{
	int nRet = 0;
	
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020032602=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpLowerPartTemp()
{
	int nRet = 0;
	
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020033002=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpMotorTemp()
{
	int nRet = 0;
	
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020034602=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpBearingTemp()
{
	int nRet = 0;

	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020034202=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);

	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::SetPumpOn()
{
	int nRet = 0;

	CString sCommand = "0021001006111111016\r";  //jkseo Checksum?
	ResetEvent(m_hPumpOnEvent);
	nRet = SendData((LPSTR)(LPCTSTR)sCommand);
	if (nRet != 0)
		SetEvent(m_hPumpOnEvent);
	else
		nRet = WaitExecGetPumpOnEvent(10000);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::SetPUmpOff()
{
	int nRet = 0;

	CString sCommand = "0021001006000000010\r";

	ResetEvent(m_hPumpOffEvent);
	nRet = SendData((LPSTR)(LPCTSTR)sCommand);
	if (nRet != 0)
		SetEvent(m_hPumpOffEvent);
	else
		nRet = WaitExecGetPumpOffEvent(10000);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::ResetPump()
{
	int nRet = 0;

	CString sCommand = "0021000906111111024\r";
	
	ResetEvent(m_hResetPumpEvent);
	nRet = SendData((LPSTR)(LPCTSTR)sCommand);
	if (nRet != 0)
		SetEvent(m_hResetPumpEvent);
	else
		nRet = WaitExecGetResetPumpEvent(5000);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::WaitExecGetPumpOnEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hPumpOnEvent, nTimeout) != WAIT_OBJECT_0)
		return -1;  //타임아웃 시 리턴

	//만약 에러가 발생하면 에러코드 리턴

	return 0;
}

int CPfeifferHiPace2300TMPCtrl::WaitExecGetPumpOffEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hPumpOffEvent, nTimeout) != WAIT_OBJECT_0)
		return -1;	//타임아웃 시 리턴

	//만약 에러가 발생하면 에러코드 리턴

	return 0;
}

int CPfeifferHiPace2300TMPCtrl::WaitExecGetResetPumpEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hResetPumpEvent, nTimeout) != WAIT_OBJECT_0)
		return -1;	//타임아웃 시 리턴

	//만약 에러가 발생하면 에러코드 리턴

	return 0;
}

