/**
 * Pfeiffer HiPace 2300 TMP Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

#pragma once
class AFX_EXT_CLASS CPfeifferHiPace2300TMPCtrl : public CSerialCom
{
public:
	CPfeifferHiPace2300TMPCtrl();
	~CPfeifferHiPace2300TMPCtrl();

	virtual	int	SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual int	ReceiveData(char *lParam, DWORD dwRead);
	
private:
	HANDLE m_hPumpOnEvent;
	HANDLE m_hPumpOffEvent;
	HANDLE m_hResetPumpEvent;

	void ParsingData(CString strRecvMsg);
	CString	GetCheckSumValue(CString command);
	CString	str_CR;

	int WaitExecGetPumpOnEvent(int nTimeout);
	int WaitExecGetPumpOffEvent(int nTimeout);
	int WaitExecGetResetPumpEvent(int nTimeout);

protected:
	int	m_nMcTmp_ReceiveDataHz;				// HZ
	int	m_nMcTmp_ReceiveDataSetHz;			// Set HZ
	int	m_nMcTmp_ReceiveDataState;			// STATE
	int	m_nMcTmp_ReceiveDataErrorCode;		//	ERROR CODE
	int	m_nMcTmp_ReceiveDataErrorState;		//	ERROR STATE 
	int	m_nMcTmp_ReceiveDataOnOffState;		// ON/OFF STATE
	int	m_nMcTmp_ReceiveDataRpm;			// 현재 rpm
	int	m_nMcTmp_ReceiveDataSetRpm;			// Set rpm
	int	m_nMcTmp_ReceiveDataTemperature_1;	//전자 장치 온도
	int	m_nMcTmp_ReceiveDataTemperature_2;	//펌프 하단부 온도
	int	m_nMcTmp_ReceiveDataTemperature_3;	//모터온도	
	int	m_nMcTmp_ReceiveDataTemperature_4;	//베어링 온도	
	int	m_nMcTmp_Error_War_State;				// Error , Warning 구분
	int	m_nMcTmp_ReveiveDataSetHz;			// 현재 설정 값 hz
	int	m_nMcTmp_ReveiveDataSetRpm;			// 현재 설정 값 rpm

	/*Error 설명*/
	CString str_mc_tmp_ReceiveDataErrorState;

	bool m_Mc_Tmp_Speed_SetOn;					//목표 속도 도달 

	int	SetPumpOn();
	int	SetPUmpOff();
	int	ResetPump();
	int	GetTmpHz();
	int	GetErrorView();
	int GetTmpSetHz();
	int	GetTmpSetRpm();
	int GetTmpRpm();
	int	GetTmpSetSpeedOn();
	int	GetTmpElectronicDeviceTemp();
	int	GetTmpLowerPartTemp();
	int	GetTmpMotorTemp();
	int	GetTmpBearingTemp();

	int m_nPumpPreData;
};

