/**
 * Serial Communication Class
 *
 * Description: A common function set using at all project.
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

#define LOG_PATH								_T("D:\\EUVSolution\\Log")
#define MARS_LOG_PATH							_T("D:\\Logs")
#define MARS_LOG_VER							_T("Applied Samsung Standard Logging Spec Version: 2.0\t\n")

#define COMMUNICATION_TIMEOUT		-99

class AFX_EXT_CLASS CECommon
{
public:
	CECommon();
	~CECommon();
	
	CRITICAL_SECTION cs;

	HANDLE					m_hStateOkEvent;
	int						WaitEvent(HANDLE hHandle, int nTimeOut = 60000);										//

	/**
	* description:		모든 Window Message가 처리되며, Loop 대기하고 싶은경우 사용(주로 for, while 문 안에서)
	* return:			void
	*/
	void				ProcessMessages(void);

	/**
	* description:		모든 Window Message가 처리되며, 일정 시간(초단위) 대기하고 싶은경우 사용.
	* parameter[in]:	double sec : 기다릴 초단위 시간
	* return:			void
	*/
	void				WaitSec(double sec);

	/**
	* description:		현 시스템의 년,월,일,시,분,초 정보 알고 싶을 때 사용
	* parameter[out]:	strDate: 년,월,일 정보 가져옴 
	* parameter[out]:	strTime: 시간, 분, 초 정보 가져옴(msec까지)
	* return			CTime: PC 년,월,일,시,분,초 정보 리턴
	*/
	CTime				GetSystemDateTime(CString* strDate, CString* strTime);

	/**
	* description:		로그 경로 초기화
	* parameter[in]:	strLogPath : 로그파일 경로(시스템마다 다를 수 있다.)
	* return			int : 에러여부 및 내용
	*/
	virtual int			SetLogPath(CString strLogPath);
	CString				m_strLogPath;

	/**
	* description:		Operation,Error,특정 File 명으로 로그를 만드는 함수
	* parameter[in]:	logFilename : Log File Name
	* parameter[in]:	logMessage : 기록할 로그
	* @return			void
	*/
	void				SaveLogFile(char* logFilename, char* logMessage);

	/**
	* description:		Operation,Error,특정 File 명으로 로그를 만드는 함수
	* parameter[in]:	logFilename : Log File Name
	* parameter[in]:	logMessage : 기록할 로그
	* parameter[in]:	logTime : 로그 타임
	* @return			void
	*/
	void				SaveLogFile(CString logFilename, CString logMessage);

	/**
	* description:		SEC Standard Log (Mars Log) 만드는 함수
	* parameter[in]:	logFilename : Log File Name
	* parameter[in]:	logMessage : 기록할 로그
	* @return			void
	*/
	void				SaveMarsLogFile(CString logFilename, CString logMessage);


	/**
	* description:		SEC Standard Log (Process 관련 Log) 출력 함수
	* parameter[in]:	DeviceID : Module 정보 (PM, MTS, LPM, ATM 등)
	* parameter[in]:	EventID : 계층 구조로 1단계(최상단)만 사용 (PreProcess, PostProcess, Process, PreLotRun, PostLotRun, IdleRun, AutoLeakCheck)
	* parameter[in]:	Status : Start/End 를 표시
	* parameter[in]:	MaterialID : 해당 Event 의 대상이 되는 Material ID
	* parameter[in]:	MaterialIDType : 대상의 Material 의 종류를 표시 : Wafer, Foup, POD, Mask 등
	* parameter[in]:	SlotNO : 대상 Material 이 위치한 Module 세부 위치 정보
	* parameter[in]:	LotID : Wafer 또는 Foup 내의 Wafer 가 속한 Lot ID (Mask ID ??)
	* parameter[in]:	RecipeID : 현재 공정 중인 Recipe ID
	* parameter[in]:	BatchID : Batch 공정 설비에서 동일 Batch 임을 표시하는 ID
	* parameter[in]:	StepNum : StepPorcess의 경우 Recipe 내 Step 번호 (자연수로 표시)
	* parameter[in]:	StepSeq : Recipe 내의 실제 진행된 Step 의  일련 순서 (자연수로 표시)
	* parameter[in]:	StepName : StepProcess 이벤트인 경우의 Step 이름
	* parameter[in]:	Data : StepSeq 총 개수 (ESOL 장비는 Recipe 내 Step 이라는 개념이 없으므로 '1' 고정 값으로 추정)
	* @return			void
	*/
	void				MarsPRCLog(CString DeviceID, CString EventID, CString Status, CStringArray MaterialID, CString MaterialType, CString SlotNo, CStringArray LotID,
								   CString RecipeID, int StepNo, int StepSeq, CString StepName, CStringArray Data);

	/**
	* description:		SEC Standard Log (Process 관련 Log) 출력 함수
	* parameter[in]:	DeviceID : Module 정보 (PM, MTS, LPM, ATM 등)
	* parameter[in]:	Category : Configuration 에 연관된 계층적 정보, Ex) VacSys/Controller/LeakCheck
	* parameter[in]:	CfgID : Configuration 이름, Ex) cnfLeakCheckTiemSec
	* parameter[in]:	Value : Configuration 값
	* parameter[in]:	Unit : Value 의 단위
	* parameter[in]:	ECID : Configuration 항목의 ECID(Equipment Constant ID) Number
	* parameter[in]:	Data : 해당 Config Range ('USL','1','sec')\t('LSL',1000','sec') / USL:하한값, LSL:상한값
	* @return			void
	*/
	void				MarsCFGLog(CString DeviceID, CString Category, CString CfgID, CString Value, CString Unit, CString ECID, CStringArray Data);


	/**
	 * Discription:		Error정보를 File로 저장하는 함수. 별도로 필요?
	 * parameter[in]:	pchErrCode : Error Code number
	 * parameter[in]:	pchErrName : Error Name
	 * parameter[in]:	pchErrData : Error Data
	 * return : void
	 */
	//void				SaveErrorLog(char* pchErrCode, char *pchErrName, char *pchErrData);

	/**
	* description:		폴더 유무 확인 함수
	* parameter[in]:	strFolderName : 폴더 풀 Path 네임
	* return			BOOL
	*/
	BOOL				IsFolderExist(CString strFolderName);

	/**
	* description		파일 유무 확인 함수
	* parameter[in]:	strFileName : 파일 풀 Path 네임
	* return			BOOL
	*/
	BOOL				IsFileExist(CString strFileName);
	   	 
	/**
	* description		Directory내 파일들을 모두 복사하는 함수, 하위 디렉토리는 복사되지 않음
	* parameter[in]:	SrcDir : 파일 풀 Path 네임
	* return			BOOL
	*/
	BOOL				CopyFiles(CString SrcFolder, CString DstFolder);// 

	/**
	* description		디렉토리 삭제 
	* parameter[in]     strPath 삭제할 폴더 풀 Path
	* return			BOOL
	*/
	int					DeleteFolder(CString strPath);
};

