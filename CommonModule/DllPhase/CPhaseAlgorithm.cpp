#include "pch.h"
#include "CPhaseAlgorithm.h"
#include <iostream>
#include "marshal.h"


CPhaseAlgorithm::CPhaseAlgorithm()
{
	PythonInitialize();
}
CPhaseAlgorithm::~CPhaseAlgorithm()
{
	Py_XDECREF(m_pyFuncHomograpy);
	Py_XDECREF(m_pyFuncFineAlignCurveFit);
	Py_XDECREF(m_pyFuncFineAlignCurveFit2);
	Py_DECREF(m_pyModule);
	Py_DECREF(m_pyName);	
}

int CPhaseAlgorithm::PythonInitialize()
{	
	m_pyName = PyUnicode_FromString("PythonAlgorithm");		
	m_pyModule = PyImport_Import(m_pyName);
	m_pyFuncFineAlignCurveFit = PyObject_GetAttrString(m_pyModule, "FineAlignCurveFit");
	m_pyFuncFineAlignCurveFit2 = PyObject_GetAttrString(m_pyModule, "FineAlignCurveFit2");	
	m_pyFuncHomograpy = PyObject_GetAttrString(m_pyModule, "Homography_solve");
	m_pyFuncEphaseFFT = PyObject_GetAttrString(m_pyModule, "EPhaseFFT");
	import_array();

	return 0;
}

int CPhaseAlgorithm::LoopBackTest(double* pos, double* intensity, int nSize, int nDir, int slope, double refAbs, double refMl)
{
	int N = nSize;

	PyObject* pName, *pModule, *pFunc;
	PyObject* pArgs, *pOutputValue;

	pName = PyUnicode_FromString("PythonAlgorithm");
	pModule = PyImport_Import(pName);
	Py_DECREF(pName);

	if (PyErr_Occurred())
		PyErr_Print();
	if (pModule != NULL)
	{
		pFunc = PyObject_GetAttrString(pModule, "LoopBackTest");
		if (pFunc && PyCallable_Check(pFunc))
		{
			int nd = 1;  // 1차원, 2차원 데이터
			npy_intp dims[1];// Data dimension

			dims[0] = nSize;
			PyArrayObject* pyPos = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(nd, dims, NPY_DOUBLE, reinterpret_cast<void*>(pos))); //소유권 pArgs로 인계
			PyArrayObject* pyIntensity = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(nd, dims, NPY_DOUBLE, reinterpret_cast<void*>(intensity))); //소유권 pArgs로 인계

			dims[0] = 1;
			PyArrayObject* pyDir = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(nd, dims, NPY_INT, reinterpret_cast<void*>(&nDir))); //소유권 pArgs로 인계
			PyArrayObject* pySlope = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(nd, dims, NPY_INT, reinterpret_cast<void*>(&slope))); //소유권 pArgs로 인계
			PyArrayObject* pyRefAbs = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(nd, dims, NPY_DOUBLE, reinterpret_cast<void*>(&refAbs))); //소유권 pArgs로 인계
			PyArrayObject* pyRefMl = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(nd, dims, NPY_DOUBLE, reinterpret_cast<void*>(&refMl))); //소유권 pArgs로 인계

			pArgs = PyTuple_New(6); //referece steal
			PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(pyPos));
			PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(pyIntensity));
			PyTuple_SetItem(pArgs, 2, reinterpret_cast<PyObject*>(pyDir));
			PyTuple_SetItem(pArgs, 3, reinterpret_cast<PyObject*>(pySlope));
			PyTuple_SetItem(pArgs, 4, reinterpret_cast<PyObject*>(pyRefAbs));
			PyTuple_SetItem(pArgs, 5, reinterpret_cast<PyObject*>(pyRefMl));

			pOutputValue = PyObject_CallObject(pFunc, pArgs);

			double* outPos = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0))));
			double* outIntensity = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 1))));
			int* outDir = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 2))));
			int* outSlope = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 3))));
			double* outRefAbs = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 4))));
			double* outRefMl = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 5))));
			int* outRefSize = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 6))));


			Py_DECREF(pArgs);
			Py_DECREF(pOutputValue);
		}
		Py_XDECREF(pFunc);
		Py_DECREF(pModule);
	}	

	return 0;
}


tuple<double, double, double, double, double*, int> CPhaseAlgorithm::PhaseFineAlignCurveFit(double* pos, double* intensity,int N, int nDir, int slope, double refAbs, double refMl)
{
	tuple<double, double, double, double, double*, int> tupleReturn;

	//int N = N;		
	PyObject* pArgs, *pOutputValue;
								
	//Input 처리
	npy_intp dims[1];// Data dimension
	dims[0] = N;
	PyArrayObject* pyPos = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(pos))); //소유권 pArgs로 인계
	PyArrayObject* pyIntensity = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(intensity))); //소유권 pArgs로 인계

	dims[0] = 1;
	PyArrayObject* pyDir = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_INT, reinterpret_cast<void*>(&nDir))); //소유권 pArgs로 인계
	PyArrayObject* pySlope = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_INT, reinterpret_cast<void*>(&slope))); //소유권 pArgs로 인계
	PyArrayObject* pyRefAbs= reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(&refAbs))); //소유권 pArgs로 인계
	PyArrayObject* pyRefMl = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(&refMl))); //소유권 pArgs로 인계


	pArgs = PyTuple_New(6); //referece steal
	PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(pyPos)); 			
	PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(pyIntensity));						
	PyTuple_SetItem(pArgs, 2, reinterpret_cast<PyObject*>(pyDir));			
	PyTuple_SetItem(pArgs, 3, reinterpret_cast<PyObject*>(pySlope));
	PyTuple_SetItem(pArgs, 4, reinterpret_cast<PyObject*>(pyRefAbs));
	PyTuple_SetItem(pArgs, 5, reinterpret_cast<PyObject*>(pyRefMl));
			 
	//함수 실행
	pOutputValue = PyObject_CallObject(m_pyFuncFineAlignCurveFit, pArgs);


	//Ouput 처리
	//double* outPos = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0))));
	//double* outIntensity = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 1))));
	//int* outDir = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 2))));
	//int* outSlope = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 3))));
	//double* outRefAbs = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 4))));
	//double* outRefMl = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 5))));
	//int* outRefSize = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 6))));

	//double* output = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0))));
	
	if (pOutputValue == nullptr)
	{
		double *err = nullptr;

		tupleReturn = make_tuple(0., 0., 0., 0., err, N);
	}
	double* output0 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0))));
	double* output1 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 1))));
	int* output2 = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 2))));
	int* output3 = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 3))));
	double* output4 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 4))));
	double* output5 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 5))));
	int* output6 = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 6))));
	double* output7 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 7))));
	double* output8 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 8))));
	double* output9 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 9))));
	
	double outputTargetPosition = output7[0];
	double outputRefAbs= output7[1];
	double outputRefMl = output7[2];
	double* outputIntensity_fit = output8; ///2022.01.11 이렇게  하면 안될꺼 같은데.. ihlee(메모리 사라짐)
	double outputTargetIntensity = *output9;

	Py_DECREF(pArgs);
	Py_DECREF(pOutputValue);
		
	tupleReturn = make_tuple(outputTargetPosition, outputTargetIntensity, outputRefAbs, outputRefMl, outputIntensity_fit, N);
		
	return tupleReturn;
}


tuple<double, double, double, double, double*, int> CPhaseAlgorithm::PhaseFineAlignCurveFit2(double* pos, double* intensity, int N, int nDir, int slope, double refAbs, double refMl)
{
	tuple<double, double, double, double, double*, int> tupleReturn;

	//int N = N;		
	PyObject* pArgs, *pOutputValue;

	//Input 처리
	npy_intp dims[1];// Data dimension
	dims[0] = N;
	PyArrayObject* pyPos = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(pos))); //소유권 pArgs로 인계
	PyArrayObject* pyIntensity = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(intensity))); //소유권 pArgs로 인계

	dims[0] = 1;
	PyArrayObject* pyDir = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_INT, reinterpret_cast<void*>(&nDir))); //소유권 pArgs로 인계
	PyArrayObject* pySlope = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_INT, reinterpret_cast<void*>(&slope))); //소유권 pArgs로 인계
	PyArrayObject* pyRefAbs = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(&refAbs))); //소유권 pArgs로 인계
	PyArrayObject* pyRefMl = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(&refMl))); //소유권 pArgs로 인계


	pArgs = PyTuple_New(6); //referece steal
	PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(pyPos));
	PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(pyIntensity));
	PyTuple_SetItem(pArgs, 2, reinterpret_cast<PyObject*>(pyDir));
	PyTuple_SetItem(pArgs, 3, reinterpret_cast<PyObject*>(pySlope));
	PyTuple_SetItem(pArgs, 4, reinterpret_cast<PyObject*>(pyRefAbs));
	PyTuple_SetItem(pArgs, 5, reinterpret_cast<PyObject*>(pyRefMl));

	//함수 실행
	pOutputValue = PyObject_CallObject(m_pyFuncFineAlignCurveFit2, pArgs);


	//Ouput 처리
	//double* outPos = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0))));
	//double* outIntensity = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 1))));
	//int* outDir = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 2))));
	//int* outSlope = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 3))));
	//double* outRefAbs = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 4))));
	//double* outRefMl = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 5))));
	//int* outRefSize = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 6))));

	//double* output = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0))));

	if (pOutputValue == nullptr)
	{
		double *err = nullptr;

		tupleReturn = make_tuple(0., 0., 0., 0., err, N);
	}
	double* output0 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0))));
	double* output1 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 1))));
	int* output2 = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 2))));
	int* output3 = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 3))));
	double* output4 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 4))));
	double* output5 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 5))));
	int* output6 = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 6))));
	double* output7 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 7))));
	double* output8 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 8))));
	double* output9 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 9))));

	double outputTargetPosition = output7[0];
	double outputRefAbs = output7[1];
	double outputRefMl = output7[2];
	double* outputIntensity_fit = output8;
	double outputTargetIntensity = *output9;

	Py_DECREF(pArgs);
	Py_DECREF(pOutputValue);

	tupleReturn = make_tuple(outputTargetPosition, outputTargetIntensity, outputRefAbs, outputRefMl, outputIntensity_fit, N);

	return tupleReturn;
}


void CPhaseAlgorithm::Homograpy(double* h, int n, double* xin, double* yin, double* xout, double* yout)
{
	//int N = N;		
	PyObject* pArgs, *pOutputValue;

	//Input 처리
	npy_intp dims[1] = {4};// Data dimension n

	
	PyArrayObject* pyXin = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(xin))); //소유권 pArgs로 인계
	PyArrayObject* pyYin = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(yin))); //소유권 pArgs로 인계
	PyArrayObject* pyXout = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(xout))); //소유권 pArgs로 인계
	PyArrayObject* pyYout = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(yout))); //소유권 pArgs로 인


	pArgs = PyTuple_New(4); //referece steal
	PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(pyXin));
	PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(pyYin));
	PyTuple_SetItem(pArgs, 2, reinterpret_cast<PyObject*>(pyXout));
	PyTuple_SetItem(pArgs, 3, reinterpret_cast<PyObject*>(pyYout));
	
	//함수 실행
	pOutputValue = PyObject_CallObject(m_pyFuncHomograpy, pArgs);


	//Ouput 처리	

	double* output = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0))));

	double* output1 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 1))));
	double* output2 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 2))));
	double* output3 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 3))));
	double* output4 = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 4))));


	for (int i = 0; i < 9; i++)
	{
		h[i] = output[i];
	}

	Py_DECREF(pArgs);
	Py_DECREF(pOutputValue);

}
void CPhaseAlgorithm::HomographyTransform(double x, double y, double *h, double & xout, double & yout)
{
	double p = (h[6] * x + h[7] * y + h[8]);
	xout = (h[0] * x + h[1] * y + h[2]) / p;
	yout = (h[3] * x + h[4] * y + h[5]) / p;
	//yout
} 
double CPhaseAlgorithm::AngleDiffrence(double ref, double target)
{
	//ref 기준으로 target의 각도  계산, rad

	double diff = target - ref;

	
	diff = fmod(diff + 2 * M_PI, 2 * M_PI);

	return diff;

}


void CPhaseAlgorithm::AngleDiffrence(double* ref, double* target, double* result, int n)
{
	double diff;
	for (int i = 0; i < n; i++)
	{
		diff = target[i] - ref[i];
		result[i] = fmod(diff + 2 * M_PI, 2 * M_PI);
	}
}



void CPhaseAlgorithm::MilInitialize(MIL_ID milSystem)
{
	m_MilSystem = milSystem;
}


void CPhaseAlgorithm::EPhaseFFT(int n_dataSize, double* intensity, int n_fftSize, double t_sampling, double* magnitude, double* phase, double* frequncy)
{
	//int N = N;		
	PyObject* pArgs, *pOutputValue;

	//Input 처리
	npy_intp dims[1];// Data dimension n


	dims[0] = n_dataSize;
	PyArrayObject* pyX = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(intensity))); //소유권 pArgs로 인계
	dims[0] = 1;
	PyArrayObject* pyN = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_INT, reinterpret_cast<void*>(&n_fftSize))); //소유권 pArgs로 인계
	dims[0] = 1;
	PyArrayObject* pyT = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(1, dims, NPY_DOUBLE, reinterpret_cast<void*>(&t_sampling))); //소유권 pArgs로 인계


	pArgs = PyTuple_New(3); //referece steal
	PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(pyX));
	PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(pyN));
	PyTuple_SetItem(pArgs, 2, reinterpret_cast<PyObject*>(pyT));
	

	//함수 실행
	pOutputValue = PyObject_CallObject(m_pyFuncEphaseFFT, pArgs);


	//Ouput 처리	

	//double* magnitude = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0))));
	//double* phase = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 1))));

	int n_output = n_fftSize / 2 + 1;  // 9:5, 10:6, 11:6

	memcpy(magnitude, reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 0)))), sizeof(double) * n_output);
	memcpy(phase, reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 1)))), sizeof(double) * n_output);

	if (frequncy != NULL)
	{
		memcpy(frequncy, reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 2)))), sizeof(double) * n_output);
		//double* frequncy = reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 2))));
	}
			
	int* N = reinterpret_cast<int*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(pOutputValue, 3))));

	Py_DECREF(pArgs);
	Py_DECREF(pOutputValue);

}
void CPhaseAlgorithm::AllocMeasure(int dataSize)
{
	//m_DataSize = dataSize;

}

void CPhaseAlgorithm::FreeMeasure()
{

}

void CPhaseAlgorithm::ImageProjection(MIL_ID milImage, double*arrProjection, int nSize)
{
	MIL_ID m_MilProjResult;
	MimAllocResult(m_MilSystem, nSize, M_PROJ_LIST, &m_MilProjResult);

	MimProject(milImage, m_MilProjResult, M_0_DEGREE);
	MimGetResult(m_MilProjResult, M_VALUE + M_TYPE_MIL_DOUBLE, arrProjection);
	MimFree(m_MilProjResult);
}

void CPhaseAlgorithm::FindCenterFrequency()
 {
	//double arr[10] = {1,12,3,4,5,10,3,4,5,20};
	//
	//int start;
	//int end;
	//
	//start = 0; //0,2
	//end = 9;   //8,9

	//double max;
	//int index;

	//double *pMax;

	//pMax = max_element(arr + start, arr + end);

	//max = *pMax;

	//index = pMax - arr;
	//index = distance(arr, pMax);
	//
	////Update Data 
	//m_CenterWaveLength = max;
	//m_CenterFreqIndex = index;
	////m_CenterFrePhase
	
}