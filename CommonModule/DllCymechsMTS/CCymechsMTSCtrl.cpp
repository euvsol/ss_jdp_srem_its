#include "stdafx.h"
#include "CCymechsMTSCtrl.h"

CCymechsMTSCtrl::CCymechsMTSCtrl()
{
	m_bOccurAlarm		= FALSE;
	m_bMtsEMOStatus		= FALSE;
	m_bMtsError			= FALSE;

	m_bRobotInitComp	= FALSE;
	m_bRobotWorking		= FALSE;
	m_bRobotWithMask	= FALSE;
	m_bRobotError		= FALSE;
	m_bRobotRetracted	= FALSE;

	m_bRotatorInitComp	= FALSE;
	m_bRotatorWorking	= FALSE;
	m_bRotatorWithMask  = FALSE;
	m_bRotatorError		= FALSE;

	m_bLpmInitComp		= FALSE;
	m_bLpmWorking		= FALSE;
	m_bPodOnLPM			= FALSE;
	m_bPodOpened		= FALSE;
	m_bLoadportClamped	= FALSE;
	m_bMaskInPod		= FALSE;
	m_bLpmError			= FALSE;

	m_bFlipperInitComp	= FALSE;
	m_bFlipperWorking	= FALSE;
	m_bFlipperWithMask	= FALSE;
	m_bFlipperError		= FALSE;

	m_bPioManualMode	= FALSE;
	
	m_strRfidTagData	= _T("");
	m_strRobotSpeed		= _T("");
	m_strErrorCode		= _T("0000");

	m_hAckEvent			= CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hLpmDoneEvent		= CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hRobotDoneEvent	= CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hAlignDoneEvent	= CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hFlipDoneEvent	= CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hInitEvent		= CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hTagReadEvent		= CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hSpeedReadEvent	= CreateEvent(NULL, TRUE, FALSE, NULL);

	m_strReceiveEndOfStreamSymbol = _T("\r");
}


CCymechsMTSCtrl::~CCymechsMTSCtrl()
{
	CloseHandle(m_hAckEvent);
	CloseHandle(m_hLpmDoneEvent);
	CloseHandle(m_hRobotDoneEvent);
	CloseHandle(m_hAlignDoneEvent);
	CloseHandle(m_hFlipDoneEvent);
	CloseHandle(m_hInitEvent);
	CloseHandle(m_hTagReadEvent);
	CloseHandle(m_hSpeedReadEvent);
}

int	CCymechsMTSCtrl::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	m_strSendMsg = lParam;
	SaveLogFile("MTS_Com", _T((LPSTR)(LPCTSTR)("PC -> MTS : " + m_strSendMsg)));	//통신 상태 기록.
	nRet = Send(lParam, nTimeOut);
	if (nRet != 0)
	{
		CString Temp;
		Temp.Format(_T("PC -> MTS : SEND FAIL (%d)"), nRet);
		SaveLogFile("MTS_Com", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
	}

	return nRet;
}

int CCymechsMTSCtrl::ReceiveData(char *lParam)
{
	m_strReceivedMsg = lParam;
	SaveLogFile("MTS_Com", _T((LPSTR)(LPCTSTR)("MTS -> PC : " + m_strReceivedMsg))); //통신 상태 기록.
	ParsingData(m_strReceivedMsg);

	return 0;
}

void CCymechsMTSCtrl::ParsingData(CString strRecvMsg)
{
	if (strRecvMsg == _T("")) return;

	m_bOccurAlarm	= FALSE;
	m_strErrorCode	= _T("0000");

	CString strCommand  = strRecvMsg.Mid(1, 5);
	CString strUnitNum  = strRecvMsg.Mid(6, 1);
	CString strRetValue = strRecvMsg.Mid(7, 1);

	int		nStatusStart;
	int		nStatusEnd;
	CString strStatus;

	if (strRetValue == _T("1"))		//At normal reception Command String Running (Wait main Equipment) - ACK
	{
		SetEvent(m_hAckEvent);

		if (strCommand == _T("RFIDR"))
		{
			nStatusStart = strRecvMsg.Find('[') + 1;
			nStatusEnd	 = strRecvMsg.Find(']');
			m_strRfidTagData = strRecvMsg.Mid(nStatusStart, nStatusEnd - nStatusStart);

			SetEvent(m_hTagReadEvent);
		}

		if (strCommand == _T("RRSPD"))
		{
			nStatusStart = strRecvMsg.Find('[') + 1;
			nStatusEnd	 = strRecvMsg.Find(']');
			strStatus	 = strRecvMsg.Mid(nStatusStart, nStatusEnd - nStatusStart);

			m_strRobotSpeed = strStatus.Left(4);

			SetEvent(m_hSpeedReadEvent);
		}

		if (strCommand == _T("STAT~"))
		{
			nStatusStart = strRecvMsg.Find('[') + 1;
			nStatusEnd	 = strRecvMsg.Find(']');
			strStatus	 = strRecvMsg.Mid(nStatusStart, nStatusEnd - nStatusStart);

			if (strUnitNum == _T("0")) //MTS
			{
				m_bMtsError			= strStatus.Left(1) == _T("0") ? FALSE : TRUE;
				m_nSignalLampRed	= atoi(strStatus.Mid(1, 1));
				m_nSignalLampYellow = atoi(strStatus.Mid(2, 1));
				m_nSignalLampGreen	= atoi(strStatus.Mid(3, 1));
				m_nSignalLampBlue	= atoi(strStatus.Mid(4, 1));
				m_bMtsEMOStatus		= strStatus.Mid(5, 1) == _T("0") ? FALSE : TRUE;
			}
			else if (strUnitNum == _T("K"))	//Flipper
			{
				m_bFlipperInitComp = strStatus.Left(1) == _T("0") ? FALSE : TRUE;
				m_bFlipperWithMask = strStatus.Mid(2, 1) == _T("0") ? FALSE : TRUE;

				if (strStatus.Mid(1, 1) == _T("0"))	//Stop
				{
					m_bFlipperWorking = FALSE;
				}
				else if (strStatus.Mid(1, 1) == _T("1")) //Moving
				{
					m_bFlipperWorking = TRUE;
					m_bFlipperError = FALSE;
				}
				else if (strStatus.Mid(1, 1) == _T("3")) //Error
				{
					m_bFlipperError = TRUE;
					m_bFlipperWorking = FALSE;
				}
			}
			else if (strUnitNum == _T("4"))	//Loadport
			{
				m_bLpmInitComp		= strStatus.Left(1)		== _T("0") ? FALSE : TRUE;
				m_bPodOnLPM			= strStatus.Mid(2, 1)	== _T("1") ? TRUE  : FALSE;
				m_bPodPresenceOn	= strStatus.Mid(2, 1)	== _T("2") ? TRUE  : FALSE;
				m_bPodPlacementOn	= strStatus.Mid(2, 1)	== _T("3") ? TRUE  : FALSE;

				m_bPodOpened		= strStatus.Mid(3, 1)	== _T("0") ? FALSE : TRUE;
				m_bLoadportClamped	= strStatus.Mid(4, 1)	== _T("0") ? FALSE : TRUE;
				m_bPioManualMode	= strStatus.Mid(10, 1)	== _T("0") ? TRUE  : FALSE;

				m_bLpmWorking		= strStatus.Mid(1, 1)	== _T("1") ? TRUE : FALSE;
				m_bLpmError			= strStatus.Mid(1, 1)	== _T("3") ? TRUE : FALSE;

				CString sHex = strStatus.Mid(13, 2);
				int nLen = sHex.GetLength();
				m_strPioInBit = _T("");

				for (int i = 0; i < nLen; i++)
					m_strPioInBit += HexStringToBinary(sHex[i]);

				sHex = strStatus.Mid(15, 2);
				nLen = sHex.GetLength();
				m_strPioOutBit = _T("");

				for (int i = 0; i < nLen; i++)
					m_strPioOutBit += HexStringToBinary(sHex[i]);
			}
			else if (strUnitNum == _T("3"))	//Aligner
			{
				m_bRotatorInitComp = strStatus.Left(1) == _T("0") ? FALSE : TRUE;
				m_bRotatorWithMask = strStatus.Mid(2, 1) == _T("0") ? FALSE : TRUE;

				if (strStatus.Mid(1, 1) == _T("0"))	//Stop
				{
					m_bRotatorWorking = FALSE;
				}
				else if (strStatus.Mid(1, 1) == _T("1")) //Moving
				{
					m_bRotatorWorking = TRUE;
					m_bRotatorError = FALSE;
				}
				else if (strStatus.Mid(1, 1) == _T("3")) //Error
				{
					m_bRotatorError = TRUE;
					m_bRotatorWorking = FALSE;
				}
			}
			else if (strUnitNum == _T("1"))	//Robot
			{
				m_bRobotInitComp  = strStatus.Left(1)   == _T("0") ? FALSE : TRUE;
				m_bRobotWithMask  = strStatus.Mid(2, 1) == _T("0") ? FALSE : TRUE;
				m_bRobotRetracted = strStatus.Mid(4, 1) == _T("0") ? TRUE : FALSE;

				if (strStatus.Mid(1, 1) == _T("0"))	//Stop
				{
					m_bRobotWorking = FALSE;
				}
				else if (strStatus.Mid(1, 1) == _T("1")) //Moving
				{
					m_bRobotWorking = TRUE;
					m_bRobotError = FALSE;
				}
				else if (strStatus.Mid(1, 1) == _T("3")) //Error
				{
					m_bRobotError = TRUE;
					m_bRobotWorking = FALSE;
				}
			}
		}
	}
	//else if (strRetValue == _T("2"))	//Command rejection 2Byte Cancel Code Reply("2" + "XX" + CS + CR)
	//{
	//	m_bCommReject = TRUE;
	//
	//	m_strCancelCode = m_strReceivedMsg.Mid(8, 2);
	//
	//	SetEvent(m_hAckEvent);
	//	SetEvent(m_hAlignDoneEvent);
	//	SetEvent(m_hFlipDoneEvent);
	//	SetEvent(m_hRobotDoneEvent);
	//	SetEvent(m_hLpmDoneEvent);
	//	SetEvent(m_hInitEvent);
	//	SetEvent(m_hTagReadEvent);
	//	SetEvent(m_hSpeedReadEvent);
	//}
	//else if (strRetValue == _T("3"))	//Bad Syntax / Bad Checksum - NAK
	//{
	//	m_bOccurError = TRUE;
	//
	//	SetEvent(m_hAckEvent);
	//	SetEvent(m_hAlignDoneEvent);
	//	SetEvent(m_hFlipDoneEvent);
	//	SetEvent(m_hRobotDoneEvent);
	//	SetEvent(m_hLpmDoneEvent);
	//	SetEvent(m_hInitEvent);
	//	SetEvent(m_hTagReadEvent);
	//	SetEvent(m_hSpeedReadEvent);
	//}
	else if (strRetValue == _T("4"))	//Event Message
	{
		if (strCommand == _T("ALMSG"))
		{
			m_bOccurAlarm = TRUE;

			CString strUnitNum = _T("");

			nStatusStart = m_strReceivedMsg.Find('[') + 1;
			nStatusEnd	 = m_strReceivedMsg.Find(']');
			strStatus	 = m_strReceivedMsg.Mid(nStatusStart, nStatusEnd - nStatusStart);
			strUnitNum	 = strStatus.Mid(0, 1);

			m_strErrorCode = strStatus.Mid(2, 4);

			SetEvent(m_hAckEvent);
			SetEvent(m_hAlignDoneEvent);
			SetEvent(m_hFlipDoneEvent);
			SetEvent(m_hRobotDoneEvent);
			SetEvent(m_hLpmDoneEvent);
			SetEvent(m_hInitEvent);
			SetEvent(m_hTagReadEvent);
			SetEvent(m_hSpeedReadEvent);
		}
		else if (strCommand == _T("STAT~"))
		{
			nStatusStart = m_strReceivedMsg.Find('[') + 1;
			nStatusEnd	 = m_strReceivedMsg.Find(']');
			strStatus	 = m_strReceivedMsg.Mid(nStatusStart, nStatusEnd - nStatusStart);

			if (strUnitNum == _T("0")) //MTS
			{
				m_bMtsError			= strStatus.Left(1) == _T("0") ? FALSE : TRUE;
				m_nSignalLampRed	= atoi(strStatus.Mid(1, 1));
				m_nSignalLampYellow = atoi(strStatus.Mid(2, 1));
				m_nSignalLampGreen	= atoi(strStatus.Mid(3, 1));
				m_nSignalLampBlue	= atoi(strStatus.Mid(4, 1));
				m_bMtsEMOStatus		= strStatus.Mid(5, 1) == _T("0") ? FALSE : TRUE;
			}
			else if (strUnitNum == _T("K"))	//Flipper
			{
				m_bFlipperInitComp = strStatus.Left(1)   == _T("0") ? FALSE : TRUE;
				m_bFlipperWithMask = strStatus.Mid(2, 1) == _T("0") ? FALSE : TRUE;

				if (strStatus.Mid(1, 1) == _T("0"))	//Stop
				{
					m_bFlipperWorking = FALSE;
					SetEvent(m_hFlipDoneEvent);
				}
				else if (strStatus.Mid(1, 1) == _T("1")) //Moving
				{
					m_bFlipperWorking = TRUE;
					m_bFlipperError	  = FALSE;
				}
				else if (strStatus.Mid(1, 1) == _T("3")) //Error
				{
					m_bFlipperError	  = TRUE;
					m_bFlipperWorking = FALSE;
					SetEvent(m_hFlipDoneEvent);
				}
			}
			else if (strUnitNum == _T("4"))	//Loadport
			{
				m_bLpmInitComp		= strStatus.Left(1)    == _T("0") ? FALSE : TRUE;
				m_bPodOnLPM			= strStatus.Mid(2, 1)  == _T("1") ? TRUE  : FALSE;
				m_bPodOpened		= strStatus.Mid(3, 1)  == _T("0") ? FALSE : TRUE;
				m_bLoadportClamped  = strStatus.Mid(4, 1)  == _T("0") ? FALSE : TRUE;
				m_bPioManualMode	= strStatus.Mid(10, 1) == _T("0") ? TRUE  : FALSE;

				if (strStatus.Mid(1, 1) == "0")	//Stop
				{
					m_bLpmWorking = FALSE;
					SetEvent(m_hLpmDoneEvent);
				}
				else if (strStatus.Mid(1, 1) == "1") //Moving
				{
					m_bLpmWorking = TRUE;
					m_bLpmError = FALSE;
				}
				else if (strStatus.Mid(1, 1) == "3") //Error
				{
					m_bLpmError = TRUE;
					m_bLpmWorking = FALSE;
					SetEvent(m_hLpmDoneEvent);
				}

				CString sHex = strStatus.Mid(13, 2);
				int nLen = sHex.GetLength();
				m_strPioInBit = _T("");

				for (int i = 0; i < nLen; i++)
					m_strPioInBit += HexStringToBinary(sHex[i]);

				sHex = strStatus.Mid(15, 2);
				nLen = sHex.GetLength();
				m_strPioOutBit = _T("");

				for (int i = 0; i < nLen; i++)
					m_strPioOutBit += HexStringToBinary(sHex[i]);
			}
			else if (strUnitNum == _T("3"))	//Aligner
			{
				m_bRotatorInitComp = strStatus.Left(1)	 == _T("0") ? FALSE : TRUE;
				m_bRotatorWithMask = strStatus.Mid(2, 1) == _T("0") ? FALSE : TRUE;

				if (strStatus.Mid(1, 1) == _T("0"))	//Stop
				{
					m_bRotatorWorking = FALSE;
					SetEvent(m_hAlignDoneEvent);
				}
				else if (strStatus.Mid(1, 1) == _T("1")) //Moving
				{
					m_bRotatorWorking = TRUE;
					m_bRotatorError = FALSE;
				}
				else if (strStatus.Mid(1, 1) == _T("3")) //Error
				{
					m_bRotatorError = TRUE;
					m_bRotatorWorking = FALSE;
					SetEvent(m_hAlignDoneEvent);
				}
			}
			else if (strUnitNum == _T("1"))	//Robot
			{
				m_bRobotInitComp  = strStatus.Left(1)   == _T("0") ? FALSE : TRUE;
				m_bRobotWithMask  = strStatus.Mid(2, 1) == _T("0") ? FALSE : TRUE;
				m_bRobotRetracted = strStatus.Mid(4, 1) == _T("0") ? FALSE : TRUE;

 				if (strStatus.Mid(1, 1) == _T("0"))	//Stop
				{
					m_bRobotWorking = FALSE;
					SetEvent(m_hRobotDoneEvent);
				}
				else if (strStatus.Mid(1, 1) == _T("1")) //Moving
				{
					m_bRobotWorking = TRUE;
					m_bRobotError   = FALSE;
				}
				else if (strStatus.Mid(1, 1) == _T("3")) //Error
				{
					m_bRobotError   = TRUE;
					m_bRobotWorking = FALSE;
					SetEvent(m_hRobotDoneEvent);
				}
			}

			if (m_bLpmInitComp == TRUE  && m_bRotatorInitComp == TRUE
				&& m_bRobotInitComp   == TRUE  && m_bFlipperInitComp == TRUE
				&& m_bLpmWorking == FALSE && m_bRotatorWorking == FALSE
				&& m_bRobotWorking    == FALSE && m_bFlipperWorking  == FALSE)
			{
				SetEvent(m_hInitEvent);
			}
		}
		else if (strCommand == _T("MAPP~"))
		{
			nStatusStart = m_strReceivedMsg.Find('[') + 1;
			nStatusEnd = m_strReceivedMsg.Find(']');
			strStatus = m_strReceivedMsg.Mid(nStatusStart, nStatusEnd - nStatusStart);

			m_bMaskInPod = strStatus.Left(1) == _T("0") ? FALSE : TRUE;
		}
	}
}

int CCymechsMTSCtrl::InitDevice()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@INIT~0");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	
	ResetEvent(m_hAckEvent);
	ResetEvent(m_hInitEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hInitEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecInitEvent(30000);
	}
	
	return nRet;
}

int CCymechsMTSCtrl::InitModule(CString strModule)
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@INIT~") + strModule;
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	
	if (strModule == _T("1"))
		ResetEvent(m_hRobotDoneEvent);
	else if (strModule == _T("3"))
		ResetEvent(m_hAlignDoneEvent);
	else if (strModule == _T("4"))
		ResetEvent(m_hLpmDoneEvent);
	else if (strModule == _T("K"))
		ResetEvent(m_hFlipDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);

		if (strModule == _T("1"))
			SetEvent(m_hRobotDoneEvent);
		else if (strModule == _T("3"))
			SetEvent(m_hAlignDoneEvent);
		else if (strModule == _T("4"))
			SetEvent(m_hLpmDoneEvent);
		else if (strModule == _T("K"))
			SetEvent(m_hFlipDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		if (strModule == _T("1"))
			nRet = WaitExecRobotDoneEvent(30000);
		else if (strModule == _T("3"))
			nRet = WaitExecAlignDoneEvent(30000);
		else if (strModule == _T("4"))
			nRet = WaitExecLpmDoneEvent(30000);
		else if (strModule == _T("K"))
			nRet = WaitExecFlipDoneEvent(30000);
	}

	return nRet;
}

void CCymechsMTSCtrl::GetPioTimeoutValue()
{
	CString strSendMsg;
	CString strCommand = _T("@TIMRD0");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::ResetPioError()
{
	CString strSendMsg;
	CString strCommand = _T("@RESET4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::GetAlarmMessage()
{
	CString strSendMsg;
	CString strCommand = _T("@ALMSG0");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::GetMtsStatus()
{
	CString strSendMsg;
	CString strCommand = _T("@STAT~0");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::GetRobotStatus()
{
	CString strSendMsg;
	CString strCommand = _T("@STAT~1");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::GetRotatorStatus()
{
	CString strSendMsg;
	CString strCommand = _T("@STAT~3");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::GetLoadportStatus()
{
	CString strSendMsg;
	CString strCommand = _T("@STAT~4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::GetFlipperStatus()
{
	CString strSendMsg;
	CString strCommand = _T("@STAT~K");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::RetryE84Communication()
{
	CString strSendMsg;
	CString strCommand = _T("@RETRY4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::ConcludeE84Communication()
{
	CString strSendMsg;
	CString strCommand = _T("@COMPL4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::SetPioControl(int nLreq, int nUreq, int nNc1, int nReady, int nNc2, int nNc3, int nHoAvbl, int nEs)
{
	CString strSendMsg;
	CString strLreq, strUreq, strNc1, strReady, strNc2, strNc3, strHoAvbl, strEs;
	strLreq.Format(_T("%d"), nLreq);
	strUreq.Format(_T("%d"), nUreq);
	strNc1.Format(_T("%d"), nNc1);
	strReady.Format(_T("%d"), nReady);
	strNc2.Format(_T("%d"), nNc2);
	strNc3.Format(_T("%d"), nNc3);
	strHoAvbl.Format(_T("%d"), nHoAvbl);
	strEs.Format(_T("%d"), nEs);

	CString strCommand = _T("@SPIO~4[") + strLreq + strUreq + strNc1 + strReady + strNc2 + strNc3 + strHoAvbl + strEs +  _T("]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}
	
int CCymechsMTSCtrl::RobotMoveHome()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@HOME~1");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

void CCymechsMTSCtrl::RobotMovePause()
{
	CString strSendMsg;
	CString strCommand = _T("@PAUSE1");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::RobotMoveResume()
{
	CString strSendMsg;
	CString strCommand = _T("@RESUM1");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::RobotMoveStop()
{
	CString strSendMsg;
	CString strCommand = _T("@STOP~1");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::GetRobotPosition()
{
	CString strSendMsg;
	CString strCommand = _T("@ROPOS1");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::GetRobotModelType()
{
	CString strSendMsg;
	CString strCommand = _T("@RMDL~1");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::SetRobotSpeed(int nLowerArmWithoutMask, int nLowerArmWithMask, int nUpperArmWithoutMask, int nUpperArmWithMask)
{
	CString strSendMsg;
	CString strLowerArmWithoutMask, strLowerArmWithMask, strUpperArmWithoutMask, strUpperArmWithMask;
	strLowerArmWithoutMask.Format(_T("%02d"), nLowerArmWithoutMask);
	strLowerArmWithMask.Format(_T("%02d"), nLowerArmWithMask);
	strUpperArmWithoutMask.Format(_T("%02d"), nUpperArmWithoutMask);
	strUpperArmWithMask.Format(_T("%02d"), nUpperArmWithMask);

	CString strCommand = _T("@SRSPD1[") + strLowerArmWithoutMask + strLowerArmWithMask + strUpperArmWithoutMask + strUpperArmWithMask + _T("]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

int CCymechsMTSCtrl::GetRobotSpeed()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@RRSPD1");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hSpeedReadEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hSpeedReadEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(5000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecSpeedReadEvent(10000);
	}

	return nRet;
}

int CCymechsMTSCtrl::LoadPod()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@LOADL4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hLpmDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hLpmDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecLpmDoneEvent(30000);
	}

	return nRet;
}

int CCymechsMTSCtrl::UnloadPod()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@UNLDL4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hLpmDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hLpmDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecLpmDoneEvent(30000);
	}

	return nRet;
}

int CCymechsMTSCtrl::MoveDockingPos()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@DOCK~4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hLpmDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hLpmDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecLpmDoneEvent(30000);
	}

	return nRet;
}

int CCymechsMTSCtrl::MoveUndockingPos()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@UDOCK4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hLpmDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hLpmDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecLpmDoneEvent(30000);
	}

	return nRet;
}

void CCymechsMTSCtrl::PlacePodOnLoadport()
{
	CString strSendMsg;
	CString strCommand = _T("@FOUPE4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::RemovePodOnLoadport()
{
	CString strSendMsg;
	CString strCommand = _T("@FOUPA4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

int CCymechsMTSCtrl::RotateMask(int nAngle)
{
	int nRet = 0;
	
	CString strAngle;
	strAngle.Format(_T("%05d"), nAngle * 100);

	CString strSendMsg;
	CString strCommand = _T("@ALNR~3[") + strAngle + _T("]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hAlignDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hAlignDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecAlignDoneEvent(30000);
	}

	return nRet;
}

int CCymechsMTSCtrl::OriginRotator()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@ALNO~3");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hAlignDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hAlignDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecAlignDoneEvent(30000);
	}

	return nRet;
}

int CCymechsMTSCtrl::GripMask(int nStatus)
{
	int nRet = 0;
		
	CString strStatus;
	strStatus.Format(_T("%d"), nStatus);

	CString strSendMsg;
	CString strCommand = _T("@GRIP~K[###") + strStatus + _T("]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
		SetEvent(m_hAckEvent);
	else
		nRet = WaitExecAckEvent(3000);

	return nRet;
}

int CCymechsMTSCtrl::FlipMask()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@FLIP~K");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hFlipDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hFlipDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecFlipDoneEvent(30000);
	}

	return nRet;
}

int CCymechsMTSCtrl::OriginFlipper()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@FLIPOK");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hFlipDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hFlipDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecFlipDoneEvent(30000);
	}
	
	return nRet;
}

void CCymechsMTSCtrl::SetPioModeToAMHS(BOOL bCheck)
{
	CString strSendMsg;
	CString strCommand;

	if(bCheck == TRUE)
		strCommand = _T("@MODE~4[1000000]");
	else
		strCommand = _T("@MODE~4[0000000]");
	
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::ClearAllAlarm()
{
	CString strSendMsg;
	CString strCommand = _T("@CCLR~0[0000]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

int CCymechsMTSCtrl::OpenPod()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@OPEN~4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hLpmDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hLpmDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecLpmDoneEvent(30000);
	}

	return nRet;
}

int CCymechsMTSCtrl::ClosePod()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@CLOSE4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hLpmDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hLpmDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecLpmDoneEvent(30000);
	}

	return nRet;
}

void CCymechsMTSCtrl::GetMappingData()
{
	CString strSendMsg;
	CString strCommand = _T("@MAPP~4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::RetryMapping()
{
	CString strSendMsg;
	CString strCommand = _T("@REMAP4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::ClampPod()
{
	CString strSendMsg;
	CString strCommand = _T("@CLAMP4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::UnclampPod()
{
	CString strSendMsg;
	CString strCommand = _T("@UCLAM4");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::AbortAllModules()
{
	CString strSendMsg;
	CString strCommand = _T("@ABORT0");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

void CCymechsMTSCtrl::SetSignalLamp(int nRed, int nYellow, int nGreen, int nBlue, int nBuzz)
{
	CString strRed, strYellow, strGreen, strBlue, strBuzz;
	strRed.Format(_T("%d"), nRed);
	strYellow.Format(_T("%d"), nYellow);
	strGreen.Format(_T("%d"), nGreen);
	strBlue.Format(_T("%d"), nBlue);
	strBuzz.Format(_T("%d"), nBuzz);

	CString strSendMsg;
	CString strCommand = _T("@SIGLM0[") + strRed + strYellow + strGreen + strBlue + strBuzz + _T("]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

int CCymechsMTSCtrl::ReadRfidData(int nPage)
{
	int nRet = 0;

	CString strPage;
	strPage.Format(_T("%02d"), nPage);

	CString strSendMsg;
	CString strCommand = _T("@RFIDR4[") + strPage + _T("]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hTagReadEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
		SetEvent(m_hTagReadEvent);
	else
		nRet = WaitExecTagReadEvent(3000);

	return nRet;
}

void CCymechsMTSCtrl::GetFfuStatus()
{
	CString strSendMsg;
	CString strCommand = _T("@FFURD0");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));
	SendData((LPSTR)(LPCTSTR)strSendMsg);
}

int CCymechsMTSCtrl::PickMaskFromLoadPort()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[401000######]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

int CCymechsMTSCtrl::PlaceMaskToLoadPort()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[000401######]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

int CCymechsMTSCtrl::PickMaskFromFlipper()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[K01000######]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

int CCymechsMTSCtrl::PlaceMaskToFlipper()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[000K01######]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

int CCymechsMTSCtrl::PickMaskFromRotator()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[301000######]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}
	
	return nRet;
}

int CCymechsMTSCtrl::PlaceMaskToRotator()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[000301######]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

int CCymechsMTSCtrl::PickMaskFromStage()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[A01000######]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

int CCymechsMTSCtrl::PlaceMaskToStage()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[000A01######]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

int CCymechsMTSCtrl::PlaceMaskToStageEx()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[###A01######EE]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

int CCymechsMTSCtrl::PlaceMaskToStageRe()
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@TRANS1[###A01######RE]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}

int CCymechsMTSCtrl::SetRobotReadyPos(CString strUnitNum)
{
	int nRet = 0;

	CString strSendMsg;
	CString strCommand = _T("@WAIT~1[") + strUnitNum + _T("01###]");
	strSendMsg.Format(strCommand + _T("%s") + m_strReceiveEndOfStreamSymbol, GetCheckSumValue(strCommand));

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRobotDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strSendMsg);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRobotDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecRobotDoneEvent(60000);
	}

	return nRet;
}


CString CCymechsMTSCtrl::HexStringToBinary(char strHex)
{
	switch (toupper(strHex))
	{
		case '0': return _T("0000");
		case '1': return _T("0001");
		case '2': return _T("0010");
		case '3': return _T("0011");
		case '4': return _T("0100");
		case '5': return _T("0101");
		case '6': return _T("0110");
		case '7': return _T("0111");
		case '8': return _T("1000");
		case '9': return _T("1001");
		case 'A': return _T("1010");
		case 'B': return _T("1011");
		case 'C': return _T("1100");
		case 'D': return _T("1101");
		case 'E': return _T("1110");
		case 'F': return _T("1111");
		default:  return _T("0000");
	}
}

CString CCymechsMTSCtrl::GetCheckSumValue(CString strMsg)
{
	byte	btCheckSum = 0x00;
	CString strCS;

	for (int nIdx = 0; nIdx < strMsg.GetLength(); nIdx++)
	{
		btCheckSum += (byte)strMsg[nIdx];
	}

	sprintf_s(LPSTR(LPCTSTR(strCS)), sizeof(strCS), "%02X", btCheckSum);

	return strCS;
}

int CCymechsMTSCtrl::WaitExecAckEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hAckEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -1;
	}	

	if (m_bOccurAlarm == TRUE)
	{
		return -3;
	}

	return 0;
}

int CCymechsMTSCtrl::WaitExecAlignDoneEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hAlignDoneEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -2;
	}

	if (m_bOccurAlarm == TRUE)
	{
		return -3;
	}

	return 0;
}

int CCymechsMTSCtrl::WaitExecFlipDoneEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hFlipDoneEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -2;
	}

	if (m_bOccurAlarm == TRUE)
	{
		return -3;
	}

	return 0;
}

int CCymechsMTSCtrl::WaitExecRobotDoneEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hRobotDoneEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -2;
	}

	if (m_bOccurAlarm == TRUE)
	{
		return -3;
	}

	return 0;
}

int CCymechsMTSCtrl::WaitExecLpmDoneEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hLpmDoneEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -2;
	}

	if (m_bOccurAlarm == TRUE)
	{
		return -3;
	}

	return 0;
}

int CCymechsMTSCtrl::WaitExecInitEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hInitEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -2;
	}

	if (m_bOccurAlarm == TRUE)
	{
		return -3;
	}

	return 0;
}

int CCymechsMTSCtrl::WaitExecTagReadEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hTagReadEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -2;
	}

	if (m_bOccurAlarm == TRUE)
	{
		return -3;
	}

	return 0;
}

int CCymechsMTSCtrl::WaitExecSpeedReadEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hSpeedReadEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -2;
	}

	if (m_bOccurAlarm == TRUE)
	{
		return -3;
	}

	return 0;
}
