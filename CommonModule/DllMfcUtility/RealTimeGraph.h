#if !defined(AFX_REALTIMEGRAPH_H__6CCC3A6E_457A_4073_9F88_F25F647DEC4F__INCLUDED_)
#define AFX_REALTIMEGRAPH_H__6CCC3A6E_457A_4073_9F88_F25F647DEC4F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RealTimeGraph.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRealTimeGraph window
#include "MemDC.h"

//화면에 표시될 그래프의 갯수는 20개로 한다.
//한번에 20개의 데이터를 동시에 표현이 가능하다.
#define _MAX_GRAPH_VALUE 20
//시간별로 들어온 데이터를 저장한다..
typedef struct _TimeValue
{
	CTime valueTime;
	double value;
}stTimeValue;

//그래프를 그리기 위한 설정들을 저장한다.
class CValue
{
public:
	BOOL        m_bUse;          //전체 데이터중 사용하지 않는 데이터를 그릴 필요가 없으므로 그에대한 설정 
	COLORREF	m_color;		// 각 데이터에 대한 색
	int			m_iLineStyle;	// 펜 스타일
	stTimeValue	*pData;		    // 데이터를 저장할 Array
	long		m_lNoValues;	
	long		m_lbegin;		// 시작포인트
	long		m_lend;			// 끝 포인트

	CValue();
	virtual ~CValue();

	void AddPoint(CTime &valuetime, double &y);
};

class AFX_EXT_CLASS CRealTimeGraph : public CWnd
{
// Construction
public:
	CRealTimeGraph();

// Attributes
public:
	static int nMaxData; //연속해서 그릴수 있는 데이터의 값

protected:
	CValue m_GraphData[_MAX_GRAPH_VALUE];
	CRect m_ClientRect;
	CRect m_GraphRect;

	double xdist;          //X축의 거리
	double ydist;          //Y축의 거리

	double m_dYGap;        //Y축 하나의 그리드가 차지하는 Gap 
	double m_dXGap;        //X축 하나의 그리드가 차자히는 Gap
	
	int minrange , maxrange;
	int ntimeSpan;
	
	double m_dSecPixel;
	double m_dSecDiff;
	
	CFont m_font;
	LOGFONT m_logfont;

	COLORREF m_GridColor;
	COLORREF m_DotColor;
	COLORREF m_StickColor;
	COLORREF m_RectGraphColor;

	CTime mintime;
	CTime maxtime;
		
	CPoint *m_GraphPoint; //라인을 그리기 위해 해당 포인터의 위치를 저장한다.

	CString strTitle;     //그래프의 타이틀을 지정한다.. 
// Operations
public:
	BOOL CreateGraphWnd(LPCTSTR szAppName , CRect m_Rect , DWORD dwStyle , CWnd* pParent , UINT nID);
	void AddPoint(int point , CTime& valueTime , double& value);
	void Redraw();
	void SetGraphPointColor(int point , COLORREF clr);
	void SetTitle(CString strTitle);	

protected:
	void ComputeDistance();
	void DrawXGrid(CDC* pDC);
	void DrawYGrid(CDC* pDC);
	void DrawGridEdge(CDC* pDC);
	void DrawGraph(CDC* pDC);
	void SetXRange(CTime& fromtime , CTime& totime);
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRealTimeGraph)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRealTimeGraph();
	
	// Generated message map functions
protected:
	//{{AFX_MSG(CRealTimeGraph)
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnNcPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REALTIMEGRAPH_H__6CCC3A6E_457A_4073_9F88_F25F647DEC4F__INCLUDED_)
