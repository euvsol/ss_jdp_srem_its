// RealTimeGraph.cpp : implementation file
//

#include "stdafx.h"
#include "RealTimeGraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


int CRealTimeGraph::nMaxData;
/////////////////////////////////////////////////////////////////////////////
//CValue
CValue::CValue()
{
	m_bUse       = FALSE;
	m_color		 = RGB(255 , 0 , 0);
	m_iLineStyle = PS_SOLID;
	m_lNoValues	 = 0;
	m_lbegin	 = 0;
	m_lend		 = 0;
	pData		 = NULL;
}

CValue::~CValue()
{
	if(pData != NULL)
		free(pData);
}

//그래프를 그릴 데이터들을 저장한다..
void CValue::AddPoint(CTime& valueTime , double& y)
{
	if(m_lNoValues > 0)
		pData = (stTimeValue *)realloc(pData , (m_lNoValues + 1) * sizeof(stTimeValue));
	else
		pData = (stTimeValue *)malloc((m_lNoValues + 1) * sizeof(stTimeValue));

	pData[m_lend].valueTime = valueTime;
	pData[m_lend].value	= y;
	
	m_lNoValues++;
	m_lend++;

	if(m_lend >= CRealTimeGraph::nMaxData)
	{
		free(pData);
		m_lNoValues = 0;
		m_lbegin =0;
		m_lend = 0;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CRealTimeGraph
#define _STICK_COLOR RGB(0 , 0 , 0)
#define _DOT_COLOR RGB(0 , 0 , 0)
#define _GRID_COLOR RGB(192 , 192 , 192)
#define _GRAPH_RECT_COLOR RGB(255 , 255 , 255)
#define _LINE_COLOR RGB(255 , 0 , 0)

CRealTimeGraph::CRealTimeGraph()
{
	xdist = 0.0;     //x축의 거리
	ydist = 0.0;     //y축의 거리
	
	m_dXGap = 0.0;
	m_dYGap = 0;

	minrange = 0;    //최소값
	maxrange = 100;  //최대값

	//초당 이동픽셀 수
	m_dSecPixel = 1.0;
	//초당 이동거리..
	m_dSecDiff = 10.0; 
	//폰트 설정..
	
	m_logfont.lfHeight			= 20;
	m_logfont.lfWidth			= 15;
	m_logfont.lfEscapement		= 10;
	m_logfont.lfOrientation		= 10;
	m_logfont.lfWeight			= 600;
	m_logfont.lfItalic			= FALSE;
	m_logfont.lfUnderline		= FALSE;
	m_logfont.lfStrikeOut		= FALSE;
	m_logfont.lfCharSet			= ANSI_CHARSET;
	m_logfont.lfOutPrecision	= OUT_DEFAULT_PRECIS;
	m_logfont.lfClipPrecision	= CLIP_DEFAULT_PRECIS;
	m_logfont.lfQuality			= PROOF_QUALITY;
	m_logfont.lfPitchAndFamily	= DEFAULT_PITCH;
	strcpy(m_logfont.lfFaceName,"Ms sans serif");

	m_font.CreateFontIndirect(&m_logfont);

	m_GridColor  = _GRID_COLOR;
	m_DotColor   = _DOT_COLOR;
	m_StickColor = _STICK_COLOR;
	m_RectGraphColor = _GRAPH_RECT_COLOR;

	mintime = 0;
	maxtime = 600;
	
	ntimeSpan = 60;
	//시간축의 범위는 ntimeSpan초로 설정한다.
	//X축을 시간축으로 할때 100초로 간격을 맞춘다..(셀이 100개가 생긴다.)
	SetXRange(CTime::GetCurrentTime() - CTimeSpan(ntimeSpan) , CTime::GetCurrentTime()); //시간축의 범위 설정

	nMaxData = 10000; //데이터의 저장은 10000개까지 가능하다..
	m_GraphPoint = new CPoint[nMaxData];
	
	strTitle = _T("그래프 TEST");
	
}

CRealTimeGraph::~CRealTimeGraph()
{
	delete [] m_GraphPoint;
}


BEGIN_MESSAGE_MAP(CRealTimeGraph, CWnd)
	//{{AFX_MSG_MAP(CRealTimeGraph)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_NCPAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CRealTimeGraph message handlers

BOOL CRealTimeGraph::CreateGraphWnd(LPCTSTR szAppName , CRect m_Rect , DWORD dwStyle , CWnd* pParent , UINT nID)
{
	//윈도우를 생성하기 위한 클래스명
	
	DWORD m_dwStyle = dwStyle & ~WS_DLGFRAME;
	m_dwStyle = m_dwStyle | WS_CAPTION;
	

	CString strClassName = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW);

	if(!CWnd::Create(strClassName , szAppName , m_dwStyle , m_Rect , pParent ,  nID))
	{
		AfxMessageBox("윈도우를 생성할 수 없습니다..");
		return FALSE;
	}

	GetClientRect(m_ClientRect);
	//그래프를 그리기 위한 영역
	m_GraphRect.left   = m_ClientRect.left + 30;
	m_GraphRect.top    = m_ClientRect.top + 10;
	m_GraphRect.right  = m_ClientRect.right - 10;
	m_GraphRect.bottom = m_ClientRect.bottom - 10;
	
	ComputeDistance();

	return TRUE;
}


void CRealTimeGraph::ComputeDistance()
{
	//x 와 y 축의 거리를 계산
	xdist = (double)(m_GraphRect.right - m_GraphRect.left);
	ydist = (double)(m_GraphRect.bottom - m_GraphRect.top);

	//Y축의 그리드 갯수를 총 10개로 한다.(100%가 되게 하기 위해..)
	m_dYGap = ydist / 10.0;

	//초당 이동픽셀
	m_dSecPixel = ((double)(maxtime.GetTime() - mintime.GetTime())) / (double)m_GraphRect.Width();	
}

void CRealTimeGraph::OnNcPaint() 
{
	// TODO: Add your message handler code here
	// Do not call CWnd::OnNcPaint() for painting messages

	CWindowDC dc(this);
	CFont *m_pFont;
	CBrush m_Brush , *m_pBrush;
	
	m_pFont = dc.SelectObject(&m_font);

	int cy = GetSystemMetrics(SM_CYCAPTION);
	CRect ncbkRect(m_ClientRect.left + 3 , m_ClientRect.top , m_ClientRect.right + 3 , cy + 2);
	
	m_Brush.CreateSolidBrush(RGB(192 , 200 , 192));
	m_pBrush = dc.SelectObject(&m_Brush);
		
	dc.FillRect(ncbkRect , &m_Brush);
	dc.DrawEdge(ncbkRect , BDR_RAISEDINNER| BDR_RAISEDOUTER , BF_RECT);
	dc.SelectObject(m_pBrush);
	
	dc.SetBkColor(RGB(192 , 200  , 192));
	dc.DrawText(strTitle , ncbkRect , DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	dc.SelectObject(m_pFont);
}


void CRealTimeGraph::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	//메모리DC를 이용 하므로 
	//메모리DC 전체를 
	CBrush hBrush , *pOldBrush;
	hBrush.CreateSolidBrush(RGB(200 , 200 , 192));
	pOldBrush = dc.SelectObject(&hBrush);
	
	CMemmDC m_MemDC(&dc);
	CFont *m_pFont;
	
	m_pFont = m_MemDC.SelectObject(&m_font);

	m_MemDC.FillRect(m_ClientRect , &hBrush);
	m_MemDC.DrawEdge(m_ClientRect , BDR_RAISEDINNER| BDR_RAISEDOUTER , BF_RECT);
	m_MemDC.SelectObject(pOldBrush);

	hBrush.DeleteObject();

	//그래프가 표시될 영역을 그린다.
	DrawGridEdge(&m_MemDC);
	
	//X - Y 축을 그린다.
	DrawXGrid(&m_MemDC);	
	DrawYGrid(&m_MemDC);
	DrawGraph(&m_MemDC);
	
	m_MemDC.SelectObject(m_pFont);
	// Do not call CWnd::OnPaint() for painting messages
}

void CRealTimeGraph::DrawGridEdge(CDC* pDC)
{
	CBrush bkbrush;
	CBrush *m_pBrush;

	bkbrush.CreateSolidBrush(m_RectGraphColor);
	m_pBrush = pDC->SelectObject(&bkbrush);
	pDC->FillRect(m_GraphRect , &bkbrush);
	pDC->DrawEdge(m_GraphRect , BDR_SUNKENINNER | BDR_SUNKENOUTER , BF_RECT);

	pDC->SelectObject(m_pBrush);
}

void CRealTimeGraph::DrawXGrid(CDC* pDC)
{
	CPen *m_pPen;
	CPen m_dotPen(PS_DOT , 1 , _DOT_COLOR);
	CPen m_GridPen(PS_SOLID , 1 , _GRID_COLOR);
	CPen m_StickPen(PS_SOLID , 1 , _STICK_COLOR);

	//최소 시작시간..
	long xGrid = mintime.GetTime();  
	//초당 시간축(default 10초)
	long delta = (long)(m_dSecDiff + ((long)(m_dSecPixel)) * (int)m_dSecDiff); 
	//셀과 셀사이의 간격..
	long dist = (long)((delta / m_dSecDiff)); 

	//한셀당 간격차
	long diff = xGrid % delta;              
	xGrid = xGrid - diff;
	
	//X축 그리드를 그린다..
	m_pPen = pDC->SelectObject(&m_GridPen);
	
	for(long sx = mintime.GetTime() ; sx < maxtime.GetTime() ; sx += dist)
	{
		if(sx > mintime.GetTime())
		{
			int x = (int)(m_GraphRect.left + ((sx - mintime.GetTime()) / m_dSecPixel));
	
			pDC->MoveTo(x , m_GraphRect.bottom - 1);
			pDC->LineTo(x , m_GraphRect.top + 1);
			
		}
	}

	pDC->SelectObject(m_pPen);
	
	//10초마다 시간축의 간격에 도트선을 그린다..
	m_pPen = pDC->SelectObject(&m_dotPen);
	while(xGrid <= maxtime.GetTime())
	{
		if(  xGrid > mintime.GetTime() && xGrid < maxtime.GetTime() )
		{
			int x = (int)(m_GraphRect.left + ((xGrid - mintime.GetTime()) / m_dSecPixel ));
			pDC->MoveTo(x , m_GraphRect.top);
			pDC->LineTo(x , m_GraphRect.bottom);
		}

		xGrid += delta;
	}

	pDC->SelectObject(m_pPen);
}

void CRealTimeGraph::DrawYGrid(CDC* pDC)
{
	CPen* m_pPen;
	CPen m_GridPen(PS_SOLID , 1 , _GRID_COLOR);
	CPen m_StickPen(PS_SOLID , 1 , _STICK_COLOR);
	CPen m_LinePen(PS_SOLID , 1 , _LINE_COLOR);
	

	CString strText;

	for(int i = minrange ; i < maxrange ; i += 10)
	{
		int off = 3;
		if( i % 50 == 0)
			off = 5;

		int nY = (int)((double)m_GraphRect.bottom - ((m_dYGap * ((double)i / 10.0)) - 1.0));

		if(i > minrange)
		{
			if((i % 50) == 0)
				m_pPen = pDC->SelectObject(&m_StickPen);
			else
				m_pPen = pDC->SelectObject(&m_GridPen);
			
			//내부 그리드
			pDC->MoveTo(m_GraphRect.left + 1, nY);
			pDC->LineTo(m_GraphRect.right - 1, nY);
		
			pDC->SelectObject(m_pPen);

			m_pPen = pDC->SelectObject(&m_StickPen);
			//왼쪽 눈금자..
			pDC->MoveTo(m_GraphRect.left , nY);
			pDC->LineTo(m_GraphRect.left - off , nY);
	
			//오른쪽 눈금자
		//	pDC->MoveTo(m_GraphRect.right - 1 , nY);
		//	pDC->LineTo(m_GraphRect.right + off , nY);
			pDC->SelectObject(m_pPen);			
		}

		strText.Format("%d" , i);
		pDC->SetBkMode(TRANSPARENT);
		
		if((i % 50) == 0)
		{
			if(i == 0)
				pDC->TextOut(m_GraphRect.left - (off + 5) , nY - 10 , strText);	
			else
				pDC->TextOut(m_GraphRect.left - (off + 15) , nY - 5 , strText);	
		}
		else if( i == 90)
		{
			strText.Format("%d" , i + 10);
			pDC->TextOut(m_GraphRect.left - (off + 20) , nY - (int)(5.0 + m_dYGap) , strText);	
		}
	}
	
}

void CRealTimeGraph::DrawGraph(CDC* pDC)
{
	for(int i = 0 ; i < _MAX_GRAPH_VALUE ; i++)
	{
		if(m_GraphData[i].m_bUse)
		{
			BOOL bMore = TRUE;      //데이터가 하나 이상일 경우 계속 그리기 위한 플래그
			int dataCnt;
			
			long lStart   = m_GraphData[i].m_lbegin;
			long lEnd     = m_GraphData[i].m_lend;
						
			CPen *m_pPen;
			CPen m_pen(m_GraphData[i].m_iLineStyle , 1 , m_GraphData[i].m_color);
			
			m_pPen = pDC->SelectObject(&m_pen);
			
			

			while(bMore)
			{
				//데이트를 그려주고 while 루프를 빠져 나간다..
				bMore = FALSE;
					
				dataCnt = 0;

				while(lStart != lEnd)
				{
					time_t timeValue = m_GraphData[i].pData[dataCnt].valueTime.GetTime();
					double g_dValue = m_GraphData[i].pData[dataCnt].value;  //표시될 그래프의 값(한줄에 계산식을 표시하니 너무 길다..); 
							
					int x = (int)(m_GraphRect.left + ((timeValue - mintime.GetTime()) / m_dSecPixel));
					int y = (int)(m_GraphRect.bottom - (((g_dValue / 10.0) * m_dYGap) + (((long)g_dValue) % 10)));
						
					m_GraphPoint[dataCnt].x = x;
					m_GraphPoint[dataCnt].y = y + 2;
						
					dataCnt++;
					lStart++;
				}
			}
			
			if(dataCnt > 0)
				pDC->Polyline(m_GraphPoint , dataCnt);
		
			pDC->SelectObject(m_pPen);
		}
	}
}

//표시할 그래프의 위치와 시간간격을 구한다.
void CRealTimeGraph::AddPoint(int point , CTime& valueTime , double& value)
{
	m_GraphData[point].AddPoint(valueTime , value);
	m_GraphData[point].m_bUse = TRUE;
		
	if(valueTime > maxtime)
	{
		time_t span = maxtime.GetTime() - mintime.GetTime(); //시간간격을 구한다.
		time_t mtime = valueTime.GetTime() - span;           //최소 시간값을 구한다.
			
		//X축(시간축)의 범위 설정)
		SetXRange(CTime(mtime) , valueTime);
	}	
}

//그래프의 색깔을 설정한다..
void CRealTimeGraph::SetGraphPointColor(int point , COLORREF clr)
{
	m_GraphData[point].m_color = clr;
}

void CRealTimeGraph::SetXRange(CTime& fromtime , CTime& totime)
{
	mintime = fromtime;
	maxtime = totime;
}

void CRealTimeGraph::SetTitle(CString strTitle)
{
	this->strTitle = strTitle;
}
//화면의 깜박거림을 제거하기 위해...
BOOL CRealTimeGraph::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return FALSE;
//	return CWnd::OnEraseBkgnd(pDC);
}

void CRealTimeGraph::Redraw()
{
	InvalidateRect(m_GraphRect);
}

