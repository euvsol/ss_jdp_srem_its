/**
 * Ethernet Communication Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

#define IDS_ERROR_WSASTARTUP					101
#define IDS_ERROR_CREATE_SOCKET					102
#define IDS_ERROR_BINDING_SOCKET				103
#define IDS_ERROR_LISTENING_SOCKET				104
#define IDS_ERROR_SEND_FAIL						106
#define IDC_ERROR_TCPIP_SOCKET_TIMER_OUT_EVENT	107
#define IDC_ERROR_NOT_CONNECTED_TO_MODULE		108
#define IDS_ERROR_INVALIDATE_SOCKET				109
#define IDD_DIALOG_TCPIP						1000
#define IDS_ERROR_INVALIDATE_ACCEPT_SOCKET		1005
#define IDC_ERROR_TIMER_OUT_EVENT1				1007
#define IDC_ERROR_TIMER_OUT_EVENT2				1008

class AFX_EXT_CLASS CEthernetCom : public CECommon
{
public:
	CEthernetCom();
	~CEthernetCom();

	char*					m_IpAddr;
	int						m_nPort;
	int						m_nRet;													
	int						m_nReceiveMaxBufferSize;																			// 슈신 버퍼크기	
	BOOL					m_bIsServerSocket;																					// 서버인지 클라인이언트 인지 구분															
	BOOL					m_bBinding;																							// 소켓 바인딩 여부 플래그
	BOOL					m_bConnected;																						// 소켓 연결 여부 플래그	
	int						m_nSocketStatus;																					// 소켓 연결 여부 확인
	SOCKET					m_Socket;																							// 소켓 객체	
	SOCKET					m_AcceptSocket;																						// 수신 허용 여부 객체
	CString					m_strSendMsg;
	CString					m_strReceivedMsg;
	CString					m_strSendEndOfStreamSymbol;																			// 송신 protocol 스트림끝 기호
	CString					m_strReceiveEndOfStreamSymbol;																		// 수신 protocol 스트림끝 기호

	int						OpenTcpIpSocket(char *Ip, int nPort, BOOL bIsServerSocket = FALSE, int nReceiveMaxBufferSize = 12401);//bIsServerSocket FALSE: 클라이언트, TRUE: 서버, nReceiveMaxBufferSize 는 513 에서 186으로 변경_KYD)20191217_Data stream error correction																																	  
	void					CloseTcpIpSocket();																					// 6201 -> 14601 Optic sensor 까지 포함									
	int						GetSocketStatus();																					// -1: Disconnected
	virtual int				WaitReceiveEventThread();																						//
	
	virtual int				On_Accept();																						//
	int						Send(char *lParam, int nTimeOut = 60000, int nRetryCnt = 3);										//
	int						Send(int nSize, char *lParam, int nTimeOut = 60000, int nRetryCnt = 3);								// 
	
	virtual	int				SendData(char *lParam, int nTimeOut = 60000, int nRetryCnt = 3);	
	virtual	int				ReceiveData(char *lParam);																			//
	virtual	int				ReceiveData(char *lParam, int nLength);

	BOOL					m_bKillReceiveThread;																				// 수신 스래드 종료 여부 플래그
	BOOL					m_bKillAcceptThread;																				// 수신 허용 스래드 종료 여부 플래그
	HANDLE					m_hSocketEvent;																						// Send() 자체에 대한 응답, 실행 혹은 이밴트 여부 핸들
	HANDLE					m_hReceiveEvent;																						// Send() 자체에 대한 응답, 실행 혹은 이밴트 여부 핸들
	CWinThread*				m_pReceiveThread;																					// 수신 스래드 객체 포인터
	CWinThread*				m_pAcceptThread;																					// 수신 허용 스래드 객체 포인터
	static	UINT __cdecl	ReceiveThreadFunc(LPVOID pClass) { ((CEthernetCom *)pClass)->WaitReceiveEventThread(); return 0; }				//	
	static	UINT __cdecl	AcceptThreadFunc(LPVOID pClass) { ((CEthernetCom *)pClass)->On_Accept(); return 0; }				//

	void					usleep(unsigned int usec);
	
};