#include "stdafx.h"
#include "CEthernetCom.h"

CEthernetCom::CEthernetCom()
{
	m_bIsServerSocket = FALSE;
	m_bBinding		  = FALSE;					
	m_bConnected	  = FALSE;				
	m_Socket		  = INVALID_SOCKET;			
	m_AcceptSocket	  = INVALID_SOCKET;	

	m_bKillReceiveThread = FALSE;		
	m_bKillAcceptThread	 = FALSE;		

	m_hSocketEvent	 = NULL;				
	m_hReceiveEvent = NULL;	

	m_pReceiveThread = NULL;			
	m_pAcceptThread  = NULL;			

	m_strSendMsg = _T("");
	m_strReceivedMsg = _T("");
	m_strSendEndOfStreamSymbol.Empty();				
	m_strReceiveEndOfStreamSymbol.Empty();			

	m_nRet = 0;
}

CEthernetCom::~CEthernetCom()
{
	CloseTcpIpSocket();
}

int CEthernetCom::GetSocketStatus()
{
	return m_nSocketStatus;
}

int CEthernetCom::OpenTcpIpSocket(char *Ip, int nPort, BOOL bIsServerSocket, int nReceiveMaxBufferSize)
{
	int nRet = 0;
	int iResult;
	WSADATA wsaData;
	CString strError;

	m_IpAddr = Ip;
	m_nPort	 = nPort;

	m_nReceiveMaxBufferSize = nReceiveMaxBufferSize;
	m_bIsServerSocket = bIsServerSocket;

	if (m_bIsServerSocket)
	{
		m_bBinding = FALSE;
		m_pAcceptThread = NULL;
		m_bKillAcceptThread = FALSE;
		m_AcceptSocket = INVALID_SOCKET;
	}

	m_bConnected = FALSE;
	m_pReceiveThread = NULL;
	m_bKillReceiveThread = FALSE;
	m_Socket = INVALID_SOCKET;	 // initialize socket to invalid handle

	if (m_hSocketEvent == NULL)
	{
		m_hSocketEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	}
	else
	{
		int test = 0;
	}
	if (m_hReceiveEvent == NULL)
	{
		m_hReceiveEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	}
	else
	{
		int test = 0;
	}
	
	//1.윈속파일(ws2_32.dll) 초기화
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR)
	{
		WSACleanup();
		nRet = IDS_ERROR_WSASTARTUP;
		return nRet;
	}

	//2.소켓 정의
	m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);	//SOCK_STREAM=TCP방식, SOCK_DGRAM=UDP방식
	if (m_Socket == INVALID_SOCKET)
	{
		strError.Format(_T("Socket Creation Fail! : %ld\n"), WSAGetLastError());
		WSACleanup();
		nRet = IDS_ERROR_CREATE_SOCKET;
		return nRet;
	}

	SOCKADDR_IN ServerAddr;
	ZeroMemory(&ServerAddr, sizeof(SOCKADDR_IN));
	ServerAddr.sin_family = AF_INET;	//AF_INET:internetwork: UDP, TCP, etc.
	ServerAddr.sin_port = htons(m_nPort);
	ServerAddr.sin_addr.s_addr = inet_addr(m_IpAddr);

	if (m_bIsServerSocket) //Server
	{
		if (bind(m_Socket, (SOCKADDR*)&ServerAddr, sizeof(ServerAddr)) == SOCKET_ERROR)
		{
			closesocket(m_Socket);
			WSACleanup();
			nRet = IDS_ERROR_BINDING_SOCKET;
			return nRet;
		}
		else
		{
			m_bBinding = TRUE;
		}

		if (listen(m_Socket, 5) == SOCKET_ERROR)
		{
			//Error listening on socket.
			closesocket(m_Socket);
			WSACleanup();
			nRet = IDS_ERROR_LISTENING_SOCKET;
			return nRet;
		}

		m_pAcceptThread = ::AfxBeginThread(AcceptThreadFunc, this, THREAD_PRIORITY_NORMAL, 0, 0);
	}
	else//Client
	{

		//int connectResult = connect(m_Socket, (struct sockaddr*)&ServerAddr, sizeof(ServerAddr)); // 0일때 Ok
		//if (connectResult)
		if (connect(m_Socket, (struct sockaddr*)&ServerAddr, sizeof(ServerAddr)))
		{
			//int closeResult = closesocket(m_Socket); // 0일때 Ok
			//int cleanUpResult = WSACleanup(); // 0일때 Ok
			closesocket(m_Socket); // 0일때 Ok
			WSACleanup(); // 0일때 Ok
			nRet = IDS_ERROR_CREATE_SOCKET;

			m_Socket = INVALID_SOCKET; // 21.01.21 ihlee 추가

			return nRet;
		}

		CString Temp;
		Temp.Format(_T("::::::: TCPIP Socket Connected IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
		SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);

		m_bConnected = TRUE;
		m_pReceiveThread = ::AfxBeginThread(ReceiveThreadFunc, this, THREAD_PRIORITY_NORMAL, 0, 0);
	}


	return nRet;
}

void CEthernetCom::CloseTcpIpSocket()
{
	if (m_bIsServerSocket)
	{
		m_bKillAcceptThread = TRUE;
		m_bBinding = FALSE;
		closesocket(m_AcceptSocket);
		m_AcceptSocket = INVALID_SOCKET;
	}

	
	m_bConnected = FALSE;
	m_bKillReceiveThread = TRUE;

	//if (m_Socket != INVALID_SOCKET) //21.01.21 ihlee 추가
	{
#if TRUE	//20200710 jkseo, 프로그램 종료 시 서버와 정상적인 종료가 이루어지지 않아서 추가.
		int code;
		code = shutdown(m_Socket, SD_BOTH); //0일때 Ok
		int status;

		if (code != SOCKET_ERROR)
		{
			fd_set  readfds;
			fd_set  errorfds;
			timeval timeout;

			FD_ZERO(&readfds);
			FD_ZERO(&errorfds);
			FD_SET(m_Socket, &readfds);
			FD_SET(m_Socket, &errorfds);

			timeout.tv_sec = 3;
			timeout.tv_usec = 0;
			status = select(1, &readfds, NULL, &errorfds, &timeout);
		}

		code = closesocket(m_Socket); //0일때 Ok
#else
		closesocket(m_Socket);
#endif
	}

	m_Socket = INVALID_SOCKET;

	if (m_pReceiveThread != NULL)
	{
		HANDLE threadHandle = m_pReceiveThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)	
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode);
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pReceiveThread = NULL;
	}

	if (m_bIsServerSocket && m_pAcceptThread != NULL)
	{
		HANDLE threadHandle = m_pAcceptThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)		
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode);
			if (dwExitCode == STILL_ACTIVE)	// 259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pAcceptThread = NULL;
	}

	//int cleanUpResult;
	//cleanUpResult= WSACleanup();
	WSACleanup();

	if (m_hSocketEvent != NULL)
	{
		CloseHandle(m_hSocketEvent);
		m_hSocketEvent = NULL; //이거 필요? ihlee
	}

	if (m_hReceiveEvent != NULL)
	{
		CloseHandle(m_hReceiveEvent);
		m_hReceiveEvent = NULL; //이거 필요? ihlee
	}
		
	return;
}

int CEthernetCom::On_Accept()
{
	int nRet = 0;

	TRACE("Accept Thread Start..\n");

	while (!m_bKillAcceptThread)
	{
		if (m_AcceptSocket == INVALID_SOCKET)
		{
			m_AcceptSocket = accept(m_Socket, NULL, NULL);
			m_bConnected = TRUE;
		}
		Sleep(10);
	}

	TRACE("Accept Thread End..\n");

	return nRet;
}

int CEthernetCom::Send(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	int nCntRetry = 0;

	EnterCriticalSection(&cs);

	SOCKET	tmpSocket = INVALID_SOCKET;

	while (nCntRetry < nRetryCnt)
	{
		if (m_bIsServerSocket)
		{
			tmpSocket = m_AcceptSocket;
		}
		else
		{
			tmpSocket = m_Socket;
		}

		if (m_bConnected)
		{
			if (tmpSocket == INVALID_SOCKET)
			{
				nRet = IDS_ERROR_INVALIDATE_SOCKET;
				LeaveCriticalSection(&cs);
				return nRet;
			}

			ResetEvent(m_hSocketEvent);
			nRet = send(tmpSocket, lParam, strlen(lParam), 0);
			if (nRet == SOCKET_ERROR)
			{
				nRet = IDS_ERROR_SEND_FAIL;
				SetEvent(m_hSocketEvent);
				LeaveCriticalSection(&cs);
				return nRet;											
			}

			if (nTimeOut != 0)
			{
				nRet = WaitEvent(m_hSocketEvent, nTimeOut);
				if (nRet != 0)
				{
					SetEvent(m_hSocketEvent);
					LeaveCriticalSection(&cs);
					return nRet;
				}
			}
			else
			{
				if (nRet == strlen(lParam))
				{
					nRet = 0;
				}					
				else
				{
					nRet = IDS_ERROR_SEND_FAIL;
					SetEvent(m_hSocketEvent);
					LeaveCriticalSection(&cs);
					return nRet;
				}
			}
		}
		else
		{
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			LeaveCriticalSection(&cs);
			return nRet;								
		}

		nCntRetry++;

		if (nRet == 0)
		{
			break;
		}
	}

	LeaveCriticalSection(&cs);

	return nRet;
}

//20200211 jkseo, monitoring data 전송을 위해 추가
int CEthernetCom::Send(int nSize, char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	int nCntRetry = 0;

	EnterCriticalSection(&cs);

	SOCKET	tmpSocket = INVALID_SOCKET;

	while (nCntRetry < nRetryCnt)
	{
		if (m_bIsServerSocket)
		{
			tmpSocket = m_AcceptSocket;
		}
		else
		{
			tmpSocket = m_Socket;
		}

		if (m_bConnected)
		{
			if (tmpSocket == INVALID_SOCKET)
			{
				nRet = IDS_ERROR_INVALIDATE_SOCKET;
				LeaveCriticalSection(&cs);
				return nRet;
			}

			ResetEvent(m_hSocketEvent);
					   
			nRet = send(tmpSocket, lParam, nSize, 0);
			if (nRet == SOCKET_ERROR)
			{
				nRet = IDS_ERROR_SEND_FAIL;
				SetEvent(m_hSocketEvent);
				LeaveCriticalSection(&cs);
				return nRet;
			}

			if (nTimeOut != 0)
			{
				nRet = WaitEvent(m_hSocketEvent, nTimeOut);
				if (nRet != 0)
				{
					SetEvent(m_hSocketEvent);
					LeaveCriticalSection(&cs);
					return nRet;
				}
			}
			else
			{
				if (nRet == nSize)
				{
					nRet = 0;
				}					
				else
				{
					nRet = IDS_ERROR_SEND_FAIL;
					SetEvent(m_hSocketEvent);
					LeaveCriticalSection(&cs);
					return nRet;
				}				
			}
		}
		else
		{
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			LeaveCriticalSection(&cs);
			return nRet;
		}

		nCntRetry++;

		if (nRet == 0)
		{
			break;
		}
	}
		
	LeaveCriticalSection(&cs);

	return nRet;
}

int	CEthernetCom::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	return nRet;
}

int CEthernetCom::ReceiveData(char *lParam)
{
	int nRet = 0;

	return nRet;
}

int CEthernetCom::ReceiveData(char *lParam, int nLength)
{
	int nRet = 0;

	return nRet;
}

int CEthernetCom::WaitReceiveEventThread()
{
	int		nRet = 0;
	int		i = 0;
	char*	pInBuf = new char[m_nReceiveMaxBufferSize];
	char*	pOutBuf = new char[m_nReceiveMaxBufferSize];
	int		total = 0;

	
	int		nCondition = 0;
	unsigned long	tmp = 0;
	struct timeval timeout;
	fd_set fds;

	SOCKET	tmpSocket = INVALID_SOCKET;
	if (m_bIsServerSocket)
	{
		tmpSocket = m_AcceptSocket;
	}
	else
	{
		tmpSocket = m_Socket;
	}

	//TRACE("TCPIP Receive Thread Start..\n");

	memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
	while (!m_bKillReceiveThread)
	{
		m_bConnected = TRUE;
		timeout.tv_sec = 0;
		timeout.tv_usec = 100;
		FD_ZERO(&fds);
		FD_SET(m_Socket, &fds);
		m_nSocketStatus = select(sizeof(fds) * 8, &fds, NULL, NULL, &timeout);

		if (m_nSocketStatus == -1)
		{
			m_bKillReceiveThread = TRUE;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);

			CString Temp;
			Temp.Format(_T("::::::: m_nSocketStatus [ -1 ] IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
			SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);
		}

		if (tmpSocket != INVALID_SOCKET)
		{
			memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
			int nResult = recv(tmpSocket, pInBuf, m_nReceiveMaxBufferSize, NULL);

			if (m_bKillReceiveThread)
			{
				break;
			}

			if (strcmp(m_IpAddr, "192.168.20.10") == 0)
				TRACE("nResult=%d, pInBuf=%s", nResult, pInBuf);
		/*	CString str;
			str = pInBuf;
			SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("ADAM -> MainPC_TCP/IP Socket:" + str + "Transfered")));*/
			SetEvent(m_hSocketEvent);
			total = 0;
			if (nResult > 0)
			{
				for (i = 0; i < nResult; i++)
				{
					pOutBuf[total] = pInBuf[i];
					if (pOutBuf[total] == (char)m_strReceiveEndOfStreamSymbol.GetAt(0)) 
					{
						pOutBuf[total] = '\0';
						nRet = ReceiveData(pOutBuf);
						m_nRet = nRet;
						memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
						total = 0;
						if (i == nResult)
							memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
					}
					else
					{
						total++;
					}
				}
			}
			else
			{
				if (nResult == 0)
				{
					if (strcmp(m_IpAddr, "192.168.20.10") != 0)
					{
						m_bKillReceiveThread = TRUE;
						m_bConnected = FALSE;

						CString Temp;
						Temp.Format(_T("::::::: recv return value [ 0 ] IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
						SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);
					}
				}

				//SaveLogFile("ADAM_Com", (LPSTR)(LPCTSTR)Temp); //ihlee test


			}
		}
		else
		{
			if (m_bIsServerSocket)
			{
				m_AcceptSocket = INVALID_SOCKET;
			}
			else
			{
				m_Socket = INVALID_SOCKET;
			}

			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}

		//Sleep(1); //김영덕 변경
		usleep(50);
	}

	delete[] pInBuf;
	pInBuf = NULL;
	delete[] pOutBuf;
	pOutBuf = NULL;
	TRACE("TCPIP Receive Thread End..\n");

	CString Temp;
	Temp.Format(_T("::::::: 815Line TCPIP Socket Disconnected IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
	SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);

	return nRet;
}

///// Micro second sleep 기능추가_KYD
void CEthernetCom::usleep(unsigned int usec)
{
	HANDLE timer;
	LARGE_INTEGER ft;

	ft.QuadPart = -(10 * (__int64)usec);

	timer = CreateWaitableTimer(NULL, TRUE, NULL);
	SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
	WaitForSingleObject(timer, INFINITE);
	CloseHandle(timer);
}