/**
 * WDI Auto Focus Module Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

#include "./Include/atf_lib_exp.h"
#pragma comment(lib, "atf_lib_dll_x64.lib")

class AFX_EXT_CLASS CWDIAFCtrl : public CECommon
{
public:
	CWDIAFCtrl();
	~CWDIAFCtrl();

	/**
	*@fn int OpenSerialPort(CString sPortName)
	*@brief 디바이스를 오픈한다
	*@date 2019/08/21
	*@param sPortName 포트 번호
	*/
	int OpenSerialPort(CString sPortName, int nBaudrate);

	/**
	*@fn int CloseDevice()
	*@brief 디바이스를 클로즈한다
	*@date 2019/08/21
	*/
	int CloseDevice();

	/**
	*@fn BOOL IsDeviceConnected()
	*@brief 디바이스 연결 상태값을 반환한다
	*@date 2019/11/21
	*/
	BOOL IsDeviceConnected();

	/**
	*@fn BOOL IsHomePosition()
	*@brief 홈 위치에 있는지 상태값을 반환한다
	*@date 2019/11/21
	*/
	BOOL IsHomePosition();

	/**
	*@fn int LaserOn()
	*@brief 레이져를 켠다
	*@date 2019/08/21
	*/
	int LaserOn();

	/**
	*@fn int LaserOff()
	*@brief 레이져를 끈다
	*@date 2019/08/21
	*/
	int LaserOff();

	/**
	*@fn int AFOn()
	*@brief 오토포커스 동작을 켠다
	*@date 2019/08/21
	*/
	int AFOn();

	/**
	*@fn int AFOff()
	*@brief 오토포커스 동작을 끈다
	*@date 2019/08/21
	*/
	int AFOff();

	/**
	*@fn int Make0()
	*@brief 포커스를 맞춘 뒤 Make0을 하면 오토포커스 실행 시 이 위치를 목표로한다
	*@date 2019/08/21
	*/
	int Make0();

	/**
	*@fn int MoveHomeAfm()
	*@brief 홈 위치로 이동한다
	*@date 2019/11/21
	*/
	int MoveHomeAfm();

	/**
	*@fn int SaveAll()
	*@brief 파라메터 정보값들을 저장한다
	*@date 2019/11/21
	*/
	int SaveAll();

	/**
	*@fn int MoveZaxisUp(float ZMove_um)
	*@brief Z축을 올린다
	*@date 2019/08/21
	*@param ZMove_um 움직일 거리
	*/
	int MoveZaxisUp(float ZMove_um);

	/**
	*@fn int MoveZaxisDown(float ZMove_um)
	*@brief Z축을 내린다
	*@date 2019/08/21
	*@param ZMove_um 움직일 거리
	*/
	int MoveZaxisDown(float ZMove_um);

	/**
	*@fn int MoveZaxisStop()
	*@brief Z축을 정지한다
	*@date 2019/11/21
	*/
	int MoveZaxisStop();

	/**
	*@fn int SetAFMotionLimit(int upper, int lower)
	*@brief AF 시 Z축 모션 리미트를 설정한다
	*@date 2020/02/03
	*@param upper upper리밋, lower lower리밋
	*/
	int SetAFMotionLimit(int upper, int lower);

	/**
	*@fn BOOL IsMotionMoving()
	*@brief Z축 동작 여부를 반환한다
	*@date 2019/11/21
	*/
	BOOL IsMotionMoving();

	/**
	*@fn BOOL IsLimitCW()
	*@brief 센서 리밋 여부를 반환한다
	*@date 2019/11/21
	*/
	BOOL IsLimitCW();

	/**
	*@fn BOOL IsLimitCCW()
	*@brief 센서 리밋 여부를 반환한다
	*@date 2019/11/21
	*/
	BOOL IsLimitCCW();

	/**
	*@fn int GetAbsoluteZPos()
	*@brief Z축 절대 위치값을 반환한다
	*@date 2019/11/21
	*/
	int GetAbsoluteZPos();

	/**
	*@fn int ChangeObjNumber(long nNum)
	*@brief 오브젝트 번호를 변경한다
	*@date 2019/08/21
	*@param 오브젝트 번호
	*/
	int ChangeObjNumber(long nNum);

	/**
	*@fn int GetObjectNumber()
	*@brief 설정된 오브젝트 번호를 가져온다
	*@date 2019/08/21
	*/
	int GetObjectNumber();

	/**
	*@fn BOOL GetInRangeStatus()
	*@brief InRange 상태를 가져온다
	*@date 2019/08/21
	*/
	BOOL GetInRangeStatus();

	/**
	*@fn BOOL GetInFocusStatus()
	*@brief InFocus 상태를 가져온다
	*@date 2019/08/21
	*/
	BOOL GetInFocusStatus();

	/**
	*@fn int SetLedPwmValue(int nChannel, u_short uPWM)
	*@brief 해당 채널의 PWM값을 설정한다
	*@date 2019/08/21
	*@param nChannel 채널, uPWM 조명값
	*/
	int SetLedPwmValue(int nChannel, u_short uPWM);

	/**
	*@fn int GetLedPwmValue(int nChannel, u_short* pPWM)
	*@brief 해당 채널에 대한 PWM값을 가져온다
	*@date 2019/08/21
	*@param nChannel 채널, uPWM 조명값
	*/
	int GetLedPwmValue(int nChannel, u_short* pPWM);

	/**
	*@fn int SetLedCurrentValue(int nChannel, u_short uCurrent)
	*@brief 해당 채널에 대한 커런트 값을 설정한다
	*@date 2019/08/21
	*@param nChannel 채널, uCurrent 커런트 값
	*/
	int SetLedCurrentValue(int nChannel, u_short uCurrent);

	/**
	*@fn int GetLedCurrentValue(int nChannel, u_short* pCurrent)
	*@brief 해당 채널에 대한 커런트 값을 가져온다
	*@date 2019/08/21
	*@param nChannel 채널, uCurrent 커런트 값
	*/
	int GetLedCurrentValue(int nChannel, u_short* pCurrent);
};