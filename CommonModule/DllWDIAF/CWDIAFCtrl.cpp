#include "stdafx.h"
#include "CWDIAFCtrl.h"


CWDIAFCtrl::CWDIAFCtrl()
{
}

CWDIAFCtrl::~CWDIAFCtrl()
{
}

int CWDIAFCtrl::OpenSerialPort(CString sPortName, int nBaudrate)
{
	sPortName = _T("\\\\.\\") + sPortName;

	int nRet = 0;

	nRet = ATF_OpenConnection((LPSTR)(LPCTSTR)sPortName, nBaudrate);

	if (nRet != ErrOK)
		return nRet;

	ATF_AfStop();
	Sleep(100);
	ATF_DisableLaser();
	Sleep(100);

	return nRet;
}

int CWDIAFCtrl::CloseDevice()
{
	ATF_AfStop();
	Sleep(100);
	ATF_DisableLaser();
	Sleep(100);
	ATF_CloseConnection();

	return 0;
}

BOOL CWDIAFCtrl::IsDeviceConnected()
{
	BOOL bConnected = FALSE;

	bConnected = ATF_isSerialConnection();

	return bConnected;
}

BOOL CWDIAFCtrl::IsHomePosition()
{
	BOOL bHomePos = FALSE;

	ATF_IsInHomePosition(&bHomePos);

	return bHomePos;
}

int CWDIAFCtrl::LaserOn()
{
	int nRet = 0;

	nRet = ATF_EnableLaser();

	return nRet;
}

int CWDIAFCtrl::LaserOff()
{
	int nRet = 0;

	ATF_AfStop();
	Sleep(100);
	nRet = ATF_DisableLaser();

	return nRet;
}

int CWDIAFCtrl::AFOn()
{
	int HwStat, LaserOnFlag;
	int nRet = -1;

	ATF_ReadHwStat(&HwStat);
	LaserOnFlag = !(HwStat & HwLaserDioDisabled);

	if (LaserOnFlag)		// Laser가 On일 경우만 AF작동.
	{
		nRet = ATF_AFTrack();
	}

	return nRet;
}

int CWDIAFCtrl::AFOff()
{
	int nRet = 0;

	nRet = ATF_AfStop();

	return nRet;
}

int CWDIAFCtrl::Make0()
{
	int nRet = 0;

	nRet = ATF_Make0();

	return nRet;
}

int CWDIAFCtrl::MoveHomeAfm()
{
	int nRet = 0;

	nRet = ATF_RunHomingZ(NULL);

	return nRet;
}

int CWDIAFCtrl::SaveAll()
{
	int nRet = 0;

	nRet = ATF_SaveAll();

	return nRet;
}

int CWDIAFCtrl::MoveZaxisUp(float ZMove_um)
{
	float Zmove_Parameter;
	int	 nRet = -1;

	u_short puStepMm, puUstep;
	ATF_ReadStepPerMmConversion(&puStepMm);
	ATF_ReadMicrostep(&puUstep);

	Zmove_Parameter = (float)((ZMove_um*puStepMm*puUstep) / 1000);

	nRet = ATF_MoveZ(Zmove_Parameter);
	
	return nRet;
}

int CWDIAFCtrl::MoveZaxisDown(float ZMove_um)
{
	float Zmove_Parameter;
	int  nRet = -1;

	u_short puStepMm, puUstep;
	ATF_ReadStepPerMmConversion(&puStepMm);
	ATF_ReadMicrostep(&puUstep);

	Zmove_Parameter = (float)((ZMove_um*puStepMm*puUstep) / 1000);

	nRet = ATF_MoveZ(-Zmove_Parameter);

	return nRet;
}

int CWDIAFCtrl::MoveZaxisStop()
{
	int nRet;

	nRet = ATF_StopZMotor();

	return nRet;
}

int CWDIAFCtrl::SetAFMotionLimit(int upper, int lower)
{
	int nRet = 0;

	u_short puStepMm, puUstep;
	nRet = ATF_ReadStepPerMmConversion(&puStepMm);
	if (nRet != 0) return nRet;
	nRet = ATF_ReadMicrostep(&puUstep);
	if (nRet != 0) return nRet;

	nRet = ATF_WriteMotionLimits((upper*puStepMm*puUstep)/1000, -(lower*puStepMm*puUstep)/1000, 2);
	
	return nRet;
}

BOOL CWDIAFCtrl::IsMotionMoving()
{
	int HwStat, MovingFlag;
	int nRet = -1;

	ATF_ReadHwStat(&HwStat);
	MovingFlag = (HwStat & HwMotionZ);

	return MovingFlag;
}

BOOL CWDIAFCtrl::IsLimitCW()
{
	int HwStat = 0;
	ATF_ReadHwStat(&HwStat);

	if (HwStat & HwMotionCWLimit) 
		return TRUE;
	else
		return FALSE;
}

BOOL CWDIAFCtrl::IsLimitCCW()
{
	int HwStat = 0;
	ATF_ReadHwStat(&HwStat);

	if (HwStat & HwMotionCCWLimit)
		return TRUE;
	else
		return FALSE;
}

int CWDIAFCtrl::GetAbsoluteZPos()
{
	int nRet, nPos;

	nRet = ATF_ReadAbsZPos(&nPos);

	if (nRet == 0)
	{
		return nPos;
	}

	return 0;
}

int CWDIAFCtrl::ChangeObjNumber(long nNum)
{
	if (nNum < 0 || nNum > 7)	
		nNum = 0;	//ObjNum은 0~7까지만

	int nRet = -1;

	nRet = ATF_WriteObjNum(nNum);

	return nRet;
}

int CWDIAFCtrl::GetObjectNumber()
{
	int nNowObjNum;
	
	ATF_ReadObjNum(&nNowObjNum);

	return nNowObjNum;
}

BOOL CWDIAFCtrl::GetInRangeStatus()
{
	short atfStatus;

	try
	{
		ATF_ReadStatus(&atfStatus);

		if ((atfStatus & MsMiv) != 0)
			return TRUE;
		else
			return FALSE;
	}
	catch(int ex)
	{
		return FALSE;
	}
	
}

BOOL CWDIAFCtrl::GetInFocusStatus()
{
	short atfStatus;

	ATF_ReadStatus(&atfStatus);

	if ((atfStatus & MsInFocus) != 0)
		return TRUE;
	else
		return FALSE;
}

int CWDIAFCtrl::SetLedPwmValue(int nChannel, u_short uPWM)
{
	int nRet = 0;

	nRet = ATF_WriteLedPwm(nChannel, uPWM);

	return nRet;
}

int CWDIAFCtrl::GetLedPwmValue(int nChannel, u_short* pPWM)
{
	int nRet = 0;

	nRet = ATF_ReadLedPwm(nChannel, pPWM);

	return nRet;
}

int CWDIAFCtrl::SetLedCurrentValue(int nChannel, u_short uCurrent)
{
	int nRet = 0;

	nRet = ATF_WriteLedCurrent(nChannel, uCurrent);

	return nRet;
}

int CWDIAFCtrl::GetLedCurrentValue(int nChannel, u_short* pCurrent)
{
	int nRet = 0;

	nRet = ATF_ReadLedCurrent(nChannel, pCurrent);

	return nRet;
}