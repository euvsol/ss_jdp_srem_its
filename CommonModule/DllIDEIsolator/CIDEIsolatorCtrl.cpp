#include "stdafx.h"
#include "CIDEIsolatorCtrl.h"


CIDEIsolatorCtrl::CIDEIsolatorCtrl()
{
	m_strReceiveEndOfStreamSymbol = _T("\r");
}


CIDEIsolatorCtrl::~CIDEIsolatorCtrl()
{
}

int	CIDEIsolatorCtrl::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	ResetEvent(m_hStateOkEvent);	//Sets the specified event object to the nonsignaled state.

	m_strSendMsg = lParam;

	nRet = Send(lParam, nTimeOut);

	nRet = WaitEvent(m_hStateOkEvent, nTimeOut);

	if (nRet != 0)
		m_strSendMsg = _T("OFFLINE");

	SaveLogFile("Isolator_Com", _T((LPSTR)(LPCTSTR)("PC -> Isolator : " + m_strSendMsg)));	//통신 상태 기록.

	return nRet;
}

int CIDEIsolatorCtrl::ReceiveData(char *lParam)
{
	int nRet = 0;

	m_strReceivedMsg = lParam;
	SaveLogFile("Isolator_Com", _T((LPSTR)(LPCTSTR)("Isolator -> PC : " + m_strReceivedMsg))); //통신 상태 기록.

	//Received 데이터가 정상 데이터이면 Signal 해준다.
	SetEvent(m_hStateOkEvent);	//Sets the specified event object to the signaled state.

	return nRet;
}