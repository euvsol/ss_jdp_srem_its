/**
 * IDE Isolator Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

class AFX_EXT_CLASS CIDEIsolatorCtrl : public CSerialCom
{
public:
	CIDEIsolatorCtrl();
	~CIDEIsolatorCtrl();

	virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int				ReceiveData(char *lParam); ///< Recive

};

