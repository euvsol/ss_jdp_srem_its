#include "stdafx.h"
#include "CRaonVactraVMTRCtrl.h"


CRaonVactraVMTRCtrl::CRaonVactraVMTRCtrl()
{
	m_bHomeComplete		= FALSE;
	m_bReadyPosition	= FALSE;
	m_bServoMotorOn		= FALSE;
	m_bTempServoOn		= FALSE;
	m_bRobotWorking		= FALSE;
	m_bChangeBattery	= FALSE;
	m_bRobotAutoMode	= FALSE;
	m_bEMOStatus		= FALSE;
	m_bOccurredErr		= FALSE;
	m_bSendAvailable	= TRUE;

	m_dPositionZ		= 0.0;
	m_dPositionT		= 0.0;
	m_dPositionL		= 0.0;
	m_sErrorCode[0]		= { 0, };

	m_strReceiveEndOfStreamSymbol = _T("\r");

	m_hAckEvent  = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hDoneEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hReqEvent  = CreateEvent(NULL, TRUE, FALSE, NULL);
}


CRaonVactraVMTRCtrl::~CRaonVactraVMTRCtrl()
{
	CloseHandle(m_hAckEvent);
	CloseHandle(m_hDoneEvent);
	CloseHandle(m_hReqEvent);
}

int	CRaonVactraVMTRCtrl::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	m_strSendMsg = lParam;
	SaveLogFile("VRobot_Com", _T((LPSTR)(LPCTSTR)("PC -> VRobot : " + m_strSendMsg)));	//통신 상태 기록.
	nRet = Send(lParam, nTimeOut);
	if (nRet != 0)
	{
		CString Temp;
		Temp.Format(_T("PC -> VRobot : SEND FAIL (%d)"), nRet);
		SaveLogFile("VRobot_Com", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
	}

	return nRet;
}

int CRaonVactraVMTRCtrl::ReceiveData(char *lParam)
{
	m_strReceivedMsg = lParam;
	SaveLogFile("VRobot_Com", _T((LPSTR)(LPCTSTR)("VRobot -> PC : " + m_strReceivedMsg))); //통신 상태 기록.
	ParsingData(m_strReceivedMsg);

	return 0;
}

void CRaonVactraVMTRCtrl::ParsingData(CString strRecvMsg)
{
	if (strRecvMsg == _T("")) return;

	CString strCommand, strRecvData;

	m_bOccurredErr = FALSE;

	AfxExtractSubString(strCommand, strRecvMsg, 0, _T(' '));

	if (strCommand == _T("STATUS"))
	{
		AfxExtractSubString(strRecvData, strRecvMsg, 1, _T(' '));

		if (strRecvData.Left(4) == _T("0000"))
		{
			m_bOccurredErr = FALSE;
			//m_sErrorCode   = _T("0000");
		}
		else
		{
			m_bOccurredErr = TRUE;
			//m_sErrorCode   = strRecvData.Left(4);
		}

		strcpy(m_sErrorCode, strRecvData.Left(4));

		m_bHomeComplete		= strRecvData.Mid(8, 1)  == _T("0") ? FALSE : TRUE;
		m_bReadyPosition	= strRecvData.Mid(9, 1)  == _T("0") ? FALSE : TRUE;
		
		m_bTempServoOn		= strRecvData.Mid(10, 1) == _T("0") ? FALSE : TRUE;
		if (m_bTempServoOn != m_bServoMotorOn)
		{
			m_bServoMotorOn = m_bTempServoOn;
			SetEvent(m_hReqEvent);
		}
		
		//m_bServoMotorOn		= strRecvData.Mid(10, 1) == _T("0") ? FALSE : TRUE;
		m_bRobotWorking		= strRecvData.Mid(11, 1) == _T("0") ? FALSE : TRUE;
		m_bChangeBattery	= strRecvData.Mid(12, 1) == _T("0") ? FALSE : TRUE;
		m_bRobotAutoMode	= strRecvData.Mid(14, 1) == _T("0") ? FALSE : TRUE;
		m_bEMOStatus		= strRecvData.Mid(15, 1) == _T("0") ? FALSE : TRUE;
	}

	if (strCommand == _T("SPEED"))
	{
		AfxExtractSubString(strRecvData, strRecvMsg, 1, _T(' '));
		m_nRobotSpeed = atoi(strRecvData);

		SetEvent(m_hReqEvent);
	}

	if (strCommand == _T("POS"))
	{
		AfxExtractSubString(strRecvData, strRecvMsg, 1, _T(' '));
		m_dPositionZ = atof(strRecvData.Mid(1, 6));

		AfxExtractSubString(strRecvData, strRecvMsg, 2, _T(' '));
		m_dPositionT = atof(strRecvData.Mid(1, 6));

		AfxExtractSubString(strRecvData, strRecvMsg, 3, _T(' '));
		m_dPositionL = atof(strRecvData.Mid(1, 6));
	}
	
	if (strCommand == _T("ACK"))
	{
		SetEvent(m_hAckEvent);
	}

	if (strCommand == _T("DONE"))
	{
		SetEvent(m_hDoneEvent);
	}

	if (strCommand == _T("_ERR"))
	{
		m_bOccurredErr = TRUE;
		strcpy(m_sErrorCode, strRecvMsg.Right(4));
		//m_sErrorCode = strRecvMsg.Right(4);

		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
}

int CRaonVactraVMTRCtrl::ServoOn()
{
#if FALSE
	CString strCommand = _T("SON") + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
#else
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strCommand = _T("SON") + m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hReqEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hReqEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecReqEvent(1000);
	}

	return nRet;
#endif
}

int CRaonVactraVMTRCtrl::ServoOff()
{
#if FALSE
	CString strCommand = _T("SOFF") + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
#else
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strCommand = _T("SOFF") + m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hReqEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hReqEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecReqEvent(1000);
	}

	return nRet;
#endif
}


int CRaonVactraVMTRCtrl::MoveRobotHome()
{
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strCommand = _T("HOME") + m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(40000);
	}

	return nRet;
}


int CRaonVactraVMTRCtrl::MoveRobotReady()
{
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strCommand = _T("READY") + m_strReceiveEndOfStreamSymbol;
	
	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);
	
	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;
	
		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(40000);
	}

	return nRet;
}

void CRaonVactraVMTRCtrl::RobotMovePause()
{
	CString strCommand = _T("PAUSE") + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

void CRaonVactraVMTRCtrl::RobotMoveResume()
{
	CString strCommand = _T("RESUME") + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

void CRaonVactraVMTRCtrl::RobotMoveStop()
{
	CString strCommand = _T("STOP") + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

void CRaonVactraVMTRCtrl::ReqRobotPosition()
{
	if (m_bSendAvailable != TRUE) return;

	CString strCommand = _T("RQ POS") + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

void CRaonVactraVMTRCtrl::ReqRobotStatus()
{
	if (m_bSendAvailable != TRUE) return;

	CString strCommand = _T("RQ STATUS") + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

void CRaonVactraVMTRCtrl::ReqRobotPosStatus(CString strArm)
{
	strArm = _T("L");
	CString strCommand = _T("RQ PRESENT ARM ") + strArm + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

int CRaonVactraVMTRCtrl::ReqRobotSpeed()
{
	int nRet = 0;

	CString strCommand = _T("RQ SPEED") + m_strReceiveEndOfStreamSymbol;
	ResetEvent(m_hReqEvent);
	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
		SetEvent(m_hReqEvent);
	else
		nRet = WaitExecReqEvent(5000);

	return nRet;
}

void CRaonVactraVMTRCtrl::ReqStationInfo(int nStation, CString strArg)
{
	CString strStation;
	strStation.Format(_T("%d"), nStation);

	CString strCommand = _T("RQ STATION ") + strStation + _T(" ") + strArg + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

int CRaonVactraVMTRCtrl::PickMask(int nStation)
{
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strStation;
	strStation.Format(_T("%d"), nStation);

	CString strCommand = _T("PICK ") + strStation + _T(" SLOT 1 ARM L") +  m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(50000);
	}

	return nRet;
}

int CRaonVactraVMTRCtrl::PlaceMask(int nStation)
{
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strStation;
	strStation.Format(_T("%d"), nStation);

	CString strCommand = _T("PLACE ") + strStation + _T(" SLOT 1 ARM L") + m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(50000);
	}

	return nRet;
}

int CRaonVactraVMTRCtrl::ExtendToPickMask(int nStation)
{
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strStation;
	strStation.Format(_T("%d"), nStation);

	CString strCommand = _T("EXPICK ") + strStation + _T(" SLOT 1 ARM L") + m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(40000);
	}
	
	return nRet;
}

int CRaonVactraVMTRCtrl::ExtendToPlaceMask(int nStation)
{
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strStation;
	strStation.Format(_T("%d"), nStation);

	CString strCommand = _T("EXPLACE ") + strStation + _T(" SLOT 1 ARM L") + m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(40000);
	}

	return nRet;
}

int CRaonVactraVMTRCtrl::RetractToPickMask(int nStation)
{
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strStation;
	strStation.Format(_T("%d"), nStation);

	CString strCommand = _T("REPICK ") + strStation + _T(" SLOT 1 ARM L") + m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(40000);
	}
	
	return nRet;
}

int CRaonVactraVMTRCtrl::RetractToPlaceMask(int nStation)
{
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strStation;
	strStation.Format(_T("%d"), nStation);

	CString strCommand = _T("REPLACE ") + strStation + _T(" SLOT 1 ARM L") + m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		m_bSendAvailable = TRUE;

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(40000);
	}

	return nRet;
}

int CRaonVactraVMTRCtrl::MoveReservedPosition(int nStation, CString strPosition)
{
	int nRet = 0;

	m_bSendAvailable = FALSE;
	Sleep(500);
	CString strStation;
	strStation.Format(_T("%d"), nStation);

	CString strCommand = _T("GOTO ") + strStation + _T(" SLOT 1 ARM L R ") + strPosition + m_strReceiveEndOfStreamSymbol;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
	{
		m_bSendAvailable = TRUE;

		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(3000);

		m_bSendAvailable = TRUE;
		
		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(40000);
	}

	return nRet;
}

void CRaonVactraVMTRCtrl::CheckCommStatus()
{
	CString strCommand = _T("HELLO") + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

void CRaonVactraVMTRCtrl::SetStationTeachingData(int nStation, CString strArg)
{
	CString strStation;
	strStation.Format(_T("%d"), nStation);

	CString strCommand = _T("SET STATION ") + strStation + _T(" ") + strArg + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

int CRaonVactraVMTRCtrl::SetRobotSpeed(int nSpeed)
{
	int nRet = 0;

	CString strSpeed;
	strSpeed.Format(_T("%d"), nSpeed);
	CString strCommand = _T("SET SPEED ") + strSpeed + m_strReceiveEndOfStreamSymbol;
	ResetEvent(m_hAckEvent);
	nRet = SendData((LPSTR)(LPCTSTR)strCommand);
	if (nRet != 0)
		SetEvent(m_hAckEvent);
	else
		nRet = WaitExecAckEvent(5000);

	return nRet;
}

void CRaonVactraVMTRCtrl::ResetError()
{
	CString strCommand = _T("RESET") + m_strReceiveEndOfStreamSymbol;
	SendData((LPSTR)(LPCTSTR)strCommand);
}

int CRaonVactraVMTRCtrl::WaitExecAckEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hAckEvent, nTimeout) != WAIT_OBJECT_0)
		return -12001;

	if (m_bOccurredErr == TRUE)
		return GetErrorCode(m_sErrorCode);

	return 0;
}

int CRaonVactraVMTRCtrl::WaitExecDoneEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hDoneEvent, nTimeout) != WAIT_OBJECT_0)
		return -12002;

	if (m_bOccurredErr == TRUE)
		return GetErrorCode(m_sErrorCode);

	return 0;
}

int CRaonVactraVMTRCtrl::WaitExecReqEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hReqEvent, nTimeout) != WAIT_OBJECT_0)
		return -12003;

	if (m_bOccurredErr == TRUE)
		return GetErrorCode(m_sErrorCode);

	return 0;
}

int CRaonVactraVMTRCtrl::GetErrorCode(CString sCode)
{
	if (sCode == _T("2000")) return -12005; //Servo Driver Error
	else if (sCode == _T("2001")) return -12006; //Software Limit Error
	else if (sCode == _T("2002")) return -12007; //Servo Off Error
	else if (sCode == _T("2003")) return -12008; //Robot Position Error
	else if (sCode == _T("2004")) return -12009; //Emergency Error
	else if (sCode == _T("2009")) return -12010; //Slot Number Error
	else if (sCode == _T("200A")) return -12011; //Arm Select Error
	else if (sCode == _T("200B")) return -12012; //Home Position Error
	else if (sCode == _T("200C")) return -12013; //Robot Stop Error
	else if (sCode == _T("200D")) return -12014; //Controller Battery Error
	else if (sCode == _T("200E")) return -12015; //Controller CPU Error
	else if (sCode == _T("200F")) return -12016; //Goto Motion Error
	else if (sCode == _T("2010")) return -12017; //AWC Range Error
	else if (sCode == _T("2012")) return -12018; //MECHATROLINK SYNC Error
	else if (sCode == _T("2013")) return -12019; //MECHATROLINK COMM Error
	else if (sCode == _T("2015")) return -12020; //Torque Limit Error
	else if (sCode == _T("201C")) return -12021; //T_Axis Position Moving Error
	else if (sCode == _T("201D")) return -12022; //TR_Axis Position Moving Error
	else if (sCode == _T("1000")) return -12023; //Unknown Command
	else if (sCode == _T("1001")) return -12024; //Too Few Argument
	else if (sCode == _T("1002")) return -12025; //Illegal Station No.
	else if (sCode == _T("1010")) return -12026; //Illegal Arm Select
	else if (sCode == _T("1011")) return -12027; //Illegal Slot No.
	else if (sCode == _T("1012")) return -12028; //Illegal Goto Position
	else if (sCode == _T("1014")) return -12029; //Delta Pick Limit
	else if (sCode == _T("1021")) return -12030; //Illegal Operation
	else if (sCode == _T("1031")) return -12031; //Unknown Argument
	else if (sCode == _T("1040")) return -12032; //Offline Mode
	else if (sCode == _T("1041")) return -12033; //Robot is Running
	else if (sCode == _T("1042")) return -12034; //Servo Off
	else if (sCode == _T("1043")) return -12035; //Robot is Error State
	else if (sCode == _T("1044")) return -12036; //Robot is not Home yet
	else return -12099; //Unknown Error
}