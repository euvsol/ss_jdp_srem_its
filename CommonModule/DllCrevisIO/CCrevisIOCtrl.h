/**
 * Crevis IO Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

#include "./Include/CrevisFnIO.h"
#pragma comment(lib, "CrevisFnIO20.lib")
#pragma warning(disable:4996)

class AFX_EXT_CLASS CCrevisIOCtrl : public CECommon
{
public:
	CCrevisIOCtrl();
	~CCrevisIOCtrl();

private:
	/**
	*@var FNIO_HANDLE m_hSystem
	시스템 핸들 변수
	*/
	FNIO_HANDLE m_hSystem;
	
	/**
	*@var FNIO_HANDLE m_hDevice
	디바이스 핸들 변수
	*/
	FNIO_HANDLE m_hDevice;

	/**
	*@var FNIO_VALUE  m_OutputSize
	 디지털 OUTPUT 사이즈 변수
	*/
	FNIO_VALUE  m_OutputSize;

	/**
	*@var FNIO_VALUE  m_InputSize
	디지털 INPUT 사이즈 변수
	*/
	FNIO_VALUE  m_InputSize;

	/**
	*@fn int GetDeviceParam()
	*@brief System 정보를 읽어온다
	*@date 2019/06/27
	*@return 정의된 FNIO_ERROR 리턴
	*/
	int GetDeviceParam();


protected:
	/**
	*@fn int OpenDevice(char* pIpAddr)
	*@brief Device를 초기화하고 Open한다
	*@date 2019/06/27
	*@param IP 주소
	*@return 정의된 FNIO_ERROR 리턴
	*/
	int OpenDevice(char* pIpAddr);

	/**
	*@fn void CloseDevice()
	*@brief Device 모듈을 종료한다
	*@date 2019/06/27
	*@return 정의된 FNIO_ERROR 리턴
	*/
	void CloseDevice();

	/**
	*@fn int StartIoUpdate()
	*@brief IO 데이터를 주기적으로, 이벤트 혹은 다중이벤트 모드로 업데이트 한다
	*@date 2019/06/27
	*@return 정의된 FNIO_ERROR 리턴
	*@remark \n
			 업데이트 타입이 IO_UPDATE_PERIODIC일 경우 DEV_UPDATE_FREQUENCE(ms)로 시간 설정 가능하다 \n
			 DEV_UPDATE_FREQUENCE가 '0'일 경우 MAX으로 설정된다 \n
			 업데이트 타입이 IO_UPDATE_EVENT일 경우 READ/WRITE 요청에 따라 데이터의 교환이 이루어진다 \n
			 다중 프로세스 환경에서 두개 이상의 소프트웨어에서 장치로 엑세스하기를 원한다면 \n
			 IO 업데이트 타입을 반드시 IO_UPDATE_EVENT_MULTIPLE로 설정한다
	*/
	int StartIoUpdate();

	/**
	*@fn int StopIoUpdate()
	*@brief IO 데이터 업데이트를 중지한다
	*@date 2019/06/27
	*@return 정의된 FNIO_ERROR 리턴
	*/
	int StopIoUpdate();

	/**
	*@fn int ReadInputData(BYTE* pInputData)
	*@brief 라이브러리의 Input 이미지 버퍼로 부터 값을 읽는다
	*@date 2019/06/27
	*@param Digital Input 데이터 값을 받을 포인트 변수
	*@return 정의된 FNIO_ERROR 리턴
	*@remark \n
			 함수를 호출하기 전에 반드시 StartIoUpdate() 함수를 호출해야한다 
	*/
	int ReadInputData(BYTE* pInputData);

	/**
	*@fn int ReadOutputData(BYTE* pOutputData)
	*@brief 라이브러리의 Output 이미지 버퍼로 부터 값을 읽는다
	*@date 2019/06/27
	*@param Digital Output 데이터 값을 받을 포인트 변수
	*@return 정의된 FNIO_ERROR 리턴
	*@remark \n
			 함수를 호출하기 전에 반드시 StartIoUpdate() 함수를 호출해야한다
	*/
	int ReadOutputData(BYTE* pOutputData);

	/**
	*@fn int ReadInputDataBit(FNIO_VALUE BitIndex, FNIO_VALUE* pBitData)
	*@brief 라이브러리의 Input 이미지 버퍼로 부터 비트 값을 읽는다
	*@date 2019/06/27
	*@param 이미지 버퍼의 인덱스, 해당 인덱스의 비트 값
	*@return 정의된 FNIO_ERROR 리턴
	*@remark \n
			 함수를 호출하기 전에 반드시 StartIoUpdate() 함수를 호출해야한다
	*/
	int ReadInputDataBit(FNIO_VALUE BitIndex, FNIO_VALUE* pBitData);

	/**
	*@fn int ReadOutputDataBit(FNIO_VALUE BitIndex, FNIO_VALUE* pBitData)
	*@brief 라이브러리의 Output 이미지 버퍼로 부터 비트 값을 읽는다
	*@date 2019/06/27
	*@param 이미지 버퍼의 인덱스, 해당 인덱스의 비트 값
	*@return 정의된 FNIO_ERROR 리턴
	*@remark \n
			 함수를 호출하기 전에 반드시 StartIoUpdate() 함수를 호출해야한다
	*/
	int ReadOutputDataBit(FNIO_VALUE BitIndex, FNIO_VALUE* pBitData);

	/**
	*@fn FNIO_VALUE GetOutputSize()
	*@brief Output 데이터의 사이즈를 리턴한다
	*@date 2019/06/27
	*@return 데이터 사이즈 
	*/
	FNIO_VALUE GetOutputSize() { return m_OutputSize; }

	/**
	*@fn FNIO_VALUE GetInputSize()
	*@brief Input 데이터의 사이즈를 리턴한다
	*@date 2019/06/27
	*@return 데이터 사이즈
	*/
	FNIO_VALUE GetInputSize()  { return m_InputSize;  }

public:
	/**
	*@fn int WriteOutputDataBit(FNIO_VALUE BitIndex, FNIO_VALUE BitData)
	*@brief 라이브러리의 Output 이미지 버퍼로 부터 값을 쓴다
	*@date 2019/06/27
	*@param 이미지 버퍼의 인덱스, 해당 인덱스의 비트 값
	*@return 정의된 FNIO_ERROR 리턴
	*@remark \n
			 함수를 호출하기 전에 반드시 StartIoUpdate() 함수를 호출해야한다
	*/
	int WriteOutputDataBit(FNIO_VALUE Addr, FNIO_VALUE BitIndex, FNIO_VALUE BitData);

	/**
	*@fn int WriteOutputData(FNIO_VALUE start_Addr, FNIO_POINTER pBuffer, FNIO_VALUE Len)
	*@brief 라이브러리의 Output 이미지 버퍼로 부터 값을 쓴다
	*@date  2019/10/01 from jhkim.
	*@param 이미지 버퍼의 인덱스, 해당 인덱스의 data 값
	*@return 정의된 FNIO_ERROR 리턴
	*@remark \n
			 함수를 호출하기 전에 반드시 StartIoUpdate() 함수를 호출해야한다
	*/
	int WriteOutputData(FNIO_VALUE start_Addr, FNIO_POINTER pBuffer, FNIO_VALUE Len);
};