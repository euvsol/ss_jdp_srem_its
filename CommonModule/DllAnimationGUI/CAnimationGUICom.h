/**
 * Animation GUI Communication Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

#pragma once
class AFX_EXT_CLASS CAnimationGUICom : public CEthernetCom
{
public:
	CAnimationGUICom();
	~CAnimationGUICom();

protected:
	enum 
	{
		MSG_ID_MONITORING = 0,
		MSG_ID_ETC,
		MSG_ID_CALIBRATION,
		MSG_ID_RECOVERY,
		MSG_ID_VACUUM_SEQ,
		MSG_ID_PTR,
		MSG_ID_PHASE,
		MSG_ID_XRAY_CAM,
		MSG_ID_MESSAGE
	};

	typedef struct EtcConfigData
	{
		int message_id;
		int message_size;

		BOOL use_material_rotate;
		int  rotate_angle;
		BOOL use_material_flip;
		BOOL use_vacrobot_cam_sensor_interlock;
		int  auto_source_off_time;
	}stEtcConfigData;

	typedef struct CalibrationData
	{
		int message_id;
		int message_size;

		char scan_stage_calibration_tx[30];
		char scan_stage_calibration_ty[30];
		char global_offset_x[30];
		char global_offset_y[30];
		char align_lb_offset_x[30];
		char align_lb_offset_y[30];
		char align_lt_offset_x[30];
		char align_lt_offset_y[30];
		char align_rt_offset_x[30];
		char align_rt_offset_y[30];
		char laser_switching_lb_offset_x[30];
		char laser_switching_lb_offset_y[30];
		char laser_switching_lt_offset_x[30];
		char laser_switching_lt_offset_y[30];
		char laser_switching_rt_offset_x[30];
		char laser_switching_rt_offset_y[30];
		char cap1_focus_distance_z[30];
		char cap2_focus_distance_z[30];
		char cap3_focus_distance_z[30];
		char cap4_focus_distance_z[30];
		char inspector_offset_x[30];
		char inspector_offset_y[30];
	}stCalibrationData;

	typedef struct RecoveryData
	{
		int  message_id;
		int  message_size;

		char om_align_lb_posx[30];
		char om_align_lb_posy[30];
		char om_align_lt_posx[30];
		char om_align_lt_posy[30];
		char om_align_rt_posx[30];
		char om_align_rt_posy[30];
		char euv_align_lb_posx[30];
		char euv_align_lb_posy[30];
		char euv_align_lt_posx[30];
		char euv_align_lt_posy[30];
		char euv_align_rt_posx[30];
		char euv_align_rt_posy[30];
		BOOL om_align_complete_flag;
		BOOL euv_align_complete_flag;
		BOOL laser_feedback_available_flag;
		BOOL laser_feedback_flag;
		int  euv_align_fov;
		int  euv_align_grid;
		char zinterlock_position[30];
		char zfocus_position[30];
		BOOL mts_rotate_done_flag;
		BOOL mts_flip_done_flag;
		int  current_process;
		int  material_location;
	}stRecoveryData;

	typedef struct VacuumSeqData
	{
		int  message_id;
		int  message_size;

		char mc_standby_vent_value[30];
		char mc_slow_vent_value[30];
		char mc_fast_vent_value[30];
		char mc_slow_rough_value[30];
		char mc_fast_rough_value[30];
		char mc_tmp_rough_value[30];
		char llc_standby_vent_value [30];
		char llc_slow_vent_value[30];
		char llc_fast_vent_value[30];
		char llc_slow_rough_value[30];
		char llc_fast_rough_value[30];
		char llc_tmp_rough_value[30];
		int  mc_standby_vent_time;
		int  mc_slow_vent_time;
		int  mc_fast_vent_time;
		int  mc_slow_rough_time;
		int  mc_fast_rough_time;
		int  mc_tmp_rough_time;
		int  llc_standby_vent_time;
		int  llc_slow_vent_time;
		int  llc_fast_vent_time;
		int  llc_slow_rough_time;
		int  llc_fast_rough_time;
		int  llc_tmp_rough_time;
	}stVacuumSeqData;

	typedef struct PtrData
	{
		int  message_id;
		int  message_size;

		char d1_reference[30];
		char d2_reference[30];
		char d3_reference[30];
		char d1_reference_std[30];
		char d2_reference_std[30];
		char d3_reference_std[30];
		char reference_reflectance[30];
	}stPtrData;

	typedef struct PhaseData
	{
		int  message_id;
		int  message_size;

		int  align_ccd_x;
		int  align_ccd_y;
		int  align_ccd_width;
		int  align_ccd_height;
		int  align_ccd_binning;
		int  align_image_x;
		int  align_image_y;
		int  align_image_width;
		int  align_image_height;
		char align_exposure_time[30];
		int  measure_ccd_x;
		int  measure_ccd_y;
		int  measure_ccd_width;
		int  measure_ccd_height;
		int  measure_ccd_binning;
		int  measure_image_x;
		int  measure_image_y;
		int  measure_image_width;
		int  measure_image_height;
		char measure_exposure_time[30];
		char horizontal_margin[30];
		char vertical_margin[30];
		int  maximum_try_count;
		char coarse_align_intensity_tolerence_percent[30];
		char step_width_x[30];
		int  half_number_of_step_x;
		char step_width_y[30];
		int  half_number_of_step_y;
		int  num_of_std_point;
		int  fine_align_algorithm_type;
		char slit_width[30];
		char slit_pitch[30];
		char slit_length[30];
		char interlock_margin[30];
		char z_control_tolerance[30];
		char cap1_pos_limit_min[30];
		char cap1_pos_limit_max[30];
		char cap2_pos_limit_min[30];
		char cap2_pos_limit_max[30];
		char cap3_pos_limit_min[30];
		char cap3_pos_limit_max[30];
		char cap4_pos_limit_min[30];
		char cap4_pos_limit_max[30];
		char cap1_measure[30];
		char cap2_measure[30];
		char cap3_measure[30];
		char cap4_measure[30];
		char z_stage_limit_min[30];
		char z_stage_limit_max[30];
		char image_rotation_angle[30];
		char xray_camera_read_timeout[30];
		char down_z_height_when_move[30];
		int  nfft;
		char continuous_wave[30];
		int  continuous_wave_date;
		int  back_ground_height;
	}stPhaseData;

	typedef struct XRayCamData
	{
		int  message_id;
		int  message_size;

		int  adc_quality;
		char adc_speed[10];
		int  adc_analog_gain;
		char sensor_temp_set_point[10];
		int  ccd_roi_pos_x;
		int  ccd_roi_pos_y;
		int  ccd_roi_width;
		int  ccd_roi_height;
		int  ccd_roi_x_binning;
		int  ccd_roi_y_binning;
		int  readout_control_mode;
		int  readout_port_count;
		char exposure_time[10];
		int  shutter_timming_mode;
		char shutter_delay_resolution[10];
		char shutter_opening_delay[10];
		char shutter_closing_delay[10];
		int  trigger_response;
		int  trigger_determination;
		int  output_signal;
		int  output_signal2;
		int  invert_output_signal;
		int  invert_output_signal2;
		int  clean_cycle_count;
		int  clean_cycle_height;
		int  roi_x;
		int  roi_y;
		int  roi_width;
		int  roi_height;
		char normal_temperature[10];
		char operation_temperature[10];
	}stXRayCamData;

	typedef struct MessageData
	{
		int  message_id;
		int  message_size;
		char Message[128];
	}stMessageData;

	virtual	int				SendData(int nSize, char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int				ReceiveData(int nMessageId, char *lParam); ///< Recive
	virtual int				WaitReceiveEventThread();

	int SendStringMessage(MessageData _data);
	int SetEtcConfigDataCtrl(EtcConfigData _data);
	int SetCalibrationDataCtrl(CalibrationData _data);
	int SetRecoveryDataCtrl(RecoveryData _data);
	int SetVacuumSeqDataCtrl(VacuumSeqData _data);
	int SetPtrDataCtrl(PtrData _data);
	int SetPhaseDataCtrl(PhaseData _data);
	int SetXRayCamDataCtrl(XRayCamData _data);
	int GetConfigDataCtrl(MessageData _data);

	stEtcConfigData*		m_stEtcData;
	stCalibrationData*		m_stCalibrationData;
	stRecoveryData*			m_stRecoveryData;
	stVacuumSeqData*		m_stVacuumSeqData;
	stPtrData*				m_stPtrData;
	stPhaseData*			m_stPhaseData;
	stXRayCamData*			m_stXRayCamData;
	stMessageData*			m_stMsgData;

	HANDLE					m_hAckEvent;
	HANDLE					m_hRspEvent;
	HANDLE					m_hDoneEvent;

	int						WaitExecAckEvent(int nTimeout);
	int						WaitExecRspEvent(int nTimeout);
	int						WaitExecDoneEvent(int nTimeout);
};

