#include "stdafx.h"
#include "CAnimationGUICom.h"


CAnimationGUICom::CAnimationGUICom()
{
	m_stEtcData			= (stEtcConfigData*)malloc(sizeof(stEtcConfigData));
	m_stCalibrationData = (stCalibrationData*)malloc(sizeof(stCalibrationData));
	m_stRecoveryData	= (stRecoveryData*)malloc(sizeof(stRecoveryData));
	m_stVacuumSeqData	= (stVacuumSeqData*)malloc(sizeof(stVacuumSeqData));
	m_stPtrData			= (stPtrData*)malloc(sizeof(stPtrData));
	m_stPhaseData		= (stPhaseData*)malloc(sizeof(stPhaseData));
	m_stXRayCamData		= (stXRayCamData*)malloc(sizeof(stXRayCamData));
	m_stMsgData			= (stMessageData*)malloc(sizeof(stMessageData));

	ZeroMemory(m_stEtcData, sizeof(stEtcConfigData));
	ZeroMemory(m_stCalibrationData, sizeof(stCalibrationData));
	ZeroMemory(m_stRecoveryData, sizeof(stRecoveryData));
	ZeroMemory(m_stVacuumSeqData, sizeof(stVacuumSeqData));
	ZeroMemory(m_stPtrData, sizeof(stPtrData));
	ZeroMemory(m_stPhaseData, sizeof(stPhaseData));
	ZeroMemory(m_stXRayCamData, sizeof(stXRayCamData));
	ZeroMemory(m_stMsgData, sizeof(stMessageData));

	m_hAckEvent	 = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hRspEvent  = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hDoneEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
}


CAnimationGUICom::~CAnimationGUICom()
{
	CloseHandle(m_hAckEvent);
	CloseHandle(m_hRspEvent);
	CloseHandle(m_hDoneEvent);

	free(m_stEtcData);
	free(m_stCalibrationData);
	free(m_stRecoveryData);
	free(m_stVacuumSeqData);
	free(m_stPtrData);
	free(m_stPhaseData);
	free(m_stXRayCamData);
	free(m_stMsgData);
}

int CAnimationGUICom::SendStringMessage(MessageData _data)
{
	int nRet;
	nRet = SendData(sizeof(MessageData), (char*)&_data);
	return nRet;
}

int CAnimationGUICom::SetEtcConfigDataCtrl(EtcConfigData _data)
{
	int nRet;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData(sizeof(EtcConfigData), (char*)&_data);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(1000);
	}

	return nRet;
}

int CAnimationGUICom::SetCalibrationDataCtrl(CalibrationData _data)
{
	int nRet;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData(sizeof(CalibrationData), (char*)&_data);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(1000);
	}

	return nRet;
}

int CAnimationGUICom::SetRecoveryDataCtrl(RecoveryData _data)
{
	int nRet;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData(sizeof(RecoveryData), (char*)&_data);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(1000);
	}

	return nRet;
}

int CAnimationGUICom::SetVacuumSeqDataCtrl(VacuumSeqData _data)
{
	int nRet;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData(sizeof(VacuumSeqData), (char*)&_data);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(1000);
	}

	return nRet;
}

int CAnimationGUICom::SetPtrDataCtrl(PtrData _data)
{
	int nRet;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData(sizeof(PtrData), (char*)&_data);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(1000);
	}

	return nRet;
}

int CAnimationGUICom::SetPhaseDataCtrl(PhaseData _data)
{
	int nRet;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData(sizeof(PhaseData), (char*)&_data);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(1000);
	}

	return nRet;
}

int CAnimationGUICom::SetXRayCamDataCtrl(XRayCamData _data)
{
	int nRet;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hDoneEvent);

	nRet = SendData(sizeof(XRayCamData), (char*)&_data);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hDoneEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);

		if (nRet != 0)
			return nRet;

		nRet = WaitExecDoneEvent(1000);
	}

	return nRet;
}

int CAnimationGUICom::GetConfigDataCtrl(MessageData _data)
{
	int nRet;

	ResetEvent(m_hAckEvent);
	ResetEvent(m_hRspEvent);

	nRet = SendData(sizeof(MessageData), (char*)&_data);
	if (nRet != 0)
	{
		SetEvent(m_hAckEvent);
		SetEvent(m_hRspEvent);
	}
	else
	{
		nRet = WaitExecAckEvent(1000);
		if (nRet != 0)
			return nRet;

		nRet = WaitExecRspEvent(1000);
	}

	return nRet;
}

int CAnimationGUICom::SendData(int nSize, char * lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	CString Temp;
	Temp.Format(_T("CTRL PGM -> Animation GUI : Monitoring Data(Size %d)"), nSize);
	SaveLogFile("AnimationGUI_Com", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
	nRet = Send(nSize, lParam, nTimeOut);
	if (nRet != 0)
	{
		Temp.Format(_T("CTRL PGM -> Animation GUI : SEND FAIL (%d)"), nRet);
		SaveLogFile("AnimationGUI_Com", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
	}
	return nRet;
}

int CAnimationGUICom::ReceiveData(int nMessageId, char * lParam)
{
	int nRet = 0;

	if (nMessageId == MSG_ID_ETC) 
	{
		ZeroMemory(m_stEtcData, sizeof(stEtcConfigData));
		*m_stEtcData = *(stEtcConfigData*)lParam;
		
		SetEvent(m_hRspEvent);
	}
	else if (nMessageId == MSG_ID_CALIBRATION)
	{
		ZeroMemory(m_stCalibrationData, sizeof(stCalibrationData));
		*m_stCalibrationData = *(stCalibrationData*)lParam;

		SetEvent(m_hRspEvent);
	}
	else if (nMessageId == MSG_ID_RECOVERY)
	{
		ZeroMemory(m_stRecoveryData, sizeof(stRecoveryData));
		*m_stRecoveryData = *(stRecoveryData*)lParam;

		SetEvent(m_hRspEvent);
	}
	else if (nMessageId == MSG_ID_VACUUM_SEQ)
	{
		ZeroMemory(m_stVacuumSeqData, sizeof(stVacuumSeqData));
		*m_stVacuumSeqData = *(stVacuumSeqData*)lParam;
		
		SetEvent(m_hRspEvent);
	}
	else if (nMessageId == MSG_ID_PTR)
	{
		ZeroMemory(m_stPtrData, sizeof(stPtrData));
		*m_stPtrData = *(stPtrData*)lParam;

		SetEvent(m_hRspEvent);
	}
	else if (nMessageId == MSG_ID_PHASE)
	{
		ZeroMemory(m_stPhaseData, sizeof(stPhaseData));
		*m_stPhaseData = *(stPhaseData*)lParam;

		SetEvent(m_hRspEvent);
	}
	else if (nMessageId == MSG_ID_XRAY_CAM)
	{
		ZeroMemory(m_stXRayCamData, sizeof(stXRayCamData));
		*m_stXRayCamData = *(stXRayCamData*)lParam;

		SetEvent(m_hRspEvent);
	}
	else if (nMessageId == MSG_ID_MESSAGE)
	{
		ZeroMemory(m_stMsgData, sizeof(stMessageData));
		*m_stMsgData = *(stMessageData*)lParam;
		
		if (strcmp(m_stMsgData->Message, _T("ACK")) == 0)
			SetEvent(m_hAckEvent);

		if (strcmp(m_stMsgData->Message, _T("DONE")) == 0)
			SetEvent(m_hDoneEvent);
	}

	//Received 데이터가 정상 데이터이면 Signal 해준다.
	//SetEvent(m_hStateOkEvent);	//Sets the specified event object to the signaled state.

	return nRet;
}

int CAnimationGUICom::WaitReceiveEventThread()
{
	int		nRet = 0;
	char*	pRecvBuffer = new char[m_nReceiveMaxBufferSize];

	struct timeval timeout;
	fd_set fds;

	SOCKET	tmpSocket = INVALID_SOCKET;
	tmpSocket = m_Socket;

	TRACE("TCPIP Receive Thread Start..\n");

	memset(pRecvBuffer, '\0', m_nReceiveMaxBufferSize);
	while (!m_bKillReceiveThread)
	{
		m_bConnected = TRUE;
		timeout.tv_sec = 0;
		timeout.tv_usec = 100;
		FD_ZERO(&fds);
		FD_SET(m_Socket, &fds);
		m_nSocketStatus = select(sizeof(fds) * 8, &fds, NULL, NULL, &timeout);

		if (m_nSocketStatus == -1)
		{
			m_bKillReceiveThread = TRUE;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}

		if (tmpSocket != INVALID_SOCKET)
		{
			int nHeaderInfo[2];

			memset(pRecvBuffer, '\0', m_nReceiveMaxBufferSize);
			int nResult = recv(tmpSocket, (char *)nHeaderInfo, sizeof(nHeaderInfo), NULL);
			
			if (nResult > 0)
			{
				nResult = recv(tmpSocket, pRecvBuffer + sizeof(nHeaderInfo), nHeaderInfo[1], NULL);

				if (nResult > 0)
				{
					nRet = ReceiveData(nHeaderInfo[0], pRecvBuffer);
					m_nRet = nRet;
					memset(pRecvBuffer, '\0', m_nReceiveMaxBufferSize);
				}
				else
				{
					if (nResult == 0)
					{
						m_bKillReceiveThread = TRUE;
						m_bConnected = FALSE;
					}
				}
			}
			else
			{
				if (nResult == 0)
				{
					m_bKillReceiveThread = TRUE;
					m_bConnected = FALSE;
				}
			}

			SetEvent(m_hSocketEvent);

			if (m_bKillReceiveThread)
				break;
		}
		else
		{
			m_Socket = INVALID_SOCKET;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}
	}

	delete[] pRecvBuffer;
	pRecvBuffer = NULL;
	TRACE("TCPIP Receive Thread End..\n");

	return nRet;
}

int CAnimationGUICom::WaitExecAckEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hAckEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -1;
	}

	return 0;
}

int CAnimationGUICom::WaitExecRspEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hRspEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -1;
	}

	return 0;
}

int CAnimationGUICom::WaitExecDoneEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hDoneEvent, nTimeout) != WAIT_OBJECT_0)
	{
		return -1;
	}

	return 0;
}

