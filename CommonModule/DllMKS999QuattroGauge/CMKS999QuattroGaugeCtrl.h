﻿/**
 * MKS 999 Quattro Transducer Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

 /*
	 Standard Addresses

	 Valid addresses are 3 digits, 001 to 253 (factory setting: 253).

	 Universal Addresses

	 The 999 receives and responds to commands sent to address 254. For
	 example, use 254 to communicate with a device if its address is unknown.
	 The 999 receives and acts upon commands sent to address 255, but does
	 not respond; use 255 to broadcast messages to multiple devices attached to
	 the same system. For example, use 255 to change the baud rate for all
	 devices.

	 Query and Command Syntax

	 Queries return current parameter settings; commands change the parameter
	 setting according to the value the user types in the command syntax. Each
	 query or command must begin with the attention character @ and end with
	 the termination string ;FF.

	 Syntax required for a query is:
	 @<device address><query>?;FF.

	 Syntax required for a command is:
	 @<device address><command>!<parameter>;FF.

	 Examples:

	 Query current baud rate: @253BR?;FF
	 Change baud rate to 19200: @253BR!19200;FF

	 where:
		 @ attention character
		 253 <device address>
		 BR? <query>? (for query syntax)
		 BR!19200 <command>!<parameter> (for command syntax)
		 ;FF terminator

	 Response Syntax (ACK/NAK)

	 The ASCII characters 'ACK' or 'NAK' preface the query or command response
	 string. The ACK sequence signifies the message was processed
	 successfully. The NAK sequence indicates there was an error.

	 The response to a query or a successful command is:
	 @<device address>ACK<data>;FF

	 The response to a message with an error is:
	 @<device address>NAK<NAK code>;FF

	 Examples:

	 ACK response: @253ACK9600;FF (baud rate changed to 9600)
	 NAK response: @253NAK160;FF (command had an error — possibly a typo)

	 The following list provides descriptions of the NAK codes that may be
	 returned.

			 Error							NAK Code
	 Unrecognized message						160
	 Invalid argument							169
	 Value out of range							172
	 Command/query character invalid (! or ?)	175
	 Control setpoint enabled					195
	 Write to nonvolatile memory failed			196
	 Read from nonvolatile memory failed			197
	 Not in measure pressure mode				198
	 Pressure too high for degas					199
	 Calibration incomplete						100-115
	 Not in Calibration Mode						178
	 Write To EEfail								300-399
	 Read from EE fail							400-499

	Transducer Status – T

	The T command returns the current status of the Hot Cathode. Related
	commands: Active Filament – AF (Set Up Commands section); Set Point
	Value – SP1, SP2, SP3 and Hysteresis Value – SH1, SH2, SH3 (Set Point
	Commands section).

	Values: F = Filament fault, filament cannot turn on
			G = Hot Cathode on
			O = OK, no errors to report
			P = Pressure fault, system pressure above protect
				pressure
			W = Hot Cathode is turning on; pressure reading not
				valid (when Hot Cathode is turned on, a few
				seconds elapse before pressure reading is
				valid).
			D = Degas ON

	Query: @001T?;FF
	Query Response: @001ACKO;FF
 */

#pragma once

class AFX_EXT_CLASS CMKS999QuattroGaugeCtrl : public CSerialCom
{
public:
	CMKS999QuattroGaugeCtrl();
	~CMKS999QuattroGaugeCtrl();

	HANDLE			m_hStateOkEvent;
	int OpenPort(CString strPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);
	int ClosePort(void);

	virtual	int		SendData(char *lParam, int nTimeOut = 60000, int nRetryCnt = 3);	
	virtual	int		ReceiveData(char *lParam, DWORD nSize);

	void initialize();
	int Command_ReadingGauge();
	int Command_GetStatusGauge();
	CString m_strPressure;
	double	m_dPressure;			//단위 Torr
	int		m_nStatusCode;			//Gauge Status를 정의하자.

	double GetValue();			//m_dPressure 값을 return 하므로 m_dPressure를 바로 사용해도 됨.
};

