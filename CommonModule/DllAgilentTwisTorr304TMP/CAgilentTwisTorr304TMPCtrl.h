/**
 * Agilent TwisTorr304 TMP Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

class AFX_EXT_CLASS CAgilentTwisTorr304TMPCtrl : public CSerialCom
{
public:
	CAgilentTwisTorr304TMPCtrl();
	~CAgilentTwisTorr304TMPCtrl();

private:
	BYTE m_BYTE[15];
	HANDLE TmpOnOffEvent;

	void GetCRCValue(BYTE* pBuf, int nSize);
	int WaitTmpOnOffEvent(int nTimeout);

	virtual	int	SendData(int nSize, BYTE *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int	ReceiveData(char *lParam, DWORD dwRead); ///< Recive

	enum
	{
		STX   = 0x02,
		ADDR  = 0x80,
		WRITE = 0x31,
		READ  = 0x30,
		ON	  = 0x31,
		OFF   = 0x30,
		ETX   = 0x03,
		ACK   = 0x06,
		NACK  = 0x15,
	};

protected:
	int m_nTmpState;
	int m_nTmpReceiveDataHz;
	int m_nTmpReceiveDataRpm;
	int m_nTmpReceiveDataErrorCode;
	int m_nTmpReceiveDataTemperature;
	int m_nTmpReceiveDataTemperatureHeatsink;
	int m_nTmpReceiveDataTemperatureAir;

	int LLCTmp_Monitor_Com();
	int LLCTmp_HzRead_Com();
	int LLCTmp_RpmRead_Com();
	int LLCTmp_ErrorCode_Check_Com();
	int LLCTmp_Temprerature_Check_Com();
	int LLCTmp_Controller_Heatsink_Temprerature_Check_Com();
	int LLCTmp_Controller_Air_Temprerature_Check_Com();
	
	int LLCTmp_On_Com();
	int LLCTmp_Off_Com();
};

