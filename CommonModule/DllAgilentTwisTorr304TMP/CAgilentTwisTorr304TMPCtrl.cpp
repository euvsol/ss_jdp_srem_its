#include "stdafx.h"
#include "CAgilentTwisTorr304TMPCtrl.h"


CAgilentTwisTorr304TMPCtrl::CAgilentTwisTorr304TMPCtrl()
{
	TmpOnOffEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_nTmpState = 0;
	m_nTmpReceiveDataHz = 0;
	m_nTmpReceiveDataRpm = 0;
	m_nTmpReceiveDataErrorCode = 0;
	m_nTmpReceiveDataTemperature = 0;
	m_nTmpReceiveDataTemperatureHeatsink = 0;
	m_nTmpReceiveDataTemperatureAir = 0;
}

CAgilentTwisTorr304TMPCtrl::~CAgilentTwisTorr304TMPCtrl()
{
	CloseHandle(TmpOnOffEvent);
}

int	CAgilentTwisTorr304TMPCtrl::SendData(int nSize, BYTE *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	CString strSendData = TEXT("");
	CString StrTemp = TEXT("");

	for (int nIdx = 0; nIdx < nSize; nIdx++)
	{
		StrTemp.Format("%02X ", lParam[nIdx]);
		strSendData += StrTemp;
	}

	nRet = Send(nSize, lParam, nTimeOut);

	if (nRet != 0)
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (SEND): " + strSendData + "Send Fail")));	//통신 상태 기록.
	else
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (SEND): " + strSendData)));	//통신 상태 기록.

	return nRet;
}

int CAgilentTwisTorr304TMPCtrl::ReceiveData(char *lParam, DWORD dwRead)
{
	CString		StrLogTemp;
	CString		StrLLCTmpState;
	CString		StrLLCTmpReceiveDataState;
	
	memset(m_BYTE, '\0', sizeof(m_BYTE));
	memcpy(m_BYTE, lParam, sizeof(m_BYTE));
	
	CString StrTemp = TEXT("");
	CString StrRecevieData = TEXT("");
	CString StrRecevieDataMid = TEXT("");
	CString StrRevevieDataResult = TEXT("");
	
	for (int i = 0; i < sizeof(m_BYTE); i++)            //String으로 변환
	{
		if (m_BYTE[i] == 0) 
			StrTemp = "0 ";
		else 
			StrTemp.Format("%02x", m_BYTE[i]);
		StrRecevieData += StrTemp;
	}
	
	SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC TMP -> PC (RECEIVE): " + StrRecevieData)));	//통신 상태 기록.
	
	if (m_BYTE[2] == ACK)
	{
		SetEvent(TmpOnOffEvent);
		StrLLCTmpReceiveDataState =_T("실행 완료");
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC TMP -> PC (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x35)	//STATUS
	{
		if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x30)
		{
			StrLLCTmpReceiveDataState.Format(_T("Stop"));
			m_nTmpState = 0;
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x31)
		{
			StrLLCTmpReceiveDataState.Format(_T("Working Intlk"));
			m_nTmpState = 1;
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x32)
		{
			StrLLCTmpReceiveDataState.Format(_T("Starting"));
			m_nTmpState = 2;
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x33)
		{
			StrLLCTmpReceiveDataState.Format(_T("Auto Tuning"));
			m_nTmpState = 3;
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x34)
		{
			StrLLCTmpReceiveDataState.Format(_T("Breaking"));
			m_nTmpState = 4;
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x35)
		{
			StrLLCTmpReceiveDataState.Format(_T("Running"));
			m_nTmpState = 5;
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x36)
		{
			StrLLCTmpReceiveDataState.Format(_T("Fail"));
			m_nTmpState = 6;
		}
	
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC TMP -> PC (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x33) // hz
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);
	
		m_nTmpReceiveDataHz = atoi(StrRevevieDataResult);
		StrLLCTmpReceiveDataState.Format(_T("%d hz"), m_nTmpReceiveDataHz); // 현재 Hz 값
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC TMP -> PC (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x32 && m_BYTE[4] == 0x36) // rpm
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);
	
		m_nTmpReceiveDataRpm = atoi(StrRevevieDataResult);
		StrLLCTmpReceiveDataState.Format(_T("%d Rpm"), m_nTmpReceiveDataRpm);
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC TMP -> PC (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x36) // Error code view
	{
		CString str;
	
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);
	
		m_nTmpReceiveDataErrorCode = atoi(StrRevevieDataResult);
	
		switch (m_nTmpReceiveDataErrorCode)
		{
		case 0:
			StrLLCTmpReceiveDataState = _T("No Error");
			break;
		case 1:
			StrLLCTmpReceiveDataState = _T("No Connection");
			str.Format(_T("LLC ERROR CODE: %d    , ERROR STATE : %s "), m_nTmpReceiveDataErrorCode, StrLLCTmpReceiveDataState);
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str))); //상태 기록.
			break;
		case 2:
			StrLLCTmpReceiveDataState = _T("Pump OverTemp");
			str.Format(_T("LLC ERROR CODE: %d    , ERROR STATE : %s "), m_nTmpReceiveDataErrorCode, StrLLCTmpReceiveDataState);
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str))); //상태 기록.
			break;
		case 4:
			StrLLCTmpReceiveDataState = _T("Controll OverTemp");
			str.Format(_T("LLC ERROR CODE: %d    , ERROR STATE : %s "), m_nTmpReceiveDataErrorCode, StrLLCTmpReceiveDataState);
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str))); //상태 기록.
			break;
		case 8:
			StrLLCTmpReceiveDataState = _T("Power Fail");
			str.Format(_T("LLC ERROR CODE: %d    , ERROR STATE : %s "), m_nTmpReceiveDataErrorCode, StrLLCTmpReceiveDataState);
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str))); //상태 기록.
			break;
		case 16:
			StrLLCTmpReceiveDataState = _T("Aux Fail");
			str.Format(_T("LLC ERROR CODE: %d    , ERROR STATE : %s "), m_nTmpReceiveDataErrorCode, StrLLCTmpReceiveDataState);
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str))); //상태 기록.
			break;
		case 32:
			StrLLCTmpReceiveDataState = _T("Over Voltage");
			str.Format(_T("LLC ERROR CODE: %d    , ERROR STATE : %s "), m_nTmpReceiveDataErrorCode, StrLLCTmpReceiveDataState);
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str))); //상태 기록.
			break;
		case 64:
			StrLLCTmpReceiveDataState = _T("Over Voltage");
			str.Format(_T("LLC ERROR CODE: %d    , ERROR STATE : %s "), m_nTmpReceiveDataErrorCode, (LPCSTR)StrLLCTmpReceiveDataState);
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str))); //상태 기록.
			break;
		case 128:
			StrLLCTmpReceiveDataState = _T("Too High Load");
			str.Format(_T("LLC ERROR CODE: %d    , ERROR STATE : %s "), m_nTmpReceiveDataErrorCode, StrLLCTmpReceiveDataState);
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str))); //상태 기록.
			break;
		default:
			StrLLCTmpReceiveDataState = _T("Error 발생 ( Define 되지 않은 Error) ");
			str.Format(_T("LLC ERROR CODE: %d    , ERROR STATE : %s "), m_nTmpReceiveDataErrorCode, StrLLCTmpReceiveDataState);
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str))); //상태 기록.
			break;
		}
	
		str.Format(_T("LLC TMP -> PC (RECEIVE): %05d"), m_nTmpReceiveDataErrorCode);
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)(str)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x34) // TMP_temprerature 
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);
	
		m_nTmpReceiveDataTemperature = atoi(StrRevevieDataResult);
		StrLLCTmpReceiveDataState.Format(_T("%d ℃"), m_nTmpReceiveDataTemperature);
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC TMP -> PC (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x31 && m_BYTE[4] == 0x31) //  Controller Heatsink Temperature 
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);
	
		m_nTmpReceiveDataTemperatureHeatsink = atoi(StrRevevieDataResult);
		StrLLCTmpReceiveDataState.Format(_T("%d ℃"), m_nTmpReceiveDataTemperatureHeatsink);
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC TMP -> PC (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x31 && m_BYTE[4] == 0x36) // Controller Air Temperature (°C). [86]
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);
	
		m_nTmpReceiveDataTemperatureAir = atoi(StrRevevieDataResult);
		StrLLCTmpReceiveDataState.Format(_T("%d ℃"), m_nTmpReceiveDataTemperatureAir);
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC TMP -> PC (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	else
	{
		StrLLCTmpReceiveDataState.Format(_T("Send Error"));
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC TMP -> PC (RECEIVE): UnKnown " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	
	switch (m_nTmpState)
	{
	case 0:
		StrLLCTmpState.Format(_T("Stop"));
		break;
	case 1:
		StrLLCTmpState.Format(_T("Wroking Intlk"));
		break;
	case 2:
		StrLLCTmpState.Format(_T("Starting"));
		break;
	case 3:
		StrLLCTmpState.Format(_T("Auto Tuning"));
		break;
	case 4:
		StrLLCTmpState.Format(_T("Breaking"));
		break;
	case 5:
		StrLLCTmpState.Format(_T("Running"));
		break;
	case 6:
		StrLLCTmpState.Format(_T("Fail"));
		break;
	default:
		StrLLCTmpState.Format(_T("Unknwon"));
		break;
	}
	StrLogTemp.Format(_T("LLC_TMP_HZ : %d , LLC_TMP_STATE : %s , LLC_TMP_ERROR : %d , LLC_TMP_TEMP : %d"), m_nTmpReceiveDataHz, StrLLCTmpState, m_nTmpReceiveDataErrorCode, m_nTmpReceiveDataTemperature);
	SaveLogFile("LLC_TMP_LOG", StrLogTemp); //통신 상태 기록.

	return 0;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Monitor_Com()
{
	int ret = -1;
	BYTE Buff[9] = { 0 };

	// PUMP STATUS
	Buff[0] = STX;
	Buff[1] = ADDR;
	Buff[2] = 0x32;
	Buff[3] = 0x30;
	Buff[4] = 0x35;
	Buff[5] = READ;
	Buff[6] = ETX;

	GetCRCValue(Buff, sizeof(Buff) - 2);

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_HzRead_Com()
{
	int ret = -1;
	BYTE Buff[9] = { 0 };
	
	// TMP HZ READ
	Buff[0] = STX;
	Buff[1] = ADDR;
	Buff[2] = 0x32;
	Buff[3] = 0x30;
	Buff[4] = 0x33;
	Buff[5] = READ;
	Buff[6] = ETX; 

	GetCRCValue(Buff, sizeof(Buff) - 2);

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_RpmRead_Com()
{
	int ret = -1;
	BYTE Buff[9] = { 0 };

	//TMP Rpm Read [85]
	Buff[0] = STX;
	Buff[1] = ADDR;
	Buff[2] = 0x32;
	Buff[3] = 0x32;
	Buff[4] = 0x36;
	Buff[5] = READ;
	Buff[6] = ETX;

	GetCRCValue(Buff, sizeof(Buff) - 2);

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_ErrorCode_Check_Com()
{
	int ret = -1;
	BYTE Buff[9] = { 0 };

	//TMP_ERROR CODE [87]
	Buff[0] = STX;
	Buff[1] = ADDR;
	Buff[2] = 0x32;
	Buff[3] = 0x30;
	Buff[4] = 0x36;
	Buff[5] = READ;
	Buff[6] = ETX;

	GetCRCValue(Buff, sizeof(Buff) - 2);

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Temprerature_Check_Com()
{
	int ret = -1;
	BYTE Buff[9] = { 0 };

	//TMP_temprerature [85]
	Buff[0] = STX;
	Buff[1] = ADDR;
	Buff[2] = 0x32;
	Buff[3] = 0x30;
	Buff[4] = 0x34;
	Buff[5] = READ;
	Buff[6] = ETX;

	GetCRCValue(Buff, sizeof(Buff) - 2);

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Controller_Heatsink_Temprerature_Check_Com()
{
	int ret = -1;
	BYTE Buff[9] = { 0 };

	// Controller Heatsink Temperature (°C) [81]
	Buff[0] = STX;
	Buff[1] = ADDR;
	Buff[2] = 0x32;
	Buff[3] = 0x31;
	Buff[4] = 0x31;
	Buff[5] = READ;
	Buff[6] = ETX;

	GetCRCValue(Buff, sizeof(Buff) - 2);

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Controller_Air_Temprerature_Check_Com()
{
	int ret = -1;
	BYTE Buff[9] = { 0 };

	//Controller Air Temperature (°C). [86]
	Buff[0] = STX;
	Buff[1] = ADDR;
	Buff[2] = 0x32;
	Buff[3] = 0x31;
	Buff[4] = 0x36;
	Buff[5] = READ;
	Buff[6] = ETX;

	GetCRCValue(Buff, sizeof(Buff) - 2);

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_On_Com()
{
	int ret = -1;
	BYTE Buff[10] = { 0 };
	
	// TMP ON
	Buff[0] = STX;
	Buff[1] = ADDR;
	Buff[2] = 0x30;
	Buff[3] = 0x30;
	Buff[4] = 0x30;
	Buff[5] = WRITE;
	Buff[6] = ON;
	Buff[7] = ETX;

	GetCRCValue(Buff, sizeof(Buff) - 2);

	ResetEvent(TmpOnOffEvent);
	ret = SendData(sizeof(Buff), Buff);
	if (ret != 0)
		SetEvent(TmpOnOffEvent);
	else
		ret = WaitTmpOnOffEvent(10000);

	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Off_Com()
{
	int ret = -1;
	BYTE Buff[10] = { 0 };
	
	// TMP OFF
	Buff[0] = STX;
	Buff[1] = ADDR;
	Buff[2] = 0x30;
	Buff[3] = 0x30;
	Buff[4] = 0x30;
	Buff[5] = WRITE;
	Buff[6] = OFF;
	Buff[7] = ETX;

	GetCRCValue(Buff, sizeof(Buff) - 2);

	ResetEvent(TmpOnOffEvent);
	ret = SendData(sizeof(Buff), Buff);
	if (ret != 0)
		SetEvent(TmpOnOffEvent);
	else
		ret = WaitTmpOnOffEvent(10000);
	
	return ret;
}

void CAgilentTwisTorr304TMPCtrl::GetCRCValue(BYTE* pBuf, int nSize)
{
	byte btCheckSum = 0x00;
	char sCRC[10] = { 0 };

	for (int nIdx = 1; nIdx < nSize; nIdx++)
		btCheckSum ^= (byte)pBuf[nIdx];

	sprintf_s(sCRC, sizeof(sCRC), "%02X", btCheckSum);

	pBuf[nSize]		= sCRC[0];
	pBuf[nSize + 1] = sCRC[1];
}

int CAgilentTwisTorr304TMPCtrl::WaitTmpOnOffEvent(int nTimeout)
{
	if (WaitForSingleObject(TmpOnOffEvent, nTimeout) == WAIT_OBJECT_0)
		return 0;
	else
		return -1;
}