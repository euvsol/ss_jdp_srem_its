#include "stdafx.h"
#include "CMKS390Gauge.h"


CMKS390Gauge::CMKS390Gauge()
{
	cnt = 0;
	m_dPressureLLC = 0.0;
	m_dPressureMC = 0.0;
	m_bLlcErrorState = FALSE;
	m_bMcErrorState = FALSE;
}


CMKS390Gauge::~CMKS390Gauge()
{
}

int CMKS390Gauge::ReceiveData(char *lParam, DWORD dwRead)
{
	CString strReceiveData;
	CString strChannel;
	CString strData;

	strReceiveData = (LPSTR)lParam;
	strChannel = strReceiveData.Mid(1, 2);
	
	// 2020.09.28
	// 센서 교체 (LLC <-> MC)
	//if (ch_value == "02") ch_2 = ReData.Mid(3, 10); //LLC
	//if (ch_value == "03") ch_3 = ReData.Mid(3, 10); //MC

	if (strChannel == _T("02"))
	{
		strData = strReceiveData.Mid(3, 10); //MC
		strData.TrimLeft();
		strData.TrimRight();

		int nCount = 0;
		int nPoint = 0;
		nPoint = strData.Find(' ');
		for (nCount = 0; nPoint != -1; nCount++)
			nPoint = strData.Find(' ', nPoint + 1);

		if (nCount > 0) //WHITE SPACE COUNT
		{
			m_StrMcGaugeStatus.Format("%s", strData);
			if (strData == _T("00 ST OK"))
			{
				m_nMcGaugeStatus = OK;
				m_bMcErrorState = TRUE;
			}
			else if (strData == _T("01 CGBAD"))
			{
				m_nMcGaugeStatus = ERR_CGBAD;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("02 DGBAD"))
			{
				m_nMcGaugeStatus = ERR_DGBAD;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("03 OVTMP"))
			{
				m_nMcGaugeStatus = WAR_OVTMP;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("04 IGDIS"))
			{
				m_nMcGaugeStatus = ERR_IGDIG;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("05 IG H "))
			{
				m_nMcGaugeStatus = ERR_IG_H_;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("06 IG EM"))
			{
				m_nMcGaugeStatus = ERR_IG_EM;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("07 IGFIL"))
			{
				m_nMcGaugeStatus = WAR_IGFIL;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("08 POWER"))
			{
				m_nMcGaugeStatus = ERR_POWER;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("09 NVRAM"))
			{
				m_nMcGaugeStatus = WAR_NVRAM;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("10 GVRAM"))
			{
				m_nMcGaugeStatus = WAR_GVRAM;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("11 DGCAL"))
			{
				m_nMcGaugeStatus = WAR_DGCAL;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("12 CGCAL"))
			{
				m_nMcGaugeStatus = WAR_CGCAL;
				m_bMcErrorState = FALSE;
			}
			else if (strData == _T("13 BGBAD"))
			{
				m_nMcGaugeStatus = ERR_BGBAD;
				m_bMcErrorState = FALSE;
			}
			else
			{
				m_nMcGaugeStatus = ERR_UNKNOWN;
				m_bMcErrorState = FALSE;
			}
		}
		else
		{
			m_dPressureMC = atof(strData);
		}
	}
	if (strChannel == _T("03"))
	{
		strData = strReceiveData.Mid(3, 10); //LLC
		strData.TrimLeft();
		strData.TrimRight();

		int nCount = 0;
		int nPoint = 0;
		nPoint = strData.Find(' ');
		for (nCount = 0; nPoint != -1; nCount++)
			nPoint = strData.Find(' ', nPoint + 1);

		if (nCount > 0)	//WHITE SPACE COUNT
		{
			m_StrLlcGaugeStatus.Format("%s", strData);
			if (strData == _T("00 ST OK"))
			{
				m_nLlcGaugeStatus = OK;
				m_bLlcErrorState = TRUE;
			}
			else if (strData == _T("01 CGBAD"))
			{
				m_nLlcGaugeStatus = ERR_CGBAD;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("02 DGBAD"))
			{
				m_nLlcGaugeStatus = ERR_DGBAD;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("03 OVTMP"))
			{
				m_nLlcGaugeStatus = WAR_OVTMP;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("04 IGDIS"))
			{
				m_nLlcGaugeStatus = ERR_IGDIG;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("05 IG H "))
			{
				m_nLlcGaugeStatus = ERR_IG_H_;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("06 IG EM"))
			{
				m_nLlcGaugeStatus = ERR_IG_EM;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("07 IGFIL"))
			{
				m_nLlcGaugeStatus = WAR_IGFIL;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("08 POWER"))
			{
				m_nLlcGaugeStatus = ERR_POWER;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("09 NVRAM"))
			{
				m_nLlcGaugeStatus = WAR_NVRAM;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("10 GVRAM"))
			{
				m_nLlcGaugeStatus = WAR_GVRAM;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("11 DGCAL"))
			{
				m_nLlcGaugeStatus = WAR_DGCAL;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("12 CGCAL"))
			{
				m_nLlcGaugeStatus = WAR_CGCAL;
				m_bLlcErrorState = FALSE;
			}
			else if (strData == _T("13 BGBAD"))
			{
				m_nLlcGaugeStatus = ERR_BGBAD;
				m_bLlcErrorState = FALSE;
			}
			else
			{
				m_nLlcGaugeStatus = ERR_UNKNOWN;
				m_bLlcErrorState = FALSE;
			}
		}
		else
		{
			m_dPressureLLC = atof(strData);
		}
	}

	if (cnt == 50)
	{
		CString sTemp;
		sTemp.Format(_T("PC -> MC_VacuumGauge : %.2e"), m_dPressureMC);
		SaveLogFile("MC_VacuumGauge_ReadLog", sTemp); //통신 상태 기록.
		sTemp.Format(_T("PC -> LLC_VaccumGauge : %.2e"), m_dPressureLLC);
		SaveLogFile("LLC_VacuumGauge_ReadLog", sTemp);	//통신 상태 기록.
		cnt = 0;
	}
	cnt++;

	return 0;
}

int CMKS390Gauge::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	
	nRet = Send(lParam, nTimeOut, nRetryCnt);
	if (nRet != 0)
	{
		SaveLogFile("MC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> MC_VacuumGauge : Send Error ")));		//통신 상태 기록.
		SaveLogFile("LLC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> LLC_VaccumGauge : Send Error ")));	//통신 상태 기록.
	}

	return nRet;
}

int CMKS390Gauge::ReadMcVacuumRate()
{
	int nRet = Send("#02RD\r", 500);
	return nRet;
}

int CMKS390Gauge::ReadLlcVacuumRate()
{
	int nRet = Send("#03RD\r", 500);
	return nRet;
}

int CMKS390Gauge::ReadMcGaugeState()
{
	int nRet = Send("#02RS\r", 500);
	return nRet;
}

int CMKS390Gauge::ReadLlcGaugeState()
{
	int nRet = Send("#03RS\r", 500);
	return nRet;
}
