/**
 * MKS 390 Vaccum Gauge Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

#pragma once
class AFX_EXT_CLASS CMKS390Gauge : public CSerialCom
{
public:
	CMKS390Gauge();
	~CMKS390Gauge();

	virtual int			ReceiveData(char *lParam, DWORD dwRead);
	virtual int			SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);



protected:

	BOOL m_bLlcErrorState;
	BOOL m_bMcErrorState;


	double m_dPressureLLC;
	double m_dPressureMC;

	int cnt;

	int ReadMcVacuumRate();
	int ReadLlcVacuumRate();
	int ReadMcGaugeState();
	int ReadLlcGaugeState();

	int GetLlcGaugeStatus() { return m_nLlcGaugeStatus; }
	int GetMcGaugeStatus() { return m_nMcGaugeStatus; }


	CString m_StrLlcGaugeStatus;
	CString m_StrMcGaugeStatus;

	CString GetLlcGaugeStatusString() { return m_StrLlcGaugeStatus; }
	CString GetMcGaugeStatusString() { return m_StrMcGaugeStatus; }

	int m_nLlcGaugeStatus;
	int m_nMcGaugeStatus;
	enum
	{
		OK = 0,
		ERR_CGBAD,
		ERR_DGBAD,
		WAR_OVTMP,
		ERR_IGDIG,
		ERR_IG_H_,
		ERR_IG_EM,
		WAR_IGFIL,
		ERR_POWER,
		WAR_NVRAM,
		WAR_GVRAM,
		WAR_DGCAL,
		WAR_CGCAL,
		ERR_BGBAD,
		ERR_UNKNOWN,
	};
};

